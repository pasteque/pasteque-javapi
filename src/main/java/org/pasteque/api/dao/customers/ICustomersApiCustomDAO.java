package org.pasteque.api.dao.customers;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.customers.Customers;

public interface ICustomersApiCustomDAO extends IGenericDAO<Customers, String>{

    List<Customers> findByCriteria(String reference, String pdvId, String from,
            String to, String insee, Integer radius, Boolean mailConstraint,
            Boolean parentConstraint, String card, String address,
            String address2, String zipCode, String city, String region,
            String country, String firstName, String lastName, String email,
            String phone, String phone2, String notes, String customersExcept,
            Boolean parentOnly, String profilId, Boolean profilConstraint,
            Long maxResults);

    <T> List<T> findDTOByCriteria(String reference, String pdvId, String from,
            String to, String insee, Integer radius, Boolean mailConstraint,
            Boolean parentConstraint, String card, String address,
            String address2, String zipCode, String city, String region,
            String country, String firstName, String lastName, String email,
            String phone, String phone2, String notes, String customersExcept,
            Boolean parentOnly, String profilId, Boolean profilConstraint,
            Long maxResults, Class<T> selector);

    List<Customers> findByIds(String stringIds, String customersExcept);



}
