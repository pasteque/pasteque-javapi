package org.pasteque.api.dao.customers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.pasteque.api.model.customers.Customers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ICustomersApiDAO extends JpaRepository<Customers, String> , JpaSpecificationExecutor<Customers> , ICustomersApiCustomDAO {

    List<Customers> findBySearchKey(String searchKey);
    Optional<Customers> findFirstBySearchKey(String searchKey);
    List<Customers> findByDateUpdateAfter(Date date);
}
