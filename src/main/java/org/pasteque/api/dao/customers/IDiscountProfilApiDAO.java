package org.pasteque.api.dao.customers;

import org.pasteque.api.model.customers.DiscountProfil;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDiscountProfilApiDAO extends JpaRepository<DiscountProfil, Long> {

    DiscountProfil findByName(String cellValue);

}
