package org.pasteque.api.dao.customers.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.pasteque.api.dao.customers.ICustomersApiCustomDAO;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.dto.customers.CustomersCirteriaSearchDTO;
import org.pasteque.api.model.customers.Customers;
import org.pasteque.api.model.saleslocations.Insee;
import org.pasteque.api.util.FilteringAndFormattingData;
import org.pasteque.api.util.GeoQuery;

import com.fasterxml.jackson.databind.ObjectMapper;

//TODO Reprise avec MultiValueMap
//TODO Faire sauter ce qui peut l'être -> la partie avec les getByIds ne semble pas optimale à première lecture
public class ICustomersApiCustomDAOImpl extends GenericDAOImpl<Customers, String> implements ICustomersApiCustomDAO {

    public static final Map<String, String> SELECT_START;

    private static final String ERROR_BASE = " 0 " ;

    static {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("CustomersLightDTO", " SELECT DISTINCT CUSTOMERS.ID as id , CUSTOMERS.SEARCHKEY as searchkey, "
                + "CUSTOMERS.FIRSTNAME as firstName , CUSTOMERS.LASTNAME AS lastName, "
                + "CUSTOMERS.PHONE as phone , CUSTOMERS.POSTAL AS zipCode, CUSTOMERS.CITY AS city, "
                + "LOC.NAME as locationLabel , DP.NAME AS discountProfilLabel "
                + " FROM CUSTOMERS LEFT JOIN LOCATIONS LOC ON LOC.ID = CUSTOMERS.LOCATION_ID "
                + " LEFT JOIN DISCOUNTPROFILES DP ON DP.ID = CUSTOMERS.DISCOUNTPROFILE_ID ");
        map.put("CustomersDetailDTO", "SELECT DISTINCT CUSTOMERS.ID AS id ," + 
                "CUSTOMERS.SEARCHKEY AS searchKey , " + 
                "CUSTOMERS.FIRSTNAME AS firstName , " + 
                "CUSTOMERS.LASTNAME AS lastName , " + 
                "CUSTOMERS.ADDRESS AS address1 , " + 
                "CUSTOMERS.ADDRESS2 AS address2 , " + 
                "CUSTOMERS.POSTAL AS zipCode , " + 
                "CUSTOMERS.INSEE AS insee , " + 
                "CUSTOMERS.CITY AS city , " + 
                "CUSTOMERS.COUNTRY AS country , " + 
                "CUSTOMERS.PHONE AS phone1 , " + 
                "CUSTOMERS.PHONE2 AS phone2 , " + 
                "CUSTOMERS.EMAIL AS email , " + 
                "CUSTOMERS.DATEOFBIRTH AS dateOfBirth , " + 
                "CUSTOMERS.CREATION_DATE AS creationDate , " + 
                "CUSTOMERS.LAST_UPDATE AS lastUpdate , " + 
                "CUSTOMERS.NEWSLETTER AS newsletter , " + 
                "CUSTOMERS.PARTENAIRES AS partenaires , " + 
                "CUSTOMERS.CIVILITE AS civilite , " + 
                "CUSTOMERS.ID_PARENT AS doublon , " + 
                "C2.SEARCHKEY AS parentsearchkey , " + 
                "LOC.ID AS locationId , " + 
                "LOC.NAME AS locationLabel , " + 
                "DP.NAME AS discountProfileName "
                + "FROM CUSTOMERS LEFT JOIN LOCATIONS LOC ON LOC.ID = CUSTOMERS.LOCATION_ID "
                + " LEFT JOIN DISCOUNTPROFILES DP ON DP.ID = CUSTOMERS.DISCOUNTPROFILE_ID "
                + " LEFT JOIN CUSTOMERS C2 ON CUSTOMERS.ID_PARENT = C2.ID");
        map.put("Default", "SELECT DISTINCT CUSTOMERS.* FROM CUSTOMERS ");
        map.put("ParentsOnly", "SELECT DISTINCT CASE WHEN ID_PARENT IS NOT NULL THEN ID_PARENT ELSE ID END AS ID FROM CUSTOMERS ");
        map.put("WithCity", "SELECT DISTINCT CUSTOMERS.* FROM CUSTOMERS ");
        SELECT_START = Collections.unmodifiableMap(map);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Customers> findByIds(String stringIds, String customersExcept) {

        customersExcept = customersExcept == null || customersExcept.isEmpty() ? "\'\'" : customersExcept ;
        stringIds = stringIds == null || stringIds.isEmpty() ? "\'\'" : stringIds ;
        String searchQueryFinal ="SELECT * FROM customers WHERE id in ("+stringIds+") AND CUSTOMERS.CARD NOT IN ("+customersExcept+") ORDER BY field(ID,"+stringIds+")";
        Query query = null;
        query = entityManager.createNativeQuery(searchQueryFinal, Customers.class);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> List<T> findDTOByCriteria(String reference, String pdvId, String from, String to , String insee , Integer radius , Boolean mailConstraint , Boolean parentConstraint , String card , String address , String address2 , String zipCode , String city , String region , String country , String firstName , String lastName , String email , String phone , String phone2 , String notes , String customersExcept , Boolean parentOnly,String profilId,Boolean profilConstraint, Long maxResults, Class<T> selector) {
        List<T> retour = new ArrayList<T>();
        Query query = null;

        String searchQueryFinal = findByCriteriaQuery(selector.getSimpleName() , reference, pdvId, from, to, insee, radius, mailConstraint, parentConstraint, card, address, address2, zipCode, city, region, country, firstName, lastName, email, phone, phone2, notes, customersExcept, parentOnly, profilId, profilConstraint, maxResults);
        query = entityManager.createNativeQuery(searchQueryFinal, selector.getSimpleName());
        retour.addAll(query.getResultList());

        return retour;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Customers> findByCriteria(String reference, String pdvId, String from, String to , String insee , Integer radius , Boolean mailConstraint , Boolean parentConstraint , String card , String address , String address2 , String zipCode , String city , String region , String country , String firstName , String lastName , String email , String phone , String phone2 , String notes , String customersExcept , Boolean parentOnly,String profilId,Boolean profilConstraint, Long maxResults) {
        List<Customers> retour = new ArrayList<Customers>();
        String nativeQueryFinalSearch = findByCriteriaQuery(null , reference, pdvId, from, to, insee, radius, mailConstraint, parentConstraint, card, address, address2, zipCode, city, region, country, firstName, lastName, email, phone, phone2, notes, customersExcept, parentOnly, profilId, profilConstraint, maxResults); 
        Query queryFinalSearch = searchCustomersFinalList(nativeQueryFinalSearch, parentOnly);

        if(parentOnly) {
            List<String> listIds = queryFinalSearch.getResultList();
            String StringIds = "";
            String prefix = "\'";
            for (String id: listIds) {
                StringIds += prefix + id+"\'";
                prefix = ",\'";
            }
            retour.addAll(findByIds(StringIds, customersExcept ));
        }else {
            retour.addAll(queryFinalSearch.getResultList());
        }

        return retour;

    }

    private String findByCriteriaQuery(String returnClass , String reference, String pdvId, String from, String to , String insee , Integer radius , Boolean mailConstraint , Boolean parentConstraint , String card , String address , String address2 , String zipCode , String city , String region , String country , String firstName , String lastName , String email , String phone , String phone2 , String notes , String customersExcept , Boolean parentOnly,String profilId,Boolean profilConstraint, Long maxResults) {
        String queryFinalSearch = null;
        String queryVille = null;
        String queryMailConstraintWhere = null;
        String queryParentConstraintWhere = null;
        String queryDateFrom = " ";
        String queryDateTo = " ";
        String queryError = ERROR_BASE;
        String queryCond = "";
        String queryLimit = "";
        java.sql.Timestamp dateFrom = null ;
        java.sql.Timestamp dateTo = null;
        String queryProfilConstraintWhere = null;

        if(insee != null && ! insee.isEmpty()) {

            //On choppe la ville
            CriteriaQuery<Insee> inseeQuery = entityManager.getCriteriaBuilder().createQuery(Insee.class);
            Root<Insee> inseeRoot = inseeQuery.from(Insee.class);
            List<Insee> listResult = entityManager.createQuery(inseeQuery.select(inseeRoot).where(entityManager.getCriteriaBuilder().equal(inseeRoot.<String> get(Insee.Fields.INSEE), insee))).getResultList();
            Insee ville = listResult == null || listResult.isEmpty() ? null : listResult.get(0);

            //Si ok on choppe le rayon
            if(ville != null){
                queryVille = " JOIN INSEE ON (CUSTOMERS.postal = INSEE.ZIPCODE AND ((INSEE.COMMUNE = CUSTOMERS.CITY)";
                double delta_lat = GeoQuery.distance2latitude(radius);
                double delta_long = GeoQuery.distance2longitude(radius, ville.getLatitude());
                String[] replacements = { 
                        "-", " ", 
                        "SAINT", "ST"}; 
                String databaseFunction = "(REPLACE"; 
                if(replacements.length>0) {
                    String replacedCity="";  
                    queryVille+=" OR "; 

                    for (int i = 0; i < replacements.length/2; i++) { 
                        if(i==0) {
                            replacedCity=databaseFunction+"(CUSTOMERS.CITY, \'"+ replacements[i*2]+"\', \'"+replacements[i*2+1]+"\')";
                        }else {
                            replacedCity=databaseFunction+replacedCity+", \'"+ replacements[i*2]+"\', \'"+replacements[i*2+1]+"\')";
                        }
                    } 
                    queryVille+=replacedCity+"= INSEE.COMMUNE))) AND INSEE.LATITUDE BETWEEN \'"+(ville.getLatitude() - delta_lat)+"\' AND \'"+(ville.getLatitude() + delta_lat)+"\' AND INSEE.LONGITUDE BETWEEN \'"+(ville.getLongitude() - delta_long)+"\' AND \'"+(ville.getLongitude() + delta_long)+"\'";
                }else {
                    queryVille+=")) AND INSEE.LATITUDE BETWEEN \'"+(ville.getLatitude() - delta_lat)+"\' AND \'"+(ville.getLatitude() + delta_lat)+"\' AND INSEE.LONGITUDE BETWEEN \'"+(ville.getLongitude() - delta_long)+"\' AND \'"+(ville.getLongitude() + delta_long)+"\'";
                }
            }
        }

        if(from != null) {
            dateFrom = new java.sql.Timestamp(Long.parseLong(from));
            queryDateFrom = " AND CUSTOMERS.LAST_UPDATE>= \'"+dateFrom+"\' ";
        }

        if(to != null) {
            dateTo = new java.sql.Timestamp(Long.parseLong(to));
            queryDateTo = " AND CUSTOMERS.LAST_UPDATE<= \'"+dateTo+"\' ";
        }

        if(pdvId != null) {
            pdvId = " AND CUSTOMERS.LOCATION_ID='"+ pdvId+"' ";
        }else {
            pdvId= " ";
        }

        if(mailConstraint != null) {
            queryMailConstraintWhere = " AND CUSTOMERS.EMAIL IS "; 
            if(mailConstraint) {
                queryMailConstraintWhere += "NOT NULL";
            } else {
                queryMailConstraintWhere += "NULL";
            }
        }else {
            queryMailConstraintWhere = " ";
        }

        if(parentConstraint != null) {
            queryParentConstraintWhere = " AND CUSTOMERS.ID_PARENT ";
            if(parentConstraint) {
                queryParentConstraintWhere += "IS NOT NULL";
            } else {
                queryParentConstraintWhere += "IS NULL";
            }
        }else {
            queryParentConstraintWhere = " ";
        }


        if(lastName != null) {
            queryCond += getCond("CUSTOMERS.LASTNAME" , lastName);
            queryError += getError("CUSTOMERS.LASTNAME" , lastName);
        }

        if(firstName != null) {
            queryCond += getCond("CUSTOMERS.FIRSTNAME" , firstName);
            queryError += getError("CUSTOMERS.FIRSTNAME" , firstName);    
        }

        if(card != null) {
            queryCond += getCond("CUSTOMERS.CARD" , card);
            queryError += getError("CUSTOMERS.CARD" , card);
        }
        
        if(reference != null) {
            reference = FilteringAndFormattingData.SQLFilter(reference);
            queryCond += " AND ( CUSTOMERS.CARD LIKE '%" + reference + "%' OR CUSTOMERS.SEARCHKEY LIKE '%" + reference + "%') ";
            queryError += "+ IF((CUSTOMERS.CARD = '"+ reference +"' OR CUSTOMERS.SEARCHKEY = '"+ reference +"') , 0 , IF((CUSTOMERS.CARD LIKE '"+ reference +"%' OR CUSTOMERS.SEARCHKEY LIKE '"+ reference +"%') , 1 ,  2)) ";
        }

        if(address != null) {
            address = FilteringAndFormattingData.SQLFilter(address);
            queryCond += getCond("CUSTOMERS.ADDRESS" , address);
            queryError += getError("CUSTOMERS.ADDRESS" , address);
        }

        if(address2 != null) {
            address2 = FilteringAndFormattingData.SQLFilter(address2);
            queryCond += getCond("CUSTOMERS.ADDRESS2" , address2);
            queryError += getError("CUSTOMERS.ADDRESS2" , address2);
        }

        if(zipCode != null) {
            queryCond += getCond("CUSTOMERS.POSTAL" , zipCode);
            queryError += getError("CUSTOMERS.POSTAL" , zipCode);
        }

        if(city != null) {
            queryCond += getCond("CUSTOMERS.CITY" , city);
            queryError += getError("CUSTOMERS.CITY" , city);  
        }

        if(region != null) {
            queryCond += getCond("CUSTOMERS.REGION" , region);
            queryError += getError("CUSTOMERS.REGION" , region);
        }

        if(country != null) {
            queryCond += getCond("CUSTOMERS.COUNTRY" , country);
            queryError += getError("CUSTOMERS.COUNTRY" , country);
        }

        if(email != null) {
            queryCond += getCond("CUSTOMERS.EMAIL" , email);
            queryError += getError("CUSTOMERS.EMAIL" , email);
        }

        if(phone != null) {
            if(phone2 == null) {
                queryCond += getMultifieldCond(Arrays.asList("CUSTOMERS.PHONE","CUSTOMERS.PHONE2") , phone);
            } else {
                queryCond += getCond("CUSTOMERS.PHONE" , phone);
                queryError += getError("CUSTOMERS.PHONE" , phone);
            }
        }

        if(phone2 != null) {
            if(phone == null) {
                queryCond += getMultifieldCond(Arrays.asList("CUSTOMERS.PHONE","CUSTOMERS.PHONE2") , phone2);
            } else {
                queryCond += getCond("CUSTOMERS.PHONE2" , phone2);
                queryError += getError("CUSTOMERS.PHONE2" , phone2);
            }
        }

        if(notes != null) {
            queryCond += getCond("CUSTOMERS.NOTES" , notes);
            queryError += getError("CUSTOMERS.NOTES" , notes);
        }
        if(profilId != null) {
            queryProfilConstraintWhere = " AND ( CUSTOMERS.DISCOUNTPROFILE_ID ";
            if(profilConstraint == null || profilConstraint) {
                queryProfilConstraintWhere += "= ";
            } else {
                queryProfilConstraintWhere += "IS NULL OR CUSTOMERS.DISCOUNTPROFILE_ID != ";
            }
            queryProfilConstraintWhere += profilId;
            queryProfilConstraintWhere += " ) ";
        }else {
            queryProfilConstraintWhere = " ";
        }

        if(maxResults != null && maxResults >0) {
            queryLimit = " LIMIT "+maxResults.toString() +" ";
        }

        queryFinalSearch = searchNativeQuery(returnClass ,queryCond, queryError, customersExcept, queryVille, queryMailConstraintWhere, queryParentConstraintWhere, queryProfilConstraintWhere, pdvId, queryDateFrom, queryDateTo, parentOnly, queryLimit);;

        return queryFinalSearch;

    }

    /**
     * PG : Création et execution de la requête finale pour la recherche des clients
     */
    private Query searchCustomersFinalList(String searchQueryFinal , boolean parentOnly)  {

        Query query = null;   
        if(parentOnly) {

            query = entityManager.createNativeQuery(searchQueryFinal);

        }else {

            query = entityManager.createNativeQuery(searchQueryFinal, Customers.class);

        }

        return query;

    }

    /**
     * Ma fonction pour calculer l'erreur 0 = correspondance exacte , 1 débute par , 2 contient et on somme sur chacun des critères
     * 
     */
    private String getError(String champ, String valeur) {
        String retour = "";
        String exact = "";
        String niv1 = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            CustomersCirteriaSearchDTO criteria = mapper.readValue(valeur, CustomersCirteriaSearchDTO.class);
            List<String> criteres = criteria.getSearchvalue() ;
            if(champ != null && criteres.size()>0) {
                boolean first = true ;
                for(String critere : criteres) {
                    if(!first && critere != null && critere.isEmpty()) {
                        exact += " OR " ;
                        niv1 += " OR " ;
                    } 
                    if (critere != null && critere.length() > 0 ) {
                        critere = FilteringAndFormattingData.SQLFilter(critere);
                        exact = champ +" = '"+ critere +"' ";
                        niv1 = champ +" LIKE '"+ critere +"%' ";
                        first = false;
                    }

                }
                if(!exact.isEmpty()) {
                    retour = "+ IF(("+ exact +") , 0 , IF(("+ niv1 +") , 1 ,  2)) ";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retour;

    }

    /**
     * Ma fonction pour le where en fonction des input
     * 
     */
    private String getCond(String champ, String valeur) {
        String retour = getBasicCond(champ, valeur);

        if(!retour.isEmpty()) {
            retour = " AND ( " + retour + ") ";
        }
        return retour;
    }

    private String getBasicCond(String champ, String valeur) {
        String retour = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            CustomersCirteriaSearchDTO criteria = mapper.readValue(valeur, CustomersCirteriaSearchDTO.class);
            List<String> criteres = criteria.getSearchvalue() ;
            if(champ != null && criteres.size()>0) {
                boolean first = true ;
                for(String critere : criteres) {
                    if(!first && critere != null && critere.isEmpty()) {
                        retour += " OR " ;
                    } 
                    if (critere != null && critere.length() > 0 ) {
                        critere = FilteringAndFormattingData.SQLFilter(critere);
                        retour = champ +" LIKE '%"+ critere +"%' ";
                        first = false;
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retour;
    }

    private String getMultifieldCond(List<String> champs, String valeur) {
        String retour = "";
        ArrayList<String> ret = new ArrayList<String>();
        for(String champ : champs) {
            String element = getBasicCond(champ, valeur);
            if(!element.isEmpty()) {
                ret.add(element);
            }
        }
        if(!ret.isEmpty()) {
            for(String element : ret) {
                if(! retour.isEmpty()) {
                    retour = retour + " OR " ;
                }
                retour = retour + " ( " + element + ") ";
            }
            retour = " AND ( " + retour + ") ";
        }
        return retour;
    }

    private String searchNativeQuerySelect(String returnClass , boolean parentOnly, String queryVille) {
        String searchQueryFinal;
        if(returnClass != null) {
            searchQueryFinal = SELECT_START.get(returnClass);
            if (queryVille!=null) {
                searchQueryFinal += queryVille;
            }
        }else if(queryVille != null && !parentOnly) {
            searchQueryFinal = SELECT_START.get("WithCity") + queryVille + " "; 
        }else if(queryVille == null && !parentOnly) {
            searchQueryFinal = SELECT_START.get("Default");
        }else {
            searchQueryFinal = SELECT_START.get("ParentsOnly");
        }
        
        return searchQueryFinal ;
    }
    
    
    private String searchNativeQueryEnd(String queryCond ,String queryError, String customersExcept , String queryMailConstraintWhere, String queryParentConstraintWhere,String queryProfilConstraintWhere,String pdvId, String queryDateFrom, String queryDateTo, Boolean parentOnly, String queryLimit)  {
        String searchQueryFinal = " ";

        customersExcept = customersExcept == null || customersExcept.isEmpty() ? "" :  " AND CUSTOMERS.CARD NOT IN (" + customersExcept + ") ";

        if (! queryCond.isEmpty()) {
            if(ERROR_BASE.equals(queryError)){
                queryError = "";
            } else {
                queryError = " ORDER BY "+ queryError +" ASC ";
            }
            searchQueryFinal +="WHERE true " + queryCond + customersExcept + queryMailConstraintWhere + queryParentConstraintWhere + queryProfilConstraintWhere + pdvId  + queryDateFrom + queryDateTo +  "  GROUP BY CUSTOMERS.ID "+ queryError + queryLimit + ";";

        }else {
            searchQueryFinal +="WHERE true " + queryMailConstraintWhere + queryParentConstraintWhere + queryProfilConstraintWhere + pdvId + queryDateFrom + queryDateTo  + " ORDER BY CUSTOMERS.LAST_UPDATE DESC" + queryLimit + ";";

        }
 
        return searchQueryFinal;
    }
    
    private String searchNativeQuery(String returnClass , String queryCond ,String queryError, String customersExcept , String queryVille, String queryMailConstraintWhere, String queryParentConstraintWhere,String queryProfilConstraintWhere,String pdvId, String queryDateFrom, String queryDateTo, Boolean parentOnly, String queryLimit)  {
        String searchQueryFinal = searchNativeQuerySelect(returnClass , parentOnly, queryVille);
        searchQueryFinal += searchNativeQueryEnd(queryCond, queryError, customersExcept, queryMailConstraintWhere, queryParentConstraintWhere, queryProfilConstraintWhere, pdvId, queryDateFrom, queryDateTo, parentOnly, queryLimit);
        return searchQueryFinal;
    }



}
