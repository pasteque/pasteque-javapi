package org.pasteque.api.dao.products;

import org.pasteque.api.model.products.TariffAreas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITariffAreasApiDAO extends JpaRepository<TariffAreas, Long> {

}
