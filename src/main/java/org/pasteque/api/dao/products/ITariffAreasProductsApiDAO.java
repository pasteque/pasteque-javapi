package org.pasteque.api.dao.products;

import org.pasteque.api.model.products.TariffAreasProducts;
import org.pasteque.api.model.products.TariffAreasProductsId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITariffAreasProductsApiDAO extends JpaRepository<TariffAreasProducts, TariffAreasProductsId> , ITariffAreasProductsApiCustomDAO {

}
