package org.pasteque.api.dao.products;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.products.TariffAreasProducts;
import org.pasteque.api.model.products.TariffAreasProductsId;

public interface ITariffAreasProductsApiCustomDAO extends IGenericDAO<TariffAreasProducts, TariffAreasProductsId>{

    TariffAreasProducts findActive(String locationId, String productsId);
    
}
