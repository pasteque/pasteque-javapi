package org.pasteque.api.dao.products;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.products.Categories;

public interface ICategoriesApiCustomDAO extends IGenericDAO<Categories,String>{

    List<Object[]> getCategoriesByClosedCash(String closedCashId);

}
