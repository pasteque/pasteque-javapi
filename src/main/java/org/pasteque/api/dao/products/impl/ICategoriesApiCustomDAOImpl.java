package org.pasteque.api.dao.products.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.dao.products.ICategoriesApiCustomDAO;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.products.Categories;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.tickets.Receipts;
import org.pasteque.api.model.tickets.TicketLines;
import org.pasteque.api.model.tickets.Tickets;

public class ICategoriesApiCustomDAOImpl extends GenericDAOImpl<Categories,String> implements ICategoriesApiCustomDAO {

    //TODO Reprendre en native
    @Override
    public List<Object[]> getCategoriesByClosedCash(String closedCashId) {
        CriteriaQuery<Object[]> query = getCriteriaBuilder().createQuery(Object[].class);
        Root<Tickets> root = query.from(Tickets.class);
        List<Selection<?>> selections = new ArrayList<>();
        List<Expression<?>> groupBy = new ArrayList<>();
        Predicate predicate = getCriteriaBuilder().equal(
                root.get(Tickets.Fields.RECEIPTS).get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.MONEY), closedCashId);

        Join<Tickets, TicketLines> join1 = root.join(Tickets.Fields.LINES);
        Join<TicketLines, Categories> join2 = join1.join(TicketLines.Fields.PRODUCT, JoinType.LEFT).join(Products.Fields.CATEGORY, JoinType.LEFT);

        //EDU - 2015-06-29 Prise en compte du discount !
        selections.add(join2.get(Categories.Fields.ID));
        selections.add(getCriteriaBuilder().sum(
                getCriteriaBuilder().prod( 
                        getCriteriaBuilder().prod(join1.<Double> get(TicketLines.Fields.UNIT_PRICE), join1.<Double> get(TicketLines.Fields.UNITS)),
                        getCriteriaBuilder().prod(
                                getCriteriaBuilder().diff(1D ,join1.<Double> get(TicketLines.Fields.DISCOUNT_RATE)), 
                                getCriteriaBuilder().diff(1D , join1.get(TicketLines.Fields.TICKET).<Double> get(Tickets.Fields.DISCOUNT_RATE)))
                        )));
        
        groupBy.add(join2.get(Categories.Fields.ID));

        query.multiselect(selections).where(predicate).groupBy(groupBy);

        TypedQuery<Object[]> tq = entityManager.createQuery(query);
        return tq.getResultList();
    }
}
