package org.pasteque.api.dao.products;

import org.pasteque.api.model.products.Categories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ICategoriesApiDAO extends JpaRepository<Categories, String> , JpaSpecificationExecutor<Categories> , ICategoriesApiCustomDAO{

}
