package org.pasteque.api.dao.products.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.dao.products.ITariffAreasProductsApiCustomDAO;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.TariffAreas;
import org.pasteque.api.model.products.TariffAreasProducts;
import org.pasteque.api.model.products.TariffAreasProductsId;
import org.pasteque.api.model.products.TariffAreasValidity;
import org.pasteque.api.model.saleslocations.Locations;

public class ITariffAreasProductsApiCustomDAOImpl extends GenericDAOImpl<TariffAreasProducts, TariffAreasProductsId> implements ITariffAreasProductsApiCustomDAO {

    @Override
    public TariffAreasProducts findActive(String locationId,
            String productId) {
        CriteriaQuery<TariffAreasProducts> query = getQuery();
        Root<TariffAreas> root = query.from(TariffAreas.class);

        Join<TariffAreas, TariffAreasValidity> join1 = root.join(TariffAreas.Fields.TARIFF_AREAS_VALIDITY);
        Join<TariffAreas, TariffAreasValidity> join2 = join1.join(TariffAreasValidity.Fields.LOCATION, JoinType.LEFT);
        Join<TariffAreas, TariffAreasProducts> join3 = root.join(TariffAreas.Fields.TARIFF_AREAS_PRODUCTS);

        Predicate predicate = getCriteriaBuilder().equal(join2.get(Locations.Fields.ID), locationId);
        Predicate predicate1 = getCriteriaBuilder().isNull(join2);

        Date today = new Date();
        Predicate predicate2 = getCriteriaBuilder().lessThanOrEqualTo(join1.<Date> get(TariffAreasValidity.Fields.START_DATE), today);
        Predicate predicate3 = getCriteriaBuilder().greaterThanOrEqualTo(join1.<Date> get(TariffAreasValidity.Fields.END_DATE), today);
        Predicate predicate4 = getCriteriaBuilder().or(predicate, predicate1);
        Predicate predicate5 = getCriteriaBuilder()
                .equal(join3.get(TariffAreasProducts.Fields.PRODUCTS).get(Products.Fields.ID), productId);

        Order order = getCriteriaBuilder().asc(root.get(TariffAreas.Fields.ORDER));
        List<TariffAreasProducts> result = getResultList(query.select(join3).where(predicate4, predicate2, predicate3, predicate5)
                .orderBy(order));
        return result.isEmpty() ? null : result.get(0);
    }

}
