package org.pasteque.api.dao.products;

import java.util.Date;
import java.util.List;

import org.pasteque.api.model.products.Categories;
import org.pasteque.api.model.products.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IProductsApiDAO extends JpaRepository<Products, String> , JpaSpecificationExecutor<Products> {

    List<Products> findByLastUpdateAfter(Date lastUpdate);
    
    List<Products> findByCategory(Categories cat);
    
    List<Products> findByReference(String reference);
}
