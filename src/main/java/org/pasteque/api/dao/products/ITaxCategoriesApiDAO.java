package org.pasteque.api.dao.products;

import org.pasteque.api.model.products.TaxCategories;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITaxCategoriesApiDAO extends JpaRepository<TaxCategories, String>{

    TaxCategories findByName(String string);

}
