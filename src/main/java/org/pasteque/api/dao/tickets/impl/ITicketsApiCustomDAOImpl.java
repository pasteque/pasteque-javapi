package org.pasteque.api.dao.tickets.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.dao.tickets.ITicketsApiCustomDAO;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.dto.tickets.POSTicketsDTO;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.customers.Customers;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.saleslocations.SalesLocations;
import org.pasteque.api.model.saleslocations.Tours;
import org.pasteque.api.model.security.People;
import org.pasteque.api.model.tickets.Receipts;
import org.pasteque.api.model.tickets.TicketLines;
import org.pasteque.api.model.tickets.Tickets;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

//TODO Reprise avec MultiValueMap
public class ITicketsApiCustomDAOImpl extends GenericDAOImpl<Tickets, String> implements ITicketsApiCustomDAO {
    @PersistenceContext(type=PersistenceContextType.EXTENDED) 
    EntityManager em;
    
    @Value("${spring.data.paging.step:10000}")
    private int step;
    
    @Value("${spring.data.paging.maxuse:75}")
    private int maxUse;
    @Override
    public List<Tickets> findByCriteria(String ticketId, String ticketType, String cashId, String dateStart, String dateStop,
            String customerId, String userId, String locationId, String saleLocationId, String tourId, String fromValue, String toValue, Boolean orderConstraint) {
        
        CriteriaQuery<Tickets> query = getQuery();
        Root<Tickets> root = query.from(Tickets.class);        
        List<Predicate> predicates = new ArrayList<>();
        if (ticketId != null && ! ticketId.trim().isEmpty()) {
            predicates.add(getCriteriaBuilder().equal(root.get(Tickets.Fields.TICKET_ID), ticketId));
        }
        if (ticketType != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Tickets.Fields.TICKET_TYPE), ticketType));
        }
                
        if (cashId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    root.get(Tickets.Fields.RECEIPTS).get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.MONEY), cashId));
        }
        if (tourId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    root.get(Tickets.Fields.RECEIPTS).get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.SALES_LOCATION).get(SalesLocations.Fields.TOUR).get(Tours.Fields.TOUR_ID), tourId));
        }
        if (saleLocationId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    root.get(Tickets.Fields.RECEIPTS).get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.SALES_LOCATION).get(SalesLocations.Fields.SALES_LOCATION_ID), saleLocationId));
        }
        if (locationId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    root.get(Tickets.Fields.RECEIPTS).get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.CASH_REGISTER).get(CashRegisters.Fields.LOCATION).get(Locations.Fields.ID), locationId));
        }
        if(orderConstraint != null) {
            String lineType = orderConstraint ? TicketLines.LineType.ORDER : TicketLines.LineType.DELIVERY ;
            predicates.add(getCriteriaBuilder().equal(
                    root.get(Tickets.Fields.LINES).get(TicketLines.Fields.TYPE), lineType));
        }
        if (dateStart != null) {
            Date dStart = new Date(Long.parseLong(dateStart));
            predicates.add(getCriteriaBuilder().greaterThanOrEqualTo(
            root.get(Tickets.Fields.RECEIPTS).<Date> get(Receipts.Fields.DATE_NEW), dStart));
        }
        if (dateStop != null ) {
            Date dStop = new Date(Long.parseLong(dateStop));
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(
            root.get(Tickets.Fields.RECEIPTS).<Date> get(Receipts.Fields.DATE_NEW), dStop));
        }
        if (customerId != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Tickets.Fields.CUSTOMER).get(Customers.Fields.ID), customerId));
        }
        if (userId != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Tickets.Fields.PERSON).get(People.Fields.ID), userId));
        }
        //On n'exploite pas les paramètres fromValue et ToValue - pour le faire on passe par la version directement en DTO
        //pb de perf en particulier
        
        Order order = getCriteriaBuilder().desc(root.get(Tickets.Fields.TICKET_ID));
        return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})).orderBy(order));
        
    }
    
    @SuppressWarnings("unchecked")
    @Transactional(isolation = Isolation.READ_COMMITTED, readOnly = true)
    @Override
    public <T extends ILoadable> DtoSQLParameters findByCriteriaDTO(String ticketId, String ticketType, String cashId, String dateStart, String dateStop,
            String customerId, String userId, String locationId, String locationCategoryName , String saleLocationId, String tourId, String fromValue, String toValue , Boolean orderConstraint, Class<T> selector) {
        
        //EDU Probleme avec le having pour la contrainte sur le montant du ticket
        //Problème avec les perfs.

        T instance;
        DtoSQLParameters model;
        Query query = null;
        ArrayList<String> tableList = new ArrayList<String>();
        ArrayList<String> constraintList = new ArrayList<String>();
        String havingFromAmount = "";
        String havingToAmount = "";
        
        //Variables pour gérer la paging à la main et les références à la mémoire disponible
        int starting = 0;
        //int size = 0;
        //Getting the runtime reference from system
        int mb = 1024*1024;
        Runtime runtime = Runtime.getRuntime();
        try {
            instance = selector.newInstance();

            model = instance.getSQLParameters();
        
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            model = (new POSTicketsDTO()).getSQLParameters();
        }
        String nativeSQL_0 = ""; 

        String orderTicket = " ORDER BY t1.TICKETID DESC" ;
        String orderDateouv = " ORDER BY YEAR(t0.DATENEW) DESC, MONTH(t0.DATENEW) DESC, DAY(t0.DATENEW) DESC, t5.LOCATION_ID DESC" ;
        String orderDateTh = " ORDER BY t3.ID DESC" ;
        String order ;
        
        String dateStartDeb = null ;
        String dateStopDeb = null ;
        String dateStartSession = null ;
        String dateStopSession = null ;
        
        String fromTotalValue = null;
        String toTotalValue = null;
        
        
        String havingFromTicket = "(SUM(t2.TOTAL) >=?) ";
        String havingToTicket = "(SUM(t2.TOTAL) <=?) ";
        String havingFromDateOuv = "(SUM(CASE WHEN t2.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' ) THEN ROUND(t2.TOTAL,2) ELSE 0 END) >=?) ";
        String havingToDateOuv = "(SUM(CASE WHEN t2.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' ) THEN ROUND(t2.TOTAL,2) ELSE 0 END) <=?) ";
        String havingFromDateTh = "( ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' ) AND P.RECEIPT = t904.ID ) >=?) ";
        String havingToDateTh = "( ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' ) AND P.RECEIPT = t904.ID ) <=?) ";
        String havingFrom = null;
        String havingTo = null;
        
        String[] base = model.tableList.get(0).trim().split("\\s+", 3);
        
        
        int paramIndex = 0;

        //construction tableau param et chaine
        
        //TODO adapter certains paramètres pour les cas des recherches sur une base différente
        // Sessions de vente ou déballage par exemple.
        switch (base[0]) {
        case "TICKETS":
            order = orderTicket ;
            havingFrom = havingFromTicket;
            havingTo = havingToTicket;
            break;
        case "DEBALLAGES":
            order = orderDateTh ;
            havingFrom = havingFromDateTh;
            havingTo = havingToDateTh;
            dateStartDeb = dateStart ;
            dateStopDeb = dateStop ;
            dateStart = null;
            dateStop = null;
            
            fromTotalValue = fromValue;
            toTotalValue = toValue;
            fromValue = null;
            toValue = null;
            
            break;
        case "TICKETLINES":
            order = orderTicket ;
            break;
        case "CLOSEDCASH":
            order = orderDateouv ;
            havingFrom = havingFromDateOuv;
            havingTo = havingToDateOuv;
            dateStartSession = dateStart ;
            dateStopSession = dateStop ;
            dateStart = null;
            dateStop = null;
            
            fromTotalValue = fromValue;
            toTotalValue = toValue;
            fromValue = null;
            toValue = null;       
            break;
        default :
            order = orderTicket ;
            havingFrom = havingFromTicket;
            havingTo = havingToTicket;
        }
        
        
        
        if (ticketId != null && !ticketId.trim().isEmpty()) {
            model.constraintList.add(" (t1.TICKETID = ?)");
            model.parameterList.put(++paramIndex, ticketId);
            if(model.tableList.contains(" RECEIPTS t0 LEFT JOIN TICKETS t1 ON (t1.ID = t0.ID) LEFT JOIN PAYMENTS t2 ON (t2.RECEIPT = t0.ID) LEFT JOIN (SELECT DATE(t00.DATENEW) AS DATENEW, t01.CASHREGISTER_ID, AMOUNT AS TOTTAXES FROM TAXLINES T, receipts t00, CLOSEDCASH t01 %s WHERE t00.MONEY = t01.MONEY AND T.RECEIPT = t00.ID %s GROUP BY DATE(t00.DATENEW) , t01.CASHREGISTER_ID %s) TMPTAXES ON TMPTAXES.DATENEW = date(t0.DATENEW) ")) {
                tableList.add(" tickets t02");
                constraintList.add(" AND t02.id=t00.id AND t02.TICKETID= "+ticketId);
            }
        }
        if (ticketType != null) {
            model.constraintList.add(" (t1.TICKETTYPE = ?)");
            model.parameterList.put(++paramIndex, ticketType);
        }
        if (customerId != null) {
            model.constraintList.add(" (t1.CUSTOMER = ?)");
            model.parameterList.put(++paramIndex, customerId);
        }
        if (userId != null) {
            model.constraintList.add(" (t1.PERSON = ?)");
            model.parameterList.put(++paramIndex, userId);
        }
        if (dateStart != null) {
            Date dStart = new Date(Long.parseLong(dateStart));
            if(model.tableHash.put("t0", "RECEIPTS") == null) {
                model.tableList.add(" RECEIPTS t0 ");
                model.constraintList.add(" t0.ID = t1.id ");
            }
            model.constraintList.add(" (t0.DATENEW >= ?)");
            model.parameterList.put(++paramIndex, dStart);
        }
        if (dateStop != null ) {
            Date dStop = new Date(Long.parseLong(dateStop));
            if(model.tableHash.put("t0", "RECEIPTS") == null) {
                model.tableList.add(" RECEIPTS t0 ");
                model.constraintList.add(" t0.ID = t1.id ");
            }
            model.constraintList.add(" (t0.DATENEW <= ?)");
            model.parameterList.put(++paramIndex, dStop);
        }
        
        if (dateStartSession != null) {
            Date dStart = new Date(Long.parseLong(dateStartSession));
            if(model.tableHash.put("t0", "RECEIPTS") == null) {
                model.tableList.add(" RECEIPTS t0 ");
                model.constraintList.add(" t0.ID = t1.id ");
            }
            if(model.tableHash.put("t4", "CLOSEDCASH") == null) {
                model.tableList.add(" CLOSEDCASH t4 ");
                model.constraintList.add(" t4.MONEY = t0.MONEY ");
            }
            model.constraintList.add(" (t4.DATESTART >= ?)");
            model.parameterList.put(++paramIndex, dStart);
            if(model.tableList.contains(" RECEIPTS t0 LEFT JOIN TICKETS t1 ON (t1.ID = t0.ID) LEFT JOIN PAYMENTS t2 ON (t2.RECEIPT = t0.ID) LEFT JOIN (SELECT DATE(t00.DATENEW) AS DATENEW, t01.CASHREGISTER_ID, AMOUNT AS TOTTAXES FROM TAXLINES T, receipts t00, CLOSEDCASH t01 %s WHERE t00.MONEY = t01.MONEY AND T.RECEIPT = t00.ID %s GROUP BY DATE(t00.DATENEW) , t01.CASHREGISTER_ID %s) TMPTAXES ON TMPTAXES.DATENEW = date(t0.DATENEW) ")) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String dateStartString = format.format(dStart);
                constraintList.add(" AND t00.DATENEW >= '"+dateStartString+"'");
            }
        }
        if (dateStopSession != null ) {
            Date dStop = new Date(Long.parseLong(dateStopSession));
            if(model.tableHash.put("t0", "RECEIPTS") == null) {
                model.tableList.add(" RECEIPTS t0 ");
                model.constraintList.add(" t0.ID = t1.id ");
            }
            if(model.tableHash.put("t4", "CLOSEDCASH") == null) {
                model.tableList.add(" CLOSEDCASH t4 ");
                model.constraintList.add(" t4.MONEY = t0.MONEY ");
            }
            model.constraintList.add(" (t4.DATEEND <= ? OR ( ISNULL(t4.DATEEND) AND t4.DATESTART <= ? ))");
            model.parameterList.put(++paramIndex, dStop);
            model.parameterList.put(++paramIndex, dStop);
            
            if(model.tableList.contains(" RECEIPTS t0 LEFT JOIN TICKETS t1 ON (t1.ID = t0.ID) LEFT JOIN PAYMENTS t2 ON (t2.RECEIPT = t0.ID) LEFT JOIN (SELECT DATE(t00.DATENEW) AS DATENEW, t01.CASHREGISTER_ID, AMOUNT AS TOTTAXES FROM TAXLINES T, receipts t00, CLOSEDCASH t01 %s WHERE t00.MONEY = t01.MONEY AND T.RECEIPT = t00.ID %s GROUP BY DATE(t00.DATENEW) , t01.CASHREGISTER_ID %s) TMPTAXES ON TMPTAXES.DATENEW = date(t0.DATENEW) ")) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String dateStopString = format.format(dStop);
                constraintList.add(" AND (t01.DATEEND <= '"+dateStopString+"' OR ( ISNULL(t01.DATEEND) AND t01.DATESTART <= '"+dateStopString+"' ))");
            }
        }
        
        if (dateStartDeb != null) {
            Date dStart = new Date(Long.parseLong(dateStartDeb));
            if(model.tableHash.put("t0", "RECEIPTS") == null) {
                model.tableList.add(" RECEIPTS t0 ");
                model.constraintList.add(" t0.ID = t1.id ");
            }
            if(model.tableHash.put("t4", "CLOSEDCASH") == null) {
                model.tableList.add(" CLOSEDCASH t4 ");
                model.constraintList.add(" t4.MONEY = t0.MONEY ");
            }
            if(model.tableHash.put("t3", "DEBALLAGES") == null) {
                model.tableList.add(" DEBALLAGES t3 ");
                model.constraintList.add(" t3.ID = t4.DEBALLAGE_ID ");
            }
            model.constraintList.add(" (t3.DEBUT >= ?)");
            model.parameterList.put(++paramIndex, dStart);
        }
        if (dateStopDeb != null ) {
            Date dStop = new Date(Long.parseLong(dateStopDeb));
            if(model.tableHash.put("t0", "RECEIPTS") == null) {
                model.tableList.add(" RECEIPTS t0 ");
                model.constraintList.add(" t0.ID = t1.id ");
            }
            if(model.tableHash.put("t4", "CLOSEDCASH") == null) {
                model.tableList.add(" CLOSEDCASH t4 ");
                model.constraintList.add(" t4.MONEY = t0.MONEY ");
            }
            if(model.tableHash.put("t3", "DEBALLAGES") == null) {
                model.tableList.add(" DEBALLAGES t3 ");
                model.constraintList.add(" t3.ID = t4.DEBALLAGE_ID ");
            }
            model.constraintList.add(" (t3.FIN <= ?)");
            model.parameterList.put(++paramIndex, dStop);
        }
        if (cashId != null) {
            if(model.tableHash.put("t0", "RECEIPTS") == null) {
                model.tableList.add(" RECEIPTS t0 ");
                model.constraintList.add(" t0.ID = t1.id ");
            }
            model.constraintList.add(" (t0.MONEY = ?)");
            model.parameterList.put(++paramIndex, cashId);
        }
        if (tourId != null) {
            if(model.tableHash.put("t0", "RECEIPTS") == null) {
                model.tableList.add(" RECEIPTS t0 ");
                model.constraintList.add(" t0.ID = t1.id ");
            }
            if(model.tableHash.put("t4", "CLOSEDCASH") == null) {
                model.tableList.add(" CLOSEDCASH t4 ");
                model.constraintList.add(" t4.MONEY = t0.MONEY ");
            }
            if(model.tableHash.put("t3", "DEBALLAGES") == null) {
                model.tableList.add(" DEBALLAGES t3 ");
                model.constraintList.add(" t3.ID = t4.DEBALLAGE_ID ");
            }
            model.constraintList.add(" (t3.TOURNEES_ID = ?)");
            model.parameterList.put(++paramIndex, tourId);
            if(model.tableList.contains(" RECEIPTS t0 LEFT JOIN TICKETS t1 ON (t1.ID = t0.ID) LEFT JOIN PAYMENTS t2 ON (t2.RECEIPT = t0.ID) LEFT JOIN (SELECT DATE(t00.DATENEW) AS DATENEW, t01.CASHREGISTER_ID, AMOUNT AS TOTTAXES FROM TAXLINES T, receipts t00, CLOSEDCASH t01 %s WHERE t00.MONEY = t01.MONEY AND T.RECEIPT = t00.ID %s GROUP BY DATE(t00.DATENEW) , t01.CASHREGISTER_ID %s) TMPTAXES ON TMPTAXES.DATENEW = date(t0.DATENEW) ")) {
                tableList.add(" deballages t03");
                constraintList.add(" AND t03.id=t01.DEBALLAGE_ID AND t03.TOURNEES_ID = "+tourId);
            }
        }
        if (saleLocationId != null) {
            if(model.tableHash.put("t0", "RECEIPTS") == null) {
                model.tableList.add(" RECEIPTS t0 ");
                model.constraintList.add(" t0.ID = t1.id ");
            }
            if(model.tableHash.put("t4", "CLOSEDCASH") == null) {
                model.tableList.add(" CLOSEDCASH t4 ");
                model.constraintList.add(" t4.MONEY = t0.MONEY ");
            }
            model.constraintList.add(" (t4.DEBALLAGE_ID = ?)");
            model.parameterList.put(++paramIndex, saleLocationId);
            if(model.tableList.contains(" RECEIPTS t0 LEFT JOIN TICKETS t1 ON (t1.ID = t0.ID) LEFT JOIN PAYMENTS t2 ON (t2.RECEIPT = t0.ID) LEFT JOIN (SELECT DATE(t00.DATENEW) AS DATENEW, t01.CASHREGISTER_ID, AMOUNT AS TOTTAXES FROM TAXLINES T, receipts t00, CLOSEDCASH t01 %s WHERE t00.MONEY = t01.MONEY AND T.RECEIPT = t00.ID %s GROUP BY DATE(t00.DATENEW) , t01.CASHREGISTER_ID %s) TMPTAXES ON TMPTAXES.DATENEW = date(t0.DATENEW) ")) {
                constraintList.add(" AND t01.DEBALLAGE_ID = "+saleLocationId);
            }
        }
        if (locationId != null) {
            if(model.tableHash.put("t0", "RECEIPTS") == null) {
                model.tableList.add(" RECEIPTS t0 ");
                model.constraintList.add(" t0.ID = t1.id ");
            }
            if(model.tableHash.put("t4", "CLOSEDCASH") == null) {
                model.tableList.add(" CLOSEDCASH t4 ");
                model.constraintList.add(" t4.MONEY = t0.MONEY ");
            }
            if(model.tableHash.put("t5", "CASHREGISTERS") == null) {
                model.tableList.add(" CASHREGISTERS t5 ");
                model.constraintList.add(" t5.ID = t4.CASHREGISTER_ID ");
            }
            model.constraintList.add(" (t5.LOCATION_ID = ?)");
            model.parameterList.put(++paramIndex, locationId);
            if(model.tableList.contains(" RECEIPTS t0 LEFT JOIN TICKETS t1 ON (t1.ID = t0.ID) LEFT JOIN PAYMENTS t2 ON (t2.RECEIPT = t0.ID) LEFT JOIN (SELECT DATE(t00.DATENEW) AS DATENEW, t01.CASHREGISTER_ID, AMOUNT AS TOTTAXES FROM TAXLINES T, receipts t00, CLOSEDCASH t01 %s WHERE t00.MONEY = t01.MONEY AND T.RECEIPT = t00.ID %s GROUP BY DATE(t00.DATENEW) , t01.CASHREGISTER_ID %s) TMPTAXES ON TMPTAXES.DATENEW = date(t0.DATENEW) ")) {
                tableList.add(" cashregisters t04");
                constraintList.add(" AND t04.id=t01.CASHREGISTER_ID AND t04.LOCATION_ID = "+locationId);
            }
        }
        
        if (locationCategoryName != null) {
            if(model.tableHash.put("t0", "RECEIPTS") == null) {
                model.tableList.add(" RECEIPTS t0 ");
                model.constraintList.add(" t0.ID = t1.id ");
            }
            if(model.tableHash.put("t4", "CLOSEDCASH") == null) {
                model.tableList.add(" CLOSEDCASH t4 ");
                model.constraintList.add(" t4.MONEY = t0.MONEY ");
            }
            if(model.tableHash.put("t5", "CASHREGISTERS") == null) {
                model.tableList.add(" CASHREGISTERS t5 ");
                model.constraintList.add(" t5.ID = t4.CASHREGISTER_ID ");
            }
            if(model.tableHash.put("t11", "LOCATIONS") == null) {
                model.tableList.add(" LOCATIONS t11 ");
                model.constraintList.add(" t11.ID = t5.LOCATION_ID ");
            }
            if(model.tableHash.put("t14", "CATEGORIES") == null) {
                model.tableList.add(" CATEGORIES t14 ");
                model.constraintList.add(" t14.ID = t11.CATEG_ID ");
            }
            model.constraintList.add(" (t14.NAME = ?)");
            model.parameterList.put(++paramIndex, locationCategoryName);
        }
        if (orderConstraint != null) {
            String lineType = orderConstraint ? TicketLines.LineType.ORDER : TicketLines.LineType.DELIVERY ;
            if(model.tableHash.put("t8", "TICKETLINES") == null) {
                model.tableList.add(" TICKETLINES t8 ");
                model.constraintList.add(" t8.ticket = t1.ID ");
            }
            model.constraintList.add(" (t8.TYPE = ?)");
            model.parameterList.put(++paramIndex, lineType);
        }

        if (fromValue != null) {
            model.groupBy = "t1.TICKETID ";

            if(model.tableHash.put("t2", "PAYMENTS") == null) {
                model.tableList.add(" PAYMENTS t2 ");
                model.constraintList.add(" t2.receipt = t1.ID ");
            }
            Double minTicket= Double.parseDouble(fromValue);
            model.havingList.add(havingFrom);
            model.parameterList.put(++paramIndex, minTicket);
        }
        if (toValue != null) {
            model.groupBy = "t1.TICKETID ";
            
            Double maxTicket= Double.parseDouble(toValue);
            if(model.tableHash.put("t2", "PAYMENTS") == null) {
                model.tableList.add(" PAYMENTS t2 ");
                model.constraintList.add(" t2.receipt = t1.ID ");
            }
            model.groupBy = "t1.TICKETID ";
            model.havingList.add(havingTo);
            model.parameterList.put(++paramIndex, maxTicket);
        }
        
        //gestion du montant total
        if (fromTotalValue != null) {

            Double minTicket= Double.parseDouble(fromTotalValue);
            model.havingList.add(havingFrom);
            model.parameterList.put(++paramIndex, minTicket);

            if(model.tableList.contains(" RECEIPTS t0 LEFT JOIN TICKETS t1 ON (t1.ID = t0.ID) LEFT JOIN PAYMENTS t2 ON (t2.RECEIPT = t0.ID) LEFT JOIN (SELECT DATE(t00.DATENEW) AS DATENEW, t01.CASHREGISTER_ID, AMOUNT AS TOTTAXES FROM TAXLINES T, receipts t00, CLOSEDCASH t01 %s WHERE t00.MONEY = t01.MONEY AND T.RECEIPT = t00.ID %s GROUP BY DATE(t00.DATENEW) , t01.CASHREGISTER_ID %s) TMPTAXES ON TMPTAXES.DATENEW = date(t0.DATENEW) ")) {
                havingFromAmount = " (SUM(CASE WHEN t05.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' ) THEN ROUND(t05.TOTAL,2) ELSE 0 END) >= '"+minTicket+"')";
            }
        }
        
        if (toTotalValue != null) {
            
            Double maxTicket= Double.parseDouble(toTotalValue);
            model.havingList.add(havingTo);
            model.parameterList.put(++paramIndex, maxTicket);
            
            if(model.tableList.contains(" RECEIPTS t0 LEFT JOIN TICKETS t1 ON (t1.ID = t0.ID) LEFT JOIN PAYMENTS t2 ON (t2.RECEIPT = t0.ID) LEFT JOIN (SELECT DATE(t00.DATENEW) AS DATENEW, t01.CASHREGISTER_ID, AMOUNT AS TOTTAXES FROM TAXLINES T, receipts t00, CLOSEDCASH t01 %s WHERE t00.MONEY = t01.MONEY AND T.RECEIPT = t00.ID %s GROUP BY DATE(t00.DATENEW) , t01.CASHREGISTER_ID %s) TMPTAXES ON TMPTAXES.DATENEW = date(t0.DATENEW) ")) {
                havingToAmount = " (SUM(CASE WHEN t05.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' ) THEN ROUND(t05.TOTAL,2) ELSE 0 END) <= '"+maxTicket+"')";
            }
        }
        
        if(model.tableList.contains(" RECEIPTS t0 LEFT JOIN TICKETS t1 ON (t1.ID = t0.ID) LEFT JOIN PAYMENTS t2 ON (t2.RECEIPT = t0.ID) LEFT JOIN (SELECT DATE(t00.DATENEW) AS DATENEW, t01.CASHREGISTER_ID, AMOUNT AS TOTTAXES FROM TAXLINES T, receipts t00, CLOSEDCASH t01 %s WHERE t00.MONEY = t01.MONEY AND T.RECEIPT = t00.ID %s GROUP BY DATE(t00.DATENEW) , t01.CASHREGISTER_ID %s) TMPTAXES ON TMPTAXES.DATENEW = date(t0.DATENEW) ")) {
            
            String tableString = "";
            String constraintString = "";
            String havingAmount ="";
            
            if (!havingFromAmount.isEmpty() && !havingToAmount.isEmpty()) {
                tableList.add(" payments t05");
                constraintList.add(" AND t05.receipt=t00.id");
                havingAmount= " HAVING"+havingFromAmount+" AND "+havingToAmount;
            }else if(!havingFromAmount.isEmpty() || !havingToAmount.isEmpty()) {
                tableList.add(" payments t05");
                constraintList.add(" AND t05.receipt=t00.id");
                havingAmount= " HAVING"+havingFromAmount+havingToAmount;
            }else {
                havingAmount="";
            }
            
            for (String table : tableList)
            {
                tableString += ", "+ table ;
            }
            
            for (String constraint : constraintList)
            {
                constraintString +=constraint;
            }
            
            model.tableList.set(3, String.format(model.tableList.get(3),tableString, constraintString, havingAmount));

        }
        
        nativeSQL_0 = nativeSQL_0.concat(StringUtils.collectionToCommaDelimitedString(model.tableList));
        if (model.constraintList.size() > 0) {
            nativeSQL_0 = nativeSQL_0.concat("WHERE ").concat(StringUtils.collectionToDelimitedString(model.constraintList, " AND "));
        }
        if(model.groupBy != null) {
            nativeSQL_0 = nativeSQL_0.concat(" GROUP BY ").concat(model.groupBy) ;
        }
        if (model.havingList.size() > 0) {
            nativeSQL_0 = nativeSQL_0.concat(" HAVING ").concat(StringUtils.collectionToDelimitedString(model.havingList, " AND "));
        }
        nativeSQL_0 = nativeSQL_0.concat(order);
        
        nativeSQL_0 = model.select.concat(nativeSQL_0);
        
        while(model.getResultSize() == starting) {
            String limit = String.format(" LIMIT %d,%d", starting , step);
            
            query = em.createNativeQuery(nativeSQL_0 + limit);
            //injection param
            for(Map.Entry<Integer, Object> entry : model.parameterList.entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
            
            System.out.println("##### Heap utilization statistics [MB] #####");
            //Print used memory
            System.out.println("Used Memory:" 
                + (runtime.totalMemory() - runtime.freeMemory()) / mb);

            //Print free memory
            System.out.println("Free Memory:" 
                + runtime.freeMemory() / mb);
            
            //Print total available memory
            System.out.println("Total Memory:" + runtime.totalMemory() / mb);

            //Print Maximum available memory
            System.out.println("Max Memory:" + runtime.maxMemory() / mb);
            
            //Print Memory for results
            long quantity =  (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) / model.getRowSize() ;
            System.out.println("Number of résults memory can cover:" + quantity);
            
            //Print Memory for results
            System.out.println("Memory percentage this result will cover:" + 100 * step / quantity );
            
            //Print Array size
            System.out.println("Current size:" + model.getResultSize() );

            if( 100 * step > quantity * maxUse) {
                model = null;
                break;
            }
            starting+= step;
            
            //Comparer avec le heap 
            //et couper court si KO
            
            model.appendResult(  query.getResultList());
        }
        
        return model;
        
    }

    
    @Override
    public List<Tickets> findOpen() {
        CriteriaQuery<Tickets> query = getQuery();
        Root<Tickets> root = query.from(Tickets.class);
        Predicate predicate = getCriteriaBuilder().isNull(
                root.get(Tickets.Fields.RECEIPTS).get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.DATE_END));
        Order order = getCriteriaBuilder().desc(root.get(Tickets.Fields.TICKET_ID));
        return getResultList(query.select(root).where(predicate).orderBy(order));
    }
    
    //TODO : On reprend en Native ?
    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> findTicketsByClosedCash(String closedCashId) {
        /**
         * On va récupérer :
         * le nombre de tickets
         * le CA HT remises non déduites
         * le nombre de clients
         * le montant des remises HT
         */
//        CriteriaQuery<Object[]> query = getCriteriaBuilder().createQuery(Object[].class);
//        Root<Tickets> root = query.from(Tickets.class);
//        List<Selection<?>> selections = new ArrayList<>();
//        Predicate predicate = getCriteriaBuilder().equal(
//                root.get(Tickets.Fields.RECEIPTS).get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.MONEY), closedCashId);
//
//        Join<Tickets, TicketLines> join1 = root.join(Tickets.Fields.LINES);
//        selections.add(getCriteriaBuilder().countDistinct(root.get(Tickets.Fields.RECEIPTS).get(Receipts.Fields.ID)));
//        selections.add(getCriteriaBuilder().sum(
//                getCriteriaBuilder().prod(join1.<Double> get(TicketLines.Fields.UNIT_PRICE), join1.<Double> get(TicketLines.Fields.UNITS))));
//        selections.add(getCriteriaBuilder().sum(root.<Double> get(Tickets.Fields.CUSTOMER_COUNT)));
//        selections.add(getCriteriaBuilder().sum(
//                getCriteriaBuilder().prod(
//                        getCriteriaBuilder().prod(join1.<Double> get(TicketLines.Fields.UNIT_PRICE),
//                                join1.<Double> get(TicketLines.Fields.UNITS)),
//                        getCriteriaBuilder().diff(
//                                getCriteriaBuilder().sum(join1.<Double> get(TicketLines.Fields.DISCOUNT_RATE),
//                                        join1.get(TicketLines.Fields.TICKET).<Double> get(Tickets.Fields.DISCOUNT_RATE)),
//                                getCriteriaBuilder().prod(join1.<Double> get(TicketLines.Fields.DISCOUNT_RATE),
//                                        join1.get(TicketLines.Fields.TICKET).<Double> get(Tickets.Fields.DISCOUNT_RATE))))));
//        query.multiselect(selections).where(predicate);
//
//        TypedQuery<Object[]> tq = entityManager.createQuery(query);
        Query query = null;
        closedCashId = closedCashId == null ? "" : closedCashId ;
        String nativeSQL = "SELECT ticketcount , SUM(t2.Price) , cc.custcount ,  "
                + " SUM(((t2.UNITPRICE * t2.UNITS) * ((t2.DISCOUNTRATE + t1.DISCOUNTRATE) - (t2.DISCOUNTRATE * t1.DISCOUNTRATE)))) "
                + " FROM TICKETLINES t2, TICKETS t1, RECEIPTS t0 , closedcash cc "
                + " WHERE (t0.money = cc.money ) AND (t0.ID = t1.ID) AND (t2.ticket = t1.ID)"
                + " AND (cc.money = '"+closedCashId + "')";
        
        query = entityManager.createNativeQuery(nativeSQL);
        return query.getResultList();
    }

}
