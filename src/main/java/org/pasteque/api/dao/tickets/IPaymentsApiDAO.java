package org.pasteque.api.dao.tickets;

import org.pasteque.api.model.tickets.Payments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPaymentsApiDAO extends JpaRepository<Payments, String> , IPaymentsApiCustomDAO {

}
