package org.pasteque.api.dao.tickets;

import org.pasteque.api.model.tickets.TicketLinesDeleted;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITicketLinesDeletedApiDAO extends JpaRepository<TicketLinesDeleted, String>{
    
}
