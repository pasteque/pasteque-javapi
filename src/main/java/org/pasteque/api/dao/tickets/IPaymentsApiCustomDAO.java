package org.pasteque.api.dao.tickets;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.tickets.Payments;

public interface IPaymentsApiCustomDAO extends IGenericDAO<Payments, String>{

    List<Object[]> getPaymentsByClosedCash(String closedCashId,
            String movementTypeConstraint);

}
