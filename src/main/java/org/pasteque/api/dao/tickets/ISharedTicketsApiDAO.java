package org.pasteque.api.dao.tickets;

import org.pasteque.api.model.tickets.SharedTickets;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ISharedTicketsApiDAO extends JpaRepository<SharedTickets, String> , JpaSpecificationExecutor<SharedTickets>{


}
