package org.pasteque.api.dao.tickets.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.dao.tickets.IPaymentsApiCustomDAO;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.cash.Currencies;
import org.pasteque.api.model.tickets.Payments;
import org.pasteque.api.model.tickets.Receipts;

public class IPaymentsApiCustomDAOImpl extends GenericDAOImpl<Payments, String> implements IPaymentsApiCustomDAO {

    //TODO on reprend en Native ?
    @Override
    public List<Object[]> getPaymentsByClosedCash(String closedCashId , String movementTypeConstraint) {
        CriteriaQuery<Object[]> query = getCriteriaBuilder().createQuery(Object[].class);
        Root<Receipts> root = query.from(Receipts.class);
        List<Selection<?>> selections = new ArrayList<>();
        List<Expression<?>> groupBy = new ArrayList<>();
        List<Predicate> predicates = new ArrayList<>();
        

        Join<Receipts, Payments> join1 = root.join(Receipts.Fields.PAYMENTS);

        predicates.add(getCriteriaBuilder().equal(root.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.MONEY), closedCashId));
        predicates.add(getCriteriaBuilder().notEqual(join1.<Double> get(Payments.Fields.TOTAL), 0D));
        
        if(movementTypeConstraint != null) {
            ArrayList<String> plist= Payments.getCategPayments(movementTypeConstraint);
            List<Predicate> orpredicates = new ArrayList<>();
            for(String payment : plist){
                orpredicates.add(getCriteriaBuilder().equal(join1.get(Payments.Fields.PAYMENT),payment));
            }
            predicates.add(getCriteriaBuilder().or(orpredicates.toArray(new Predicate[] {})));
        }
        
        selections.add(join1.get(Payments.Fields.PAYMENT));
        selections.add(join1.get(Payments.Fields.CURRENCY).get(Currencies.Fields.ID));
        selections.add(getCriteriaBuilder().sum(join1.<Double> get(Payments.Fields.TOTAL)));
        selections.add(getCriteriaBuilder().count(join1.get(Payments.Fields.ID)));
        selections.add(getCriteriaBuilder().sum(join1.<Double> get(Payments.Fields.TOTAL_CURRENCY)));

        groupBy.add(join1.get(Payments.Fields.PAYMENT));
        groupBy.add(join1.get(Payments.Fields.CURRENCY));

        query.multiselect(selections).where(predicates.toArray(new Predicate[] {})).groupBy(groupBy);

        TypedQuery<Object[]> tq = entityManager.createQuery(query);
        return tq.getResultList();
    }
    
}
