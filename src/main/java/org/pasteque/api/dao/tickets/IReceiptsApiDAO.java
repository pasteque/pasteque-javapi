package org.pasteque.api.dao.tickets;

import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.tickets.Receipts;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IReceiptsApiDAO extends JpaRepository<Receipts, String> {

    Receipts findTopByClosedCashOrderByDateNewDesc(ClosedCash closedCash);
}
