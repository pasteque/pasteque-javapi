package org.pasteque.api.dao.tickets;

import java.util.List;
import java.util.Optional;

import org.pasteque.api.model.tickets.Tickets;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ITicketsApiDAO extends JpaRepository<Tickets, String> , JpaSpecificationExecutor<Tickets> , ITicketsApiCustomDAO{

    List<Tickets> findByTicketId(Long ticketId);
    Optional<Tickets> findFirstByTicketId(Long ticketId);

}
