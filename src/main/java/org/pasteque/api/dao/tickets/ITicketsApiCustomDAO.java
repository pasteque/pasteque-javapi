package org.pasteque.api.dao.tickets;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.model.tickets.Tickets;

public interface ITicketsApiCustomDAO extends IGenericDAO<Tickets, String>{

    List<Tickets> findOpen();

    List<Tickets> findByCriteria(String ticketId, String ticketType,
            String cashId, String dateStart, String dateStop, String customerId,
            String userId, String locationId, String saleLocationId,
            String tourId, String fromValue, String toValue,
            Boolean orderConstraint);

    <T extends ILoadable> DtoSQLParameters findByCriteriaDTO(String ticketId,
            String ticketType, String cashId, String dateStart, String dateStop,
            String customerId, String userId, String locationId,
            String locationCategoryName, String saleLocationId, String tourId,
            String fromValue, String toValue, Boolean orderConstraint,
            Class<T> selector);

    List<Object[]> findTicketsByClosedCash(String closedCashId);

}
