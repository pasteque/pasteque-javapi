package org.pasteque.api.dao.locations.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.dao.locations.ISalesLocationsCustomDAO;
import org.pasteque.api.model.saleslocations.Insee;
import org.pasteque.api.model.saleslocations.SalesLocations;


public class ISalesLocationsCustomDAOImpl
        extends GenericDAOImpl<SalesLocations, Long>
        implements ISalesLocationsCustomDAO {

    @Override
    public List<SalesLocations> findPastSalesLocation(String inseeNum, Date startDate) {
        CriteriaQuery<SalesLocations> query = getQuery();
        Root<SalesLocations> root = query.from(SalesLocations.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(getCriteriaBuilder().equal(root.get(SalesLocations.Fields.INSEE).get(Insee.Fields.INSEE), inseeNum));
        predicates.add(getCriteriaBuilder().lessThan(root.<Date> get(SalesLocations.Fields.START_DATE), startDate));
        Order order = getCriteriaBuilder().asc(root.get(SalesLocations.Fields.START_DATE));
        return getResultList(query.select(root).distinct(true).where(predicates.toArray(new Predicate[] {})).orderBy(order));
    }
    
    @Override
    public SalesLocations findNextSalesLocation(String inseeNum, Date startDate) {
        CriteriaQuery<SalesLocations> query = getQuery();
        Root<SalesLocations> root = query.from(SalesLocations.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(getCriteriaBuilder().equal(root.get(SalesLocations.Fields.INSEE).get(Insee.Fields.INSEE), inseeNum));
        predicates.add(getCriteriaBuilder().greaterThan(root.<Date> get(SalesLocations.Fields.START_DATE), startDate));
        Order order = getCriteriaBuilder().asc(root.get(SalesLocations.Fields.START_DATE));
        List<SalesLocations> l = getResultList(query.select(root).distinct(true).where(predicates.toArray(new Predicate[] {})).orderBy(order));
        return l.isEmpty() ? null : l.get(0);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Map<String, String>> findLocationAffectedToManyTours() {
        String nativeSQL = "SELECT tl.location_id ,l.name as location_name, tl.tournees_id,t.nom tournee_name,tl.debut,tl.fin "
                            +"FROM   locations_tournees AS tl, locations_tournees AS tl2,tournees t, locations l "
                            +"WHERE  t.id = tl.tournees_id "
                            +" AND  l.id = tl.location_id "
                            + "AND tl.id <> tl2.id "
                            +"AND tl.debut <= tl2.fin "
                            +"AND tl.fin >= tl2.debut "
                            +"AND tl.fin > now() "
                            + "AND tl2.fin >=now() "
                            +" AND tl.tournees_id <> tl2.tournees_id "
                            +" AND tl.location_id = tl2.location_id "
                            +"GROUP BY tl.location_id,tl.TOURNEES_ID "
                            +" order by tl.location_id,tl.debut ";
        
        Query query = entityManager.createNativeQuery(nativeSQL);
        query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
        
        return query.getResultList();
    }

}
