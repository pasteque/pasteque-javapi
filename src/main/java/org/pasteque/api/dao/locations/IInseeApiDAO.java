package org.pasteque.api.dao.locations;

import java.util.Date;
import java.util.List;

import org.pasteque.api.model.saleslocations.Insee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IInseeApiDAO extends JpaRepository<Insee, String> , JpaSpecificationExecutor<Insee> {
    
    List<Insee> findByZipCodeAndInseeNum(String zipCode , String inseeNum);
    
    List<Insee> findByInseeNum(String inseeNum);
    
    List<Insee> findByZipCode(String zipCode);
    
    List<Insee> findByLastUpdateAfter(Date lastUpdate);

}
