package org.pasteque.api.dao.locations;

import org.pasteque.api.model.saleslocations.Tours;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IToursApiDAO extends JpaRepository<Tours, Long> , JpaSpecificationExecutor<Tours>{

}
