package org.pasteque.api.dao.locations;

import org.pasteque.api.model.saleslocations.SalesLocations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ISalesLocationsApiDAO extends JpaRepository<SalesLocations, Long> , JpaSpecificationExecutor<SalesLocations> , ISalesLocationsCustomDAO{

}
