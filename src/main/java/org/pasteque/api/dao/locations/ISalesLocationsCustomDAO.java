package org.pasteque.api.dao.locations;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.pasteque.api.model.saleslocations.SalesLocations;

public interface ISalesLocationsCustomDAO {
    List<SalesLocations> findPastSalesLocation(String inseeNum, Date startDate);
    List<Map<String, String>> findLocationAffectedToManyTours();
    
    SalesLocations findNextSalesLocation(String inseeNum, Date startDate);
}
