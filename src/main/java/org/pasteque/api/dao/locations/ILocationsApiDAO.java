package org.pasteque.api.dao.locations;

import java.util.List;

import org.pasteque.api.model.saleslocations.Locations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface ILocationsApiDAO extends JpaRepository<Locations, String> , JpaSpecificationExecutor<Locations>{
    
    Locations findTopByServerDefaultTrue() ;
    List<Locations> findByUrlNotNull() ;

}
