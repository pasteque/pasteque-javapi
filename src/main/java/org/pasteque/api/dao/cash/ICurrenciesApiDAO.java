package org.pasteque.api.dao.cash;

import org.pasteque.api.model.cash.Currencies;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICurrenciesApiDAO extends JpaRepository<Currencies, Long>{

}
