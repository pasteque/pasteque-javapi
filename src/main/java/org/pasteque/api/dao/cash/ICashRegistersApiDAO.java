package org.pasteque.api.dao.cash;

import java.util.List;

import org.pasteque.api.model.cash.CashRegisters;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ICashRegistersApiDAO extends JpaRepository<CashRegisters, String> , JpaSpecificationExecutor<CashRegisters> {

    List<CashRegisters> findByName(String name);
    
}
