package org.pasteque.api.dao.cash;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.cash.ClosedCash;

public interface IClosedCashApiCustomDAO extends IGenericDAO<ClosedCash , String>{

    List<ClosedCash> findByCriteria(String cashRegisterId, String dateStart,
            String dateStop);

}
