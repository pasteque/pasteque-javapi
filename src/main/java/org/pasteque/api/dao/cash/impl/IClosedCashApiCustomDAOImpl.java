package org.pasteque.api.dao.cash.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.pasteque.api.dao.cash.IClosedCashApiCustomDAO;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.cash.ClosedCash;

public class IClosedCashApiCustomDAOImpl extends GenericDAOImpl<ClosedCash,String> implements IClosedCashApiCustomDAO{

    @Override
    public List<ClosedCash> findByCriteria(String cashRegisterId, String dateStart, String dateStop) {
        CriteriaQuery<ClosedCash> query = getQuery();
        Root<ClosedCash> root = query.from(ClosedCash.class);
        List<Predicate> predicates = new ArrayList<>();
        if (cashRegisterId != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(ClosedCash.Fields.CASH_REGISTER).get(CashRegisters.Fields.ID),
                    cashRegisterId));
        }
        
        //ATTENTION DATE START et DATESTOP 
        // debut ... datestart ... datestop ... fin  -> session remonte
        // debut ... datestart ...fin debut datestop ... (pas de date fin) -> 2 sessions remontent
        // debut ... datestart ... datestop ... 
        if (dateStart != null) {
            Date dStart = new Date(Long.parseLong(dateStart));
            predicates.add(getCriteriaBuilder().or(getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(ClosedCash.Fields.DATE_END), dStart) , getCriteriaBuilder().isNull(root.<Date> get(ClosedCash.Fields.DATE_END))));
        }
        if (dateStop != null) {
            Date dStop = new Date(Long.parseLong(dateStop));
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(ClosedCash.Fields.DATE_START), dStop));
        }
        Order order = getCriteriaBuilder().desc(root.get(ClosedCash.Fields.DATE_START));
        return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})).orderBy(order));
    }
}
