package org.pasteque.api.dao.cash;

import java.util.Date;
import java.util.List;

import org.pasteque.api.model.products.TaxCategories;
import org.pasteque.api.model.products.TaxCustCategories;
import org.pasteque.api.model.products.Taxes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITaxesApiDAO extends JpaRepository<Taxes, String> , ITaxesApiCustomDAO {

    List<Taxes> findByCategoryAndCustCategoryAndValidFromBeforeAndValidToIsNull(TaxCategories category , TaxCustCategories custCategory , Date date);
    List<Taxes> findByCategoryAndCustCategoryAndValidFromBeforeAndValidToAfter(TaxCategories category , TaxCustCategories custCategory , Date date , Date dateFin);
}
