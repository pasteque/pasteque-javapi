package org.pasteque.api.dao.cash.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.pasteque.api.dao.cash.ITaxesApiCustomDAO;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.products.Taxes;
import org.pasteque.api.model.tickets.Receipts;
import org.pasteque.api.model.tickets.TaxLines;

public class ITaxesApiCustomDAOImpl extends GenericDAOImpl<Taxes,String> implements ITaxesApiCustomDAO{

    @Override
    public List<Object[]> getTaxesByClosedCash(String closedCashId) {
        CriteriaQuery<Object[]> query = getCriteriaBuilder().createQuery(Object[].class);
        Root<Receipts> root = query.from(Receipts.class);
        List<Selection<?>> selections = new ArrayList<>();
        List<Expression<?>> groupBy = new ArrayList<>();
        Predicate predicate = getCriteriaBuilder().equal(root.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.MONEY), closedCashId);

        Join<Receipts, TaxLines> join1 = root.join(Receipts.Fields.TAXES);
        Join<TaxLines, Taxes> join2 = join1.join(TaxLines.Fields.TAXES);

        selections.add(join2.get(Taxes.Fields.ID));
        selections.add(getCriteriaBuilder().sum(join1.<Double> get(TaxLines.Fields.BASE)));
        selections.add(getCriteriaBuilder().sum(join1.<Double> get(TaxLines.Fields.AMOUNT)));

        groupBy.add(join2.get(Taxes.Fields.ID));

        query.multiselect(selections).where(predicate).groupBy(groupBy);

        TypedQuery<Object[]> tq = entityManager.createQuery(query);
        return tq.getResultList();
    }
}
