package org.pasteque.api.dao.cash;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.products.Taxes;

public interface ITaxesApiCustomDAO  extends IGenericDAO<Taxes,String>{

    List<Object[]> getTaxesByClosedCash(String closedCashId);

}
