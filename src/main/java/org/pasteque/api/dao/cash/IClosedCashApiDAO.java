package org.pasteque.api.dao.cash;

import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.cash.ClosedCash;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IClosedCashApiDAO extends JpaRepository<ClosedCash, String> , JpaSpecificationExecutor<ClosedCash> , IClosedCashApiCustomDAO {

    ClosedCash findFirstByCashRegisterOrderByHostSequenceDesc(CashRegisters cashRegister);
}
