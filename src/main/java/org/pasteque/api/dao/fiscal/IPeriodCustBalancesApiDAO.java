package org.pasteque.api.dao.fiscal;

import org.pasteque.api.model.fiscal.PeriodCustBalances;
import org.pasteque.api.model.fiscal.PeriodCustBalancesId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPeriodCustBalancesApiDAO extends JpaRepository<PeriodCustBalances, PeriodCustBalancesId> {

}
