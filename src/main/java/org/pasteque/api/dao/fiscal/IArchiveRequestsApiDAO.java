package org.pasteque.api.dao.fiscal;

import org.pasteque.api.model.archives.ArchiveRequests;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IArchiveRequestsApiDAO extends JpaRepository<ArchiveRequests, Integer> , JpaSpecificationExecutor<ArchiveRequests> {
    ArchiveRequests findTopByProcessingTrue();
    ArchiveRequests findTopByProcessingFalseOrderByIdAsc();
}
