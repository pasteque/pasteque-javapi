package org.pasteque.api.dao.fiscal;

import java.util.Date;
import java.util.List;

import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.fiscal.Periods;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IPeriodsApiDAO extends JpaRepository<Periods, String> , JpaSpecificationExecutor<Periods>{

    List<Periods> findAllByTypeAndCashRegisterAndClosedFalseOrderBySequenceDesc(String type,
            CashRegisters caisse);

    Periods findTopByTypeAndCashRegisterOrderBySequenceDesc(String type, CashRegisters caisse);
    
    Periods findTopByTypeAndDateStartBeforeAndDateEndAfterOrderBySequenceDesc(String type, Date start , Date end);

}
