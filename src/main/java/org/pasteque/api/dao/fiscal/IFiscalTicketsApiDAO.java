package org.pasteque.api.dao.fiscal;

import org.pasteque.api.model.fiscal.FiscalTickets;
import org.pasteque.api.model.fiscal.FiscalTicketsId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IFiscalTicketsApiDAO extends JpaRepository<FiscalTickets, FiscalTicketsId> , JpaSpecificationExecutor<FiscalTickets> {
    FiscalTickets findTopByTypeAndSequenceOrderByNumberDesc(String type , String sequence);
}
