package org.pasteque.api.dao.fiscal;

import org.pasteque.api.model.archives.Archives;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IArchivesApiDAO extends JpaRepository<Archives, Integer> , JpaSpecificationExecutor<Archives> {
    Archives findTopByOrderByNumberDesc();
}
