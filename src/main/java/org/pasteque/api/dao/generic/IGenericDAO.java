package org.pasteque.api.dao.generic;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IGenericDAO<T, ID extends Serializable> {

    public T create(T object);

    public T update(T object);
    
    public void invalidateCache();
    
    public void updateBatch(List<T> objectList);

    void update(List<T> objectList);

    void create(List<T> objectList);

    T findByPrimaryKey(ID id);

}
