package org.pasteque.api.dao.generic;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.transaction.annotation.Transactional;


public class GenericDAOImpl<T, ID extends Serializable> implements IGenericDAO<T, ID> {

    @PersistenceContext
    protected EntityManager entityManager;
    protected Class<T> entityClass;
    
    @SuppressWarnings("unchecked")
    public GenericDAOImpl() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }

    @Override
    public T findByPrimaryKey(ID id) {
        return this.entityManager.find(entityClass, id);
    }
    
    @Override
    @Transactional
    public T create(T object) {
        this.entityManager.persist(object);
        this.entityManager.flush();
        this.entityManager.refresh(object);
        return object;
    }

    @Override
    @Transactional
    public T update(T object) {
        object = entityManager.merge(object);
        this.entityManager.flush();
        this.entityManager.refresh(object);
        return object;
    }
    
    @Override
    @Transactional
    public void update(List<T> objectList) {
        for(T object : objectList) {
            object = entityManager.merge(object);
            
        }
        this.entityManager.flush();
    }
    
    @Override
    @Transactional
    public void create(List<T> objectList) {
        for(T object : objectList) {
            entityManager.persist(object);
        }
        this.entityManager.flush();
    }

//TODO Arbitrer entre les versions avec et sans clear à la fin
// Il y aura aussi une version avec des flush / clear intermédiaires
// Là on pousse tout en mémoire, disons 10 000 entrées si le batch size est de 1000
// on aura 10 requêtes à la fin puis un clear
// peut-etre avoir directement requête et clear à chaque 1000° passage ?    
// insertBatch servira peut être aussi.
    
    @Override
    @Transactional
    public void updateBatch(List<T> objectList) {
        for (T object : objectList) {
            object = entityManager.merge(object);
            
        }
        entityManager.flush();
        entityManager.clear();
    }    


    protected CriteriaBuilder getCriteriaBuilder() {
        return entityManager.getCriteriaBuilder();
    }

    @SuppressWarnings("unchecked")
    protected CriteriaQuery<T> getQuery() {
        return (CriteriaQuery<T>) entityManager.getCriteriaBuilder().createQuery(super.getClass());
    }

    protected List<T> getResultList(CriteriaQuery<T> query) {
        return entityManager.createQuery(query).getResultList();
    }

    protected List<T> getResultList(CriteriaQuery<T> query, Integer limit) {
        if(limit!=null) {
            return entityManager.createQuery(query).setMaxResults(limit.intValue()).getResultList();
        }
        return getResultList(query);
    }
    
    
    //EDU - PGA 
    @Override
    public void invalidateCache() {
        entityManager.getEntityManagerFactory().getCache().evictAll();
    }
    
}
