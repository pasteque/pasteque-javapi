package org.pasteque.api.dao.security;

import org.pasteque.api.model.security.People;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;



public interface IPeopleApiDAO extends JpaRepository<People, String> ,JpaSpecificationExecutor<People>{

    People findByUsername(String login);
}
