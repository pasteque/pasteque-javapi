package org.pasteque.api.dao.security;

import org.pasteque.api.model.security.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface IRolesApiDAO extends JpaRepository<Roles, String> , JpaSpecificationExecutor<Roles> {

    Roles findByName(String name);

}
