package org.pasteque.api.dao.application.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.pasteque.api.dao.application.IApplicationApiCustomDAO;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.application.Applications;
import org.springframework.beans.factory.annotation.Value;

public class IApplicationApiCustomDAOImpl extends GenericDAOImpl<Applications, String> implements IApplicationApiCustomDAO {

    @PersistenceContext(type=PersistenceContextType.EXTENDED) 
    EntityManager em;
    
    @Value("${spring.data.fiscalyearend:0831}")
    private String fiscalYearEnd ;
    
    @Override
    public Applications findCurrent(String type) {
        CriteriaQuery<Applications> query = getQuery();
        Root<Applications> root = query.from(Applications.class);
        List<Predicate> predicates = new ArrayList<>();
        if(type != null) {
            predicates.add(em.getCriteriaBuilder().equal(root.get(Applications.Fields.ID), type));
        }
        List<Applications> l = getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})));
        return !l.isEmpty() ? l.get(0) : null;
    }
    
    @Override
    public String getFiscalYearEnd() {
        return fiscalYearEnd;
    }

}
