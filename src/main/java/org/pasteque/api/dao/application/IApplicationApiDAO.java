package org.pasteque.api.dao.application;

import org.pasteque.api.model.application.Applications;
import org.springframework.data.jpa.repository.JpaRepository;


public interface IApplicationApiDAO extends JpaRepository<Applications, String> , IApplicationApiCustomDAO {

}
