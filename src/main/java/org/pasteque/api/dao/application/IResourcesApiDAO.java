package org.pasteque.api.dao.application;

import org.pasteque.api.model.application.Resources;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IResourcesApiDAO extends JpaRepository<Resources, String>{

    Resources findByName(String name);
}
