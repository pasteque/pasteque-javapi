package org.pasteque.api.dao.application;

import org.pasteque.api.model.application.PosMessageType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPosMessageTypeApiDAO extends JpaRepository<PosMessageType, String> {

}
