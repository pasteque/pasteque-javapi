package org.pasteque.api.dao.application;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.application.Applications;

public interface IApplicationApiCustomDAO extends IGenericDAO<Applications, String>{

    Applications findCurrent(String type);

    String getFiscalYearEnd();

}
