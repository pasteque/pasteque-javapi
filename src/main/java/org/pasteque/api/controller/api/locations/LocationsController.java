package org.pasteque.api.controller.api.locations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.service.saleslocations.ILocationsService;

@RestController
@RequestMapping(value = LocationsController.BASE_URL)
public class LocationsController {

    public static final String BASE_URL = "/locations";

    @Autowired
    private ILocationsService locationService;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam(value = "id", required = false) String id) {
        return new ServerResponse(locationService.getById(id));
    }

    /**
     * Retourne l'objet location associé à ce serveur 
     * @return locationDTO ou null
     */
    @RequestMapping(value = "/getDefault", method = RequestMethod.GET)
    public ServerResponse getDefault() {
        return new ServerResponse(locationService.getDefault());
    }
    
    @RequestMapping(value = "/getUrl", method = RequestMethod.GET)
    public ServerResponse getUrl() {
        return new ServerResponse(locationService.getUrl());
    }

}
