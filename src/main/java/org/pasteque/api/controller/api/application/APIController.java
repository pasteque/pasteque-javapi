package org.pasteque.api.controller.api.application;

import java.io.IOException;

import org.pasteque.api.controller.api.cash.CashMovementsController;
import org.pasteque.api.controller.api.cash.CashRegistersController;
import org.pasteque.api.controller.api.cash.CashesController;
import org.pasteque.api.controller.api.cash.CurrenciesController;
import org.pasteque.api.controller.api.customers.CustomersController;
import org.pasteque.api.controller.api.customers.DiscountProfilesController;
import org.pasteque.api.controller.api.locations.InseeController;
import org.pasteque.api.controller.api.locations.LocationsController;
import org.pasteque.api.controller.api.locations.SalesLocationsController;
import org.pasteque.api.controller.api.products.CompositionsController;
import org.pasteque.api.controller.api.products.ProductsController;
import org.pasteque.api.controller.api.products.TariffAreasController;
import org.pasteque.api.controller.api.products.TaxCategoriesController;
import org.pasteque.api.controller.api.tickets.TicketLinesDeletedController;
import org.pasteque.api.controller.api.tickets.TicketsController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
@RequestMapping(value = APIController.BASE_URL)
public class APIController {

    public static final String BASE_URL = "/api";

    public final class API {
        public static final String VERSION_API = "VersionAPI";
        public static final String CASH_REGISTERS_API = "CashRegistersAPI";
        public static final String CASHES_API = "CashesAPI";
        public static final String LOCATIONS_API = "LocationsAPI";
        public static final String USERS_API = "UsersAPI";
        public static final String ROLES_API = "RolesAPI";
        public static final String RESOURCES_API = "ResourcesAPI";
        public static final String CATEGORIES_API = "CategoriesAPI";
        public static final String PRODUCTS_API = "ProductsAPI";
        public static final String TAXES_API = "TaxesAPI";
        public static final String CURRENCIES_API = "CurrenciesAPI";
        public static final String TARIFF_AREAS_API = "TariffAreasAPI";
        public static final String COMPOSITIONS_API = "CompositionsAPI";
        public static final String CUSTOMERS_API = "CustomersAPI";
        public static final String TICKETS_API = "TicketsAPI";
        public static final String CASH_MOVEMENTS_API = "CashMvtsAPI";
        public static final String SALES_LOCATIONS_API = "SalesLocationsAPI";
        public static final String INSEE_API = "InseeAPI";
        public static final String TICKETlINESDELETED_API = "TicketLinesDeletedAPI";
        public static final String DISCOUNT_PROFILES_API = "DiscountProfilesAPI";

    }

    @RequestMapping(method = RequestMethod.GET)
    public String get(@RequestParam(value = "login", required = false) String login,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "action", required = false) String action, @RequestParam(value = "p", required = false) String api)
                    throws IOException {
        return "forward:" + getControllerUrlToRedirect(api, action);
    }

    @RequestMapping(method = RequestMethod.POST)
    public String post(@RequestParam(value = "login", required = false) String login,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "action", required = false) String action, @RequestParam(value = "p", required = false) String api) {
        return "forward:" + getControllerUrlToRedirect(api, action);
    }

    private String getControllerUrlToRedirect(String api, String action) {
        String url = "";
        if (api != null) {
            switch (api) {
            case APIController.API.VERSION_API:
                url += VersionController.BASE_URL;
                break;
            case APIController.API.CASH_REGISTERS_API:
                url += CashRegistersController.BASE_URL;
                break;
            case APIController.API.CASHES_API:
                url += CashesController.BASE_URL;
                break;
            case APIController.API.LOCATIONS_API:
                url += LocationsController.BASE_URL;
                break;
            case APIController.API.USERS_API:
                url += UsersController.BASE_URL;
                break;
            case APIController.API.ROLES_API:
                url += RolesController.BASE_URL;
                break;
            case APIController.API.RESOURCES_API:
                url += ResourcesController.BASE_URL;
                break;
            case APIController.API.CATEGORIES_API:
                url += CategoriesController.BASE_URL;
                break;
            case APIController.API.PRODUCTS_API:
                url += ProductsController.BASE_URL;
                break;
            case APIController.API.TAXES_API:
                url += TaxCategoriesController.BASE_URL;
                break;
            case APIController.API.CURRENCIES_API:
                url += CurrenciesController.BASE_URL;
                break;
            case APIController.API.TARIFF_AREAS_API:
                url += TariffAreasController.BASE_URL;
                break;    
            case APIController.API.COMPOSITIONS_API:
                url += CompositionsController.BASE_URL;
                break;
            case APIController.API.CUSTOMERS_API:
                url += CustomersController.BASE_URL;
                break;
            case APIController.API.TICKETS_API:
                url += TicketsController.BASE_URL;
                break;
            case APIController.API.CASH_MOVEMENTS_API:
                url += CashMovementsController.BASE_URL;
                break;
            case APIController.API.SALES_LOCATIONS_API:
                url += SalesLocationsController.BASE_URL;
                break;
            case APIController.API.INSEE_API:
                url += InseeController.BASE_URL;
                break;    
            case APIController.API.TICKETlINESDELETED_API:
                url += TicketLinesDeletedController.BASE_URL;
                break;
            case APIController.API.DISCOUNT_PROFILES_API:
                url += DiscountProfilesController.BASE_URL;
                break;
            default:
                break;
            }
        } else {
            url += "/login";
        }
        if (action != null && !action.isEmpty()) {
            url += ("/" + action);
        }

        return url;
    }

}
