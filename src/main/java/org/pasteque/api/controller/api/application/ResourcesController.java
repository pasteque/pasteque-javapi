package org.pasteque.api.controller.api.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.pasteque.api.dto.application.POSResourcesDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.exception.NotImplementedYetException;
import org.pasteque.api.service.application.IResourcesService;

@RestController
@RequestMapping(value = ResourcesController.BASE_URL)
public class ResourcesController {

    public static final String BASE_URL = "/resources";

    @Autowired
    private IResourcesService resourcesService;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam(value = "label", required = true) String label) {
        return new ServerResponse(resourcesService.getByLabel(label));
    }
    
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll() {
        return new ServerResponse(resourcesService.getAll());
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ServerResponse create() {
        throw new NotImplementedYetException();
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ServerResponse update() {
        throw new NotImplementedYetException();
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ServerResponse save(@RequestBody POSResourcesDTO dto) {
        return new ServerResponse(resourcesService.save(dto));
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ServerResponse deleteUser(@RequestBody POSResourcesDTO dto) {
        return new ServerResponse(resourcesService.delete(dto));
    }

}
