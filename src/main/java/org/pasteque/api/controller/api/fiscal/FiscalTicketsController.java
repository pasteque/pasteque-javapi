package org.pasteque.api.controller.api.fiscal;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.pasteque.api.dto.fiscal.FiscalTicketsDTO;
import org.pasteque.api.dto.fiscal.FiscalTicketsImportResultDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.model.fiscal.FiscalTickets;
import org.pasteque.api.model.fiscal.Periods;
import org.pasteque.api.service.fiscal.IFiscalTicketsService;
import org.pasteque.api.service.fiscal.IPeriodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class FiscalTicketsController {
    
    @Autowired
    IFiscalTicketsService fiscalTicketsService;
    @Autowired
    IPeriodsService periodsService;
    @Autowired
    private HttpServletRequest request;
    
    @RequestMapping(value = "/api/fiscal/export", method = RequestMethod.GET)
    public Object export(@RequestParam(value = "dateStart", required = true) Long dateStart, 
            @RequestParam(value = "dateStop", required = true) Long dateStop) {
        
        List<FiscalTickets> retour =  fiscalTicketsService.fiscalExport(new Date(dateStart), new Date(dateStop));
        
        return AdaptableHelper.getAdapter(retour, FiscalTicketsDTO.class);
    }
    
    @RequestMapping(value = "/api/fiscal/import", method = RequestMethod.POST)
    public FiscalTicketsImportResultDTO importFiscal(@RequestBody List<FiscalTicketsDTO> tickets) {
        FiscalTicketsImportResultDTO retour = new FiscalTicketsImportResultDTO();
        
        for(FiscalTicketsDTO ticket : tickets) {
            // pour chaque ticket on construit le Ticket fiscal correspondant si il n'existe pas
            FiscalTickets fTicket = fiscalTicketsService.getById(ticket.getSequence(), ticket.getType(), ticket.getNumber());
            Boolean importOk = false;
            if(fTicket == null || FiscalTickets.EOS.equals(ticket.getNumber())) {
                fTicket = new FiscalTickets();
                fTicket.setContent(ticket.getContent());
                fTicket.setNumber(ticket.getNumber());
                fTicket.setSequence(ticket.getSequence());
                fTicket.setSignature(ticket.getSignature());
                fTicket.setType(ticket.getType());
                fTicket.setDate(ticket.getDate());
                
                try {
                    importOk = fiscalTicketsService.fiscalImport(fTicket);
                } catch (APIException e) {
                    e.printStackTrace();
                } finally {
                    if(importOk) {
                        retour.addImport(ticket);
                    } else {
                        retour.addEchec(ticket);
                    }
                }
            // On essaie de la sauvegarder
            // si ok on ajoute à sauvé
            // si ko on ajoute à echecs
            } else {
            // si il existe on incrémente le nombre de tickets non ajoutés car déja existants 
                retour.addPresent(ticket);
            }
        }
        
        return retour;
    }
    
    @RequestMapping(value = "/fiscal/import", method = RequestMethod.POST)
    public FiscalTicketsImportResultDTO importZip(@RequestParam("file") MultipartFile file) 
            throws IllegalStateException, IOException {
        List<FiscalTicketsDTO> tickets = new ArrayList<FiscalTicketsDTO>();
        //On sauve le fichier reçu en local
        if (!file.isEmpty()) {
            String uploadsDir = "/uploads/";
            String realPathtoUploads =  request.getServletContext().getRealPath(uploadsDir);
            if(! new File(realPathtoUploads).exists())
            {
                new File(realPathtoUploads).mkdir();
            }

            String orgName = file.getOriginalFilename().replaceAll("[^\\p{ASCII}]", "");;
            String filePath = realPathtoUploads + "/" + orgName;
            File dest = new File(filePath);
            file.transferTo(dest);

//            if(filePath.startsWith("/")) {
//                filePath = "file://".concat(filePath);
//            } else {
//                filePath = "file:///".concat(filePath);
//            }
            //store file in storage
            //On sauve puis on importe 
            System.out.println(String.format("receive %s saved here %s", orgName , filePath));
            //On ouvre le zip
            
            ZipInputStream zis = new ZipInputStream(new FileInputStream(filePath));
            ZipEntry zipEntry = zis.getNextEntry();
            byte[] buffer = new byte[1024];
            String content = null;
            while (zipEntry != null) {

                int len;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                while ((len = zis.read(buffer)) > 0) {
                    bos.write(buffer, 0, len);
                }
                content = new String(bos.toByteArray(), StandardCharsets.UTF_8);
                tickets.addAll(new ObjectMapper().readValue(content, new TypeReference<List<FiscalTicketsDTO>>(){}));
                zipEntry = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
        //On en extrait la liste des tickets
        return importFiscal(tickets);
        }
        return new FiscalTicketsImportResultDTO();
    }
    
    @RequestMapping(value = "/fiscal/export", method = RequestMethod.GET)
    public void exportZip(HttpServletResponse response ,
            @RequestParam(value = "from", required = false) Long dateStart, 
            @RequestParam(value = "to", required = false) Long dateStop,
            @RequestParam(value = "period", required = false) String period)  throws IOException {
        Date start = dateStart == null ? null : new Date(dateStart);
        Date stop = dateStop == null ? null : new Date(dateStop);
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyyMMdd_HHmm");
        
        if(period != null) {
            Periods p = periodsService.findByDescription(period);
            if(p != null && p.isClosed()) {
                start = p.getDateStart();
                stop = p.getDateEnd();
            }
        }       
        
        String filename = start != null ? start.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().format(fmt) : "";
        filename  = filename + "-" + ( stop != null ? stop.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().format(fmt) : "" );
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Content-Disposition", "attachment; filename=\"fiscalExport-"+filename+".zip\"");
        ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());
        
            fiscalTicketsService.createZipExport(zipOutputStream , start , stop);
            zipOutputStream.close();
        
    }
    
}
