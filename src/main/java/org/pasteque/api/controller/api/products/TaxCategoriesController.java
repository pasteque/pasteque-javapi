package org.pasteque.api.controller.api.products;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.service.products.ITaxCategoriesService;

@RestController
@RequestMapping(value = TaxCategoriesController.BASE_URL)
public class TaxCategoriesController {
    
    public static final String BASE_URL = "/tax-categories";
    
    @Autowired
    private ITaxCategoriesService taxCategoriesService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll() {
        return new ServerResponse(taxCategoriesService.getAll());
    }

    // @RequestMapping(method = RequestMethod.GET)
    // public Object get(Principal principal) {
    // return null;
    // }
    //
    // @RequestMapping(method = RequestMethod.GET)
    // public Object getByName(Principal principal) {
    // return null;
    // }
    //
    // @RequestMapping(method = RequestMethod.GET)
    // public Object updateCat(Principal principal) {
    // return null;
    // }
    //
    // @RequestMapping(method = RequestMethod.GET)
    // public Object createCat(Principal principal) {
    // return null;
    // }
    //
    // @RequestMapping(method = RequestMethod.GET)
    // public Object deleteCat(Principal principal) {
    // return null;
    // }
    //
    // @RequestMapping(method = RequestMethod.GET)
    // public Object getTax(Principal principal) {
    // return null;
    // }
    //
    // @RequestMapping(method = RequestMethod.GET)
    // public Object updateTax(Principal principal) {
    // return null;
    // }
    //
    // @RequestMapping(method = RequestMethod.GET)
    // public Object createTax(Principal principal) {
    // return null;
    // }
    //
    // @RequestMapping(method = RequestMethod.GET)
    // public Object deteTax(Principal principal) {
    // return null;
    // }

}
