package org.pasteque.api.controller.api.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.exception.NotImplementedYetException;
import org.pasteque.api.service.products.ICategoriesService;

@RestController
@RequestMapping(value = CategoriesController.BASE_URL)
public class CategoriesController {

    public static final String BASE_URL = "/categories";
    
    @Autowired
    private ICategoriesService categoriesService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll() {
        return new ServerResponse(categoriesService.getAll());
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse get() {
        throw new NotImplementedYetException();
    }
}
