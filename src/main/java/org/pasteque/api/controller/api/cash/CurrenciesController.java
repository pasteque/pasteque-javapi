package org.pasteque.api.controller.api.cash;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.service.cash.ICurrenciesService;

@RestController
@RequestMapping(value = CurrenciesController.BASE_URL)
public class CurrenciesController {

    public static final String BASE_URL = "/currencies";
    
    @Autowired
    private ICurrenciesService currenciesService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll() {
        return new ServerResponse(currenciesService.getAll());
    }

    // @RequestMapping(method = RequestMethod.GET)
    // public Object get(Principal principal) {
    // return null;
    // }
    //
    // @RequestMapping(method = RequestMethod.GET)
    // public Object getMain(Principal principal) {
    // return null;
    // }
    //
    // @RequestMapping(method = RequestMethod.GET)
    // public Object getDefault(Principal principal) {
    // return null;
    // }
    //
    // @RequestMapping(method = RequestMethod.GET)
    // public Object update(Principal principal) {
    // return null;
    // }

}
