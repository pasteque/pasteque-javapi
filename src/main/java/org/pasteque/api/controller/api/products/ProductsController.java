package org.pasteque.api.controller.api.products;

import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.service.products.IProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = ProductsController.BASE_URL)
public class ProductsController {

    public static final String BASE_URL = "/products";

    @Autowired
    private IProductsService productsService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll(@RequestParam(value = "date", required = false) String date) {
        if (date == null) {
            return new ServerResponse(productsService.getAll());
        } else {
            return new ServerResponse(productsService.getRecent(date));
        }
    }
}
