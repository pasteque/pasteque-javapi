package org.pasteque.api.controller.api.application;

import java.security.Principal;
import java.util.ArrayList;

import org.pasteque.api.dto.security.POSUsersDTO;
import org.pasteque.api.dto.security.UsersObfDTO;
import org.pasteque.api.dto.security.UsersPwdUpdDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.security.Permissions;
import org.pasteque.api.service.security.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = UsersController.BASE_URL)
public class UsersController {

    public static final String BASE_URL = "/users";

    @Autowired
    private IUsersService usersService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll(Principal principal) {
        if (principal != null) {
            Authentication authentication = (Authentication) principal;
            // La PERM_ADMIN_USER_LOG est une restriction de la PERM_ADMIN_USER, elle doit donc
            // systématiquement être traitée en premier
            if (authentication.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Administration.PERM_ADMIN_USER_LOG))) {
                return new ServerResponse(usersService.getAllLog());
            }
            if (authentication.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Administration.PERM_ADMIN_USER))) {
                return new ServerResponse(usersService.getAll());
            }
            return new ServerResponse(new ArrayList<POSUsersDTO>());
        } else {
            // TODO AME : le 02/12/2019 -> vérifier le pourquoi on renvoie tout.
            return new ServerResponse(usersService.getAll());
        }
    }

    @RequestMapping(value = "/getAllByPermission", method = RequestMethod.GET)
    public ServerResponse getAllByPermission(@RequestParam(value = "permission", required = true) String permission,@RequestParam(value = "activeOnly", required = false) Boolean activeOnly) {
        return new ServerResponse(usersService.getAllByPermission(permission));
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam(value = "id", required = true) String id) {
        return new ServerResponse(usersService.getById(id , UsersObfDTO.class));
    }

    @RequestMapping(value = "/updPwd", method = RequestMethod.POST)
    public ServerResponse updPwd(@RequestParam(value = "id", required = false) String id, @RequestParam(value = "oldPwd", required = false) String oldPwd,
            @RequestParam(value = "newPwd", required = false) String newPwd) {
        return usersService.updatePassword(id, oldPwd, newPwd);
    }

    @RequestMapping(value = "/updPwdBack", method = RequestMethod.POST)
    public ServerResponse updPwdBack(@RequestParam(value = "id", required = false) String id, @RequestParam(value = "oldPwd", required = false) String oldPwd,
            @RequestParam(value = "newPwd", required = false) String newPwd, @RequestBody UsersPwdUpdDTO dto) {
        // pouvoir jouer avec le status de la reponse
        if (dto != null) {
            id = dto.getId();
            oldPwd = dto.getOldPwd();
            newPwd = dto.getNewPwd();
        }
        return usersService.updatePassword(id, oldPwd, newPwd);
    }

    @RequestMapping(value = "/updPwdForced", method = RequestMethod.POST)
    public ServerResponse forceUpdPwd(@RequestParam(value = "id", required = true) String id, @RequestParam(value = "newPwd", required = true) String newPwd) {
        return new ServerResponse(usersService.forceUpdatePassword(id, newPwd));
    }

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public ServerResponse saveUser(@RequestBody POSUsersDTO dto) {
        return new ServerResponse(usersService.save(dto));
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    public ServerResponse deleteUser(@RequestBody POSUsersDTO dto) {
        return new ServerResponse(usersService.delete(dto));
    }

}
