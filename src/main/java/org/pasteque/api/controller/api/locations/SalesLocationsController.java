package org.pasteque.api.controller.api.locations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.pasteque.api.dto.locations.POSSalesLocationsDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.saleslocations.SalesLocations;
import org.pasteque.api.service.saleslocations.ISalesLocationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = SalesLocationsController.BASE_URL)
public class SalesLocationsController {
    public static final String BASE_URL = "/sales-locations";

    @Autowired
    private ISalesLocationsService salesLocationsService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll() {
        return new ServerResponse(salesLocationsService.getAll());

    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse getBy(@RequestParam(value = "tourId", required = false) String tourId, @RequestParam(value = "city", required = false) String city,
            @RequestParam(value = "insee", required = false) String insee) {

        MultiValueMap<String ,String> map = new  LinkedMultiValueMap<String ,String>();
        map.add(SalesLocations.Fields.TOUR, tourId);
        map.add(SalesLocations.Fields.INSEE, insee);
        map.add(SalesLocations.Fields.CITY, city);

        List<POSSalesLocationsDTO> salesList = salesLocationsService.getDTOByCriteria(map, null);

        return new ServerResponse(salesList);

    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ServerResponse create(@RequestParam(value = "salesLocation", required = false) String salesLocation,
            @RequestParam(value = "salesLocations", required = false) String salesLocations) throws JsonParseException, JsonMappingException, IOException {
        List<POSSalesLocationsDTO> l = new ArrayList<POSSalesLocationsDTO>();
        if (salesLocations != null) {
            l = new ObjectMapper().readValue(salesLocations, new TypeReference<List<POSSalesLocationsDTO>>() {
            });
        }
        if (salesLocation != null) {
            l.add(new ObjectMapper().readValue(salesLocation, POSSalesLocationsDTO.class));
        }
        return new ServerResponse(salesLocationsService.save(l));
    }

//    @RequestMapping(value = "/{salesLocationId}", method = RequestMethod.GET)
//    public ServerResponse get(@PathVariable(value = "salesLocationId") Long salesLocationId) {
//        return new ServerResponse(salesLocationsService.getById(salesLocationId, SalesLocationsDetailDTO.class));
//    }

    /**
     * paramètre reference : Match sur insee ou codepostal ou ville
     * paramètre from : depuis cette date = date de fin après
     * paramètre to : avant cette date = date de début avant
     * 
     * il y a tour : le numéro de la tournée
     * 
     * par contruction on peut ajouter n'importe quel élément d'un déballage en correspondance exacte
     * 
     * historiquement il y avait locationId mais non implémenté jusque là (2020 08)
     * 
     * @param params
     * @param limit
     * @return
     */
        @RequestMapping(value = "/getCriteria", method = RequestMethod.GET)
        public ServerResponse getByCriteria(@RequestParam MultiValueMap<String, String> params ,
                @RequestParam(value = "limit", required = false, defaultValue = "-1") int limit) {

            return new ServerResponse(salesLocationsService.getDTOByCriteria(params, limit));
        }

    @RequestMapping(value = "/saveSalesLocations", method = RequestMethod.POST)
    public ServerResponse create(@RequestBody POSSalesLocationsDTO dto) {
        return new ServerResponse(salesLocationsService.save(dto));
    }
}
