package org.pasteque.api.controller.api.locations;

import org.pasteque.api.dto.locations.InseeSimpleDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.service.saleslocations.IInseeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = InseeController.BASE_URL)
public class InseeController {
    public static final String BASE_URL = "/insee";
    
    @Autowired
    private IInseeService inseeService;
    
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll(@RequestParam(value = "date", required = false) String date ) {
        if(date == null) {
            return new ServerResponse(inseeService.getAll());
        } else {
            return new ServerResponse(inseeService.getRecent(date));
        }
    }
    
    @RequestMapping(value = "/{inseeId}", method = RequestMethod.GET)
    public ServerResponse get(@PathVariable(value = "inseeId") String inseeId) {
        return new ServerResponse(inseeService.getById(inseeId, InseeSimpleDTO.class));
    }
    
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ServerResponse updateAll() {
        Integer nbCommunes = inseeService.updateLocalData();
        return  new ServerResponse(nbCommunes);
    }
}
