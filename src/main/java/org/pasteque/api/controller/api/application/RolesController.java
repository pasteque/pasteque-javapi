package org.pasteque.api.controller.api.application;

import java.security.Principal;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.pasteque.api.dto.security.POSRolesDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.security.Permissions;
import org.pasteque.api.service.security.IRolesService;

@RestController
@RequestMapping(value = RolesController.BASE_URL)
public class RolesController {

    public static final String BASE_URL = "/roles";

    @Autowired
    private IRolesService rolesService;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam(value = "id", required = true) String id) {
        return new ServerResponse(rolesService.getById(id));
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll(Principal principal) {
        if (principal != null) {
            
            Authentication authentication = (Authentication) principal;
            // J'imagine que c'est pour avoir que les users de la logistique
            // dans le cas de la création d'un utilisateur
            if (authentication.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Administration.PERM_ADMIN_USER_LOG))) {
                return new ServerResponse(rolesService.getAllLog());
            }
            if (authentication.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Administration.PERM_ADMIN_USER))) {
                return new ServerResponse(rolesService.getAll());
            }
            return new ServerResponse(new ArrayList<POSRolesDTO>());
        } else {
            return new ServerResponse(rolesService.getAll());
        }
    }

    @RequestMapping(value = "/saveRole", method = RequestMethod.POST)
    public ServerResponse saveUser(@RequestBody POSRolesDTO dto) {
        return new ServerResponse(rolesService.save(dto));
    }

    @RequestMapping(value = "/deleteRole", method = RequestMethod.POST)
    public ServerResponse deleteUser(@RequestBody POSRolesDTO dto) {
        return new ServerResponse(rolesService.delete(dto));
    }
}
