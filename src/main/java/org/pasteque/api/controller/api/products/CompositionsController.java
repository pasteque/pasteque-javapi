package org.pasteque.api.controller.api.products;

import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.service.products.ICompositionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = CompositionsController.BASE_URL)
public class CompositionsController {
    
    public static final String BASE_URL = "/compositions";

    @Autowired
    private ICompositionsService compositionsService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll() {
        return new ServerResponse(compositionsService.getAll());
    }

    // @RequestMapping(method = RequestMethod.GET)
    // public Object get(Principal principal) {
    // return null;
    // }
    // @RequestMapping(method = RequestMethod.GET)
    // public Object delete(Principal principal) {
    // return null;
    // }
    // @RequestMapping(method = RequestMethod.GET)
    // public Object update(Principal principal) {
    // return null;
    // }

}
