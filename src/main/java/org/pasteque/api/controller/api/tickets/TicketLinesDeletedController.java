package org.pasteque.api.controller.api.tickets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.pasteque.api.dto.tickets.POSTicketLinesDeletedDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.service.tickets.ITicketLinesDeletedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = TicketLinesDeletedController.BASE_URL)
public class TicketLinesDeletedController {
    
    public static final String BASE_URL = "/ticketLinesDeleted";
    @Autowired
    private ITicketLinesDeletedService ticketLinesDeletedService;
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ServerResponse save(@RequestParam(value = "ticketLinesDeleted", required = false) String ticketLinesDeleted,
                               @RequestParam(value = "ticketLineDeleted", required = false) String ticketLineDeleted,
    		                   @RequestParam(value = "ticketId", required = false) String ticketId ,
    		                   @RequestParam(value = "isOpenedPayment", required = false) String isOpenedPayment) 
    		                   throws JsonParseException, JsonMappingException, IOException,APIException {
         
        List<POSTicketLinesDeletedDTO> l = new ArrayList<POSTicketLinesDeletedDTO>();
         if (ticketLinesDeleted != null) {
             l = new ObjectMapper().readValue(ticketLinesDeleted, new TypeReference<List<POSTicketLinesDeletedDTO>>() {
             });
         }
         if (ticketLineDeleted != null) {
             l.add(new ObjectMapper().readValue(ticketLineDeleted, POSTicketLinesDeletedDTO.class));
         }
         return new ServerResponse(ticketLinesDeletedService.save( l,ticketId,isOpenedPayment));
    }

}
