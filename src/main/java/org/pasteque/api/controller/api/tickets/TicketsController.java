package org.pasteque.api.controller.api.tickets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pasteque.api.dto.tickets.POSSharedTicketsDTO;
import org.pasteque.api.dto.tickets.POSTicketsDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.response.ICsvResponse;
import org.pasteque.api.service.tickets.ITicketsService;
import org.pasteque.api.util.ComplexReturnType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = TicketsController.BASE_URL)
public class TicketsController {

    public static final String BASE_URL = "/tickets";

    //TODO - Voir si ces éléments méritent d'être définis dans l'api de base

    @Autowired
    private ITicketsService ticketsService;

    @RequestMapping(value = "/getShared", method = RequestMethod.GET)
    public ServerResponse getShared(@RequestParam(value = "id", required = false) String id) {
        return new ServerResponse(ticketsService.getSharedTicketsById(id));
    }

    @RequestMapping(value = "/getAllShared", method = RequestMethod.GET)
    public ServerResponse getAllShared() {
        return new ServerResponse(ticketsService.getAllSharedTickets());
    }

    @RequestMapping(value = "/delShared", method = RequestMethod.POST)
    public ServerResponse delShared(@RequestParam(value = "id", required = false) String id) {
        boolean delete = ticketsService.deleteSharedTicketsById(id);
        return new ServerResponse(delete);
    }

    @RequestMapping(value = "/share", method = RequestMethod.POST)
    public ServerResponse share(@RequestParam(value = "ticket", required = false) String ticket) throws JsonParseException, JsonMappingException, IOException {
        boolean share = ticketsService.share(new ObjectMapper().readValue(ticket , POSSharedTicketsDTO.class));
        return new ServerResponse(share);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ServerResponse save(@RequestParam(value = "ticket", required = false) String ticket,
            @RequestParam(value = "tickets", required = false) String tickets, @RequestParam(value = "cashId", required = false) String cashId)
                    throws JsonParseException, JsonMappingException, IOException, APIException {

        List<POSTicketsDTO> l = new ArrayList<POSTicketsDTO>();
        if (tickets != null) {
            l = new ObjectMapper().readValue(tickets, new TypeReference<List<POSTicketsDTO>>() {
            });
        }
        if (ticket != null) {
            l.add(new ObjectMapper().readValue(ticket, POSTicketsDTO.class));
        }

        return new ServerResponse(ticketsService.save(cashId, l));
    }

    @RequestMapping(value = "/getOpen", method = RequestMethod.GET)
    public ServerResponse getOpen() {
        return new ServerResponse(ticketsService.getOpen());
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public Object search(@RequestParam(value = "ticketId", required = false) String ticketId,
            @RequestParam(value = "ticketType", required = false) String ticketType, @RequestParam(value = "cashId", required = false) String cashId,
            @RequestParam(value = "dateStart", required = false) String dateStart, @RequestParam(value = "dateStop", required = false) String dateStop,
            @RequestParam(value = "customerId", required = false) String customerId, @RequestParam(value = "userId", required = false) String userId,
            @RequestParam(value = "resultType", required = false) String resultType, @RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "saleLocationId", required = false) String saleLocationId,
            @RequestParam(value = "locationCategoryName", required = false) String locationCategoryName,
            @RequestParam(value = "tourId", required = false) String tourId, @RequestParam(value = "fromValue", required = false) String fromValue,
            @RequestParam(value = "toValue", required = false) String toValue,
            @RequestParam(value = "orderConstraint", required = false) Boolean orderConstraint,
            @RequestParam(value = "_format", required = false) String _format) {
        
        ComplexReturnType returnInfos = ticketsService.getTicketReturnInfos(resultType);

        List<ICsvResponse> returnListDTO = ticketsService.search(ticketId, ticketType, cashId, dateStart, dateStop, customerId, userId, locationId,
                locationCategoryName, saleLocationId, tourId, fromValue, toValue, orderConstraint, returnInfos.getMainReturnType());
        Map<String, Object> model = new HashMap<>();
        model.put("filename", returnInfos.getMainSheet());


        return new ServerResponse(
                returnListDTO == null ? "Un problème est survenu - Le serveur est sans doute trop sollicité ou votre requête trop lourde" : returnListDTO,
                        returnListDTO != null);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam(value = "ticketId", required = false) String ticketId) {
        return new ServerResponse(ticketsService.getByTicketId(ticketId));
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ServerResponse delete(@RequestParam(value = "id", required = false) String id) {
        return new ServerResponse(null);
    }

}
