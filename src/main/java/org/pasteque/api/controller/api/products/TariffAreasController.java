package org.pasteque.api.controller.api.products;

import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.service.products.ITariffAreasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = TariffAreasController.BASE_URL)
public class TariffAreasController {
    
    public static final String BASE_URL = "/tariff-areas";
    
    @Autowired
    private ITariffAreasService tariffAreasService;
    
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll(@RequestParam(value = "indexFormat", required = false) Boolean indexFormat) {
        if(indexFormat == null) {
            indexFormat = false;
        }
        return new ServerResponse(tariffAreasService.getAll(indexFormat));
    }
//    
//    @RequestMapping(value = "/getCriteria", method = RequestMethod.GET)
//    public Object get(@RequestParam(value = "reference", required = false) String reference,
//            @RequestParam(value = "_format", required = false) String _format) {
//                if (_format != null && _format.equals("csv")) {
//                    if (reference == null) {
//                        reference = "%" ;
//                    }
//                    Map<String, Object> model = new HashMap<>();
//                    model.put("filename", "products");
//                    model.put("content", CsvHelper.getCSVContent( tariffAreasService.getByCriteria(reference)));
//                    return new ModelAndView( new CsvResponse(), model);
//                    
//                }
//        return new ServerResponse(tariffAreasService.getByCriteria(reference));
//    }
//    
//    @RequestMapping(value="/getCurrent", method = RequestMethod.GET)
//    public ServerResponse getCurrent(@RequestParam(value = "locationId", required = false) String locationId,
//            @RequestParam(value = "indexFormat", required = false) Boolean indexFormat) {
//        if(indexFormat) {
//            return new ServerResponse(tariffAreasService.getCurrentTariffAreas(locationId, TariffAreasIndexDTO.class));
//        } else {
//            return new ServerResponse(tariffAreasService.getCurrentTariffAreas(locationId, TariffAreasDTO.class));
//        }
//    }
//    
//    @RequestMapping(value="/getTariffAreas", method = RequestMethod.GET)
//    public ServerResponse getTariffAreas(@RequestParam(value = "tariffAreaId", required = false) String tariffAreaId ,
//            @RequestParam(value = "tariffAreaName", required = false) String tariffAreaName) {
//        TariffAreasDTO tariffArea = null;
//        if(tariffAreaId != null) {
//            tariffArea = tariffAreasService.getTariffAreaById(Integer.parseInt(tariffAreaId)) ;
//        }
//        if(tariffAreaName != null) {
//            if(tariffArea == null) {
//                tariffArea = tariffAreasService.getTariffAreaByName(tariffAreaName);
//            } else {
//                if(tariffArea.getLabel() != tariffAreaName) tariffArea = null;
//            }
//        }
//        return new ServerResponse(tariffArea);
//        
//    }
//    
//    @RequestMapping(value="/create", method = RequestMethod.POST)
//    public ServerResponse create(@RequestParam(value = "tariffArea", required = false) String tariffArea,
//            @RequestParam(value = "tariffAreas", required = false) String tariffAreas) throws JsonParseException, JsonMappingException, IOException{
//        List<TariffAreasDTO> l = new ArrayList<TariffAreasDTO>();
//        if (tariffAreas != null) {
//            l = new ObjectMapper().readValue(tariffAreas, new TypeReference<List<TariffAreasDTO>>() {
//            });
//        }
//        if (tariffArea != null) {
//            l.add(new ObjectMapper().readValue(tariffArea, TariffAreasDTO.class));
//        }
//        return new ServerResponse(tariffAreasService.save(l));
//    }
//    
//    @RequestMapping(value="/saveLink", method = RequestMethod.POST)
//    public ServerResponse saveLink(@RequestBody TariffAreasLocationsDTO TariffArea) {
//        return new ServerResponse(tariffAreasService.saveLink(TariffArea));
//    }
//    
//    @RequestMapping(value="/deleteLink", method = RequestMethod.POST)
//    public ServerResponse deleteLink(@RequestBody TariffAreasLocationsDTO TariffArea){
//        return new ServerResponse(tariffAreasService.deleteLink(TariffArea));
//    }
//    
//    @RequestMapping(value="/getCurrentLinks", method = RequestMethod.GET)
//    public ServerResponse getCurrentLinks(@RequestParam(value = "locationId", required = false) String locationId,
//            @RequestParam(value = "tariffAreaId", required = false) String tariffAreaId,
//            @RequestParam(value = "categoryId", required = false) String categoryId,
//            @RequestParam(value = "from", required = false) String from, 
//            @RequestParam(value = "to", required = false) String to) throws ParseException {
//            return new ServerResponse(tariffAreasService.getCurrentTariffAreasLinks(locationId, tariffAreaId , categoryId , from , to , TariffAreasLocationsDTO.class));
//    }
//
//    // @RequestMapping(method = RequestMethod.GET)
//    // public Object get(Principal principal) {
//    // return null;
//    // }
//    //
//    // @RequestMapping(method = RequestMethod.GET)
//    // public Object insertAreaPrices(Principal principal) {
//    // return null;
//    // }
//    //
//    // @RequestMapping(method = RequestMethod.GET)
//    // public Object create(Principal principal) {
//    // return null;
//    // }
//    //
//    // @RequestMapping(method = RequestMethod.GET)
//    // public Object update(Principal principal) {
//    // return null;
//    // }
//    //
//    // @RequestMapping(method = RequestMethod.GET)
//    // public Object delete(Principal principal) {
//    // return null;
//    // }

}
