package org.pasteque.api.controller.api.cash;

import java.io.IOException;

import org.pasteque.api.dto.tickets.POSPaymentsDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.service.cash.ICashMovementsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = CashMovementsController.BASE_URL)
public class CashMovementsController {

    public static final String BASE_URL = "/cash-movements";

    @Autowired
    private ICashMovementsService cashMovementsService;

    @RequestMapping(value = "/move", method = RequestMethod.POST)
    public ServerResponse move(@RequestParam(value = "cashId", required = false) String cashId,
            @RequestParam(value = "date", required = false) long date, @RequestParam(value = "note", required = false) String note,
            @RequestParam(value = "payment", required = false) String payment) throws JsonParseException, JsonMappingException,
            IOException, APIException {
        return new ServerResponse(cashMovementsService.move(cashId, date, note, new ObjectMapper().readValue(payment, POSPaymentsDTO.class)));
    }

}
