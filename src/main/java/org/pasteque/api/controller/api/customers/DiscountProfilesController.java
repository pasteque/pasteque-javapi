package org.pasteque.api.controller.api.customers;

import java.security.Principal;

import org.pasteque.api.dto.customers.POSDiscountProfilDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.exception.NotImplementedYetException;
import org.pasteque.api.service.customers.IDiscountProfilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = DiscountProfilesController.BASE_URL)
public class DiscountProfilesController {
	public static final String BASE_URL = "/discount-profiles";
	
    @RequestMapping(method = RequestMethod.GET)
    public Object get(Principal principal) {
        throw new NotImplementedYetException();
    }
    
    @Autowired
    private IDiscountProfilService discountProfilService;
    
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll() {
            return new ServerResponse(discountProfilService.getAll(POSDiscountProfilDTO.class));
    }
    
}
