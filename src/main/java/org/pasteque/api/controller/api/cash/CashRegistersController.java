package org.pasteque.api.controller.api.cash;

import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.service.cash.ICashRegistersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = CashRegistersController.BASE_URL)
public class CashRegistersController {
    
    public static final String BASE_URL = "/cash-registers";

    @Autowired
    private ICashRegistersService cashRegisterService;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam(value = "label", required = false) String label,
            @RequestParam(value = "location", required = false) String location) {
        return new ServerResponse(cashRegisterService.getBy(label,location));
    }
    
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam(value = "id", required = true) String id) {
        return new ServerResponse(cashRegisterService.get(id));
    }

    @RequestMapping(value = "/getByCriteria", method = RequestMethod.GET)
    public ServerResponse getByCriteria(@RequestParam(value = "label", required = false) String label,
            @RequestParam(value = "location", required = false) String location) {
        return new ServerResponse(cashRegisterService.getByCriteria(label,location));
    }
    
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll() {
        return new ServerResponse(cashRegisterService.getAll());
    }

}
