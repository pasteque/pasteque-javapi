package org.pasteque.api.controller.api.cash;

import java.io.IOException;

import org.pasteque.api.dto.cash.POSClosedCashDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.service.cash.ICashesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
@RestController
@RequestMapping(value = CashesController.BASE_URL)
public class CashesController {

    public static final String BASE_URL = "/cashes";

    @Autowired
    private ICashesService cashesService;

    @RequestMapping(value = "/getByCashRegister", method = RequestMethod.GET)
    public ServerResponse getByCashRegister(@RequestParam(value = "cashRegisterId", required = true) String cashRegisterId) {
        return new ServerResponse(cashesService.getClosedCashByCashRegisterId(cashRegisterId));
    }
    
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam(value = "id", required = true) String id) {
        return new ServerResponse(cashesService.getById(id , POSClosedCashDTO.class));
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ServerResponse update(@RequestParam(value = "cash", required = false) String cash) throws JsonParseException,
            JsonMappingException, IOException, APIException {
        POSClosedCashDTO pOSClosedCashDTO = new ObjectMapper().readValue(cash, POSClosedCashDTO.class);
        return new ServerResponse(cashesService.updateClosedCash(pOSClosedCashDTO));
    }

    @RequestMapping(value = "/zticket", method = RequestMethod.GET)
    public ServerResponse zTicket(@RequestParam(value = "id", required = false) String id) {
        return new ServerResponse(cashesService.getZTicket(id));
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ServerResponse search(@RequestParam(value = "cashRegisterId", required = false) String cashRegisterId,
            @RequestParam(value = "dateStart", required = false) String dateStart,
            @RequestParam(value = "dateStop", required = false) String dateStop) {
        return new ServerResponse(cashesService.search(cashRegisterId, dateStart, dateStop));
    }
}
