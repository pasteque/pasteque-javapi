package org.pasteque.api.controller.api.customers;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.pasteque.api.dto.customers.POSCustomersDTO;
import org.pasteque.api.dto.customers.CustomersDetailDTO;
import org.pasteque.api.dto.customers.CustomersLightDTO;
import org.pasteque.api.dto.customers.CustomersParentDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.security.Permissions;
import org.pasteque.api.response.CsvHelper;
import org.pasteque.api.response.CsvResponse;
import org.pasteque.api.response.XlsResponse;
import org.pasteque.api.service.customers.ICustomersService;
import org.pasteque.api.util.ComplexReturnType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping(value = CustomersController.BASE_URL)
public class CustomersController {

    public static final String BASE_URL = "/customers";

    private static final Map<String, ComplexReturnType> returnTypesRules;

    static {
        HashMap<String, ComplexReturnType> map = new HashMap<String, ComplexReturnType>();
        map.put("CustomersDetailDTO", new ComplexReturnType("CustomersDetailDTO", "CustomersDetailDTO", "Clients", CustomersDetailDTO.types, null, null, null));
        returnTypesRules = Collections.unmodifiableMap(map);
    }

    @Autowired
    private ICustomersService customersService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll(@RequestParam(value = "date", required = false) String date) {
        Date dateSearch = null ;
        try {
            dateSearch = new Date(Long.parseLong(date));
        } catch (Exception e) {
            dateSearch = null ;
        }
        if (dateSearch == null) {
            // TODO : Passer par GregorianCalendar pour manipuler les dates
            // PG lors du premier lancement de la caisse on charge uniquement
            // les clients
            // créés au cours des 30 derniers jours
            long oneMonthInMs = TimeUnit.DAYS.toMillis(30);
            dateSearch = new Date(new Date().getTime() - oneMonthInMs);
        }
        return new ServerResponse(customersService.getSinceDate(dateSearch));
    }

    /**
     * PG : Modification recherche client afin d'inclure les recherches en
     * provenance du logiciel de caisse ainsi que l'extension des champs de
     * recherches pour les recherches en provenance de la caisse.
     * 
     * le paramètre reference sert à faire une recherche approximative sur carte et code client
     */
    @RequestMapping(value = "/getCriteria", method = RequestMethod.GET)
    public Object getCriteria(@RequestParam(value = "reference", required = false) String reference,
            @RequestParam(value = "pdvId", required = false) String pdvId, @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to, @RequestParam(value = "insee", required = false) String insee,
            @RequestParam(value = "radius", required = false) Integer radius,
            @RequestParam(value = "emailConstraint", required = false) Boolean emailConstraint,
            @RequestParam(value = "parentConstraint", required = false) Boolean parentConstraint, @RequestParam(value = "card", required = false) String card,
            @RequestParam(value = "address", required = false) String address, @RequestParam(value = "address2", required = false) String address2,
            @RequestParam(value = "postal", required = false) String zipCode, @RequestParam(value = "city", required = false) String city,
            @RequestParam(value = "region", required = false) String region, @RequestParam(value = "country", required = false) String country,
            @RequestParam(value = "firstName", required = false) String firstName, @RequestParam(value = "lastName", required = false) String lastName,
            @RequestParam(value = "email", required = false) String email, @RequestParam(value = "phone", required = false) String phone,
            @RequestParam(value = "phone2", required = false) String phone2, @RequestParam(value = "notes", required = false) String notes,
            @RequestParam(value = "customersExcept", required = false) String customersExcept,
            @RequestParam(value = "parentOnly", required = false, defaultValue = "false") Boolean parentOnly,
            @RequestParam(value = "maxResults", required = false, defaultValue = "100") Long maxResults,
            @RequestParam(value = "profilId", required = false) String profilId,
            @RequestParam(value = "profilConstraint", required = false) Boolean profilConstraint,
            @RequestParam(value = "_format", required = false) String _format) {

        if (_format != null && (_format.equals("csv") || _format.equals("xls"))) {
            ComplexReturnType returnInfos = returnTypesRules.get("CustomersDetailDTO");

            List<CustomersDetailDTO> returnListDTO = customersService.getByCriteria(reference, pdvId, from, to, insee, radius, emailConstraint,
                    parentConstraint, card, address, address2, zipCode, city, region, country, firstName, lastName, email, phone, phone2, notes,
                    customersExcept, parentOnly, null , profilId,profilConstraint,CustomersDetailDTO.class, true);
            Map<String, Object> model = new HashMap<>();
            model.put("filename", returnInfos.getMainSheet());
            model.put("content", CsvHelper.getCSVContent(returnListDTO));
            if (_format.equals("csv"))
                return new ModelAndView(new CsvResponse(), model);

            if (_format.equals("xls")) {
                model.put("content_type", returnInfos.getMainContentType());
                return new ModelAndView(new XlsResponse(), model);
            }

        }
        if(_format != null && _format.equals("list")) {
            return new ServerResponse(customersService.getByCriteria(reference, pdvId, from, to, insee, radius, emailConstraint, parentConstraint, card, address,
                    address2, zipCode, city, region, country, firstName, lastName, email, phone, phone2, notes, customersExcept, parentOnly, maxResults ,profilId,profilConstraint,CustomersLightDTO.class));
        }

        return new ServerResponse(customersService.getByCriteria(reference, pdvId, from, to, insee, radius, emailConstraint, parentConstraint, card, address,
                address2, zipCode, city, region, country, firstName, lastName, email, phone, phone2, notes, customersExcept, parentOnly, maxResults ,profilId,profilConstraint,POSCustomersDTO.class));
    }

    @PreAuthorize("hasAuthority('" + Permissions.Clients.PERM_CLIENT_EXTRACT + "')")
    @RequestMapping(value = "/getCriteria/download", method = RequestMethod.GET)
    public Object download(UsernamePasswordAuthenticationToken token, @RequestParam(value = "reference", required = false) String reference,
            @RequestParam(value = "pdvId", required = false) String pdvId, @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to, @RequestParam(value = "insee", required = false) String insee,
            @RequestParam(value = "radius", required = false) Integer radius,
            @RequestParam(value = "emailConstraint", required = false) Boolean emailConstraint,
            @RequestParam(value = "parentConstraint", required = false) Boolean parentConstraint, @RequestParam(value = "card", required = false) String card,
            @RequestParam(value = "address", required = false) String address, @RequestParam(value = "address2", required = false) String address2,
            @RequestParam(value = "postal", required = false) String zipCode, @RequestParam(value = "city", required = false) String city,
            @RequestParam(value = "region", required = false) String region, @RequestParam(value = "country", required = false) String country,
            @RequestParam(value = "firstName", required = false) String firstName, @RequestParam(value = "lastName", required = false) String lastName,
            @RequestParam(value = "email", required = false) String email, @RequestParam(value = "phone", required = false) String phone,
            @RequestParam(value = "phone2", required = false) String phone2, @RequestParam(value = "notes", required = false) String notes,
            @RequestParam(value = "customersExcept", required = false) String customersExcept,
            @RequestParam(value = "parentOnly", required = false, defaultValue = "false") Boolean parentOnly,
            @RequestParam(value = "profilId", required = false) String profilId,
            @RequestParam(value = "profilConstraint", required = false) Boolean profilConstraint,
            @RequestParam(value = "_format", required = false) String _format) {
        return getCriteria(reference, pdvId, from, to, insee, radius, emailConstraint, parentConstraint, card, address, address2, zipCode, city, region,
                country, firstName, lastName, email, phone, phone2, notes, customersExcept, parentOnly, null ,profilId,profilConstraint, _format);
    }

    /**
     * Appelé dans la caisse
     * Sensé retourner les 10 clients avec le plus de tickets
     * Mais jamais utilisé / exploité
     * @return une liste vide
     */
    @RequestMapping(value = "/getTop", method = RequestMethod.GET)
    public ServerResponse getTop() {
        return new ServerResponse(customersService.getTop());
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam("id") String id) {
        return new ServerResponse(customersService.getById(id, POSCustomersDTO.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ServerResponse getCustomers(@PathVariable("id") String id) {
        CustomersDetailDTO retour = customersService.getById(id, CustomersDetailDTO.class);
        if (retour != null && retour.getIdParent() != null) {
            retour.getCustomerParent().setSearchKeyParent(customersService.findSearchKey(retour.getIdParent()));
        }
        return new ServerResponse(retour);
    }

    @RequestMapping(value = "/saveCustomer", method = RequestMethod.POST)
    public ServerResponse saveCustomer(@RequestBody CustomersDetailDTO dto) {
        return new ServerResponse(customersService.save(dto));
    }

    @RequestMapping(value = "/saveCustomerParent", method = RequestMethod.POST)
    public ServerResponse saveCustomerParent(@RequestBody CustomersParentDTO customerParentSave) {
        Map<String,CustomersParentDTO> listCustomerParentSave = new HashMap<String,CustomersParentDTO>();
        listCustomerParentSave.put(customerParentSave.getSearchKey() , customerParentSave);
        return new ServerResponse(customersService.saveCustomerParent(listCustomerParentSave, true));
    }

    @RequestMapping(value = "/deleteCustomerParent", method = RequestMethod.POST)
    public ServerResponse deleteCustomerParent(@RequestBody CustomersParentDTO customerParent) {

        return new ServerResponse(customersService.deleteCustomerParent(customerParent));

    }

}
