package org.pasteque.api.controller.api.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.pasteque.api.model.application.Applications;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.service.application.IVersionService;

@RestController
@RequestMapping(value = VersionController.BASE_URL)
public class VersionController {

    public static final String BASE_URL = "/version";

    @Autowired
    private IVersionService versionService;

    @RequestMapping(method = RequestMethod.GET)
    public ServerResponse get() {
        return new ServerResponse(versionService.getCurrent(Applications.Type.POS_PASTEQUE));
    }
    
    @RequestMapping(value = "/getServer", method = RequestMethod.GET)
    public ServerResponse getServer() {
        return new ServerResponse(versionService.getCurrent(Applications.Type.SERVER_OSE));
    }

}
