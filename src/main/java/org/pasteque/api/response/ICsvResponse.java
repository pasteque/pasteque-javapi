package org.pasteque.api.response;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface ICsvResponse {

    @JsonIgnore
    String[] header();

    @JsonIgnore
    String[] CSV();

}