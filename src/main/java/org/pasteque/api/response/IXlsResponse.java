package org.pasteque.api.response;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface IXlsResponse extends ICsvResponse{
 
    @JsonIgnore
    public static CellType[] types = null;
    
    public CellStyle getHeaderStyle(Workbook workbook);
    
    public CellStyle getDefaultStyle(Workbook workbook);
    
    int getColWidth(int index);

}