package org.pasteque.api.response;

import java.io.IOException;
import java.nio.charset.Charset;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;

@Component
public class CsvMessageConverter extends AbstractHttpMessageConverter<CsvResponse> {

    public static final MediaType MEDIA_TYPE = new MediaType("text", "csv", Charset.forName("utf-8"));
    public static final char TABULATION = '\t';
    public static final char SEMI_COLUMN = ';';

    public CsvMessageConverter() {
        super(MEDIA_TYPE);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return CsvResponse.class.equals(clazz);
    }

    @Override
    protected void writeInternal(CsvResponse csvResponse, HttpOutputMessage outputMessage) throws IOException,
            HttpMessageNotWritableException {
        /*
        outputMessage.getHeaders().setContentType(CsvMessageConverter.MEDIA_TYPE);
        outputMessage.getHeaders().set("Content-Disposition", "attachment; filename=" + csvResponse.getFileName());
        CSVWriter writer = new CSVWriter(new OutputStreamWriter(outputMessage.getBody()), CsvMessageConverter.SEMI_COLUMN);
        writer.writeAll(csvResponse.getContent());
        writer.flush();
        writer.close();
        */
    }

    @Override
    protected CsvResponse readInternal(Class<? extends CsvResponse> clazz, HttpInputMessage inputMessage) throws IOException,
            HttpMessageNotReadableException {
        // TODO Auto-generated method stub
        return null;
    }
}
