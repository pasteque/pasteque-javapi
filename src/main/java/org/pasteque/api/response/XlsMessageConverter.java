package org.pasteque.api.response;

import java.io.IOException;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;


@Component
public class XlsMessageConverter extends AbstractHttpMessageConverter<XlsResponse> {

    public static final MediaType MEDIA_TYPE = new MediaType("application", "vnd.ms-excel");

    public XlsMessageConverter() {
        super(MEDIA_TYPE);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return XlsResponse.class.equals(clazz);
    }

    @Override
    protected void writeInternal(XlsResponse xlsResponse, HttpOutputMessage outputMessage) throws IOException,
            HttpMessageNotWritableException {
        outputMessage.getHeaders().setContentType(XlsMessageConverter.MEDIA_TYPE);
        //outputMessage.getHeaders().set("Content-Disposition", "attachment; filename=" + xlsResponse.getFileName());
        //xlsResponse.getWorkbook().write(outputMessage.getBody());
    }


    @Override
    protected XlsResponse readInternal(Class<? extends XlsResponse> clazz, HttpInputMessage inputMessage) throws IOException,
            HttpMessageNotReadableException {
        // TODO Auto-generated method stub
        return null;
    }
}
