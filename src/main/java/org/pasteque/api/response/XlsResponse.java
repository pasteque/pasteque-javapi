package org.pasteque.api.response;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

public class XlsResponse extends AbstractXlsxStreamingView{

   
    @SuppressWarnings("unchecked")
    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook, HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {
        
        
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String name = (String) model.get("filename");
        String fileName = name + format.format(new Date()) + ".xlsx";
        CellType[] content_type = (CellType[]) model.get("content_type");
        
        IXlsResponse origine = (IXlsResponse) model.get("object");
        
        ArrayList<Map<String, Object>> nextSheets = (ArrayList<Map<String, Object>>) model.get("nextSheets");
        
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        
        List<String[]> content = (List<String[]>) model.get("content");
        
        populateNewSheet( workbook , name , content , content_type, origine);
        
        if(nextSheets != null){
            for(Map<String, Object> sheet : nextSheets){
                name = (String) sheet.get("name");
                content_type = (CellType[]) sheet.get("content_type");
                content = (List<String[]>) sheet.get("content");
                populateNewSheet( workbook , name , content , content_type, origine);
            }
        }
        
        
    }

    @SuppressWarnings("static-access")
    public void populateNewSheet(Workbook workbook , String sheetName , List<String[]> content , CellType[] content_type, IXlsResponse origine) {
        String safeName = WorkbookUtil.createSafeSheetName(sheetName);
        Sheet newSheet = workbook.createSheet(safeName);
        String[] line = null ;
        Runtime runtime = Runtime.getRuntime();
        int mb = 1024*1024;
        
        CellStyle cellHeadStyle = null;
        CellStyle cellStyle = null;
        if(origine != null) {
            cellHeadStyle = origine.getHeaderStyle(workbook);
            cellStyle = origine.getDefaultStyle(workbook);
        }
        

        CellStyle cellDateStyle =  workbook.createCellStyle() ;
        if(cellStyle != null) {
            cellDateStyle.cloneStyleFrom(cellStyle);
        }
        CreationHelper createHelper = workbook.getCreationHelper();
        short dateFormat = createHelper.createDataFormat().getFormat("dd/MM/yyyy");
        cellDateStyle.setDataFormat(dateFormat);
        
        //cas de l'erreur 
        if(content.size() == 1 && content.get(0).length == 1 && content_type.length != 1) {
            Row row = newSheet.createRow(0);
            Cell cell= row.createCell(0);
            cell.setCellStyle(cellHeadStyle);
            cell.setCellValue(content.get(0)[0]);
        } else {
            if(content != null && ! content.isEmpty()) {
                for(int rownum = 0 ;  content.size() > 0 ; rownum++) {
                    Row row = newSheet.createRow(rownum);

                    
                    if(rownum % 1000 == 0) {
                    System.out.println("##### Heap utilization statistics [MB] #####" + rownum);
                    
                    //Print free memory
                    System.out.println("Free Memory:" 
                        + runtime.freeMemory() / mb);
                    
                    //Print total available memory
                    System.out.println("Total Memory:" + runtime.totalMemory() / mb);

                    //Print Maximum available memory
                    System.out.println("Max Memory:" + runtime.maxMemory() / mb);
                    
                    //Print used memory
                    System.out.println("Row Number:" 
                        + rownum);
                    
                    //Print used memory
                    System.out.println("Remaining rows:" 
                        + content.size());
                    }
                    line = content.remove(0);
                    
                    if(line != null) {
                        for(int column = 0 ; column < line.length ; column++) {
                            Cell cell= row.createCell(column); 


                            if(rownum > 0 && content_type != null && column < content_type.length) {
                                if(line[column] != null && ! line[column].trim().isEmpty()) {
                                    switch(content_type[column]) {
                                    case NUMERIC :
                                        if(Pattern.matches("\\d{2}\\/\\d{2}\\/\\d{4}" , line[column])) {
                                         Calendar c = Calendar.getInstance();
                                         SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                                         try {
                                            c.setTime(df.parse(line[column]));
                                            cell.setCellValue(c);
                                            cell.setCellStyle(cellDateStyle);
                                        } catch (ParseException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                            cell.setCellStyle(cellStyle);
                                            cell.setCellValue(line[column]);
                                        }
                                         
                                        } else {
                                            cell.setCellStyle(cellStyle);
                                            cell.setCellValue(Double.parseDouble(line[column].replace(',', '.')));
                                        }
                                        break;
                                    case BOOLEAN :
                                        cell.setCellStyle(cellStyle);
                                        cell.setCellValue(Boolean.valueOf(line[column]));
                                        break;
                                    default :
                                        cell.setCellStyle(cellStyle);
                                        cell.setCellValue(line[column]);
                                    }
                                } else {
                                    cell.setCellStyle(cellStyle); 
                                }

                                //2020 08 Deprecated le set est fait de manière implicite avec le setCellValue
                                //cell.setCellType(content_type[column]);
                            } else {
                                if(origine != null && origine.getColWidth(column) > 0) {
                                    newSheet.setColumnWidth(column, origine.getColWidth(column));
                                }
                                cell.setCellStyle(cellHeadStyle);
                                cell.setCellValue(line[column]);
                            }
                        }
                    }
                }
            }
        }

    }

}
