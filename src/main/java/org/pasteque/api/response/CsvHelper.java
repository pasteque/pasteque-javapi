package org.pasteque.api.response;

import java.util.ArrayList;
import java.util.List;

public final class CsvHelper {

    private CsvHelper() {
    }

    public static <T extends ICsvResponse> List<String[]> getCSVContent(List<T> list) {
        List<String[]> content = new ArrayList<>();
        boolean first = true;
        if(list == null) {
            content.add(new String[]{"Un problème est survenu - Le serveur est sans doute trop sollicité ou votre requête trop lourde"});
        } else {
            for (ICsvResponse csvResponse : list) {
                if (first) {
                    content.add(csvResponse.header());
                    first = false;
                }
                content.add(csvResponse.CSV());
            }
        }
        return content;
    }

}
