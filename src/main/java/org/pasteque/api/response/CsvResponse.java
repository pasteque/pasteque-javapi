package org.pasteque.api.response;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;

public class CsvResponse extends AbstractCsvView{

    @Override
    protected void buildCsvDocument(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String name = (String) model.get("filename");
        String fileName = name + format.format(new Date()) + ".csv";
        
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        
        @SuppressWarnings("unchecked")
        List<String[]> content = (List<String[]>) model.get("content");
        
        PrintWriter pwriter = response.getWriter();
        //EDU Hack pour avoir le BOM afin d'être en UTF-8 avec BOM
        pwriter.print('\ufeff');
        
        CSVWriter writer = new CSVWriter( pwriter, CsvMessageConverter.SEMI_COLUMN , ICSVWriter.DEFAULT_QUOTE_CHARACTER , ICSVWriter.DEFAULT_ESCAPE_CHARACTER , ICSVWriter.DEFAULT_LINE_END);
        writer.writeAll(content);
        writer.flush();
        writer.close();
        
    }

}
