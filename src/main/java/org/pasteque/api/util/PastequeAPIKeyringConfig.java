package org.pasteque.api.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPException;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.callbacks.KeyringConfigCallbacks;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.keyrings.InMemoryKeyring;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.keyrings.KeyringConfigs;

public class PastequeAPIKeyringConfig {
    
    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    String passphrase;
    String privateKey;
    String publicKey;
    String sender;

    public PastequeAPIKeyringConfig(String passphrase, String privateKey,
            String publicKey, String sender) {
        this.passphrase = passphrase;
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.sender = sender;
    }

    public InMemoryKeyring keyring(String... recipientsArmoredPublicKey)
            throws IOException, PGPException {
        
        InMemoryKeyring keyring = KeyringConfigs.forGpgExportedKeys(KeyringConfigCallbacks.withPassword(passphrase));

        keyring.addPublicKey(publicKey.getBytes(StandardCharsets.US_ASCII));
        keyring.addSecretKey(privateKey.getBytes(StandardCharsets.US_ASCII));
        for (String recipientArmoredPublicKey : recipientsArmoredPublicKey) {
            keyring.addPublicKey(recipientArmoredPublicKey.getBytes(StandardCharsets.US_ASCII));
        }
        return keyring;
    }

    public String getSender() {
        return sender;
    }
}
