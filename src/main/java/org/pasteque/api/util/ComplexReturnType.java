package org.pasteque.api.util;

import org.apache.poi.ss.usermodel.CellType;
import org.pasteque.api.model.adaptable.ILoadable;

public class ComplexReturnType {
    private String key;
    private String mainReturnType;
    private String mainSheet;
    private CellType[] mainContentType;
    private Class<? extends ILoadable>[] nextReturnTypes;
    private String[] nextSheetNames;
    private CellType[][] nextContentType;
    
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getMainReturnType() {
        return mainReturnType;
    }
    public void setMainReturnType(String mainReturnType) {
        this.mainReturnType = mainReturnType;
    }
    public String getMainSheet() {
        return mainSheet;
    }
    public void setMainSheet(String mainSheet) {
        this.mainSheet = mainSheet;
    }
    public Class<? extends ILoadable>[] getNextReturnTypes() {
        return nextReturnTypes;
    }
    public void setNextReturnTypes(Class<? extends ILoadable>[] nextReturnTypes) {
        this.nextReturnTypes = nextReturnTypes;
    }
    public String[] getNextSheetNames() {
        return nextSheetNames;
    }
    public void setNextSheetNames(String[] nextSheetNames) {
        this.nextSheetNames = nextSheetNames;
    }
    public ComplexReturnType(String key, String mainReturnType, String mainSheet, CellType[] mainContentType , Class<? extends ILoadable>[] nextReturnTypes, String[] nextSheetNames , CellType[][] nextContentType) {
        this.key = key;
        this.mainReturnType = mainReturnType;
        this.mainSheet = mainSheet;
        this.mainContentType = mainContentType;
        this.nextReturnTypes = nextReturnTypes;
        this.nextSheetNames = nextSheetNames;
        this.nextContentType = nextContentType;
    }
    
   public int getNumberOfSheets() {
       int retour = 0;
//       if(mainSheet != null && mainSheet.trim().length() > 0) {
//           retour ++;
//       }
       
       //TODO gérer proprement tout ceci , y a  t il des cas où la feuille 2 est nulle mais ni la 1 ni la 3 ?
       if(nextSheetNames != null && nextSheetNames.length > 0) {
           for(String sheet : nextSheetNames) {
               if(sheet != null && sheet.trim().length() > 0) {
                   retour ++;
               }
           }
       }
       
       return retour;
   }
public CellType[] getMainContentType() {
    return mainContentType;
}
public void setMainContentType(CellType[] mainContentType) {
    this.mainContentType = mainContentType;
}
public CellType[][] getNextContentType() {
    return nextContentType;
}
public void setNextContentType(CellType[][] nextContentType) {
    this.nextContentType = nextContentType;
}
    
}
