package org.pasteque.api.util;

import java.lang.reflect.Field;

import org.pasteque.api.model.customers.Customers;

/**
 * @author Pascal Gattino Une classe pour supprimer les caractères spéciaux lors enregistrement en base (Exemple: ; ou " dans les notes ou dans la fiche client)
 *         et mise en forme des données lors de l'enregistrement en base (Exemple: Nom, Prénom et adresse en majuscules).
 */

public class FilteringAndFormattingData {
    
    public static String SQLFilter(String source) {
        source = source.replaceAll("'", "\\\\'");
        return source;
    }

    public static void filter(Object object) {
        try {
            for (Field field : object.getClass().getDeclaredFields()) {
                field.setAccessible(true);// Obligatoire si le field est private
                if (field.get(object) instanceof String) {
                    String value = (String) field.get(object);
                    value = value.replaceAll("\"", "");
                    value = value.replaceAll(";", ".");
                    field.set(object, value);
                }

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    //nouvelle règle contient et start with on vire les ST SAINT les - et les ' 
    //les - et ' remplacées par espaces
    //STE -> SAINT 
    //ST -> SAINTE
    //é -> E
    //é -> E
    //ê -> E
    //à -> A
    //ô -> O
    //â -> A
    public static String filterCityName(String input) {

                    String value = input == null ? "" : input.toUpperCase();
                    value = value.replaceAll("-", " ");
                    value = value.replaceAll("'", " ");
                    value = value.replaceAll("SAINT", "ST");
                    value = value.replaceAll("É", "E");
                    value = value.replaceAll("È", "E");
                    value = value.replaceAll("Ë", "E");
                    value = value.replaceAll("Ê", "E");
                    value = value.replaceAll("Â", "A");
                    value = value.replaceAll("À", "A");
                    value = value.replaceAll("Ô", "O");
                    return value;

    }

    public static void format(Object object) {
        try {
            for (Field field : object.getClass().getDeclaredFields()) {
                field.setAccessible(true);// Obligatoire si le field est private
                if (field.get(object) instanceof String && (field.getName().equals(Customers.Fields.FIRST_NAME)
                        || field.getName().equals(Customers.Fields.LAST_NAME) || field.getName().equals(Customers.Fields.NAME)
                        || field.getName().equals(Customers.Fields.ADDRESS) || field.getName().equals(Customers.Fields.ADDRESS2)
                        || field.getName().equals(Customers.Fields.CITY) || field.getName().equals(Customers.Fields.COUNTRY))) {
                    String value = (String) field.get(object);
                    field.set(object, value.toUpperCase());
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
