package org.pasteque.api.util;


/**
 * 
 * @author edouard.ducray
 * Une classe pour gérer les question liées à la géographie et aux calculs associés
 */
public class GeoQuery {
    
    public static double KM_PER_DEGREE = 111.045;
    
    /**
     * 
     * @param latitude 
     * @param longitude
     * @param lat2
     * @param long2
     * @return le carré de la distance en km
     */
    public static double distance_squared(double latitude , double longitude , double lat2 , double long2){
        return  ( Math.pow(KM_PER_DEGREE * (latitude - lat2), 2) +
                Math.pow(KM_PER_DEGREE * (long2 - longitude) * Math.cos(Math.toRadians(latitude)), 2));
    }
    
    /**
     * 
     * @param latitude
     * @param longitude
     * @param lat2
     * @param long2
     * @return la distance en km
     */
    public static double distance(double latitude , double longitude , double lat2 , double long2){
        return Math.sqrt(distance_squared(latitude , longitude , lat2 , long2));
    }
    
    
    public static double distance2latitude(double distance ){
        return distance / KM_PER_DEGREE ;
    }
    
    /**
     * 
     * @param distance en kilomètres
     * @param latitude 
     * @return le nombre de degrés de longitude correspondant à cette distance à la latitude donnée
     */
    public static double distance2longitude(double distance , double latitude ){
        return (distance/Math.abs(Math.cos(Math.toRadians(latitude))*KM_PER_DEGREE));
        
    }
    

}
