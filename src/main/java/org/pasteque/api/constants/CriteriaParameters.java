package org.pasteque.api.constants;

public final class CriteriaParameters {

    public static final String DATE_DEBUT = "dateDeb";
    public static final String DATE_FIN = "dateFin";
    
    //Pour tous
    public static final String LIMIT = "limit";
    
    public static final String EXCLUDELOGIN = "login";
    public static final String EXCLUDEP = "p";
    public static final String EXCLUDEACTION = "action";
    public static final String EXCLUDEPASSWORD = "password";
    
    
}
