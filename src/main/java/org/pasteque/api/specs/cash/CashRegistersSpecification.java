package org.pasteque.api.specs.cash;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;

import org.pasteque.api.model.saleslocations.Locations;

public class CashRegistersSpecification extends GenericSpecification<CashRegisters> {

    private static final long serialVersionUID = -779500898975196268L;
    
    @Override
    protected <R> Expression<R> constructExpression (Root<CashRegisters> root , SearchCriteria criteria) {
        if(CashRegisters.Fields.LOCATION.equals(criteria.getKey())) {
            return root.get(CashRegisters.Fields.LOCATION).get(Locations.Fields.ID);
        }
        
        return super.constructExpression(root, criteria);
    }

}
