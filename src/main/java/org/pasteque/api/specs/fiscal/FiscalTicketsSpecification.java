package org.pasteque.api.specs.fiscal;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.pasteque.api.model.fiscal.FiscalTickets;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;

public class FiscalTicketsSpecification extends GenericSpecification<FiscalTickets> {
    
    private static final long serialVersionUID = -4072064550155576692L;

    @Override
    protected <R> Expression<R> constructExpression (Root<FiscalTickets> root , SearchCriteria criteria) {
        if(FiscalTickets.SearchCritria.FROM.equals(criteria.getKey()) 
                || FiscalTickets.SearchCritria.TO.equals(criteria.getKey())) {
            return root.get(FiscalTickets.Fields.DATE);
        }
        if(FiscalTickets.SearchCritria.EXCLUDE_NUMBER.equals(criteria.getKey())) {
            return root.get(FiscalTickets.Fields.NUMBER);
        }
        
        return super.constructExpression(root, criteria);
    }

}
