package org.pasteque.api.specs.security;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.model.security.People;
import org.pasteque.api.model.security.Roles;
public class PeopleSpecification extends GenericSpecification<People> {
    private static final long serialVersionUID = -779500898975196268L;
    
    @Override
    protected <R> Expression<R> constructExpression (Root<People> root , SearchCriteria criteria) {
        if(People.Fields.ROLES.equals(criteria.getKey())) {
            return root.get(People.Fields.ROLES).get(Roles.Fields.NAME);
        }
        
        return super.constructExpression(root, criteria);
    }
}