package org.pasteque.api.specs.saleslocations;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.model.saleslocations.Insee;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.saleslocations.SalesLocations;

public class SalesLocationsSpecification extends GenericSpecification<SalesLocations> {

    private static final long serialVersionUID = 8237688084708896676L;

    @Override
    protected <R> Expression<R> constructExpression (Root<SalesLocations> root , SearchCriteria criteria) {
        if(SalesLocations.Fields.TOUR.equals(criteria.getKey())) {
            return root.get(SalesLocations.Fields.TOUR).get(Locations.Fields.ID);
        }
        if(SalesLocations.SearchCritria.FROM.equals(criteria.getKey())) {
            return root.get(SalesLocations.Fields.END_DATE);
        }
        if(SalesLocations.SearchCritria.TO.equals(criteria.getKey())) {
            return root.get(SalesLocations.Fields.START_DATE);
        }
        if(SalesLocations.SearchCritria.ENDAFTER.equals(criteria.getKey())) {
            return root.get(SalesLocations.Fields.END_DATE);
        }
        if(SalesLocations.SearchCritria.STARTBEFORE.equals(criteria.getKey())) {
            return root.get(SalesLocations.Fields.START_DATE);
        }
        if(SalesLocations.Fields.INSEE.equals(criteria.getKey())) {
            return root.get(SalesLocations.Fields.INSEE).get(Insee.Fields.INSEE);
        }
        if(SalesLocations.SearchCritria.ZIP.equals(criteria.getKey())) {
            return root.get(SalesLocations.Fields.INSEE).get(Insee.Fields.ZIPCODE);
        }
        
        return super.constructExpression(root, criteria);
    }

}
