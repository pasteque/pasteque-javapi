package org.pasteque.api.specs;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class GenericSpecification<T> implements Specification<T> {

    private static final long serialVersionUID = 1183637406698935263L;

    private final String joker = "%";

    private List<SearchCriteria> list;

    public GenericSpecification() {
        this.list = new ArrayList<>();
    }

    public void add(SearchCriteria criteria) {
        list.add(criteria);
    }


    protected <R> Expression<R> constructExpression (Root<T> root , SearchCriteria criteria) {
        return root.<R> get(criteria.getKey());
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query,
            CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();

        for (SearchCriteria criteria : list) {
            if (criteria.getOperation().equals(SearchOperation.IS_NULL)) {
                predicates.add(criteriaBuilder.isNull(
                        constructExpression(root , criteria)));
            } else if (criteria.getOperation().equals(SearchOperation.IS_NOT_NULL)) {
                predicates.add(criteriaBuilder.isNotNull(
                        constructExpression(root , criteria)));
            } else if(criteria.getValue() != null ) {
                if (criteria.getOperation().equals(SearchOperation.GREATER_THAN)) {
                    predicates.add(criteriaBuilder.greaterThan(
                            constructExpression(root , criteria), criteria.getValue().toString()));
                } else if (criteria.getOperation().equals(SearchOperation.LESS_THAN)) {
                    predicates.add(criteriaBuilder.lessThan(
                            constructExpression(root , criteria), criteria.getValue().toString()));
                } else if (criteria.getOperation().equals(SearchOperation.GREATER_THAN_EQUAL)) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                            constructExpression(root , criteria), criteria.getValue().toString()));
                } else if (criteria.getOperation().equals(SearchOperation.LESS_THAN_EQUAL)) {
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(
                            constructExpression(root , criteria), criteria.getValue().toString()));
                } else if (criteria.getOperation().equals(SearchOperation.NOT_EQUAL)) {
                    predicates.add(criteriaBuilder.notEqual(
                            constructExpression(root , criteria), criteria.getValue()));
                } else if (criteria.getOperation().equals(SearchOperation.EQUAL)) {
                    predicates.add(criteriaBuilder.equal(
                            constructExpression(root , criteria), criteria.getValue()));
                } else if (criteria.getOperation().equals(SearchOperation.MATCH)) {
                    predicates.add(criteriaBuilder.like(
                            criteriaBuilder.lower(constructExpression(root , criteria)),
                            joker + criteria.getValue().toString().toLowerCase() + joker));
                } else if (criteria.getOperation().equals(SearchOperation.MATCH_END)) {
                    predicates.add(criteriaBuilder.like(
                            criteriaBuilder.lower(constructExpression(root , criteria)),
                            joker + criteria.getValue().toString().toLowerCase()));
                } else if (criteria.getOperation().equals(SearchOperation.MATCH_START)) {
                    predicates.add(criteriaBuilder.like(
                            criteriaBuilder.lower(constructExpression(root , criteria)),
                            criteria.getValue().toString().toLowerCase() + joker));
                } else if (criteria.getOperation().equals(SearchOperation.IN)) {
                    predicates.add(criteriaBuilder.in(constructExpression(root , criteria)).value(criteria.getValue()));
                } else if (criteria.getOperation().equals(SearchOperation.NOT_IN)) {
                    predicates.add(criteriaBuilder.not(constructExpression(root , criteria)).in(criteria.getValue()));
                }
            }
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }


}
