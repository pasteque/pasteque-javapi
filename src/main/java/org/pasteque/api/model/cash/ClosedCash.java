package org.pasteque.api.model.cash;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.pasteque.api.dto.cash.CashRegistersSimpleDTO;
import org.pasteque.api.dto.cash.POSClosedCashDTO;
import org.pasteque.api.dto.fiscal.PeriodInfoDTO;
import org.pasteque.api.dto.locations.POSSalesLocationsDTO;
import org.pasteque.api.dto.sessions.CashSessionInfoDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.fiscal.Periods;
import org.pasteque.api.model.saleslocations.SalesLocations;

@Entity
@Table(name = ClosedCash.Table.NAME)
public class ClosedCash implements IAdaptable {
    public final class Table {
        public static final String NAME = "closedcash";
    }

    public final class Fields {
        public static final String MONEY = "money";
        public static final String HOST_SEQUENCE = "hostSequence";
        public static final String DATE_START = "dateStart";
        public static final String DATE_END = "dateEnd";
        public static final String OPEN_CASH = "openCash";
        public static final String CLOSE_CASH = "closeCash";
        public static final String EXPECTED_CASH = "expectedCash";
        public static final String CASH_REGISTER = "cashRegister";
        public static final String SALES_LOCATION = "salesLocation";
        public static final String CONTINUOUS ="continuous";
        public static final String CUST_COUNT ="custCount";
        public static final String TICKET_COUNT ="ticketCount";
        public static final String CS ="cs";
        public static final String CS_PERIOD ="csPeriod";
        public static final String CS_FYEAR ="csFYear";
        public static final String CS_PERPETUAL ="csPerpetual";
        public static final String DETAILS = "details";
    }

    @Id
    private String money;
    @Column(name = "hostsequence")
    private int hostSequence;
    @Column(name = "datestart")
    @Temporal(value=TemporalType.TIMESTAMP)
    private Date dateStart;
    @Column(name = "dateend")
    @Temporal(value=TemporalType.TIMESTAMP)
    private Date dateEnd;
    @Column(name = "opencash")
    private Double openCash;
    @Column(name = "closecash")
    private Double closeCash;
    @Column(name = "expectedcash")
    private Double expectedCash;

    @ManyToOne
    @JoinColumn(name = "deballage_id")
    private SalesLocations salesLocation;

    @ManyToOne
    @JoinColumn(name = "cashregister_id")
    private CashRegisters cashRegister;

    //EDU 2020 08 Nouveaux champs

    //la continuité par défaut true
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean continuous = true;
    //Le nombre de clients et de tickets initialisés à 0
    private Long custCount = 0L;
    private Long ticketCount = 0L;

    //Les chiffres d'affaires cumulés sur 4 plans
    //La session amorce à zéro
    private Double cs = 0D;
    //La période ( journée )
    private Double csPeriod = 0D;
    //L'année fiscale
    private Double csFYear = 0D;
    //L'installation
    private Double csPerpetual = 0D;

    //L'objet périods associé
    @OneToOne
    @JoinColumn(name = "money" , insertable = false, updatable = false)
    private Periods details;

    public Periods getDetails() {
        return details;
    }

    public void setDetails(Periods details) {
        this.details = details ;
    }

    public SalesLocations getSalesLocation() {
        return salesLocation;
    }

    public void setSalesLocation(SalesLocations salesLocation) {
        this.salesLocation = salesLocation;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public int getHostSequence() {
        return hostSequence;
    }

    public void setHostSequence(int hostSequence) {
        this.hostSequence = hostSequence;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
        if(dateEnd != null && details != null) {
            details.setDateEnd(dateEnd);
            details.setClosed(true);
        }
    }

    public Double getOpenCash() {
        return openCash;
    }

    public void setOpenCash(Double openCash) {
        this.openCash = openCash;
    }

    public Double getCloseCash() {
        return closeCash;
    }

    public void setCloseCash(Double closeCash) {
        this.closeCash = closeCash;
    }

    public Double getExpectedCash() {
        return expectedCash;
    }

    public void setExpectedCash(Double expectedCash) {
        this.expectedCash = expectedCash;
    }

    public CashRegisters getCashRegister() {
        return cashRegister;
    }

    public void setCashRegister(CashRegisters cashRegister) {
        this.cashRegister = cashRegister;
    }

    public ClosedCashManager getClosedCashManager() {
        return new ClosedCashManager(this);
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector , false);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getAdapter(Class<T> selector , boolean filtered) {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator('.');
        DecimalFormat money = new DecimalFormat("0.00", otherSymbols);

        if (selector.equals(POSClosedCashDTO.class)) {
            POSClosedCashDTO pOSClosedCashDTO = new POSClosedCashDTO();
            pOSClosedCashDTO.setId(getMoney());
            pOSClosedCashDTO.setCashRegisterId(getCashRegister().getId());
            if (getCloseCash() != null) {
                pOSClosedCashDTO.setCloseCash(money.format(getCloseCash()));
            }
            pOSClosedCashDTO.setCloseDate(getDateEnd());
            if (getExpectedCash() != null) {
                pOSClosedCashDTO.setExpectedCash(money.format(getExpectedCash()));
            }
            if (getOpenCash() != null) {
                pOSClosedCashDTO.setOpenCash(money.format(getOpenCash()));
            }
            pOSClosedCashDTO.setOpenDate(getDateStart());
            pOSClosedCashDTO.setSequence(getHostSequence());
            if( salesLocation != null) {
                pOSClosedCashDTO.setSalesLocation(salesLocation.getAdapter(POSSalesLocationsDTO.class));
            }
            // TODO : AME => initialiser correctement Tickets & Total
            pOSClosedCashDTO.setTickets(null);
            pOSClosedCashDTO.setTotal(null);
            return (T) pOSClosedCashDTO;
        }
        if(selector.equals(CashSessionInfoDTO.class)) {
            CashSessionInfoDTO dto = new CashSessionInfoDTO();
            dto.setCashRegister(cashRegister.getAdapter(CashRegistersSimpleDTO.class));
            if(dateEnd != null) {
                dto.setCloseDate(new Timestamp(dateEnd.getTime()).toLocalDateTime());
            }
            if(dateStart != null) {
                dto.setOpenDate(new Timestamp(dateStart.getTime()).toLocalDateTime());
            }
            if(getDetails() != null) {
                PeriodInfoDTO detailsDTO = getDetails().getAdapter(PeriodInfoDTO.class);
                dto.setCatSales(detailsDTO.getCumulCAParCategorie());
                dto.setCatTaxes(detailsDTO.getCumulTaxesParCategorie());
                dto.setCustBalances(detailsDTO.getBalancesClients());
                dto.setPayments(detailsDTO.getEncaissements());
                dto.setTaxes(detailsDTO.getTaxes());
            }
            return (T) dto;
        }
        return null;
    }

    public String getCashRegisterId() {
        return (cashRegister == null ? null : cashRegister.getId());
    }

    public Long getSalesLocationId() {
        return (salesLocation == null ? null : salesLocation.getId());
    }

    public boolean isContinuous() {
        return continuous;
    }

    public void setContinuous(boolean continuous) {
        this.continuous = continuous;
    }

    public Long getCustCount() {
        return custCount == null ? 0L : custCount;
    }

    public void setCustCount(Long custCount) {
        this.custCount = custCount;
    }

    public Long getTicketCount() {
        return ticketCount == null ? 0L : ticketCount;
    }

    public void setTicketCount(Long ticketCount) {
        this.ticketCount = ticketCount;
    }

    public Double getCs() {
        return cs == null ? 0D : cs;
    }

    public void setCs(Double cs) {
        this.cs = cs;
    }

    public Double getCsPeriod() {
        return csPeriod == null ? 0D : csPeriod;
    }

    public void setCsPeriod(Double csPeriod) {
        this.csPeriod = csPeriod;
    }

    public Double getCsFYear() {
        return csFYear == null ? 0D : csFYear;
    }

    public void setCsFYear(Double csFYear) {
        this.csFYear = csFYear;
    }

    public Double getCsPerpetual() {
        return csPerpetual == null ? 0D : csPerpetual;
    }

    public void setCsPerpetual(Double csPerpetual) {
        this.csPerpetual = csPerpetual;
    }
}
