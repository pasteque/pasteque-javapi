package org.pasteque.api.model.cash;

import org.pasteque.api.model.tickets.Tickets;

public class ClosedCashManager {
    private ClosedCash closedCash;
    
    public ClosedCashManager(ClosedCash closedCash) {
        this.closedCash = closedCash ;
    }
    
    
    public void updateCs(Tickets ticket , boolean newCust) {
        //TVA et remises incluses
        double amount = ticket.getFinalTaxedPrice();
        closedCash.setCs(closedCash.getCs()+ amount);
        closedCash.setCsFYear(closedCash.getCsFYear() + amount);
        closedCash.setCsPeriod(closedCash.getCsPeriod()+ amount);
        closedCash.setCsPerpetual(closedCash.getCsPerpetual()+ amount);
        closedCash.setTicketCount(closedCash.getTicketCount()+1);
        
        //TODO revenir ici pour déterminer plus finement si c'est un nouveau client 
        //Pour eviter de prendre les retour et modif
        if(newCust) {
            closedCash.setCustCount(closedCash.getCustCount()+1);
        }
        
    }

}
