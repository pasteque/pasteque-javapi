package org.pasteque.api.model.cash;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.pasteque.api.dto.cash.POSCashRegistersDTO;
import org.pasteque.api.dto.cash.CashRegistersSimpleDTO;
import org.pasteque.api.dto.locations.POSLocationsDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.saleslocations.Locations;

@Entity
public class CashRegisters implements IAdaptable {

    public final class Table {
        public static final String NAME = "cashregisters";
    }

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String ADDRESS = "address";
        public static final String NEXT_TICKET_ID = "nextTicketId";
        public static final String LOCATION = "location";
        public static final String NEXT_SESSION_ID = "nextSessionId";
    }

    @Id
    private String id;
    private String name;
    @Column(name = "nextticketid")
    private long nextTicketId;

    private Locations location;
    
    private int nextSessionId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getNextTicketId() {
        return nextTicketId;
    }

    public void setNextTicketId(long nextTicketId) {
        this.nextTicketId = nextTicketId;
    }

    public int getNextSessionId() {
        return nextSessionId;
    }

    public void setNextSessionId(int nextSessionId) {
        this.nextSessionId = nextSessionId;
    }

    public Locations getLocation() {
        return location;
    }

    public void setLocation(Locations location) {
        this.location = location;
    }
    
    @Override
    public <T> T getAdapter(Class<T> selector) {
           return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector , boolean filtered) {
        if (selector.equals(POSCashRegistersDTO.class)) {
            POSCashRegistersDTO cashRegisterDTO = new POSCashRegistersDTO();
            cashRegisterDTO.setId(getId());
            cashRegisterDTO.setLabel(getName());
            if (getLocation() != null) {
                cashRegisterDTO.setLocationId(getLocation().getId());
                if (getLocation().getTaxCustCategory() != null) {
                    cashRegisterDTO.setTaxCustCategId(getLocation().getTaxCustCategory().getId());
                }
                cashRegisterDTO.setLocation(getLocation().getAdapter(POSLocationsDTO.class , filtered));
            }
            cashRegisterDTO.setNextTicketId(getNextTicketId());
            cashRegisterDTO.setNextSessionId(getNextSessionId());
            return (T) cashRegisterDTO;
        }
        if (selector.equals(CashRegistersSimpleDTO.class)) {
            CashRegistersSimpleDTO cashRegisterSimpleDTO = new CashRegistersSimpleDTO();
            cashRegisterSimpleDTO.setId(getId());
            cashRegisterSimpleDTO.setLabel(getName());
            cashRegisterSimpleDTO.setNextTicketId(getNextTicketId());
            cashRegisterSimpleDTO.setNextSessionId(getNextSessionId());
            cashRegisterSimpleDTO.setLocationId(getLocation().getId());
            return (T) cashRegisterSimpleDTO;
        }
        return null;
    }

}
