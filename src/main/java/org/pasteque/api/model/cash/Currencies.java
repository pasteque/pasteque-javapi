package org.pasteque.api.model.cash;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.pasteque.api.dto.cash.POSCurrenciesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;

@Entity
public class Currencies implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String SYMBOL = "symbol";
        public static final String DECIMAL_SEPARATOR = "decimalSeparator";
        public static final String THOUSANDS_SEPARATOR = "thousandsSeparator";
        public static final String RATE = "rate";
        public static final String FORMAT = "format";
        public static final String MAIN = "main";
        public static final String ACTIVE = "active";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String symbol;
    @Column(name = "decimalsep")
    private String decimalSeparator;
    @Column(name = "thousandssep")
    private String thousandsSeparator;
    private double rate;
    private String format;
    private boolean main;
    private boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDecimalSeparator() {
        return decimalSeparator;
    }

    public void setDecimalSeparator(String decimalSeparator) {
        this.decimalSeparator = decimalSeparator;
    }

    public String getThousandsSeparator() {
        return thousandsSeparator;
    }

    public void setThousandsSeparator(String thousandsSeparator) {
        this.thousandsSeparator = thousandsSeparator;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
           return getAdapter(selector , false);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector , boolean filtered) {
        if (selector.equals(POSCurrenciesDTO.class)) {
            POSCurrenciesDTO pOSCurrenciesDTO = new POSCurrenciesDTO();
            pOSCurrenciesDTO.setIsActive(isActive());
            pOSCurrenciesDTO.setDecimalSeparator(getDecimalSeparator());
            pOSCurrenciesDTO.setFormat(getFormat());
            pOSCurrenciesDTO.setId(getId());
            pOSCurrenciesDTO.setLabel(getName());
            pOSCurrenciesDTO.setIsMain(isMain());
            pOSCurrenciesDTO.setRate(getRate());
            pOSCurrenciesDTO.setSymbol(getSymbol());
            pOSCurrenciesDTO.setThousandsSeparator(getThousandsSeparator());
            return (T) pOSCurrenciesDTO;
        }
        return null;
    }

}
