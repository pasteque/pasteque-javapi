package org.pasteque.api.model.customers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;

import org.pasteque.api.dto.customers.POSCustomersDTO;
import org.pasteque.api.dto.customers.CustomersDetailDTO;
import org.pasteque.api.dto.customers.CustomersIndexDTO;
import org.pasteque.api.dto.customers.CustomersLightDTO;
import org.pasteque.api.dto.customers.CustomersParentDTO;
import org.pasteque.api.dto.customers.POSDiscountProfilDTO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.TaxCategories;
import org.pasteque.api.model.products.Taxes;
import org.pasteque.api.model.saleslocations.Locations;
import javax.persistence.ConstructorResult;

//import com.google.common.collect.Ordering;


@SqlResultSetMappings({
        @SqlResultSetMapping(name = "CustomersLightDTO", classes = @ConstructorResult(targetClass = CustomersLightDTO.class, columns = {
                @ColumnResult(name = "id", type = String.class), @ColumnResult(name = "searchkey", type = String.class),
                @ColumnResult(name = "firstName", type = String.class), @ColumnResult(name = "lastName", type = String.class),
                @ColumnResult(name = "phone", type = String.class), @ColumnResult(name = "zipCode", type = String.class),
                @ColumnResult(name = "city", type = String.class), @ColumnResult(name = "locationLabel", type = String.class),
                @ColumnResult(name = "discountProfilLabel", type = String.class) })),
        @SqlResultSetMapping(name = "CustomersDetailDTO", classes = @ConstructorResult(targetClass = CustomersDetailDTO.class, columns = {
                @ColumnResult(name = "id", type = String.class), @ColumnResult(name = "searchKey", type = String.class),
                @ColumnResult(name = "firstName", type = String.class), @ColumnResult(name = "lastName", type = String.class),
                @ColumnResult(name = "address1", type = String.class), @ColumnResult(name = "address2", type = String.class),
                @ColumnResult(name = "zipCode", type = String.class), @ColumnResult(name = "insee", type = String.class),
                @ColumnResult(name = "city", type = String.class), @ColumnResult(name = "country", type = String.class),
                @ColumnResult(name = "phone1", type = String.class), @ColumnResult(name = "phone2", type = String.class),
                @ColumnResult(name = "email", type = String.class), @ColumnResult(name = "dateOfBirth", type = Date.class),
                @ColumnResult(name = "creationDate", type = Date.class), @ColumnResult(name = "lastUpdate", type = Date.class),
                @ColumnResult(name = "newsletter", type = boolean.class), @ColumnResult(name = "partenaires", type = boolean.class),
                @ColumnResult(name = "civilite", type = String.class), @ColumnResult(name = "doublon", type = String.class),
                @ColumnResult(name = "parentsearchkey", type = String.class), @ColumnResult(name = "locationId", type = String.class),
                @ColumnResult(name = "locationLabel", type = String.class), @ColumnResult(name = "discountProfileName", type = String.class) })) })
@Entity
public class Customers implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String SEARCH_KEY = "searchKey";
        public static final String TAX = "tax";
        public static final String NAME = "name";
        public static final String TAX_CATEGORY = "taxCategory";
        public static final String DISCOUNT_PROFILE = "discountProfile";
        public static final String CARD = "card";
        public static final String MAX_DEBT = "maxDebt";
        public static final String ADDRESS = "address";
        public static final String ADDRESS2 = "address2";
        public static final String ZIP_CODE = "zipCode";
        public static final String INSEE = "insee";
        public static final String CITY = "city";
        public static final String REGION = "region";
        public static final String COUNTRY = "country";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String EMAIL = "email";
        public static final String PHONE = "phone";
        public static final String PHONE2 = "phone2";
        public static final String FAX = "fax";
        public static final String NOTES = "notes";
        public static final String VISIBLE = "visible";
        public static final String CURRENT_DATE = "currentDate";
        public static final String CURRENT_DEBT = "currentDebt";
        public static final String PRE_PAID = "prePaid";
        public static final String TICKETS = "tickets";
        public static final String CREATION_DATE = "dateCreation";
        public static final String LAST_UPDATE = "dateUpdate";
        public static final String ID_PARENT = "idParent";
        public static final String LOCATION = "location";
    }
    
    // Le nom des préfixes connus pour les csv
    public class CustColumns {
        public static final String NOM = "Nom";
        public static final String PRENOM = "Prenom";
        public static final String CODEMAG = "CodeMagasinOrigine";
        public static final String CODECLIENT = "CodeClient";
        public static final String CODECARTE = "NumCarte";
        public static final String CREATION = "DateInscription";
        public static final String MODIFIACTION = "DateModification";
        public static final String MODIFICATION = "DateModification";
        public static final String ZIPCODE = "CodePostal";
        public static final String CITY = "Ville";
        public static final String COUNTRY = "Pays";
        public static final String ADDRESS = "Adresse";
        public static final String ADDRESS2 = "ComplementAdresse";
        public static final String MOBILE = "TelephonePortable";
        public static final String PHONE = "TelephoneFixe";
        public static final String BIRTH = "DateNaissance";
        public static final String MAIL = "Email";
        public static final String CIV = "Civilite";
        public static final String NEWS = "InscNewsletter";
        public static final String PART = "OffrePartenaire";
        public static final String TYPE = "TypeContact";
        public static final String SOURCE = "SourceContact";
        public static final String CODE = "CodeClient";
        public static final String DOUBLON = "Parent";
        public static final String IDCLIENT = "IdClient";
        public static final String PROFIL = "Profil";
        public static final String INSEE = "Insee";
        public static final String CODEMAGRAT = "CodeMagasinRattachement";
        public static final String MAGRAT = "MagasinRattachement";
        public static final String CODEPARENT = "CodeParent";
        public Map<Integer, String> columns;
    }

    @Id
    private String id;
    private String searchKey;
    @ManyToOne
    @JoinColumn(name = "taxid")
    private Taxes tax;
    private String name;
    @ManyToOne
    @JoinColumn(name = "taxcategory")
    private TaxCategories taxCategory;
    @ManyToOne
    @JoinColumn(name = "discountprofile_id")
    private DiscountProfil discountProfil;
    private String card;
    private double maxDebt;
    private String address;
    private String address2;
    @Column(name = "postal")
    private String zipCode;
    private String insee;
    private String city;
    private String region;
    private String country;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String phone2;
    private String fax;
    private String notes;
    private boolean visible;
    @Column(name = "curdate")
    private Date currentDate;
    @Column(name = "curdebt")
    private Double currentDebt;
    private double prePaid;
    private Date dateOfBirth;
    @Column(name = "CREATION_DATE")
    private Date dateCreation;
    @Column(name = "LAST_UPDATE")
    private Date dateUpdate;
    private String type;
    private String source;
    private boolean newsletter;
    private boolean partenaires;
    private String civilite;
    @ManyToOne()
    // @JoinFetch(JoinFetchType.INNER)
    @JoinColumn(name = "location_id")
    private Locations location;

    @Column(name = "ID_PARENT")
    private String idParent;

//    @OneToMany(mappedBy = Tickets.Fields.CUSTOMER, fetch = FetchType.LAZY)
//    private List<Tickets> tickets;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PARENT")
    private List<Customers> customers;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public Taxes getTax() {
        return tax;
    }

    public void setTax(Taxes tax) {
        this.tax = tax;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TaxCategories getTaxCategory() {
        return taxCategory;
    }

    public void setTaxCategory(TaxCategories taxCategory) {
        this.taxCategory = taxCategory;
    }

    public DiscountProfil getDiscountProfil() {
        return discountProfil;
    }

    public void setDiscountProfil(DiscountProfil discountProfile) {
        this.discountProfil = discountProfile;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public double getMaxDebt() {
        return maxDebt;
    }

    public void setMaxDebt(double maxDebt) {
        this.maxDebt = BigDecimal.valueOf(maxDebt).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getInsee() {
        return insee;
    }

    public void setInsee(String insee) {
        this.insee = insee;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName != null) {
            firstName = firstName.trim();
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName != null) {
            lastName = lastName.trim();
        }
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    public Double getCurrentDebt() {
        return currentDebt;
    }

    public void setCurrentDebt(Double currentDebt) {
        this.currentDebt = BigDecimal.valueOf(currentDebt).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public double getPrePaid() {
        return prePaid;
    }

    public void setPrePaid(double prePaid) {
        this.prePaid = BigDecimal.valueOf(prePaid).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

//    public List<Tickets> getTickets() {
//        return tickets;
//    }
//
//    public List<Tickets> getOrderedTickets() {
//        Comparator<Tickets> comparator = new Comparator<Tickets>() {
//            @Override
//            public int compare(Tickets tickets, Tickets tickets1) {
//                return tickets1.getReceipts().getDateNew().compareTo(tickets.getReceipts().getDateNew());
//            }
//        };
//        List<Tickets> retour = getTickets();
//        retour = getTickets().stream().collect(Collectors.toList());
//        retour.sort(comparator);
//        return retour;
//    }
//
//    public void setTickets(List<Tickets> tickets) {
//        this.tickets = tickets;
//    }

    public Locations getLocation() {
        return location;
    }

    public void setLocation(Locations location) {
        this.location = location;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSCustomersDTO.class)) {
            POSCustomersDTO pOSCustomersDTO = new POSCustomersDTO();
            pOSCustomersDTO.setId(getId());
            pOSCustomersDTO.setNumber(getCard());
            pOSCustomersDTO.setKey(getSearchKey());
            pOSCustomersDTO.setDispName(getName());
            pOSCustomersDTO.setCard(getCard());
            if (getTaxCategory() != null) {
                pOSCustomersDTO.setCustTaxId(getTaxCategory().getId());
            }
            pOSCustomersDTO.setDiscountProfile(getDiscountProfil() == null ? null : getDiscountProfil().getAdapter(POSDiscountProfilDTO.class));
            pOSCustomersDTO.setDiscountProfileId(getDiscountProfil() == null ? null : getDiscountProfil().getId());
            pOSCustomersDTO.setPrepaid(getPrePaid());
            pOSCustomersDTO.setMaxDebt(getMaxDebt());
            pOSCustomersDTO.setCurrDebt(getCurrentDebt());
            pOSCustomersDTO.setDebtDate(getCurrentDate());
            pOSCustomersDTO.setFirstName(getFirstName());
            pOSCustomersDTO.setLastName(getLastName());
            pOSCustomersDTO.setEmail(getEmail());
            pOSCustomersDTO.setPhone1(getPhone());
            pOSCustomersDTO.setPhone2(getPhone2());
            pOSCustomersDTO.setFax(getFax());
            pOSCustomersDTO.setAddr1(getAddress());
            pOSCustomersDTO.setAddr2(getAddress2());
            pOSCustomersDTO.setZipCode(getZipCode());
            pOSCustomersDTO.setInsee(getInsee());
            pOSCustomersDTO.setCity(getCity());
            pOSCustomersDTO.setRegion(getRegion());
            pOSCustomersDTO.setCountry(getCountry());
            pOSCustomersDTO.setNote(getNotes());
            pOSCustomersDTO.setVisible(isVisible());
            pOSCustomersDTO.setDateOfBirth(getDateOfBirth());
            pOSCustomersDTO.setType(getType());
            pOSCustomersDTO.setSource(getSource());
            pOSCustomersDTO.setNewsletter(isNewsletter());
            pOSCustomersDTO.setPartenaires(isPartenaires());
            pOSCustomersDTO.setCivilite(getCivilite());
            pOSCustomersDTO.setDateCreation(getDateCreation());
            pOSCustomersDTO.setDateUpdate(getDateUpdate());
            pOSCustomersDTO.setIdParent(getIdParent());
            pOSCustomersDTO.setLocation(getLocation() == null ? null : getLocation().getAdapter(LocationsInfoDTO.class));
            return (T) pOSCustomersDTO;
        } else if (selector.equals(CustomersDetailDTO.class)) {
            CustomersDetailDTO customersDTO = new CustomersDetailDTO();
            customersDTO.setId(getId());
            customersDTO.setCardNumber(getCard());
            customersDTO.setSearchKey(getSearchKey());
            customersDTO.setFirstName(getFirstName());
            customersDTO.setLastName(getLastName());
            customersDTO.setEmail(getEmail());
            customersDTO.setPhone1(getPhone());
            customersDTO.setPhone2(getPhone2());
            customersDTO.setAddress1(getAddress());
            customersDTO.setAddress2(getAddress2());
            customersDTO.setZipCode(getZipCode());
            customersDTO.setInsee(getInsee());
            customersDTO.setCity(getCity());
            customersDTO.setCountry(getCountry());
            customersDTO.setNote(getNotes());
            customersDTO.setDateOfBirth(getDateOfBirth());
            customersDTO.setType(getType());
            customersDTO.setSource(getSource());
            customersDTO.setNewsletter(isNewsletter());
            customersDTO.setPartenaires(isPartenaires());
            customersDTO.setCivilite(getCivilite());
            customersDTO.setCreationDate(getDateCreation());
            customersDTO.setLastUpdate(getDateUpdate());
            customersDTO.setIdParent(getIdParent());
            customersDTO.setLocation(getLocation() == null ? null : getLocation().getAdapter(LocationsInfoDTO.class));
            customersDTO.setDiscountProfile(getDiscountProfil() == null ? null : getDiscountProfil().getAdapter(POSDiscountProfilDTO.class));
//            if (!filtered) {
//
//                if (getCustomers().size() > 0) {
//                    List<Tickets> allTickets = new ArrayList<Tickets>();
//                    allTickets.addAll(getTickets());
//                    for (Customers customer : getCustomers()) {
//                        allTickets.addAll(customer.getTickets());
//                    }
//                    this.setTickets(allTickets);
//                    customersDTO.setTickets(AdaptableHelper.getAdapter(getOrderedTickets(), TicketsHistoryDTO.class));
//                    int size = getTickets().size();
//                    for (TicketsHistoryDTO dto : customersDTO.getTickets()) {
//                        dto.setTicketNumber(size--);
//                    }
//                } else {
//                    customersDTO.setTickets(AdaptableHelper.getAdapter(getOrderedTickets(), TicketsHistoryDTO.class));
//                    int size = getTickets().size();
//                    for (TicketsHistoryDTO dto : customersDTO.getTickets()) {
//                        dto.setTicketNumber(size--);
//                    }
//                }
//            }

            if (!filtered && getCustomers().size() > 0) {
                customersDTO.setDoublon("Parent");
            } else if (getIdParent() != null) {
                customersDTO.setDoublon(getIdParent());
            } else {
                customersDTO.setDoublon("");
            }

            if (!filtered) {
                customersDTO.setCustomers(AdaptableHelper.getAdapter(getCustomers(), CustomersDetailDTO.class));
            }

            if (getIdParent() != null) {
                // TODO voir
                customersDTO.setCustomerParent(getCustomerParentDto());
            }

            return (T) customersDTO;
        } else if (selector.equals(CustomersIndexDTO.class)) {
            CustomersIndexDTO customersIndexDTO = new CustomersIndexDTO();
            customersIndexDTO.setId(getSearchKey());
            customersIndexDTO.setNom(getLastName());
            customersIndexDTO.setPrenom(getFirstName());
            customersIndexDTO.setNumeroCarte(getCard());
            customersIndexDTO.setTelephone(getPhone());
            customersIndexDTO.setEmail(getEmail());
            customersIndexDTO.setDateNaissance(getDateOfBirth());
            //customersIndexDTO.setCommune(getCustomersManager().getInsee(InseeSimpleDTO.class));
            //EDU 2020 09 La seule fois où on en a besoin c'est pour le TicketIndexDTO 
            //et il est obtenu via une requête native sinon customerService.getinsee
            return (T) customersIndexDTO;

        }
        return null;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isNewsletter() {
        return newsletter;
    }

    public void setNewsletter(boolean newsletter) {
        this.newsletter = newsletter;
    }

    public boolean isPartenaires() {
        return partenaires;
    }

    public void setPartenaires(boolean partenaires) {
        this.partenaires = partenaires;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public String getIdParent() {
        return idParent;
    }

    public void setIdParent(String idParent) {
        this.idParent = idParent;
    }

    public List<Customers> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customers> customers) {
        this.customers = customers;
    }

//    public void addTickets(Tickets ticket) {
//        tickets.add(ticket);
//    }
//
//    public void removeTickets(String ticketId) {
//        int index = -1;
//        if (ticketId != null) {
//            for (int i = getTickets().size(); i > 0;) {
//                if (getTickets().get(--i).getId().equals(ticketId)) {
//                    index = i;
//                    break;
//                }
//            }
//        }
//        if (index >= 0) {
//            tickets.remove(index);
//        }
//    }

    // TODO - Voir comment modifier
    public CustomersParentDTO getCustomerParentDto() {

        CustomersParentDTO customerParent = new CustomersParentDTO();
        customerParent.setId(getId());
        customerParent.setSearchKey(getSearchKey());
        customerParent.setIdParent(getIdParent());
        // PGA : Recherche client avec son id et si touve le retourne sous forme d'objet Customers
        // afin d'obtenir son numéro client (searchKey) pour afficher la liaison client parent/enfant
        // en utilisant le numéro client
        // stomerParent.setSearchKeyParent(getCustomersManager().findParentSearchKey(getIdParent()));
        customerParent.setSearchKeyParent(null);
        return customerParent;

    }

}
