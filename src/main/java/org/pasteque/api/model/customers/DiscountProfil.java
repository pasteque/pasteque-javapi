package org.pasteque.api.model.customers;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.pasteque.api.dto.customers.POSDiscountProfilDTO;
import org.pasteque.api.model.adaptable.IAdaptable;


@Entity
@Table(name = DiscountProfil.Table.NAME)
public class DiscountProfil implements IAdaptable {

   public final class Table {
	        public static final String NAME = "discountprofiles";
	}
    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String RATE = "rate";
        public static final String DISPLAY_ORDER = "displayOrder";
    }

    @Id
    private Long id;
    private String name;
    private double rate;
    @Column(name = "disporder")
    private Integer displayOrder;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSDiscountProfilDTO.class)) {

            POSDiscountProfilDTO pOSDiscountProfilDTO = new POSDiscountProfilDTO();
            pOSDiscountProfilDTO.setId(String.valueOf(getId()));
            pOSDiscountProfilDTO.setLabel(getName());
            pOSDiscountProfilDTO.setRate(String.valueOf(getRate()));
            pOSDiscountProfilDTO.setDisporder(String.valueOf(getDisplayOrder()));
            return (T) pOSDiscountProfilDTO;
        }
        return null;
    }

}
