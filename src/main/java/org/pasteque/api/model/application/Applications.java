package org.pasteque.api.model.application;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.pasteque.api.model.adaptable.IAdaptable;

import org.pasteque.api.dto.application.POSVersionDTO;

@Entity
public class Applications implements IAdaptable {

	public final class Fields {
		public static final String ID = "id";
		public static final String NAME = "name";
		public static final String VERSION = "version";
	}
	
	public final class Type {
	        public static final String POS_PASTEQUE = "pasteque";
	        public static final String SERVER_OSE = "ose-server";
	    }

	@Id
	private String id;
	private String name;
	private String version;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public <T> T getAdapter(Class<T> selector) {
	       return getAdapter(selector , false);
	}

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSVersionDTO.class)) {
            POSVersionDTO pOSVersionDTO = new POSVersionDTO();
            pOSVersionDTO.setVersion(getVersion());
            pOSVersionDTO.setLevel(getVersion());
            return (T) pOSVersionDTO;
        }
        return null;
    }

}
