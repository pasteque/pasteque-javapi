package org.pasteque.api.model.application;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = PosMessageType.Table.NAME)
public class PosMessageType {

    public final class Table {
        public static final String NAME = "MESSAGES_TYPES";
    }
    public final class Fields {
        public static final String TYPE = "type";
        public static final String DESCRIPTION = "description";
    }
    
    public final class Values {
        public static final String EMPL = "EMPL";
        public static final String INFO = "INFO";
        public static final String ALERT = "ALERT";
        public static final String RENFLOUEMENT = "RENFLOUEMENT";
        public static final String PREPARATION = "PREPARATION";
    }
    
    @Id
    @Column(name = "type")
    private String type;
    @Column(name = "description")
    private String description;
    
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
