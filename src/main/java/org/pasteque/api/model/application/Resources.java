package org.pasteque.api.model.application;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.pasteque.api.dto.application.POSResourcesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;

import com.mysql.cj.util.Base64Decoder;


@Entity
public class Resources implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String RESTYPE = "restype";
        public static final String CONTENT = "content";
    }

    @Id
    private String id;
    private String name;
    private int restype;
    private byte[] content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRestype() {
        return restype;
    }

    public void setRestype(int restype) {
        this.restype = restype;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
    
    public boolean loadFromDTO(Object source) {
        boolean retour = false;
        byte[] tmpContent = null;
        
        if (POSResourcesDTO.class.isInstance(source)) {
            POSResourcesDTO dto = (POSResourcesDTO) source;
            if(dto.getId() != null) {
                setId(dto.getId());
            }
            if((getName() == null && dto.getLabel() != null) || !getName().equals(dto.getLabel())  ){
                retour = true;
                setName(dto.getLabel());
            }
            if(dto.getType() != getRestype()){
                retour = true;
                setRestype(dto.getType());
            }
            if(getRestype() == 0) {
                tmpContent = ((String) dto.getContent()).getBytes(StandardCharsets.UTF_8);
            } else {
                byte[] imageContent = ((String) dto.getContent()).getBytes(StandardCharsets.UTF_8);
                tmpContent = Base64Decoder.decode(imageContent, 0, imageContent.length);
            }
            if((getContent() == null && tmpContent != null) || !Arrays.equals(getContent(), tmpContent)){
                retour = true;
                setContent(tmpContent);
            }
        }
        return retour;
    }
    
    @Override
    public <T> T getAdapter(Class<T> selector) {
           return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector , boolean filtered) {
        if (selector.equals(POSResourcesDTO.class)) {
            POSResourcesDTO pOSResourcesDTO = new POSResourcesDTO();
            pOSResourcesDTO.setId(getId());
            pOSResourcesDTO.setLabel(getName());
            pOSResourcesDTO.setType(getRestype());
            if (getContent() != null) {
                if (getRestype() == 0) {
                    pOSResourcesDTO.setContent(new String(getContent() ,StandardCharsets.UTF_8 ));
                } else {
                    pOSResourcesDTO.setContent(getContent());
                }
            }
            return (T) pOSResourcesDTO;
        }
        return null;
    }
}
