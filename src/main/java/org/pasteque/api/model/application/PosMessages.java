package org.pasteque.api.model.application;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.pasteque.api.dto.application.POSMessagesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.security.People;

@Entity
@Table(name = PosMessages.Table.NAME)
public class PosMessages implements IAdaptable {

    public final class Table {
        public static final String NAME = "MESSAGES";
    }

    public final class Fields {
        public static final String ID = "id";
        public static final String START_DATE = "startDate";
        public static final String END_DATE = "endDate";
        public static final String TYPE = "type";
        public static final String CONTENT = "content";

        public static final String LOCATIONS = "location";
        public static final String PEOPLE = "people";
        public static final String PRODUCTS = "product";
    }

    @Id
    private String id;

    @Column(name = "debut")
    private Date startDate;
    @Column(name = "fin")
    private Date endDate;

    @Column(name = "contenu")
    private String content;

    @ManyToOne
    @JoinColumn(name = PosMessageType.Fields.TYPE)
    private PosMessageType type;

    @ManyToOne()
    @JoinColumn(name = "location")
    private Locations location;

    @ManyToOne()
    @JoinColumn(name = "people")
    private People people;

    @ManyToOne()
    @JoinColumn(name = "product")
    private Products product;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public PosMessageType getType() {
        return type;
    }

    public void setType(PosMessageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Locations getLocation() {
        return location;
    }

    public void setLocation(Locations location) {
        this.location = location;
    }

    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSMessagesDTO.class)) {
            POSMessagesDTO dto = new POSMessagesDTO();
            dto.setId(getId());
            dto.setContent(getContent());
            dto.setStartDate(getStartDate());
            dto.setEndDate(getEndDate());
            dto.setType(getType().getType());
            if (getProduct() != null) {
                dto.setProductId(getProduct().getId());
            }
            if (getLocation() != null) {
                dto.setLocationId(getLocation().getId());
            }
            return (T) dto;
        }
        return null;
    }

    public boolean isAvailable() {
        Date today = new Date();
        boolean available = false;
        // si les 2 dates sont null le message est valable tous le temps...
        if (getStartDate() == null && getEndDate() == null) {
            available = true;
        }
        // s'il y a une date de début, elle doit etre avant la date du jour
        // et s'il y a une date de fin, elle doit etre apres la date du jour
        if (getStartDate() != null && getStartDate().before(today)) {
            if (getEndDate() == null || (getEndDate() != null && getEndDate().after(today))) {
                available = true;
            }
        }
        if (available) {
            // si l'intervale de temps est ok et qu'il y a pas de location, c'est que
            // le message est a destination de tt le monde
            if (getLocation() == null) {
                available = true;
                // si l'intervale de temps est ok et qu'il y a une location, il faut que ce soit celle
                // par défaut, ie le mag/camion ou on travaille...
            } else if (getLocation().isServerDefault() || Locations.SIEGE_ID.equals(getLocation().getId())) {
                available = true;
            } else {
                available = false;
            }
        }
        return available;
    }
    
    public boolean equals(Object b){
        boolean retour = false;
        if (b != null && PosMessages.class.isInstance(b)) {
            PosMessages comp = (PosMessages) b;

            //NOTE : Pas de test sur la date de départ pour les emplacements
            if (((getContent() != null && getContent().equals(comp.getContent())) 
            || (getContent() == null && comp.getContent()== null)) &&
           (getType().getType().equals(PosMessageType.Values.EMPL) || getStartDate() == comp.getStartDate()) &&
           (getEndDate() == comp.getEndDate())
                    ) {
                retour = true ;
                if(getProduct() != null && comp.getProduct() != null) {
                    retour = getProduct().getId().equals(comp.getProduct().getId());
                } else {
                    retour = (getProduct() == null && comp.getProduct() == null);
                }
                if(retour && getLocation() != null && comp.getLocation() != null) {
                    retour = getLocation().getId().equals(comp.getLocation().getId());
                } else {
                    retour = (retour && getLocation() == null && comp.getLocation() == null);
                }
                if(retour && getPeople() != null && comp.getPeople() != null) {
                    retour = getPeople().getId().equals(comp.getPeople().getId());
                } else {
                    retour = (retour && getPeople() == null && comp.getPeople() == null);
                }
                if(retour && getType() != null && comp.getType() != null) {
                    retour = getType().getType().equals(comp.getType().getType());
                } else {
                    retour = (retour && getType() == null && comp.getType() == null);
                }
            }
        }

        return retour;
    }

    /**
     * Create an id for this message object
     */
    public String createId() {
        if(getId() == null) {
            id= String.format("%d-%d-%s-%s-%s-%s", new Date().getTime() , 
                    getStartDate() == null ? "" : getStartDate().getTime() ,
                            getType().getType(),
                            getLocation() == null ? "" : getLocation().getId(),
                                    getProduct() == null ? "" : getProduct().getId() ,
                                            getPeople() == null ? "" : getPeople().getId());
            //TODO nouvelle chaine pourl'id
        }
        return getId();
        
    }

    public String getTypeId() {
        return getType() == null ? "" : getType().getType();
    }
    
    public String getLocationId() {
        return getLocation() == null ? "" : getLocation().getId();
    }
    
    public String getPeopleId() {
        return getPeople() == null ? "" : getPeople().getId();
    }
    
    public String getProductId() {
        return getProduct() == null ? "" : getProduct().getId();
    }

}
