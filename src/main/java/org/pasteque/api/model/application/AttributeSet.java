package org.pasteque.api.model.application;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = AttributeSet.Table.NAME)
public class AttributeSet {

	public final class Table {
		public static final String NAME = "attributeset";
	}

	public final class Fields {
		public static final String ID = "id";
		public static final String NAME = "name";
	}

	@Id
	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
