package org.pasteque.api.model.application;

import javax.persistence.Id;

public class ServerResponse {

	public final class Status {
		public static final String OK = "ok";
		public static final String KO = "ko";
	}

	@Id
	private String status;
	private Object content;

	public ServerResponse(Object object) {
		this.status = ServerResponse.Status.OK;
		this.content = object;
	}
	
	public ServerResponse(Object object , boolean ok) {
	        this.status = ok ? ServerResponse.Status.OK : ServerResponse.Status.KO;
	        this.content = object;
	 }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}
}
