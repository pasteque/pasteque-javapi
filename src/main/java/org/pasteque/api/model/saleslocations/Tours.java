package org.pasteque.api.model.saleslocations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.pasteque.api.dto.locations.POSSalesLocationsDTO;
import org.pasteque.api.dto.locations.TourIndexDTO;
import org.pasteque.api.dto.locations.ToursDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.adaptable.IAdaptable;

@Entity
@Table(name = Tours.Table.NAME)
public class Tours implements IAdaptable{
    public final class Table {
        public static final String NAME = "TOURNEES";
    }
    public final class Fields {
        public static final String TOUR_ID = "id";
        public static final String TOUR_NAME = "name";
        
        public static final String SALES_LOCATIONS = "salesLocations";
        
        public static final String STOCK_LOCATION = "stockLocations";
    }
    
    public final class SearchCritria {
        public static final String FROM = "from";
        public static final String TO = "to";
        public static final String LOCATION = "locationId";
    }
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = SalesLocations.Fields.TOUR, orphanRemoval = false)
    private List<SalesLocations> salesLocations;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = ToursLocations.Fields.TOUR, orphanRemoval = false)
    private List<ToursLocations> stockLocations;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "nom")
    private String name;
    

    public List<SalesLocations> getSalesLocations() {
        return salesLocations;
    }
    
    public List<SalesLocations> getSalesLocations(boolean filtered) {
        if(filtered) {
            return getFilteredSalesLocations();
        } else {
            return salesLocations;
        }
    }
    
    public List<SalesLocations> getFilteredSalesLocations() {
        List<SalesLocations> filteredSalesLocations = new ArrayList<SalesLocations>();
        Calendar cal = new GregorianCalendar();
        Date filteredStartDate , filteredEndDate;
        
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.add(Calendar.DATE, -1);
        filteredStartDate = cal.getTime();
        cal.add(Calendar.DATE, 3);
        filteredEndDate = cal.getTime();
        
        for(SalesLocations element : salesLocations) {
            if(element.getStartDate().after(filteredStartDate) && element.getStartDate().before(filteredEndDate)) {
                filteredSalesLocations.add(element);
            }
        }
        return filteredSalesLocations;
    }

    public void setSalesLocations(List<SalesLocations> salesLocations) {
        this.salesLocations = salesLocations;
    }

    public long getId() {
        return id == null ? 0L : id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ToursLocations> getStockLocations() {
        return stockLocations;
    }

    public void setStockLocations(List<ToursLocations> stockLocations) {
        this.stockLocations = stockLocations;
    }
    
    @Override
    public <T> T getAdapter(Class<T> selector) {
           return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector , boolean filtered) {
        if (selector.equals(ToursDTO.class)) {
            ToursDTO toursDTO = new ToursDTO();
            toursDTO.setId(this.id);
            toursDTO.setName(this.name);
            if(this.getSalesLocations(filtered) != null) {
              //TODO EDU - Attention l'information sur la prochaine date de déballage à cet endroit n'est pas disponible par cet adapter
                toursDTO.setSalesLocations(AdaptableHelper.getAdapter(this.getSalesLocations(filtered) , POSSalesLocationsDTO.class));
            }
            return (T) toursDTO;
        }
        if (selector.equals(TourIndexDTO.class)) {
            TourIndexDTO toursDTO = new TourIndexDTO();
            toursDTO.setId(this.id);
            toursDTO.setNom(this.name);
            return (T) toursDTO;
        }
        return null;
    }
    
}
