package org.pasteque.api.model.saleslocations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.pasteque.api.dto.locations.POSSalesLocationsDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.cash.ClosedCash;
import org.springframework.data.annotation.Transient;

@Entity
@Table(name = SalesLocations.Table.NAME)
public class SalesLocations implements IAdaptable {
    public final class Table {
        public static final String NAME = "DEBALLAGES";
    }

    public final class Fields {
        public static final String SALES_LOCATION_ID = "id";
        public static final String INSEE = "insee";
        public static final String CITY = "city";
        public static final String START_DATE = "startDate";
        public static final String END_DATE = "endDate";
        public static final String PUB_NB = "pubNb";
        public static final String FEE = "fee";
        public static final String PAID = "paid";

        public static final String TOUR = "tour";

        public static final String SALES_SESSIONS = "salesSessions";
    }
    public final class SearchCritria {
        public static final String FROM = "from";
        public static final String TO = "to";
        public static final String REFERENCE = "reference";
        public static final String LOCATION = "location";
        public static final String ZIP = "zip";
        public static final String STARTBEFORE = "startBefore";
        public static final String ENDAFTER = "endAfter";
    }

    @Id
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "tournees_id")
    private Tours tour;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = ClosedCash.Fields.SALES_LOCATION, orphanRemoval = false)
    private List<ClosedCash> salesSessions;

    @ManyToOne()
    @JoinColumn(name = "num_insee")
    private Insee insee;
    @Column(name = "commune")
    private String city;
    @Column(name = "debut")
    private Date startDate;
    @Column(name = "fin")
    private Date endDate;
    @Column(name = "nb_pub")
    private int pubNb;
    @Column(name = "droit_place")
    private double fee;
    @Column(name = "paye")
    private boolean paid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tours getTour() {
        return tour;
    }

    public void setTour(Tours tour) {
        this.tour = tour;
    }

    @Transient
    public String getInseeNum() {
        return insee == null ? null : insee.getInseeNum();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getPubNb() {
        return pubNb;
    }

    public void setPubNb(int pubNb) {
        this.pubNb = pubNb;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public List<ClosedCash> getSalesSessions() {
        return salesSessions;
    }
    
    public String toString() {
        return getInseeNum().concat("-").concat(getCity());
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSSalesLocationsDTO.class)) {
            POSSalesLocationsDTO pOSSalesLocationsDTO = new POSSalesLocationsDTO();
            pOSSalesLocationsDTO.setCity(city);
            pOSSalesLocationsDTO.setEndDate(endDate);
            pOSSalesLocationsDTO.setFee(fee);
            pOSSalesLocationsDTO.setId(id);
            pOSSalesLocationsDTO.setInseeNb(getInseeNum());
            pOSSalesLocationsDTO.setPaid(paid);
            pOSSalesLocationsDTO.setPubNb(pubNb);
            pOSSalesLocationsDTO.setStartDate(startDate);
            List<String> salesSessionsIdList = new ArrayList<String>();
            for (ClosedCash cC : salesSessions) {
                salesSessionsIdList.add(cC.getMoney());
            }
            pOSSalesLocationsDTO.setSalesSessions(salesSessionsIdList);
            if( tour != null) {
                pOSSalesLocationsDTO.setTourId(tour.getId());
                pOSSalesLocationsDTO.setTourName(tour.getName());
            }
            return (T) pOSSalesLocationsDTO;
        } 
//        else if (selector.equals(SalesLocationsDetailDTO.class)) {
//            return (T) getSalesLocationsManager().getSalesLocationsDetail();
//        } 
//        else if (selector.equals(SalesLocationIndexDTO.class)) {
//        return (T) getSalesLocationsManager().getSalesLocationsIndex();
//        }
        return null;
    }

    public Insee getInsee() {
        return insee;
    }

    public void setInsee(Insee insee) {
        this.insee = insee;
    }

    public void  createId() {
        Calendar cal = Calendar.getInstance();
       
        Date dateTournee = this.startDate;
        cal.setTime(dateTournee);
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = String.valueOf(cal.get(Calendar.MONTH) +1 ); // parce que les mois commencent à 0. 0 Janvier -> 12 Décembre
        month= month.length()== 2 ? month:"0".concat(month);
        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        day= day.length()== 2 ? day:"0".concat(day);
        String hour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
        hour= hour.length()== 2 ? hour:"0".concat(hour);
        String idTournee = String.valueOf(this.getTour().getId());
        idTournee= idTournee.length()== 2 ? idTournee:"0".concat(idTournee);
        String annee2Chiffre = year.substring(2);
        
        String idDeballage  = annee2Chiffre.concat(month)
                        .concat(String.valueOf(day))
                        .concat(String.valueOf(hour))
                        .concat(String.valueOf(idTournee));
        
        this.setId(Long.valueOf(idDeballage));
        
    }

}
