package org.pasteque.api.model.saleslocations;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.locations.TourIndexDTO;
import org.pasteque.api.dto.locations.POSToursLocationsDTO;
import org.pasteque.api.model.adaptable.IAdaptable;

@Entity
@Table(name = ToursLocations.Table.NAME)
public class ToursLocations implements IAdaptable{
    public final class Table {
        public static final String NAME = "LOCATIONS_TOURNEES";
    }
    public final class Fields {
        public static final String ID = "id";
        public static final String START_DATE = "startDate";
        public static final String END_DATE = "endDate";
        
        public static final String TOUR = "tour";
        public static final String LOCATION = "location";
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "debut")
    private Date startDate;
    @Column(name = "fin")
    private Date endDate;
    
    @ManyToOne()
    @JoinColumn(name = "location_id")
    private Locations location;
    
    @ManyToOne()
    @JoinColumn(name = "tournees_id")
    private Tours tour;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Locations getLocation() {
        return location;
    }

    public void setLocation(Locations location) {
        this.location = location;
    }

    public Tours getTour() {
        return tour;
    }

    public void setTour(Tours tour) {
        this.tour = tour;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSToursLocationsDTO.class)) {
            POSToursLocationsDTO pOSToursLocationsDTO = new POSToursLocationsDTO();
            pOSToursLocationsDTO.setId(this.id);
            pOSToursLocationsDTO.setStartDate(this.startDate);
            pOSToursLocationsDTO.setEndDate(this.endDate);
            pOSToursLocationsDTO.setLocation(this.location.getAdapter(LocationsInfoDTO.class));
            pOSToursLocationsDTO.setTour(this.getTour().getAdapter(TourIndexDTO.class));
            return (T) pOSToursLocationsDTO;
        }
        return null;
    }
    
    
}
