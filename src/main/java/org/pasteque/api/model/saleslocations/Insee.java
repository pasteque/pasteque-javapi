package org.pasteque.api.model.saleslocations;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.pasteque.api.model.adaptable.IAdaptable;


@Entity
@Table(name = Insee.Table.NAME)
public class Insee implements IAdaptable {
    public final class Table {
        public static final String NAME = "INSEE";
    }
    public final class Fields {
        public static final String INSEE = "inseeNum";
        public static final String ZIPCODE = "zipCode";
        public static final String COMMUNE = "commune";        
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String LAST_UPDATE = "lastUpdate";
    }

    @Id
    private String id;
    @Column(name = "num_insee")
    private String inseeNum;
    private String zipCode;
    private String commune;
    private Double latitude;
    private Double longitude;
    private Date lastUpdate;
    private String codeRegion;
    private String nomRegion;
    private String codeDepartement;
    private String nomDepartement;
    private Double surface;
    private Integer population;
    private String etat;

    public String getEtat() {
        return etat;
    }
    public void setEtat(String etat) {
        this.etat = etat;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getCodeRegion() {
        return codeRegion;
    }
    public void setCodeRegion(String codeRegion) {
        this.codeRegion = codeRegion;
    }
    public String getNomRegion() {
        return nomRegion;
    }
    public void setNomRegion(String nomRegion) {
        this.nomRegion = nomRegion;
    }
    public String getCodeDepartement() {
        return codeDepartement;
    }
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }
    public String getNomDepartement() {
        return nomDepartement;
    }
    public void setNomDepartement(String nomDepartement) {
        this.nomDepartement = nomDepartement;
    }
    public Double getSurface() {
        return surface;
    }
    public void setSurface(Double surface) {
        this.surface = surface;
    }
    public Integer getPopulation() {
        return population;
    }
    public void setPopulation(Integer population) {
        this.population = population;
    }
    public String getInseeNum() {
        return inseeNum;
    }
    public void setInseeNum(String insee) {
        this.inseeNum = insee;
    }
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    public String getCommune() {
        return commune;
    }
    public void setCommune(String commune) {
        this.commune = commune;
    }
    public Double getLatitude() {
        return latitude;
    }
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
    public Double getLongitude() {
        return longitude;
    }
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
    
    public SalesLocationsManager getSalesLocationsManager() {
        return new SalesLocationsManager(this);
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(org.pasteque.api.dto.locations.InseeSimpleDTO.class)) {
            return (T) getSalesLocationsManager().getInseeSimple();
        }
        if (selector.equals(org.pasteque.api.dto.locations.POSInseeDTO.class)) {
            return (T) getSalesLocationsManager().getInseePOS();
        }
        if (selector.equals(org.pasteque.api.dto.locations.InseeDTO.class)) {
            return (T) getSalesLocationsManager().getInsee();
        }
        return null;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public double distance(Insee b) {
        double retour = 0D ;
        if(b.getLatitude() != null && b.getLongitude() != null && getLatitude() != null && getLongitude() != null) {
            retour = Math.acos(Math.sin(Math.toRadians(getLatitude()))*Math.sin(Math.toRadians(b.getLatitude())) + Math.cos(Math.toRadians(getLatitude()))*Math.cos(Math.toRadians(b.getLatitude()))*Math.cos(Math.toRadians(getLongitude()-b.getLongitude())) ) ;
        }
        return retour;
    }

    public String toString() {
        return String.format("%s-%s-%s", this.inseeNum , this.zipCode , this.commune);
    }


}
