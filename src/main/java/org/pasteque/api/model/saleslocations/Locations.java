package org.pasteque.api.model.saleslocations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.pasteque.api.dto.locations.POSLocationsDTO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.locations.POSLocationsUrlDTO;
import org.pasteque.api.dto.locations.TourIndexDTO;
import org.pasteque.api.dto.locations.POSToursLocationsDTO;
import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Categories;
import org.pasteque.api.model.products.TaxCustCategories;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;



@Entity
public class Locations implements IAdaptable {
    
    public static final String SIEGE_ID = "0";

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String ADDRESS = "address";

        public static final String MESSAGES = "posMessages";
        public static final String TOURS = "tours";

        public static final String TAX_CUST_CATEGORY = "taxCustCategory";

        public static final String PARENT = "parent";
        public static final String CHILDREN = "children";
        public static final String INSEE = "insee";
        public static final String DEFAULT = "serverDefault";
        public static final String CATEGORY = "category";
        public static final String URL = "url";
        public static final String ETAT = "etat";

    }
    
    public final class Etats {
        public static final String VIRTUEL = "VIRTUEL";
        public static final String FERME = "FERME";
        public static final String ACTIF = "ACTIF";
    }

    @Id
    private String id;
    private String name;
    private String address;
// EDU 2020 07 8S - On vire le lien PosMessage - non utilisé
//    @OneToMany(mappedBy = PosMessages.Fields.LOCATIONS, fetch = FetchType.LAZY)
//    private List<PosMessages> posMessages;
    @OneToMany(mappedBy = Locations.Fields.PARENT)
    private List<Locations> children;

    //EDU - TODO Evaluer la perte si on fait sauter - 
    //utilisation anecdotique ( obtenir le tourname du premier élément de la liste ) dans la caisse
    //Valeur en plus pas convenable
    @OneToMany(mappedBy = ToursLocations.Fields.LOCATION, fetch = FetchType.LAZY)
    @OrderBy(ToursLocations.Fields.START_DATE + " DESC")
    private List<ToursLocations> tours;
    @ManyToOne
    @JoinColumn(name = "TAXCUSTCATEG_ID")
    private TaxCustCategories taxCustCategory;
    @ManyToOne
    @JoinColumn(name = "PARENT_LOCATION_ID")
    private Locations parent;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUM_INSEE")
    private Insee insee;
    @Column(name = "SERVER_DEFAULT")
    private boolean serverDefault;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEG_ID")
    private Categories category;
    @Column(name = "URL_TEXT")
    private String url;
    private String etat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

//    public List<PosMessages> getPosMessages() {
//        return posMessages;
//    }
//
//    public void setPosMessages(List<PosMessages> posMessages) {
//        this.posMessages = posMessages;
//    }

    public List<ToursLocations> getTours() {
        return tours;
    }

    public void setTours(List<ToursLocations> tours) {
        this.tours = tours;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSLocationsDTO.class)) {
            //List<MessagesDTO> messagesList;
            List<POSToursLocationsDTO> toursList;
            Date now = new Date();

            POSLocationsDTO locationDTO = new POSLocationsDTO();
            locationDTO.setId(getId());
            locationDTO.setLabel(getName());
            locationDTO.setAddress(getAddress());
            locationDTO.setInseeNum(getInseeNum());
            locationDTO.setParentId(getParentId());
            locationDTO.setServerDefault(isServerDefault());
            if(getEtat() != null) {
                locationDTO.setEtat(getEtat());
            }
            
            if(getCategory() != null) {
                POSCategoriesDTO categ = new POSCategoriesDTO();
                categ.setDispOrder(getCategory().getDisplayOrder());
                categ.setHasImage(getCategory().hasImage());
                categ.setId(getCategory().getId());
                categ.setLabel(getCategory().getName());
                if(getCategory().getParent() != null) {
                    categ.setParentId(getCategory().getParent().getId());
                }
                locationDTO.setCategory(categ);
            }

            /*
             * EDU 2020-02 Les messages ralentissent beaucoup et font beaucoup de volume
             * On vire pour le moment car ce n'est utilisé nulle part
            messagesList = new ArrayList<MessagesDTO>();
            for (PosMessages elements : getPosMessages()) {
                MessagesDTO el = new MessagesDTO();
                el.setContent(elements.getContent());
                el.setStartDate(elements.getStartDate());
                el.setEndDate(elements.getEndDate());
                el.setType(elements.getType().getType());
                el.setLocationId(id);
                el.setId(elements.getId());
                messagesList.add(el);
            }
            locationDTO.setMessages(messagesList);
            */
            toursList = new ArrayList<POSToursLocationsDTO>();
            //EDU 
            for (ToursLocations elements : getTours()) {
                POSToursLocationsDTO el = new POSToursLocationsDTO();
                el.setEndDate(elements.getEndDate());
                el.setStartDate(elements.getStartDate());
                el.setLocation(elements.getLocation().getAdapter(LocationsInfoDTO.class));
                el.setTour(elements.getTour().getAdapter(TourIndexDTO.class, filtered));
                
                if(now.after(elements.getStartDate())) {
                    toursList.add(el);
                }
            }
            locationDTO.setTours(toursList);

            return (T) locationDTO;
        }      
        else if (selector.equals(LocationsInfoDTO.class)) {
            LocationsInfoDTO locationDTO = new LocationsInfoDTO();
            locationDTO.setId(getId());
            locationDTO.setLabel(getName());
            if(getCategory() != null) {
                locationDTO.setCategory(getCategory().getName());
            }
            if(getParentId() != null) {
                locationDTO.setParentId(getParentId());
            }
            if(getEtat() != null) {
                locationDTO.setEtat(getEtat());
            }
            return (T) locationDTO;
        }
        else if (selector.equals(POSLocationsUrlDTO.class)) {
            POSLocationsUrlDTO locationDTO = new POSLocationsUrlDTO();
            locationDTO.setUrl(getUrl());
            return (T) locationDTO;
        }

        return null;
    }

    public TaxCustCategories getTaxCustCategory() {
        return taxCustCategory;
    }

    public void setTaxCustCategory(TaxCustCategories taxCustCategory) {
        this.taxCustCategory = taxCustCategory;
    }

    public Locations getParent() {
        return parent;
    }

    public String getParentId() {
        return parent == null ? null : parent.getId();
    }

    public void setParent(Locations parent) {
        this.parent = parent;
    }

    public Insee getInsee() {
        return insee;
    }

    public void setInsee(Insee insee) {
        this.insee = insee;
    }

    public String getInseeNum() {
        return insee == null ? null : insee.getInseeNum();
    }

    public boolean isServerDefault() {
        return serverDefault;
    }

    public void setServerDefault(boolean serverDefault) {
        this.serverDefault = serverDefault;
    }

    public List<Locations> getChildren() {
        return children;
    }

    public void setChildren(List<Locations> children) {
        this.children = children;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

//    public LocationsManager getLocationsManager() {
//        return new LocationsManager(this);
//    }

    public String getCategoryId() {
        return category == null ? null : category.getId();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

}
