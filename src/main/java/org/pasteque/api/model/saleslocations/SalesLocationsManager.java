package org.pasteque.api.model.saleslocations;

import org.pasteque.api.dto.locations.InseeDTO;
import org.pasteque.api.dto.locations.POSInseeDTO;
import org.pasteque.api.dto.locations.InseeSimpleDTO;


public class SalesLocationsManager {

    private Insee insee;

    public SalesLocationsManager(Insee insee) {
        this.insee = insee;
    }

    public InseeSimpleDTO getInseeSimple() {
        InseeSimpleDTO inseeSimpleDTO = new InseeSimpleDTO();

        inseeSimpleDTO.setInsee(insee.getInseeNum());
        inseeSimpleDTO.setZipCode(insee.getZipCode());
        inseeSimpleDTO.setCommune(insee.getCommune());
        inseeSimpleDTO.setLatitude(insee.getLatitude());
        inseeSimpleDTO.setLongitude(insee.getLongitude());
        return inseeSimpleDTO;
    }

    public POSInseeDTO getInseePOS() {
        POSInseeDTO pOSInseeDTO = new POSInseeDTO();

        pOSInseeDTO.setInsee(insee.getId());
        pOSInseeDTO.setZipCode(insee.getZipCode());
        pOSInseeDTO.setCommune(insee.getCommune());
        pOSInseeDTO.setLatitude(insee.getLatitude());
        pOSInseeDTO.setLongitude(insee.getLongitude());
        return pOSInseeDTO;
    }

    public InseeDTO getInsee() {
        InseeDTO inseeDTO = new InseeDTO();

        inseeDTO.setInsee(insee.getInseeNum());
        inseeDTO.setZipCode(insee.getZipCode());
        inseeDTO.setCommune(insee.getCommune());
        inseeDTO.setLatitude(insee.getLatitude());
        inseeDTO.setLongitude(insee.getLongitude());

        inseeDTO.setCodeDepartement(insee.getCodeDepartement());
        inseeDTO.setCodeRegion(insee.getCodeRegion());
        inseeDTO.setCodeDepartement(insee.getCodeDepartement());
        inseeDTO.setNomDepartement(insee.getNomDepartement());

        inseeDTO.setSurface(insee.getSurface());
        inseeDTO.setPopulation(insee.getPopulation());

        inseeDTO.setEtat(insee.getEtat());

        return inseeDTO;
    }

}
