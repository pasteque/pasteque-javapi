package org.pasteque.api.model.security;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.pasteque.api.model.adaptable.IAdaptable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import org.pasteque.api.dto.security.POSRolesDTO;

@Entity
public class Roles implements IAdaptable, GrantedAuthority {

    private static final long serialVersionUID = -2290280498463705645L;

    public static final String ROLE_LOG = "ROLE_LOG_%";
    public static final String ROLE_VENTE = "ROLE_VTE_%";
    public static final String ROLE_SIEGE = "ROLE_SIEGE%";
    
    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String PERMISSIONS = "permissions";
    }

    @Id
    private String id;
    private String name;
    @Lob
    private byte[] permissions;

    @Transient
    private List<GrantedAuthority> allowedOperations;

    @Transient
    private ArrayList<String> allowedOperationsString;

    @Transient
    public ArrayList<String> getAllowedOperationsString() {
        if (allowedOperations == null) {
            createAllowedOperations();
        }
        return allowedOperationsString;
    }

    public boolean isAllowed(String permission) {
        return getAllowedOperationsString().contains(permission);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getPermissions() {
        return permissions;
    }

    public void setPermissions(byte[] permissions) {
        this.permissions = permissions;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSRolesDTO.class)) {
            POSRolesDTO pOSRolesDTO = new POSRolesDTO();
            pOSRolesDTO.setId(getId());
            pOSRolesDTO.setName(getName());
            if (getPermissions() != null) {
                String permissions = new String(getPermissions());
                pOSRolesDTO.setPermissions(permissions);
            }
            return (T) pOSRolesDTO;
        }
        return null;
    }

    @Override
    public String getAuthority() {
        return getName();
    }

    @Transient
    public List<GrantedAuthority> getAllowedOperations() {
        if (allowedOperations == null) {
            createAllowedOperations();
        }
        return allowedOperations;
    }

    private void createAllowedOperations() {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        String sortie = null, restriction = null;
        Boolean poursuite, inside;
        allowedOperations = new ArrayList<GrantedAuthority>();
        allowedOperationsString = new ArrayList<String>();
        try {
            XMLStreamReader reader = factory.createXMLStreamReader(new ByteArrayInputStream(permissions));
            int eventType;
            poursuite = reader.hasNext();
            inside = false;
            while (poursuite) {
                eventType = reader.next();
                if (eventType == XMLEvent.START_ELEMENT) {
                    // Si début d'objet on se tient prêt
                    if (inside && reader.getLocalName().equals("auth")) {
                        // Gestion des limites : droits sur tout mais uniquement sur magasins !
                        sortie = reader.getAttributeValue(null, "name");
                        restriction = reader.getAttributeValue(null, "restrict");
                    }
                    // Si début du bloc on passe inside à true
                    if (reader.getLocalName().equals("permissions")) {
                        inside = true;
                    }
                }

                if (inside && eventType == XMLEvent.END_ELEMENT) {
                    // Si fin d'un objet on stocke
                    if (inside && reader.getLocalName().equals("auth")) {
                        if (sortie != null) {

                            if (restriction != null) {
                                sortie = "RESTRICTED_".concat(sortie);
                                allowedOperations.add(new SimpleGrantedAuthority(sortie.concat("_").concat(restriction)));
                                allowedOperationsString.add(sortie.concat("_").concat(restriction));
                            }
                            allowedOperations.add(new SimpleGrantedAuthority(sortie));
                            allowedOperationsString.add(sortie);
                        }
                        sortie = null;
                        restriction = null;
                    }
                    // si fin du bloc on passe poursuite à false
                    if (reader.getLocalName().equals("permissions")) {
                        poursuite = false;
                    }
                }
                poursuite &= reader.hasNext();
            }
        } catch (XMLStreamException e) {
            // TODO Auto-generated catch block
            System.out.println(name);
            e.printStackTrace();
        }
    }

}
