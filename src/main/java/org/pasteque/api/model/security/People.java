package org.pasteque.api.model.security;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.pasteque.api.dto.security.POSUsersDTO;
import org.pasteque.api.dto.security.UsersObfDTO;
import org.pasteque.api.dto.security.UserIndexDTO;
import org.pasteque.api.model.adaptable.IAdaptable;

@Entity
public class People implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String USERNAME = "username";
        public static final String APP_PASSWORD = "appPassword";
        public static final String CARD = "card";
        public static final String VISIBLE = "visible";
        public static final String IMAGE = "image";
        public static final String NOM = "nom";
        public static final String PRENOM = "prenom" ;
        public static final String DDEBUT = "ddebut" ;
        public static final String DFIN = "dfin" ;
        public static final String LASTUPDATE = "lastupdate" ;


        public static final String ROLES = "role";
        public static final String MESSAGES = "posMessages";
    }

    @Id
    private String id;
    @Column(name = "NAME" , nullable = false, unique = true)
    private String username;
    @Column(name = "apppassword")
    private String appPassword;
    private String card;
    private boolean visible;
    private byte[] image;
    private String nom;
    private String prenom;
    @Column(name = "DDEBUT")
    private Date ddebut;
    @Column(name = "DFIN")
    private Date dfin;
    @Column(name = "LASTUPDATE")
    private Date lastupdate;

    @ManyToOne
    @JoinColumn(name = "role")
    private Roles role;

// EDU - 2020 07 - Passage 8S - Nous n'avons jamais exploité les messages ciblés pour un user 
//    @OneToMany(mappedBy = PosMessages.Fields.PEOPLE)
//    private List<PosMessages> posMessages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAppPassword() {
        return appPassword;
    }

    public void setAppPassword(String appPassword) {
        this.appPassword = appPassword;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

//    public List<PosMessages> getMessages() {
//        return posMessages;
//    }
//
//    public void setMessages(List<PosMessages> posMessages) {
//        this.posMessages = posMessages;
//    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDdebut() {
        return ddebut;
    }

    public void setDdebut(Date ddebut) {
        this.ddebut = ddebut;
    }

    public Date getDfin() {
        return dfin;
    }

    public void setDfin(Date dfin) {
        this.dfin = dfin;
    }

    public Date getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(Date lastupdate) {
        this.lastupdate = lastupdate;
    }

    public String getLongName() {
        String retour = "" ;
        if(getNom() != null && ! getNom().isEmpty()) {
            retour = getNom() ;
            if(getPrenom() != null && ! getPrenom().isEmpty()) {
                retour = retour.concat(" ").concat(getPrenom()) ;
            }
        }
        if(retour.isEmpty()) {
            retour = getUsername();
        }
        return retour;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector , boolean filtered) {
        if (selector.equals(UsersObfDTO.class)) {
            UsersObfDTO usersDTO = new UsersObfDTO();
            usersDTO.setCard(getCard());
            if (getImage() != null) {
                usersDTO.setHasImage(true);
            }
            usersDTO.setId(getId());
            usersDTO.setName(getUsername());
            usersDTO.setPassword(getAppPassword());
            if (getRole() != null) {
                usersDTO.setRoleId(getRole().getId());
                usersDTO.setAuthorizations(getRole().getAllowedOperationsString());
            }
            usersDTO.setVisible(isVisible());
            usersDTO.setDdebut(getDdebut());
            usersDTO.setDfin(getDfin());
            usersDTO.setLastupdate(getLastupdate());
            return (T) usersDTO;
        }
        if (selector.equals(POSUsersDTO.class)) {
            POSUsersDTO pOSUsersDTO = new POSUsersDTO();
            pOSUsersDTO.setCard(getCard());
            if (getImage() != null) {
                pOSUsersDTO.setHasImage(true);
            }
            pOSUsersDTO.setId(getId());
            pOSUsersDTO.setName(getUsername());
            pOSUsersDTO.setPrenom(getPrenom());
            pOSUsersDTO.setNom(getNom());
            pOSUsersDTO.setPassword(getAppPassword());
            if (getRole() != null) {
                pOSUsersDTO.setRoleId(getRole().getId());
            }
            pOSUsersDTO.setVisible(isVisible());
            pOSUsersDTO.setDdebut(getDdebut());
            pOSUsersDTO.setDfin(getDfin());
            pOSUsersDTO.setLastupdate(getLastupdate());
            return (T) pOSUsersDTO;
        }
        if (selector.equals(UserIndexDTO.class)) {
            UserIndexDTO userIndexDTO = new UserIndexDTO();
            userIndexDTO.setId(getId());
            userIndexDTO.setNomCaisse(getUsername());
            userIndexDTO.setPrenom(getPrenom());
            userIndexDTO.setNom(getNom());
            userIndexDTO.setDdebut(getDdebut());
            userIndexDTO.setDfin(getDfin());
            userIndexDTO.setLastupdate(getLastupdate());
            return (T) userIndexDTO;
        }
        return null;
    }

    /*
    public boolean loadFromDTO(UsersDTO element , Roles role) {
        if(getId() == null) {
            setId(element.getId());
        }
        setName(element.getName());
        setPrenom(element.getPrenom());
        setNom(element.getNom());
        setRole(role);
        setVisible(element.isVisible());
        return true;
    }
    */

    public boolean isActif() {
        Date currentDate = new Date();
        boolean actif = false ;
        actif = this.dfin == null || this.dfin.after(currentDate) || this.dfin.equals(currentDate) ;
        actif = actif && ( this.ddebut == null || currentDate.after(this.ddebut) || this.ddebut.equals(currentDate)) ;
        return actif;
    }

}
