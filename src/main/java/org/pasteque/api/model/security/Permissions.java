package org.pasteque.api.model.security;

public final class Permissions {

    public final class Administration {
        public static final String PERM_ADMIN_USER = "PERM_ADMIN_USER";
        public static final String PERM_ADMIN_USER_LOG = "PERM_ADMIN_USER_LOG";
        
        public static final String PERM_ADMIN_ROLES = "PERM_ADMIN_ROLES";
        public static final String PERM_ADMIN_RESSOURCES = "PERM_ADMIN_RESSOURCES";
        public static final String PERM_ADMIN_TOURNEES = "PERM_ADMIN_TOURNEES";
        public static final String PERM_ADMIN_TARIFFAREAS = "PERM_ADMIN_TARIFFAREAS";
        public static final String PERM_ADMIN_UPLOAD = "PERM_ADMIN_UPLOAD";
        public static final String PERM_ADMIN_DOC = "PERM_ADMIN_DOC";
        public static final String PERM_ADMIN_JOBS = "PERM_ADMIN_JOBS";
    }

    public final class Produits {
        public static final String PERM_PRODUIT = "PERM_PRODUIT";
        public static final String PERM_PRODUIT_EXTRACT = "PERM_PRODUIT_EXTRACT";
        public static final String PERM_PRODUIT_VENTE = "PERM_PRODUIT_VENTE";

        private Produits() {
        }
    }

    public final class Clients {
        public static final String PERM_CLIENT = "PERM_CLIENT";
        public static final String PERM_CLIENT_EXTRACT = "PERM_CLIENT_EXTRACT";
        public static final String PERM_CLIENT_MODIF = "PERM_CLIENT_MODIF";
        public static final String PERM_CLIENT_DEDUP_SAVE = "PERM_CLIENT_DEDUP_SAVE";
        public static final String PERM_CLIENT_DEDUP_DELETE = "PERM_CLIENT_DEDUP_DELETE";

        private Clients() {
        }
    }

    public final class Ventes {
        public static final String PERM_VTE_TICKET = "PERM_VTE_TICKET";
        public static final String PERM_VTE_TICKET_EXTRACT = "PERM_VTE_TICKET_EXTRACT";

        public static final String PERM_VTE_PV = "PERM_VTE_PV";
        public static final String PERM_VTE_PV_EXTRACT = "PERM_VTE_PV_EXTRACT";

        public static final String PERM_VTE_COMPTA = "PERM_VTE_COMPTA";
        public static final String PERM_VTE_COMPTA_EXTRACT = "PERM_VTE_COMPTA_EXTRACT";

        public static final String PERM_VTE_PDT = "PERM_VTE_PDT";
        public static final String PERM_VTE_PDT_EXTRACT = "PERM_VTE_PDT_EXTRACT";

        public static final String PERM_VTE_DEBALLAGE = "PERM_VTE_DEBALLAGE";

        private Ventes() {
        }
    }

    public final class Commandes {
        public static final String PERM_COMMANDE_VISU = "PERM_COMMANDE_VISU";
        public static final String PERM_COMMANDE = "PERM_COMMANDE";
        public static final String PERM_COMMANDE_GENERER = "PERM_COMMANDE_GENERER";
        public static final String PERM_COMMANDE_AJOUTER_PDT = "PERM_COMMANDE_AJOUTER_PDT";
        public static final String PERM_COMMANDE_SUPERVISION = "PERM_COMMANDE_SUPERVISION";

        public static final String PERM_COMMANDE_EXTRACT = "PERM_COMMANDE_EXTRACT";
        public static final String PERM_COMMANDE_BON_LIVRAISON = "PERM_COMMANDE_BON_LIVRAISON";
        public static final String PERM_COMMANDE_EXPORT_OFBIZ ="PERM_COMMANDE_EXPORT_OFBIZ";

        public static final String PERM_COMMANDE_PLANIFICATION = "PERM_COMMANDE_PLANIFICATION";
        public static final String PERM_COMMANDE_PREPARATION = "PERM_COMMANDE_PREPARATION";
        public static final String PERM_COMMANDE_CONTROLE = "PERM_COMMANDE_CONTROLE";
        public static final String PERM_COMMANDE_RECEPTION = "PERM_COMMANDE_RECEPTION";
        public static final String PERM_COMMANDE_RENFLOUEMENT = "PERM_COMMANDE_RENFLOUEMENT";
        public static final String PERM_COMMANDE_VALORISATION = "PERM_COMMANDE_VALORISATION";

        private Commandes() {
        }
    }

    public final class Stocks {
        public static final String PERM_STOCK = "PERM_STOCK";
        public static final String PERM_STOCK_EXTRACT = "PERM_STOCK_EXTRACT";

        public static final String PERM_STOCK_MVT = "PERM_STOCK_MVT";
        public static final String PERM_STOCK_MVT_EXTRACT = "PERM_STOCK_MVT_EXTRACT";

        public static final String PERM_STOCK_INVENTAIRE = "PERM_STOCK_INVENTAIRE";
        public static final String PERM_STOCK_INVENTAIRE_CREER = "PERM_STOCK_INVENTAIRE_CREER";
        public static final String PERM_STOCK_INVENTAIRE_VALIDER = "PERM_STOCK_INVENTAIRE_VALIDER";
        public static final String PERM_STOCK_INVENTAIRE_EXTRACT = "PERM_STOCK_INVENTAIRE_EXTRACT";

        public static final String PERM_STOCK_EVOL = "PERM_STOCK_EVOL";
        public static final String PERM_STOCK_EVOL_EXTRACT = "PERM_STOCK_EVOL_EXTRACT";

        public static final String PERM_STOCK_TYPE = "PERM_STOCK_TYPE";
        public static final String PERM_STOCK_TYPE_VISU = "PERM_STOCK_TYPE_VISU";
        public static final String PERM_STOCK_TYPE_MODIF = "PERM_STOCK_TYPE_MODIF";
        public static final String PERM_STOCK_TYPE_EXTRACT = "PERM_STOCK_TYPE_EXTRACT";

        private Stocks() {
        }
    }

}