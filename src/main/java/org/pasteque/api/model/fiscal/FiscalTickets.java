package org.pasteque.api.model.fiscal;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Lob;

import org.pasteque.api.dto.fiscal.FiscalTicketsDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.signable.ISignable;

@Entity
@IdClass(FiscalTicketsId.class)
public class FiscalTickets implements IAdaptable , ISignable {

    
    public final class Table {
        public static final String NAME = "fiscaltickets";
    }

    public final class Fields {
        public static final String TYPE = "type";
        public static final String SEQUENCE = "sequence";
        public static final String NUMBER = "number";
        public static final String DATE = "date";
        public static final String CONTENT = "content";
        public static final String SIGNATURE = "signature";
        
    }
    
    public final class SearchCritria {
        public static final String FROM = "from";
        public static final String TO = "to";
        public static final String EXCLUDE_NUMBER = "notNumber";

    }
    
    public final class Types {
        public static final String ZTICKET = "z";
        public static final String TICKET = "tkt";
        public static final String CLOTURE = "cloture";        
    }
    
    public static final Integer EOS = 0;
    public static final String EOS_STRING = "EOS";
    
    public final class Prefixes{
        public static final String FAILURE = "failure-";
    }
    
    @Id
    private String type;
    @Id
    private String sequence;
    @Id
    private Integer number;
    private Date date;
    @Lob
    private String content;
    private String signature;
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String getSignature() {
        return signature;
    }

    @Override
    public void setSignature(String signature) {
        this.signature = signature;
    }
    
    @Override
    public String getHashBase() {
        //EDU : potentiellement précision à la seconde côté BdD hors on peut être amené à calculer la hashbase avant intégration donc avec une pércision encore à la ms
        return sequence + "-" + number.toString() + "-" + String.valueOf(date.getTime()/1000) + "-" + ( content == null ? "" : content );
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(FiscalTicketsDTO.class)) {
            FiscalTicketsDTO ticketsDTO = new FiscalTicketsDTO();
            ticketsDTO.setNumber(getNumber());
            ticketsDTO.setContent(getContent());
            ticketsDTO.setDate(getDate());
            ticketsDTO.setSequence(getSequence());
            ticketsDTO.setType(getType());
            ticketsDTO.setSignature(getSignature());
            return (T) ticketsDTO;
        }
        return null;
    }

}
