package org.pasteque.api.model.fiscal;

import java.io.Serializable;

public class PeriodCatTaxesId implements Serializable {

    private static final long serialVersionUID = -2823159564692723822L;

    private String period;
    
    private String category;

    private String tax;
    
    public PeriodCatTaxesId() {

    }    

    public PeriodCatTaxesId(String period, String category, String tax) {
        this.period = period;
        this.category = category;
        this.tax = tax;
    }

    public String getPeriod() {
        return period;
    }

    public String getCategory() {
        return category;
    }

    public String getTax() {
        return tax;
    }
    
    @Override
    public int hashCode() {
        final int prime = 83;
        int result = 1;
        result = prime * result
                + ((period == null) ? 0 : period.hashCode());
        result = prime * result
                + ((category == null) ? 0 : category.hashCode());
        result = prime * result
                + ((tax == null) ? 0 : tax.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PeriodCatTaxesId other = (PeriodCatTaxesId) obj;
        if (period == null) {
            if (other.getPeriod() != null)
                return false;
        } else if (!period.equals(other.getPeriod()))
            return false;
        if (category == null ) {
            if (other.getCategory() != null )
                return false;
        } else if (!category.equals(other.getCategory()))
            return false;
        if (tax == null) {
            if (other.getTax() != null)
                return false;
        } else if (!tax.equals(other.getTax()))
            return false;
        return true;
    }

}
