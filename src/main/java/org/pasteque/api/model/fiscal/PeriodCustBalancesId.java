package org.pasteque.api.model.fiscal;

import java.io.Serializable;

public class PeriodCustBalancesId implements Serializable {

    private static final long serialVersionUID = -2823159564692723822L;

    private String period;
    
    private String customer;
    
    public PeriodCustBalancesId() {

    }    

    public PeriodCustBalancesId(String period, String customer) {
        this.period = period;
        this.customer = customer;
    }

    public String getPeriod() {
        return period;
    }

    public String getCustomer() {
        return customer;
    }
    
    @Override
    public int hashCode() {
        final int prime = 101;
        int result = 1;
        result = prime * result
                + ((period == null) ? 0 : period.hashCode());
        result = prime * result
                + ((customer == null) ? 0 : customer.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PeriodCustBalancesId other = (PeriodCustBalancesId) obj;
        if (period == null ) {
            if (other.getPeriod() != null )
                return false;
        } else if (!period.equals(other.getPeriod()))
            return false;
        if (customer == null ) {
            if (other.getCustomer() != null )
                return false;
        } else if (!customer.equals(other.getCustomer()))
            return false;
        return true;
    }

}
