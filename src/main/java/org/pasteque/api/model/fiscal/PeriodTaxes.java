package org.pasteque.api.model.fiscal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.dto.fiscal.PeriodTaxesDTO;
import org.pasteque.api.dto.products.POSTaxesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Taxes;

@Entity
@IdClass(PeriodTaxesId.class)
public class PeriodTaxes implements IAdaptable {

    public final class Table {
        public static final String NAME = "periodtaxes";
    }

    public final class Fields {
        public static final String PERIOD = "period";
        public static final String TAX = "tax";
        public static final String LABEL = "label";
        public static final String BASE = "base";
        public static final String AMOUNT = "amount";
        public static final String BASE_PERIOD = "basePeriod";
        public static final String AMOUNT_PERIOD = "amountPeriod";
        public static final String BASE_FYEAR = "baseFYear";
        public static final String AMOUNT_FYEAR = "amountFYear";
        public static final String RATE = "rate";
        
    }
    
    @Id
    @ManyToOne
    @JoinColumn(name = "period_id")
    private Periods period;
    
    @Id
    @ManyToOne
    @JoinColumn(name = "tax_id")
    private Taxes tax;
    
    private String label;
    
    private Double base;
    
    private Double amount;
    
    private Double basePeriod;
    
    private Double amountPeriod;
    
    private Double baseFYear;
    
    private Double amountFYear;
    
    @Column(name = "taxRate")
    private Double rate;
    
    public Periods getPeriod() {
        return period;
    }

    public void setPeriod(Periods period) {
        this.period = period;
    }

    public Taxes getTax() {
        return tax;
    }

    public void setTax(Taxes tax) {
        this.tax = tax;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Double getBase() {
        return base;
    }

    public void setBase(Double base) {
        this.base = base;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getBasePeriod() {
        return basePeriod;
    }

    public void setBasePeriod(Double basePeriod) {
        this.basePeriod = basePeriod;
    }

    public Double getAmountPeriod() {
        return amountPeriod;
    }

    public void setAmountPeriod(Double amountPeriod) {
        this.amountPeriod = amountPeriod;
    }

    public Double getBaseFYear() {
        return baseFYear;
    }

    public void setBaseFYear(Double baseFYear) {
        this.baseFYear = baseFYear;
    }

    public Double getAmountFYear() {
        return amountFYear;
    }

    public void setAmountFYear(Double amountFYear) {
        this.amountFYear = amountFYear;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(PeriodTaxesDTO.class)) {
            PeriodTaxesDTO periodTaxesDTO = new PeriodTaxesDTO();
            periodTaxesDTO.setTaux(getRate());
            periodTaxesDTO.setMontant(getAmount());
            periodTaxesDTO.setCumulMensuel(getAmountPeriod());
            periodTaxesDTO.setCumulExercice(getAmountFYear());
            periodTaxesDTO.setBase(getBase());
            periodTaxesDTO.setBaseMensuelle(getBasePeriod());
            periodTaxesDTO.setBaseExercice(getBaseFYear());
            periodTaxesDTO.setTaxe(getTax().getAdapter(POSTaxesDTO.class));
            periodTaxesDTO.setIdPeriode(getPeriod().getId());
            return (T) periodTaxesDTO;
        }
        return null;
    }

}
