package org.pasteque.api.model.fiscal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.dto.fiscal.PeriodCatsDTO;
import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Categories;

@Entity
@IdClass(PeriodCatsId.class)
public class PeriodCats implements IAdaptable {

    public final class Table {
        public static final String NAME = "periodcats";
    }

    public final class Fields {
        public static final String PERIOD = "period";
        public static final String CATEGORY = "category";
        public static final String LABEL = "label";
        public static final String BASE = "base";
        public static final String AMOUNT = "amount";
        
    }
    
    @Id
    @ManyToOne
    @JoinColumn(name = "period_id")
    private Periods period;
    
    @Id
    @ManyToOne
    @JoinColumn(name = "categ_id")
    private Categories category;
    
    private String label;
    
    private Double amount;
    
    public Periods getPeriod() {
        return period;
    }

    public void setPeriod(Periods period) {
        this.period = period;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if(selector.equals(PeriodCatsDTO.class)) {
            PeriodCatsDTO periodCatsDTO = new PeriodCatsDTO();
            periodCatsDTO.setIdPeriode(getPeriod().getId());
            periodCatsDTO.setMontant(getAmount());
            periodCatsDTO.setCategorie(getCategory().getAdapter(POSCategoriesDTO.class));
            return (T) periodCatsDTO;
        }
        return null;
    }

}
