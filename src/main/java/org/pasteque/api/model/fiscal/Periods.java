package org.pasteque.api.model.fiscal;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.pasteque.api.dto.cash.POSCashRegistersDTO;
import org.pasteque.api.dto.fiscal.PeriodCatTaxesDTO;
import org.pasteque.api.dto.fiscal.PeriodCatsDTO;
import org.pasteque.api.dto.fiscal.PeriodCustBalancesDTO;
import org.pasteque.api.dto.fiscal.PeriodInfoDTO;
import org.pasteque.api.dto.fiscal.PeriodPaymentsDTO;
import org.pasteque.api.dto.fiscal.PeriodTaxesDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.cash.ClosedCash;

@Entity
public class Periods implements IAdaptable {
    
    public final class Types {
        public static final String JOUR = "jour";
        public static final String MOIS = "mois";
        public static final String FYEAR = "anneeFiscale";
        public static final String SESSION = "session";
    }

    public final class Table {
        public static final String NAME = "periods";
    }

    public final class Fields {
        public static final String ID = "id";
        public static final String SEQUENCE = "sequence";
        public static final String DATE_START = "dateStart";
        public static final String DATE_END = "dateEnd";
        public static final String CASH_REGISTER = "cashRegister";
        public static final String TYPE ="type";
        public static final String CLOSED ="closed";
        public static final String CONTINUOUS ="continuous";
        public static final String CUST_COUNT ="custCount";
        public static final String TICKET_COUNT ="ticketCount";
        public static final String CS ="cs";
        public static final String CS_PERIOD ="csPeriod";
        public static final String CS_FYEAR ="csFYear";
        public static final String CS_PERPETUAL ="csPerpetual";
        public static final String PERIOD_TAXES ="periodTaxes";
        public static final String PERIOD_CAT_TAXES ="periodCatTaxes";
        public static final String PERIOD_CUST_BALANCES ="periodCustBalances";
        public static final String PERIOD_CATS ="periodCats";
        public static final String PERIOD_PAYMENTS ="periodPayments";
    }

    @Id
    private String id;
    @Column(name = "datestart")
    @Temporal(value=TemporalType.TIMESTAMP)
    private Date dateStart;
    @Column(name = "dateend")
    @Temporal(value=TemporalType.TIMESTAMP)
    private Date dateEnd;
    
    @Column(name = "sequence")
    private int sequence;
    
    @ManyToOne
    @JoinColumn(name = "cashregister_id")
    private CashRegisters cashRegister;
    
    private String type;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean closed = false;
    
    //la continuité par défaut true
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean continuous = true;
    //Le nombre de clients et de tickets initialisés à 0
    private Long custCount = 0L;
    private Long ticketCount = 0L;
    
    //Les chiffres d'affaires cumulés sur 4 plans
    //La session amorce à zéro
    private Double cs = 0D;
    //La période ( journée )
    private Double csPeriod = 0D;
    //L'année fiscale
    private Double csFYear = 0D;
    //L'installation
    private Double csPerpetual = 0D;
    
    @OneToOne(mappedBy=ClosedCash.Fields.DETAILS)
    private ClosedCash session;
    
    @OneToMany(mappedBy = PeriodCats.Fields.PERIOD, fetch = FetchType.LAZY)
    private List<PeriodCats> periodCats;
    
    @OneToMany(mappedBy = PeriodCatTaxes.Fields.PERIOD, fetch = FetchType.LAZY)
    private List<PeriodCatTaxes> periodCatTaxes;

    @OneToMany(mappedBy = PeriodCustBalances.Fields.PERIOD, fetch = FetchType.LAZY)
    private List<PeriodCustBalances> periodCustBalances;
    
    @OneToMany(mappedBy = PeriodPayments.Fields.PERIOD, fetch = FetchType.LAZY)
    private List<PeriodPayments> periodPayments;
    
    @OneToMany(mappedBy = PeriodTaxes.Fields.PERIOD, fetch = FetchType.LAZY)
    private List<PeriodTaxes> periodTaxes;    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public CashRegisters getCashRegister() {
        return cashRegister;
    }

    public void setCashRegister(CashRegisters cashRegister) {
        this.cashRegister = cashRegister;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public boolean isContinuous() {
        return continuous;
    }

    public void setContinuous(boolean continuous) {
        this.continuous = continuous;
    }

    public Long getCustCount() {
        return custCount;
    }

    public void setCustCount(Long custCount) {
        this.custCount = custCount;
    }

    public Long getTicketCount() {
        return ticketCount;
    }

    public void setTicketCount(Long ticketCount) {
        this.ticketCount = ticketCount;
    }

    public Double getCs() {
        return cs;
    }

    public void setCs(Double cs) {
        this.cs = cs;
    }

    public Double getCsPeriod() {
        return csPeriod;
    }

    public void setCsPeriod(Double csPeriod) {
        this.csPeriod = csPeriod;
    }

    public Double getCsFYear() {
        return csFYear;
    }

    public void setCsFYear(Double csFYear) {
        this.csFYear = csFYear;
    }

    public Double getCsPerpetual() {
        return csPerpetual;
    }

    public void setCsPerpetual(Double csPerpetual) {
        this.csPerpetual = csPerpetual;
    }

    public List<PeriodCats> getPeriodCats() {
        return periodCats;
    }

    public void setPeriodCats(List<PeriodCats> periodCats) {
        this.periodCats = periodCats;
    }

    public List<PeriodCatTaxes> getPeriodCatTaxes() {
        return periodCatTaxes;
    }

    public void setPeriodCatTaxes(List<PeriodCatTaxes> periodCatTaxes) {
        this.periodCatTaxes = periodCatTaxes;
    }

    public List<PeriodCustBalances> getPeriodCustBalances() {
        return periodCustBalances;
    }

    public void setPeriodCustBalances(List<PeriodCustBalances> periodCustBalances) {
        this.periodCustBalances = periodCustBalances;
    }

    public List<PeriodPayments> getPeriodPayments() {
        return periodPayments;
    }

    public void setPeriodPayments(List<PeriodPayments> periodPayments) {
        this.periodPayments = periodPayments;
    }

    public List<PeriodTaxes> getPeriodTaxes() {
        return periodTaxes;
    }

    public void setPeriodTaxes(List<PeriodTaxes> periodTaxes) {
        this.periodTaxes = periodTaxes;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(PeriodInfoDTO.class)) {
            PeriodInfoDTO periodDTO = new PeriodInfoDTO();
            periodDTO.setCashRegister(getCashRegister().getAdapter(POSCashRegistersDTO.class));
            periodDTO.setClosed(isClosed());
            periodDTO.setContinuous(isContinuous());
            periodDTO.setCumulCATTC(getCs());
            periodDTO.setCumulCATTCAnneeFiscale(getCsFYear());
            periodDTO.setCumulCATTCInstallation(getCsPerpetual());
            periodDTO.setCumulCATTCMois(getCsPeriod());
            periodDTO.setCustCount(getCustCount());
            periodDTO.setDateEnd(getDateEnd());
            periodDTO.setDateStart(getDateStart());
            periodDTO.setId(getId());
            periodDTO.setSequence(getSequence());
            periodDTO.setTicketCount(getTicketCount());
            periodDTO.setType(getType());
            
            periodDTO.setTaxes(AdaptableHelper.getAdapter( getPeriodTaxes() , PeriodTaxesDTO.class));
            periodDTO.setEncaissements(AdaptableHelper.getAdapter(getPeriodPayments(),PeriodPaymentsDTO.class));
            periodDTO.setBalancesClients(AdaptableHelper.getAdapter(getPeriodCustBalances(),PeriodCustBalancesDTO.class));
            periodDTO.setCumulTaxesParCategorie(AdaptableHelper.getAdapter(getPeriodCatTaxes(),PeriodCatTaxesDTO.class));
            periodDTO.setCumulCAParCategorie(AdaptableHelper.getAdapter(getPeriodCats(),PeriodCatsDTO.class));
            
            return (T) periodDTO;
        }
        return null;
    }

}
