package org.pasteque.api.model.fiscal;

import java.io.Serializable;

public class PeriodPaymentsId implements Serializable {

    private static final long serialVersionUID = -2823159564692723822L;

    private String period;
    
    private String paymentModeId;

    private Long currency;
    
    public PeriodPaymentsId() {

    }    

    public PeriodPaymentsId(String period, String paymentModeId,
            Long currency) {
        this.period = period;
        this.paymentModeId = paymentModeId;
        this.currency = currency;
    }

    public String getPeriod() {
        return period;
    }
    
    public String getPaymentModeId() {
        return paymentModeId;
    }

    public Long getCurrency() {
        return currency;
    }

    @Override
    public int hashCode() {
        final int prime = 83;
        int result = 1;
        result = prime * result
                + ((period == null) ? 0 : period.hashCode());
        result = prime * result
                + ((paymentModeId == null) ? 0 : paymentModeId.hashCode());
        result = prime * result
                + ((currency == null) ? 0 : currency.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PeriodPaymentsId other = (PeriodPaymentsId) obj;
        if (period == null ) {
            if (other.getPeriod() != null )
                return false;
        } else if (!period.equals(other.getPeriod()))
            return false;
        if (paymentModeId == null ) {
            if (other.getPaymentModeId() != null)
                return false;
        } else if (!paymentModeId.equals(other.getPaymentModeId()))
            return false;
        if (currency == null ) {
            if (other.getCurrency() != null )
                return false;
        } else if (!currency.equals(other.getCurrency()))
            return false;
        return true;
    }

}
