package org.pasteque.api.model.fiscal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.dto.fiscal.PeriodCatTaxesDTO;
import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.dto.products.POSTaxesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Categories;
import org.pasteque.api.model.products.Taxes;

@Entity
@IdClass(PeriodCatTaxesId.class)
public class PeriodCatTaxes implements IAdaptable {

    public final class Table {
        public static final String NAME = "periodcattaxes";
    }

    public final class Fields {
        public static final String PERIOD = "period";
        public static final String CATEGORY = "category";
        public static final String TAX = "tax";
        public static final String LABEL = "label";
        public static final String BASE = "base";
        public static final String AMOUNT = "amount";
        
    }
    
    @Id
    @ManyToOne
    @JoinColumn(name = "period_id")
    private Periods period;
    
    //TODO une question se pose ici - 
    //pour nous il y a des categories de taxes afin par exemple de regrouper tous les taux plein quel que soit le taux
    //Il n'est pas clair dans la doc si c'est par taxe / categorie produit
    //Ou si c'est par taxe par categorie de taxe ... mais dans ce cas le tax_Id n'a pas trop de sens 
    @Id
    @ManyToOne
    @JoinColumn(name = "categ_id")
    private Categories category;
    
    @Id
    @ManyToOne
    @JoinColumn(name = "tax_id")
    private Taxes tax;
    
    private String label;
    
    private Double base;
    
    private Double amount;
    
    public Periods getPeriod() {
        return period;
    }

    public void setPeriod(Periods period) {
        this.period = period;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

    public Taxes getTax() {
        return tax;
    }

    public void setTax(Taxes tax) {
        this.tax = tax;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Double getBase() {
        return base;
    }

    public void setBase(Double base) {
        this.base = base;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(PeriodCatTaxesDTO.class)) {
            PeriodCatTaxesDTO periodCatTaxesDTO = new PeriodCatTaxesDTO();
            periodCatTaxesDTO.setMontant(getAmount());
            periodCatTaxesDTO.setBase(getBase());
            periodCatTaxesDTO.setTaxe(getTax().getAdapter(POSTaxesDTO.class));
            periodCatTaxesDTO.setIdPeriode(getPeriod().getId());
            periodCatTaxesDTO.setCategorie(getCategory().getAdapter(POSCategoriesDTO.class));
            return (T) periodCatTaxesDTO;
        }
        return null;
    }

}
