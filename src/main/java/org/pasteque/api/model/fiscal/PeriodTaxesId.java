package org.pasteque.api.model.fiscal;

import java.io.Serializable;

public class PeriodTaxesId implements Serializable {

    private static final long serialVersionUID = -3112108385102426911L;

    private String period;

    private String tax;
    
    public PeriodTaxesId() {

    }    

    public PeriodTaxesId(String period, String tax) {
        this.period = period;
        this.tax = tax;
    }

    public String getPeriod() {
        return period;
    }

    public String getTax() {
        return tax;
    }
    
    @Override
    public int hashCode() {
        final int prime = 89;
        int result = 1;
        result = prime * result
                + ((period == null) ? 0 : period.hashCode());
        result = prime * result
                + ((tax == null) ? 0 : tax.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PeriodTaxesId other = (PeriodTaxesId) obj;
        if (period == null ) {
            if (other.getPeriod() != null )
                return false;
        } else if (!period.equals(other.getPeriod()))
            return false;
        if (tax == null ) {
            if (other.getTax() != null )
                return false;
        } else if (!tax.equals(other.getTax()))
            return false;
        return true;
    }

}
