package org.pasteque.api.model.fiscal;

import java.io.Serializable;

public class PeriodCatsId implements Serializable {

    private static final long serialVersionUID = -2344814446394903865L;

    private String period;
    
    private String category;
    
    public PeriodCatsId() {

    }    

    public PeriodCatsId(String period, String category) {
        this.period = period;
        this.category = category;
    }

    public String getPeriod() {
        return period;
    }

    public String getCategory() {
        return category;
    }
    
    @Override
    public int hashCode() {
        final int prime = 97;
        int result = 1;
        result = prime * result
                + ((period == null) ? 0 : period.hashCode());
        result = prime * result
                + ((category == null) ? 0 : category.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PeriodCatsId other = (PeriodCatsId) obj;
        if (period == null) {
            if (other.getPeriod() != null)
                return false;
        } else if (!period.equals(other.getPeriod()))
            return false;
        if (category == null ) {
            if (other.getCategory() != null)
                return false;
        } else if (!category.equals(other.getCategory()))
            return false;
        return true;
    }

}
