package org.pasteque.api.model.fiscal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.dto.customers.CustomersLightDTO;
import org.pasteque.api.dto.fiscal.PeriodCustBalancesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.customers.Customers;

@Entity
@IdClass(PeriodCustBalancesId.class)
public class PeriodCustBalances implements IAdaptable {

    public final class Table {
        public static final String NAME = "periodcustbalances";
    }

    public final class Fields {
        public static final String PERIOD = "period";
        public static final String CUSTOMER = "customer";
        public static final String BALANCE = "balance";
        
    }
    
    @Id
    @ManyToOne
    @JoinColumn(name = "period_id")
    private Periods period;
    
    @Id
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customers customer;
    
    private Double balance;
    
    public Periods getPeriod() {
        return period;
    }

    public void setPeriod(Periods period) {
        this.period = period;
    }

    public Customers getCustomer() {
        return customer;
    }

    public void setCustomer(Customers customer) {
        this.customer = customer;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if(selector.equals(PeriodCustBalancesDTO.class)) {
            PeriodCustBalancesDTO periodCustBalancesDTO = new PeriodCustBalancesDTO();
            periodCustBalancesDTO.setBalance(getBalance());
            periodCustBalancesDTO.setIdPeriode(getPeriod().getId());
            periodCustBalancesDTO.setClient(getCustomer().getAdapter(CustomersLightDTO.class));
            return (T) periodCustBalancesDTO;
        }
        return null;
    }

}
