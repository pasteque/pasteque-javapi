package org.pasteque.api.model.fiscal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.dto.cash.POSCurrenciesDTO;
import org.pasteque.api.dto.fiscal.PeriodPaymentsDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.cash.Currencies;

@Entity
@IdClass(PeriodPaymentsId.class)
public class PeriodPayments implements IAdaptable {

    public final class Table {
        public static final String NAME = "periodpayments";
    }

    public final class Fields {
        public static final String PERIOD = "period";
        public static final String PAYMENT_MODE_ID = "paymentModeId";
        public static final String CURRENCY = "currency";
        public static final String AMOUNT = "amount";
        public static final String CURRENCY_AMOUNT = "currencyAmount";
        
    }
    
    @Id
    @ManyToOne
    @JoinColumn(name = "period_id")
    private Periods period;
    
    @Id
    @Column(name = "paymentmode_id")
    private String paymentModeId;
    
    @Id
    @ManyToOne
    @JoinColumn(name = "currency_id")
    private Currencies currency;
    
    private Double amount;
    
    private Double currencyAmount;
    
    public Periods getPeriod() {
        return period;
    }

    public void setPeriod(Periods period) {
        this.period = period;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPaymentModeId() {
        return paymentModeId;
    }

    public void setPaymentModeId(String paymentModeId) {
        this.paymentModeId = paymentModeId;
    }

    public Currencies getCurrency() {
        return currency;
    }

    public void setCurrency(Currencies currency) {
        this.currency = currency;
    }

    public Double getCurrencyAmount() {
        return currencyAmount;
    }

    public void setCurrencyAmount(Double currencyAmount) {
        this.currencyAmount = currencyAmount;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if(selector.equals(PeriodPaymentsDTO.class)) {
            PeriodPaymentsDTO periodPaymentsDTO = new PeriodPaymentsDTO();
            periodPaymentsDTO.setIdPeriode(getPeriod().getId());
            periodPaymentsDTO.setMontant(getAmount());
            periodPaymentsDTO.setMonnaie(getCurrency().getAdapter(POSCurrenciesDTO.class));
            periodPaymentsDTO.setMontantMonnaie(getCurrencyAmount());
            return (T) periodPaymentsDTO;
        }
        return null;
    }

}
