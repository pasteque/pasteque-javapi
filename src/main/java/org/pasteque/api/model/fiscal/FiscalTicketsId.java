package org.pasteque.api.model.fiscal;

import java.io.Serializable;

public class FiscalTicketsId implements Serializable{

    private static final long serialVersionUID = 4790851652395300856L;

    private String type;

    private String sequence;

    private Integer number;
    
    public FiscalTicketsId() {
    }
    
    public FiscalTicketsId(String type, String sequence, Integer number) {
        this.type = type;
        this.sequence = sequence;
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public String getSequence() {
        return sequence;
    }

    public Integer getNumber() {
        return number;
    }

    @Override
    public int hashCode() {
        final int prime = 79;
        int result = 1;
        result = prime * result
                + ((type == null) ? 0 : type.hashCode());
        result = prime * result
                + ((sequence == null) ? 0 : sequence.hashCode());
        result = prime * result
                + ((number == null) ? 0 : number.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FiscalTicketsId other = (FiscalTicketsId) obj;
        if (type == null) {
            if (other.getType() != null)
                return false;
        } else if (!type.equals(other.getType()))
            return false;
        if (sequence == null) {
            if (other.getSequence() != null)
                return false;
        } else if (!sequence.equals(other.getSequence()))
            return false;
        if (number == null) {
            if (other.getNumber() != null)
                return false;
        } else if (!number.equals(other.getNumber()))
            return false;
        return true;
    }

}
