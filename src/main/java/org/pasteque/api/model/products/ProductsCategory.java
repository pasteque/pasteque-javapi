package org.pasteque.api.model.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "products_cat")
public class ProductsCategory {

    public final class Fields {
        public static final String PRODUCTS = "products";
        public static final String CATEGORY_ORDER = "categoryOrder";
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "product")
    private Products products;
    @Column(name = "catorder")
    private Integer categoryOrder;

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public Integer getCategoryOrder() {
        return categoryOrder;
    }

    public void setCategoryOrder(Integer categoryOrder) {
        this.categoryOrder = categoryOrder;
    }
}
