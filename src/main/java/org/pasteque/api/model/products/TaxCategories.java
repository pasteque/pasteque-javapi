package org.pasteque.api.model.products;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.pasteque.api.dto.products.POSTaxCategoriesDTO;
import org.pasteque.api.dto.products.POSTaxesDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.adaptable.IAdaptable;

@Entity
public class TaxCategories implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";

        public static final String TAXES = "taxes";
    }

    @Id
    private String id;
    private String name;
    @OneToMany(mappedBy = Taxes.Fields.CATEGORY)
    private List<Taxes> taxes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Taxes> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<Taxes> taxes) {
        this.taxes = taxes;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSTaxCategoriesDTO.class)) {
            POSTaxCategoriesDTO pOSTaxCategoriesDTO = new POSTaxCategoriesDTO();
            pOSTaxCategoriesDTO.setId(getId());
            pOSTaxCategoriesDTO.setLabel(getName());
            pOSTaxCategoriesDTO.setTaxes(AdaptableHelper.getAdapter(getTaxes(), POSTaxesDTO.class));
            return (T) pOSTaxCategoriesDTO;
        }
        return null;
    }

}
