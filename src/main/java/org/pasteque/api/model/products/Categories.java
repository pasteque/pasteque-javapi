package org.pasteque.api.model.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;

@Entity
public class Categories implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String IMAGE = "image";
        public static final String DISPLAY_ORDER = "displayOrder";

        public static final String PARENT = "parent";
    }

    public static final String DEFAULT_CATEGORY = "000";

    public static final String COMPOSITION = "0";

    @Id
    private String id;
    private String name;
    private byte[] image;
    @Column(name = "disporder")
    private int displayOrder;

    @ManyToOne
    @JoinColumn(name = "parentid")
    private Categories parent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public Categories getParent() {
        return parent;
    }

    public void setParent(Categories parent) {
        this.parent = parent;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSCategoriesDTO.class)) {
            POSCategoriesDTO pOSCategoriesDTO = new POSCategoriesDTO();
            pOSCategoriesDTO.setDispOrder(getDisplayOrder());
            if (getImage() != null) {
                pOSCategoriesDTO.setHasImage(true);
            }
            pOSCategoriesDTO.setId(getId());
            pOSCategoriesDTO.setLabel(getName());
            if (getParent() != null) {
                pOSCategoriesDTO.setParentId(getParent().getId());
            }
            return (T) pOSCategoriesDTO;
        }
        return null;
    }

    public boolean hasImage() {
        // TODO Auto-generated method stub
        return image != null && image.length >0 ;
    }
}
