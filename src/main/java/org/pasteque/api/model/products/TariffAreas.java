package org.pasteque.api.model.products;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.pasteque.api.dto.products.POSTariffAreaPriceDTO;
import org.pasteque.api.dto.products.POSTariffAreasDTO;
import org.pasteque.api.dto.products.TariffAreasIndexDTO;
import org.pasteque.api.dto.products.POSTariffAreasValidityDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.adaptable.IAdaptable;


@Entity
public class TariffAreas implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String ORDER = "order";
        public static final String TARIFF_AREAS_PRODUCTS = "tariffAreasProducts";
        public static final String TARIFF_AREAS_VALIDITY = "tariffAreasValidity";
    }
    
    @Id
    private Long id;
    private String name;
    @Column(name = "tarifforder")
    private int order;

    @OneToMany(mappedBy = TariffAreasProducts.Fields.TARIFF_AREAS)
    private List<TariffAreasProducts> tariffAreasProducts;
    
    @OneToMany(mappedBy = TariffAreasValidity.Fields.TARIFF_AREAS)
    @OrderBy("startDate DESC")
    private List<TariffAreasValidity> tariffAreasValidity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<TariffAreasProducts> getTariffAreasProducts() {
        return tariffAreasProducts;
    }

    public void setTariffAreasProducts(List<TariffAreasProducts> tariffAreasProducts) {
        this.tariffAreasProducts = tariffAreasProducts;
    }

    public List<TariffAreasValidity> getTariffAreasValidity() {
        return tariffAreasValidity;
    }

    public void setTariffAreasValidity(List<TariffAreasValidity> tariffAreasValidity) {
        this.tariffAreasValidity = tariffAreasValidity;
    }
    
    public TarriffAreasManager getTarriffAreasManager() {
        return new TarriffAreasManager(this);
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
           return getAdapter(selector , false);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector , boolean filtered) {
        if (selector.equals(POSTariffAreasDTO.class)) {
            POSTariffAreasDTO pOSTariffAreasDTO = new POSTariffAreasDTO();
            pOSTariffAreasDTO.setId(getId());
            pOSTariffAreasDTO.setDispOrder(getOrder());
            pOSTariffAreasDTO.setLabel(getName());
            pOSTariffAreasDTO.setPrices(AdaptableHelper.getAdapter(getTariffAreasProducts(), POSTariffAreaPriceDTO.class));
            pOSTariffAreasDTO.setValidity(AdaptableHelper.getAdapter(getTariffAreasValidity(), POSTariffAreasValidityDTO.class));
            return (T) pOSTariffAreasDTO;
        } else if (selector.equals(TariffAreasIndexDTO.class)) {
            TariffAreasIndexDTO tariffAreasIndexDTO = new TariffAreasIndexDTO();
            tariffAreasIndexDTO.setId(String.valueOf(getId()));
            tariffAreasIndexDTO.setNom(getName());
            return (T) tariffAreasIndexDTO;
        }
        return null;
    }

}
