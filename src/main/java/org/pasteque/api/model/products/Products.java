package org.pasteque.api.model.products;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;

import org.pasteque.api.dto.application.POSMessagesDTO;
import org.pasteque.api.dto.products.POSCompositionsDTO;
import org.pasteque.api.dto.products.POSProductsComponentsDTO;
import org.pasteque.api.dto.products.POSProductsDTO;
import org.pasteque.api.dto.products.ProductsIndexDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.application.AttributeSet;
import org.pasteque.api.model.application.PosMessageType;
import org.pasteque.api.model.application.PosMessages;

@Entity
public class Products implements IAdaptable {

    public static final String EMPLACEMENT_INCONNU = "_NR";

    public Products() {
        this.isCom = false;
        this.isScale = false;
        this.discountEnabled = false;
        this.discountRate = (double) 0;
        this.deleted = false;
        this.stockable = true;
    }

    public final class Fields {
        public static final String ID = "id";
        public static final String REFERENCE = "reference";
        public static final String CODE = "code";
        public static final String NAME = "name";
        public static final String CODE_TYPE = "codeType";
        public static final String PRICE_BUY = "priceBuy";
        public static final String PRICE_SELL = "priceSell";
        public static final String STOCK_COST = "stockCost";
        public static final String STOCK_VOLUME = "stockVolume";
        public static final String IS_COM = "isCom";
        public static final String IS_SCALE = "isScale";
        public static final String ATTRIBUTES = "attributes";
        public static final String DISCOUNT_ENABLED = "discountEnabled";
        public static final String DISCOUNT_RATE = "discountRate";
        public static final String DELETED = "deleted";

        public static final String LAST_UPDATE = "lastUpdate";

        public static final String CATEGORY = "category";
        public static final String TAX_CATEGORY = "taxCategory";
        public static final String ATTRIBUTE_SET = "attributeSet";
        public static final String SUB_GROUPS = "subGroups";
        public static final String PRODUCTS_CATEGORIES = "productsCategories";
        public static final String COMPONENTS_PRODUCTS = "componentsProducts";

        public static final String MESSAGES = "posMessages";
        public static final String REFERENCES = "references";

        public static final String TAX_CATEGORIES = "taxCategories";

        public static final String STOCKABLE = "stockable";
    }

    @Id
    private String id;
    private String reference;
    private String code;
    @Column(name = "codetype")
    private String codeType;
    private String name;
    @Column(name = "pricebuy")
    private Double priceBuy;
    @Column(name = "pricesell")
    private Double priceSell;
    @Column(name = "stockcost")
    private Double stockCost;
    @Column(name = "stockvolume")
    private Double stockVolume;
    private byte[] image;
    @Column(name = "iscom")
    private boolean isCom;
    @Column(name = "isscale")
    private boolean isScale;
    private byte[] attributes;
    @Column(name = "discountenabled")
    private boolean discountEnabled;
    @Column(name = "discountrate")
    private Double discountRate;
    @Column(name = "deleted")
    private boolean deleted;
    @Column(name = "stockable")
    private boolean stockable;

    private Date lastUpdate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category")
    private Categories category;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "taxcat")
    private TaxCategories taxCategory;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "attributeset_id")
    private AttributeSet attributeSet;

//    @OneToMany(mappedBy = SubGroups.Fields.COMPOSITION)
//    private List<SubGroups> subGroups;
    
    @OneToMany(mappedBy = ProductsCategory.Fields.PRODUCTS, cascade = CascadeType.PERSIST)
    private List<ProductsCategory> productsCategories;

    @OneToMany(mappedBy = PosMessages.Fields.PRODUCTS, cascade = CascadeType.PERSIST)
    private List<PosMessages> posMessages;

    @OneToMany(mappedBy = ProductReferences.Fields.PRODUCT)
    private List<ProductReferences> references;

    @OneToMany(mappedBy = ProductsComponents.Fields.COMPONENT)
    private List<ProductsComponents> mastersProducts;

    @OneToMany(mappedBy = ProductsComponents.Fields.MASTER)
    private List<ProductsComponents> componentsProducts;

    @ManyToMany
    @JoinTable(name = "PRODUCT_TAX_CATEG", joinColumns = { @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "ID") }, inverseJoinColumns = {
            @JoinColumn(name = "TAX_CATEG_ID", referencedColumnName = "ID") })
    @OrderColumn(name = "ORDER")
    private List<TaxCategories> taxCategories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public boolean setReference(String reference) {
        boolean retour = this.reference != null ? this.reference.equals(reference) : reference == null;
        this.reference = reference;
        return !retour;
    }

    public String getCode() {
        return code;
    }

    public boolean setCode(String code) {
        boolean retour = this.code != null ? this.code.equals(code) : code == null;
        this.code = code;
        return !retour;
    }

    public String getCodeType() {
        return codeType;
    }

    public boolean setCodeType(String codeType) {
        boolean retour = this.codeType != null ? this.codeType.equals(codeType) : codeType == null;
        this.codeType = codeType;
        return !retour;
    }

    public String getName() {
        return name;
    }

    public boolean setName(String name) {
        boolean retour = this.name != null ? this.name.equals(name) : name == null;
        this.name = name;
        return !retour;
    }

    public Double getPriceBuy() {
        return priceBuy;
    }

    public boolean setPriceBuy(Double priceBuy) {
        boolean retour = this.priceBuy != null ? this.priceBuy.equals(priceBuy) : priceBuy == null;
        this.priceBuy = priceBuy;
        return !retour;
    }

    public Double getPriceSell() {
        return priceSell;
    }

    public boolean setPriceSell(Double priceSell) {
        boolean retour = this.priceSell != null ? this.priceSell.equals(priceSell) : priceSell == null;
        this.priceSell = priceSell;
        return !retour;
    }

    public Double getStockCost() {
        return stockCost;
    }

    public boolean setStockCost(Double stockCost) {
        boolean retour = this.stockCost != null ? this.stockCost.equals(stockCost) : stockCost == null;
        this.stockCost = stockCost;
        return !retour;
    }

    public Double getStockVolume() {
        return stockVolume;
    }

    public boolean setStockVolume(Double stockVolume) {
        boolean retour = this.stockVolume != null ? this.stockVolume.equals(stockVolume) : stockVolume == null;
        this.stockVolume = stockVolume;
        return !retour;
    }

    public byte[] getImage() {
        return image;
    }

    public boolean setImage(byte[] image) {
        boolean retour = this.image != null ? this.image.equals(image) : image == null;
        this.image = image;
        return !retour;
    }

    public boolean isCom() {
        return isCom;
    }

    public boolean setCom(boolean isCom) {
        boolean retour = isCom == this.isCom;
        this.isCom = isCom;
        return !retour;
    }

    public boolean isScale() {
        return isScale;
    }

    public boolean setScale(boolean isScale) {
        boolean retour = isScale == this.isScale;
        this.isScale = isScale;
        return !retour;
    }

    public byte[] getAttributes() {
        return attributes;
    }

    public void setAttributes(byte[] attributes) {
        this.attributes = attributes;
    }

    public boolean isDiscountEnabled() {
        return discountEnabled;
    }

    public boolean setDiscountEnabled(boolean discountEnabled) {
        boolean retour = this.discountEnabled == discountEnabled;
        this.discountEnabled = discountEnabled;
        return !retour;
    }

    public Double getDiscountRate() {
        return discountRate;
    }

    public boolean setDiscountRate(Double discountRate) {
        boolean retour = this.discountRate != null ? this.discountRate.equals(discountRate) : discountRate == null;
        this.discountRate = discountRate;
        return !retour;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public boolean setDeleted(boolean deleted) {
        boolean retour = this.deleted == deleted;
        this.deleted = deleted;
        return !retour;
    }

    public Categories getCategory() {
        return category;
    }

    public boolean setCategory(Categories category) {
        boolean retour = this.category != null ? this.category.getId().equals(category == null ? "" : category.getId()) : category == null;
        this.category = category;
        return !retour;
    }

    public TaxCategories getTaxCategory() {
        return taxCategory;
    }

    public boolean setTaxCategory(TaxCategories taxCategory) {
        boolean retour = this.taxCategory != null ? this.taxCategory.getId().equals(taxCategory == null ? "" : taxCategory.getId()) : taxCategory == null;
        this.taxCategory = taxCategory;
        return !retour;
    }

    public AttributeSet getAttributeSet() {
        return attributeSet;
    }

    public void setAttributeSet(AttributeSet attributeSet) {
        this.attributeSet = attributeSet;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {

        if (selector.equals(POSProductsDTO.class)) {
            List<POSProductsComponentsDTO> parentProductsIds;
            List<POSProductsComponentsDTO> childProductsIds;
            // List<MessagesDTO> messagesList;
            List<String> barcodeList;

            POSProductsDTO pOSProductsDTO = new POSProductsDTO();
            pOSProductsDTO.setId(getId());
            pOSProductsDTO.setReference(getReference());
            pOSProductsDTO.setBarcode(getCode());
            pOSProductsDTO.setLabel(getName());

            if (getPriceBuy() != null) {
                pOSProductsDTO.setPriceBuy(Double.toString(getPriceBuy()));
            }
            if (getPriceSell() != null) {
                pOSProductsDTO.setPriceSell(Double.toString(getPriceSell()));
            }
            pOSProductsDTO.setVisible(!isDeleted());
            pOSProductsDTO.setScaled(isScale());
            pOSProductsDTO.setStockable(isStockable());
            pOSProductsDTO.setCom(isCom());
            if (getCategory() != null) {
                pOSProductsDTO.setCategoryId(getCategory().getId());
            }
            if (getProductsCategories() != null) {
                // normalement, il doit y avoir un seul enregistrement.
                for (ProductsCategory productsCategory : getProductsCategories()) {
                    pOSProductsDTO.setDispOrder(productsCategory.getCategoryOrder());
                }
            }
            if (getTaxCategory() != null) {
                pOSProductsDTO.setTaxCatId(getTaxCategory().getId());
            }
            if (getAttributeSet() != null) {
                pOSProductsDTO.setAttributeSetId(getAttributeSet().getId());
            }
            if (getImage() != null) {
                pOSProductsDTO.setHasImage(true);
            }
            pOSProductsDTO.setDiscountEnabled(isDiscountEnabled());
            pOSProductsDTO.setDiscountRate(Double.toString(getDiscountRate()));

            childProductsIds = new ArrayList<POSProductsComponentsDTO>();
            for (ProductsComponents elements : getComponentsProducts()) {
                POSProductsComponentsDTO el = new POSProductsComponentsDTO();
                el.setChildId(elements.getComponent().getId());
                el.setParentId(elements.getMaster().getId());
                el.setQuantity(elements.getQuantity());
                childProductsIds.add(el);
            }
            pOSProductsDTO.setChildProducts(childProductsIds);

            parentProductsIds = new ArrayList<POSProductsComponentsDTO>();
            for (ProductsComponents elements : getMastersProducts()) {
                POSProductsComponentsDTO el = new POSProductsComponentsDTO();
                el.setChildId(elements.getComponent().getId());
                el.setParentId(elements.getMaster().getId());
                el.setQuantity(elements.getQuantity());
                parentProductsIds.add(el);
            }
            pOSProductsDTO.setParentProducts(parentProductsIds);

            barcodeList = new ArrayList<String>();
            for (ProductReferences elements : getReferences()) {
                barcodeList.add(elements.getReference());
            }
            pOSProductsDTO.setBarcodes(barcodeList);
            pOSProductsDTO.setMessages(AdaptableHelper.getAdapter(getMessages(), POSMessagesDTO.class));

            // messagesList = new ArrayList<MessagesDTO>();
            // for (PosMessages elements : getMessages()) {
            // MessagesDTO el = new MessagesDTO();
            // el.setContent(elements.getContent());
            // el.setStartDate(elements.getStartDate());
            // el.setEndDate(elements.getEndDate());
            // el.setType(elements.getType());
            // el.setProductId(id);
            // messagesList.add(el);
            // }
            // productsDTO.setMessages(messagesList);

            List<String> taxCategId = new ArrayList<String>();
            for (TaxCategories taxCategories : getTaxCategories()) {
                taxCategId.add(taxCategories.getId());
            }
            pOSProductsDTO.setTaxCategories(taxCategId);
            return (T) pOSProductsDTO;
        } else if (selector.equals(POSCompositionsDTO.class)) {
            POSCompositionsDTO pOSCompositionsDTO = new POSCompositionsDTO();
            pOSCompositionsDTO.setId(getId());
            pOSCompositionsDTO.setReference(getReference());
            pOSCompositionsDTO.setBarcode(getCode());
            pOSCompositionsDTO.setLabel(getName());
            pOSCompositionsDTO.setPriceBuy(Double.toString(getPriceBuy()));
            pOSCompositionsDTO.setPriceSell(Double.toString(getPriceSell()));
            pOSCompositionsDTO.setVisible(!isDeleted());
            pOSCompositionsDTO.setScaled(isScale());
            pOSCompositionsDTO.setStockable(isStockable());
            if (getCategory() != null) {
                pOSCompositionsDTO.setCategoryId(getCategory().getId());
            }
            if (getProductsCategories() != null) {
                // normalement, il doit y avoir un seul enregistrement.
                for (ProductsCategory productsCategory : getProductsCategories()) {
                    pOSCompositionsDTO.setDispOrder(productsCategory.getCategoryOrder());
                }
            }
            if (getTaxCategory() != null) {
                pOSCompositionsDTO.setTaxCatId(getTaxCategory().getId());
            }
            if (getAttributeSet() != null) {
                pOSCompositionsDTO.setAttributeSetId(getAttributeSet().getId());
            }
            if (getImage() != null) {
                pOSCompositionsDTO.setHasImage(true);
            }
            pOSCompositionsDTO.setDiscountEnabled(isDiscountEnabled());
            pOSCompositionsDTO.setDiscountRate(Double.toString(getDiscountRate()));
//            compositionsDTO.setGroups(AdaptableHelper.getAdapter(getSubGroups(), GroupsDTO.class));

            return (T) pOSCompositionsDTO;
        } else if (selector.equals(ProductsIndexDTO.class)) {
            ProductsIndexDTO productsIndexDTO = new ProductsIndexDTO();
            productsIndexDTO.setId(getId());
            productsIndexDTO.setLibelle(getName());
            productsIndexDTO.setCategorie(getCategory().getName());
            return (T) productsIndexDTO;
        }
        return null;
    }

    public List<PosMessages> getAvailableMessages() {
        List<PosMessages> messages = new ArrayList<PosMessages>();
        for (PosMessages message : getMessages()) {
            if (message.isAvailable()) {
                messages.add(message);
            }
        }
        return messages;
    }

    public List<ProductsCategory> getProductsCategories() {
        return productsCategories;
    }

    public void setProductsCategories(List<ProductsCategory> productsCategories) {
        this.productsCategories = productsCategories;
    }

    public List<PosMessages> getMessages() {
        return posMessages;
    }

    public void setMessages(List<PosMessages> posMessages) {
        this.posMessages = posMessages;
    }

    public List<ProductReferences> getReferences() {
        return references;
    }

    public void setReferences(List<ProductReferences> references) {
        this.references = references;
    }

    public List<ProductsComponents> getMastersProducts() {
        return mastersProducts;
    }

    public void setMastersProducts(List<ProductsComponents> mastersProducts) {
        this.mastersProducts = mastersProducts;
    }

    public List<ProductsComponents> getComponentsProducts() {
        return componentsProducts;
    }

    public void setComponentsProducts(List<ProductsComponents> componentsProducts) {
        this.componentsProducts = componentsProducts;
    }

    public List<TaxCategories> getTaxCategories() {
        return taxCategories;
    }

    public void setTaxCategories(List<TaxCategories> taxCategories) {
        this.taxCategories = taxCategories;
    }

    public boolean isStockable() {
        return stockable;
    }

    public void setStockable(boolean stockable) {
        this.stockable = stockable;
    }

    /*
     * public List<InventoryDetails> getInventoryDetails() { return inventoryDetails; }
     * 
     * public void setInventoryDetails(List<InventoryDetails> inventoryDetails) { this.inventoryDetails = inventoryDetails; }
     */
    public String getProductLocation(String locationId) {
        for (PosMessages message : getAvailableMessages()) {
            if (message.getType().getType().equals(PosMessageType.Values.EMPL) && message.getLocationId().equals(locationId)) {
                return message.getContent();
            }
        }
        return Products.EMPLACEMENT_INCONNU;
    }
}
