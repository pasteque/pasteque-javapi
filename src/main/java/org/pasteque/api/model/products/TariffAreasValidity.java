package org.pasteque.api.model.products;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.TariffAreasIndexDTO;
import org.pasteque.api.dto.products.TariffAreasLocationsDTO;
import org.pasteque.api.dto.products.POSTariffAreasValidityDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.saleslocations.Locations;

@Entity
@Table(name = TariffAreasValidity.Table.NAME)
public class TariffAreasValidity implements IAdaptable {
    public final class Table {
        public static final String NAME = "tariffareas_location";
    }

    public final class Fields {
        public static final String TARIFF_AREAS = "tariffAreas";
        public static final String LOCATION = "locations";
        public static final String START_DATE = "startDate";
        public static final String END_DATE = "endDate";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "tariffareas_id")
    private TariffAreas tariffAreas;

    @ManyToOne
    @JoinColumn(name = "location_id")
    private Locations locations;

    @Column(name = "date_from")
    private Date startDate;

    @Column(name = "date_to")
    private Date endDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TariffAreas getTariffAreas() {
        return tariffAreas;
    }

    public void setTariffAreas(TariffAreas tariffAreas) {
        this.tariffAreas = tariffAreas;
    }

    public Locations getLocations() {
        return locations;
    }

    public void setLocations(Locations locations) {
        this.locations = locations;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public TarriffAreasManager getTarriffAreasManager() {
        return new TarriffAreasManager(this);
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        boolean actif = false;
        Date currentDate = new Date();
        if (getStartDate() != null && getEndDate() != null && ((getStartDate().before(currentDate) || getStartDate().equals(currentDate))
                && (getEndDate().after(currentDate) || getEndDate().equals(currentDate)))) {
            actif = true;
        } else {
            actif = false;
        }
        if (selector.equals(POSTariffAreasValidityDTO.class)) {
            POSTariffAreasValidityDTO pOSTariffAreasValidityDTO = new POSTariffAreasValidityDTO();
            pOSTariffAreasValidityDTO.setId(getId());
            pOSTariffAreasValidityDTO.setAreaId(getTariffAreas() == null ? null : getTariffAreas().getId());
            pOSTariffAreasValidityDTO.setLocationId(getLocations() == null ? null : getLocations().getId());
            pOSTariffAreasValidityDTO.setStartDate(getStartDate() == null ? null : getStartDate().getTime());
            pOSTariffAreasValidityDTO.setEndDate(getEndDate() == null ? null : getEndDate().getTime());
            pOSTariffAreasValidityDTO.setActif(actif);
            return (T) pOSTariffAreasValidityDTO;
        } else if (selector.equals(TariffAreasLocationsDTO.class)) {
            TariffAreasLocationsDTO tariffAreasLocationsDTO = new TariffAreasLocationsDTO();
            tariffAreasLocationsDTO.setId(this.id);
            tariffAreasLocationsDTO.setStartDate(this.startDate);
            tariffAreasLocationsDTO.setEndDate(this.endDate);
            if (getLocations() != null) {
                tariffAreasLocationsDTO.setLocation(getLocations().getAdapter(LocationsInfoDTO.class));
            }
            if (getTariffAreas() != null) {
                tariffAreasLocationsDTO.setTariffArea(getTariffAreas().getAdapter(TariffAreasIndexDTO.class));
            }
            tariffAreasLocationsDTO.setActif(actif);
            return (T) tariffAreasLocationsDTO;
        }
        return null;
    }

    public String getLocationId() {
        return getLocations() == null ? null : getLocations().getId();
    }

}
