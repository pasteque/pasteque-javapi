package org.pasteque.api.model.products;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.dto.products.POSTaxesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;

@Entity
public class Taxes implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String VALID_FROM = "validFrom";
        public static final String VALID_TO = "validTo";
        public static final String RATE = "rate";
        public static final String AMOUNT = "amount";
        public static final String RATE_CASCADE = "rateCascade";
        public static final String RATE_ORDER = "rateOrder";
        public static final String CATEGORY = "category";
        public static final String CUST_CATEGORY = "custCategory";
        public static final String PARENT = "parent";
    }

    @Id
    private String id;
    private String name;
    private Date validFrom;
    private Date validTo;
    private Double rate;
    private Double amount;
    private boolean rateCascade;
    private int rateOrder;

    @ManyToOne
    @JoinColumn(name = "category")
    private TaxCategories category;
    @ManyToOne
    @JoinColumn(name = "custcategory")
    private TaxCustCategories custCategory;
    @ManyToOne
    @JoinColumn(name = "parentid")
    private Taxes parent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public boolean isRateCascade() {
        return rateCascade;
    }

    public void setRateCascade(boolean rateCascade) {
        this.rateCascade = rateCascade;
    }

    public int getRateOrder() {
        return rateOrder;
    }

    public void setRateOrder(int rateOrder) {
        this.rateOrder = rateOrder;
    }

    public TaxCategories getCategory() {
        return category;
    }

    public void setCategory(TaxCategories category) {
        this.category = category;
    }

    public TaxCustCategories getCustCategory() {
        return custCategory;
    }

    public void setCustumersCategory(TaxCustCategories custumersCategory) {
        this.custCategory = custumersCategory;
    }

    public Taxes getParent() {
        return parent;
    }

    public void setParent(Taxes parent) {
        this.parent = parent;
    }
    
    @Override
    public <T> T getAdapter(Class<T> selector) {
           return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector , boolean filtered) {
        if (selector.equals(POSTaxesDTO.class)) {
            POSTaxesDTO pOSTaxesDTO = new POSTaxesDTO();
            pOSTaxesDTO.setId(getId());
            pOSTaxesDTO.setLabel(getName());
            // J'ai cherché un bon moment ... c'est décodé comme ca :
            // return new Date(timestamp * 1000);
            pOSTaxesDTO.setStartDatetime(getValidFrom());
            pOSTaxesDTO.setEndDatetime(getValidTo());
            if (getCategory() != null) {
                pOSTaxesDTO.setTaxCatId(getCategory().getId());
            }
            if (getCustCategory() != null) {
                pOSTaxesDTO.setTaxCustCategoryId(getCustCategory().getId());
            }
            if (getRate() != null) {
                pOSTaxesDTO.setRate(getRate());
            }
            if (getAmount() != null) {
                pOSTaxesDTO.setAmount(getAmount());
            }
            return (T) pOSTaxesDTO;
        }
        return null;
    }

}
