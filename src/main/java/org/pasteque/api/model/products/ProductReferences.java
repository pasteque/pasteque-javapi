package org.pasteque.api.model.products;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.pasteque.api.dto.products.ProductReferencesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;

@Entity
@Table(name = ProductReferences.Table.NAME)
public class ProductReferences implements IAdaptable{
    
    public final class Table {
        public static final String NAME = "PRODUCTS_REF";
    }
    
    public final class Fields {
        public static final String ID = "id";
        public static final String PRODUCT = "product";
        public static final String REFERENCE = "reference";
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @ManyToOne()
    @JoinColumn(name = "product_id")
    private Products product;
    private String reference;
    
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    public String getReference() {
        return reference;
    }


    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
           return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector , boolean filtered) {
        if (selector.equals(ProductReferencesDTO.class)) {
            ProductReferencesDTO productReferencesDTO =  new ProductReferencesDTO();
            productReferencesDTO.setId(id);
            productReferencesDTO.setReference(reference);
            productReferencesDTO.setProductId(product.getId());
            
            return (T) productReferencesDTO;
        }
        return null;
    }

}
