package org.pasteque.api.model.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = ProductsComponents.Table.NAME)
public class ProductsComponents {

    public final class Table {
        public static final String NAME = "PRODUCTS_COM";
    }
    public final class Fields {
        public static final String ID = "id";
        public static final String QUANTITY = "quantity";
        
        public static final String MASTER = "master";
        public static final String COMPONENT = "component";
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "quantity")
    private double quantity;
    
    @ManyToOne()
    @JoinColumn(name = "product")
    private Products master;
    
    @ManyToOne()
    @JoinColumn(name = "product2")
    private Products component;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Products getMaster() {
        return master;
    }

    public void setMaster(Products master) {
        this.master = master;
    }

    public Products getComponent() {
        return component;
    }

    public void setComponent(Products component) {
        this.component = component;
    }
    
    
    
}
