package org.pasteque.api.model.products;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.pasteque.api.dto.products.POSTariffAreaPriceDTO;
import org.pasteque.api.model.adaptable.IAdaptable;

@Entity
@Table(name = TariffAreasProducts.Table.NAME)
@IdClass(TariffAreasProductsId.class)
public class TariffAreasProducts implements IAdaptable {

    public final class Table {
        public static final String NAME = "tariffareas_prod";
    }

    public final class Fields {
        public static final String TARIFF_AREAS = "tariffAreas";
        public static final String PRODUCTS = "products";
        public static final String PRICE_SELL = "priceSell";
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "tariffid")
    private TariffAreas tariffAreas;
    @Id
    @ManyToOne
    @JoinColumn(name = "productid")
    private Products products;
    private double priceSell;

    public TariffAreas getTariffAreas() {
        return tariffAreas;
    }

    public void setTariffAreas(TariffAreas tariffAreas) {
        this.tariffAreas = tariffAreas;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public double getPriceSell() {
        return priceSell;
    }

    public void setPriceSell(double priceSell) {
        this.priceSell = priceSell;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
           return getAdapter(selector , false);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector , boolean filtered) {
        if (selector.equals(POSTariffAreaPriceDTO.class)) {
            POSTariffAreaPriceDTO pOSTariffAreaPriceDTO = new POSTariffAreaPriceDTO();
            pOSTariffAreaPriceDTO.setProductId(getProducts().getId());
            pOSTariffAreaPriceDTO.setPrice(getPriceSell());
            pOSTariffAreaPriceDTO.setArreaId(getTariffAreas().getId());
            return (T) pOSTariffAreaPriceDTO;
        }
        return null;
    }

    public TarriffAreasManager getTarriffAreasManager() {
        return new TarriffAreasManager(this);
    }
}
