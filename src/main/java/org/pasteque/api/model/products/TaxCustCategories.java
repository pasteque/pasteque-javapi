package org.pasteque.api.model.products;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class TaxCustCategories {
    
    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        
        public static final String TAXES = "taxes";
    }


    @Id
    private String id;
    private String name;
    @OneToMany(mappedBy = Taxes.Fields.CUST_CATEGORY)
    private List<Taxes> taxes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Taxes> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<Taxes> taxes) {
        this.taxes = taxes;
    }

}
