package org.pasteque.api.model.products;

import java.io.Serializable;

public class TariffAreasProductsId implements Serializable{

    private static final long serialVersionUID = 359575523127267458L;

    private Long tariffAreas;

    private String products;

    TariffAreasProductsId(){

    }

    public TariffAreasProductsId(Long tariffAreas , String products) {
        this.tariffAreas = tariffAreas ;
        this.products = products ;
    }

    public Long getTariffAreas() {
        return tariffAreas;
    }

    public String getProducts() {
        return products;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((tariffAreas == null) ? 0 : tariffAreas.hashCode());
        result = prime * result
                + ((products == null) ? 0 : products.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TariffAreasProductsId other = (TariffAreasProductsId) obj;
        if (products == null) {
            if (other.getProducts() != null)
                return false;
        } else if (!products.equals(other.getProducts()))
            return false;
        if (tariffAreas == null) {
            if (other.getTariffAreas() != null)
                return false;
        } else if (!tariffAreas.equals(other.getTariffAreas()))
            return false;
        return true;
    }

}
