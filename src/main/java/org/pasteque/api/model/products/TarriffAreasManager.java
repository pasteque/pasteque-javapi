package org.pasteque.api.model.products;

import java.util.Date;
import java.util.Optional;

import org.pasteque.api.dto.products.POSTariffAreaPriceDTO;
import org.pasteque.api.dto.products.POSTariffAreasDTO;
import org.pasteque.api.dto.products.POSTariffAreasValidityDTO;
import org.pasteque.api.model.saleslocations.Locations;

public class TarriffAreasManager {

    private TariffAreas tariffAreas;
    private TariffAreasProducts tariffAreasProducts;
    private TariffAreasValidity tariffAreasValidity;
    
    public TarriffAreasManager(TariffAreasProducts tariffAreasProducts) {
        this.tariffAreasProducts = tariffAreasProducts;
    }

    public TarriffAreasManager(TariffAreas tariffAreas) {
        this.tariffAreas = tariffAreas;
    }

    public TarriffAreasManager(TariffAreasValidity tariffAreasValidity) {
        this.tariffAreasValidity = tariffAreasValidity;
    }

    public boolean loadFromDTO(Object source) {
        boolean retour = false;
        if (POSTariffAreasDTO.class.isInstance(source)) {
            POSTariffAreasDTO dto = (POSTariffAreasDTO) source;
            if (dto.getDispOrder() != tariffAreas.getOrder()) {
                retour = true;
                tariffAreas.setOrder(dto.getDispOrder());
            }
            if (!dto.getId().equals(tariffAreas.getId())) {
                retour = true;
                tariffAreas.setId(dto.getId());
            }
            if ((dto.getLabel() != null && !dto.getLabel().equals(tariffAreas.getName())) ||
            (dto.getLabel() == null && tariffAreas.getName()!= null)){
                retour = true;
                tariffAreas.setName(dto.getLabel());
            }
        }
        if (POSTariffAreasValidityDTO.class.isInstance(source)) {
            POSTariffAreasValidityDTO dto = (POSTariffAreasValidityDTO) source;
            try{
            if (tariffAreasValidity.getTariffAreas() == null || dto.getAreaId() != tariffAreasValidity.getTariffAreas().getId()) {
                retour = true;
                System.out.println("AreaId : " + String.valueOf(dto.getAreaId()));
                //EDU - Essai récriture sans tariffAreasDAO
                /*
                if(tariffAreasDAO == null) {
                    System.out.println("tariffAreasDAO est null ! ");
                } else {
                    TariffAreas temp = tariffAreasDAO.findByPrimaryKey(dto.getAreaId());;
                    tariffAreasValidity.setTariffAreas(temp );
                }
                */
                TariffAreas temp = new TariffAreas();
                temp.setId(dto.getAreaId());
                tariffAreasValidity.setTariffAreas(temp );
                
            }
            } catch (NullPointerException ex) {
                System.err.println("Catched NullPointerException during use of TariffAreasValidityDTO - ".concat(dto ==null || dto.getAreaId() == 0 ? "Null" : "tariffAreasDAO is null AreaId = ".concat(String.valueOf(dto.getAreaId()))));
                return false; 
            }
            if ((dto.getLocationId() != null && !dto.getLocationId().equals(tariffAreasValidity.getLocationId())) ||
            (dto.getLocationId() == null && tariffAreasValidity.getLocationId()!= null)){
                retour = true;
                /*
                tariffAreasValidity.setLocations(locationsDAO.findByPrimaryKey(dto.getLocationId()));
                */
                Locations temp = new Locations();
                temp.setId(dto.getLocationId());
                tariffAreasValidity.setLocations(temp );
            }
            if ((dto.getStartDate() != null && !dto.getStartDate().equals(Optional.ofNullable(tariffAreasValidity).map(TariffAreasValidity::getStartDate).map(Date::getTime).orElse(null))) ||
            (dto.getStartDate() == null && tariffAreasValidity.getStartDate()!= null)){
                retour = true;
                tariffAreasValidity.setStartDate(new Date(dto.getStartDate()));
            }
            if ((dto.getEndDate() != null && !dto.getEndDate().equals(Optional.ofNullable(tariffAreasValidity).map(TariffAreasValidity::getEndDate).map(Date::getTime).orElse(null))) ||
            (dto.getEndDate() == null && tariffAreasValidity.getEndDate()!= null)){
                retour = true;
                tariffAreasValidity.setEndDate(new Date(dto.getEndDate()));
            }
        }
        if (POSTariffAreaPriceDTO.class.isInstance(source)) {
            POSTariffAreaPriceDTO dto = (POSTariffAreaPriceDTO) source;
            if (tariffAreasProducts.getProducts() == null || !dto.getProductId().equals(tariffAreasProducts.getProducts().getId())) {
                retour = true;
                //EDU - Essai récriture sans productsDAO
                /*
                try {
                tariffAreasProducts.setProducts(dto == null || dto.getProductId() == null ? null : productsDAO.findById(dto.getProductId()));
                if (tariffAreasProducts.getProducts() == null) {
                    //Pas de produit pas d'enregistrement
                    return false;
                }
                } catch (NullPointerException ex) {
                    System.err.println("Catched NullPointerException during use of TariffAreaPriceDTO - ".concat(dto ==null || dto.getProductId() == null ? "Null" : "productsDAO is null Product = ".concat(dto.getProductId())));
                    return false; 
                }
                */
                Products temp = new Products();
                temp.setId(dto.getProductId());
                tariffAreasProducts.setProducts(temp);
                
            }
            if (tariffAreasProducts.getTariffAreas() == null || !dto.getAreaId().equals(tariffAreasProducts.getTariffAreas().getId())) {
                retour = true;
                //EDU - Essai récriture sans tariffAreasDAO
                /*
                tariffAreasProducts.setTariffAreas(tariffAreasDAO.findByPrimaryKey(dto.getAreaId()));
                if (tariffAreasProducts.getTariffAreas() == null ) {
                    //Pas de catalogue pas d'enregistrement !
                    return false;
                }
                */
                TariffAreas temp = new TariffAreas();
                temp.setId(dto.getAreaId());
                tariffAreasProducts.setTariffAreas(temp );
                
            }
            if (dto.getPrice() != tariffAreasProducts.getPriceSell()) {
                retour = true;
                tariffAreasProducts.setPriceSell(dto.getPrice());
            }
        }
        return retour;
    }

}
