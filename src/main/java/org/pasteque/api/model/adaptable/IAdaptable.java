package org.pasteque.api.model.adaptable;

public interface IAdaptable {

    <T> T getAdapter(Class<T> selector);

    <T> T getAdapter(Class<T> selector, boolean filtered);

}
