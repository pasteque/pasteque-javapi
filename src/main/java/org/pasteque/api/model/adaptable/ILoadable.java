package org.pasteque.api.model.adaptable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.pasteque.api.dto.generator.DtoSQLParameters;

public interface ILoadable {

    @JsonIgnore
    public DtoSQLParameters getSQLParameters();
    
    @JsonIgnore
    boolean loadFromObjectArray(Object[] array , int starting_index);

}
