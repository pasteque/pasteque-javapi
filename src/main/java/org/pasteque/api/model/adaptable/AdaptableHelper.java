package org.pasteque.api.model.adaptable;

import java.util.ArrayList;
import java.util.List;

public final class AdaptableHelper {

    private AdaptableHelper() {
    }

    public static <T, S extends IAdaptable> List<T> getAdapter(List<S> list, Class<T> selector) {
        return  getAdapter( list, selector , false);
    }
    
    public static <T, S extends IAdaptable> List<T> getAdapter(List<S> list, Class<T> selector , boolean filtered) {
        List<T> listDTO = new ArrayList<>();
        for (IAdaptable adaptable : list) {
            listDTO.add((T) adaptable.getAdapter(selector , filtered));
        }
        return listDTO;
    }

}
