package org.pasteque.api.model.archives;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import org.pasteque.api.model.adaptable.IAdaptable;

@Entity
public class ArchiveRequests implements IAdaptable {
    public final class Table {
        public static final String NAME = "archiverequests";
    }

    public final class Fields {
        public static final String ID = "id";
        public static final String START_DATE = "startDate";
        public static final String STOP_DATE = "stopDate";
        public static final String PROCESSING = "processing";
    }
    
    @Id @GeneratedValue
    private Integer id;
    private Date startDate;
    private Date stopDate;
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean processing=false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStopDate() {
        return stopDate;
    }

    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public boolean isProcessing() {
        return processing;
    }

    public void setProcessing(boolean processing) {
        this.processing = processing;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        // TODO Auto-generated method stub
        return null;
    }

}

