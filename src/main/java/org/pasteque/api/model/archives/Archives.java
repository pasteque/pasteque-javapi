package org.pasteque.api.model.archives;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.eclipse.persistence.annotations.PrimaryKey;
import org.eclipse.persistence.annotations.IdValidation;
import org.pasteque.api.dto.fiscal.ArchivesDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.signable.ISignable;
import org.springframework.security.crypto.bcrypt.BCrypt;

@Entity
@PrimaryKey(validation=IdValidation.NULL)
public class Archives implements IAdaptable , ISignable{
    public final class Table {
        public static final String NAME = "archives";
    }

    public final class Fields {
        public static final String NUMBER = "number";
        public static final String INFO = "info";
        public static final String CONTENT = "content";
        public static final String CONTENTHASH = "contentHash";
        public static final String SIGNATURE = "signature";
    }

    public static final Integer EOS = 0;
    public static final String EOS_STRING = "EOS";
    
    @Id
    private Integer number;
    @Lob
    private String info;
    @Lob
    private byte[] content;
    private String contentHash;
    private String signature;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
        this.contentHash = BCrypt.hashpw(content, BCrypt.gensalt());
    }

    public String getContentHash() {
        return contentHash;
    }

    public void setContentHash(String contentHash) {
        this.contentHash = contentHash;
    }

    @Override
    public String getSignature() {
        return signature;
    }

    @Override
    public void setSignature(String signature) {
        this.signature = signature;
    }
    
    @Override
    public String getHashBase() {
        return number.toString() + "-" + ( info == null ? "" : info ) + "-" + ( contentHash == null ? "" : contentHash );
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector , false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(ArchivesDTO.class)) {
            ArchivesDTO archivesDTO = new ArchivesDTO();
            archivesDTO.setNumber(getNumber());
            archivesDTO.setInfo(getInfo());
            archivesDTO.setSignature(getSignature());
            return (T) archivesDTO;
        }
        return null;
    }

}
