package org.pasteque.api.model.signable;

public interface ISignable {

    String getHashBase();

    String getSignature();

    void setSignature(String signature);

}
