package org.pasteque.api.model.tickets;

import java.io.Serializable;

public class TicketLinesId implements Serializable{

    private static final long serialVersionUID = 8251093807690831186L;

    String ticket;
    Integer line;

    public TicketLinesId() {
    }
    
    public TicketLinesId(String ticket, Integer line) {
        this.ticket = ticket;
        this.line = line;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((ticket == null) ? 0 : ticket.hashCode());
        result = prime * result
                + ((line == null) ? 0 : line.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TicketLinesId other = (TicketLinesId) obj;
        if (ticket == null) {
            if (other.getTicket() != null)
                return false;
        } else if (!ticket.equals(other.getTicket()))
            return false;
        if (line == null) {
            if (other.getLine() != null)
                return false;
        } else if (!line.equals(other.getLine()))
            return false;
        return true;
    }
}
