package org.pasteque.api.model.tickets;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.pasteque.api.dto.tickets.POSTicketLinesDTO;
import org.pasteque.api.dto.tickets.TicketsLineInfoDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.TariffAreas;
import org.pasteque.api.model.products.Taxes;

@Entity
@IdClass(TicketLinesId.class)
public class TicketLines implements IAdaptable {

    public final class Fields {
        public static final String TICKET = "ticket";
        public static final String LINE = "line";
        public static final String PRODUCT = "product";
        public static final String ATTRIBUTE_SET_INSTANCE_ID = "attributeSetInstanceId";
        public static final String UNITS = "units";
        public static final String PRICE = "price";
        public static final String TAX = "tax";
        public static final String DISCOUNT_RATE = "discountRate";
        public static final String ATTRIBUTES = "attributes";
        public static final String TYPE = "type";
        public static final String TARIFF_AREA = "tariffArea";
        public static final String NOTE = "note";
        public static final String PARENTLINE = "parentLine";
        public static final String FINAL_PRICE = "finalPrice";
        public static final String FINAL_TAXED_PRICE = "finalTaxedPrice";
        public static final String UNIT_PRICE = "unitPrice";
        public static final String TAXED_UNIT_PRICE = "taxedUnitPrice";
        public static final String TAXED_PRICE = "taxedPrice";
        public static final String TAXRATE = "taxRate";
    }

    public final class LineType {
        public static final String ORDER = "O";
        public static final String DELIVERY = "S";
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "ticket")
    private Tickets ticket;
    @Id
    private Integer line;
    @ManyToOne
    @JoinColumn(name = "product")
    private Products product;
    @Column(name = "attributesetinstance_id")
    private String attributeSetInstanceId;
    private double units;
    //Montant HT de la ligne sans remises
    //Attention avant il s'agissait du montant HT unitaire ->unitPrice
    //On a les PU HT et TTC mais un seul des deux est setté
    //Ainsi on peut conserver l'information duquel a servi de base de calcul
    private double price;
    @ManyToOne
    @JoinColumn(name = "taxid")
    private Taxes tax;
    private double discountRate;
    private byte[] attributes;
    private char type;
    @ManyToOne
    @JoinColumn(name = "tariffArea")
    private TariffAreas tariffArea;
    private String note;
    @OneToOne
    @JoinColumns({ @JoinColumn(name = "parent_ticket_id", referencedColumnName = "ticket", insertable = false, updatable = false),
        @JoinColumn(name = "parent_ticket_line", referencedColumnName = "line", insertable = false, updatable = false) })
    private TicketLines parentLine;

    @Column(name = "parent_ticket_line")
    private Integer parentLineNumber;
    @Column(name = "parent_ticket_id")
    private Long parentTicketId;

    //EDU 2020 08 Nouveau champs
    //On avait déjà la notion de final price mais sans le champ
    //TODO adapter le getter
    //Montant HT de la ligne  
    private Double finalPrice;
    //Montant TTC de la ligne
    private Double finalTaxedPrice;
    //Prix Unitaire HT
    private Double unitPrice;
    //Prix Unitaire TTC
    private Double taxedUnitPrice;
    //Montant TTC de la ligne sans remises
    private Double taxedPrice;
    //Taux de Taxes
    private Double taxRate;

    public Tickets getTicket() {
        return ticket;
    }

    public void setTicket(Tickets ticket) {
        this.ticket = ticket;
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    public String getAttributeSetInstanceId() {
        return attributeSetInstanceId;
    }

    public void setAttributeSetInstanceId(String attributeSetInstanceId) {
        this.attributeSetInstanceId = attributeSetInstanceId;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Taxes getTax() {
        return tax;
    }

    public void setTax(Taxes tax) {
        this.tax = tax;
    }

    public double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(double discountRate) {
        this.discountRate = discountRate;
    }

    public byte[] getAttributes() {
        return attributes;
    }

    public void setAttributes(byte[] attributes) {
        this.attributes = attributes;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public boolean isOrder() {
        return LineType.ORDER.equals(String.valueOf(type));
    }

    public TariffAreas getTariffArea() {
        return tariffArea;
    }

    public void setTariffArea(TariffAreas tariffArea) {
        this.tariffArea = tariffArea;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public TicketLines getParentLine() {
        return parentLine;
    }

    public void setParentLine(TicketLines parentLine) {
        this.parentLine = parentLine;
        parentLineNumber = parentLine == null ? null : parentLine.getLine();
        parentTicketId = parentLine == null ? null : parentLine.getTicket().getTicketId();
    }

    public Integer getParentLineNumber() {
        return parentLineNumber;
    }

    public Long getParentTicketId() {
        return parentTicketId;
    }

    public void setParentLineNumber(Integer parentLineNumber) {
        this.parentLineNumber = parentLineNumber;
    }

    public void setParentTicketId(Long parentTicketId) {
        this.parentTicketId = parentTicketId;
    }

    public Double getFinalTaxedPrice() {
        return finalTaxedPrice;
    }

    public void setFinalTaxedPrice(Double finalTaxedPrice) {
        this.finalTaxedPrice = finalTaxedPrice;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    //Si on set un prix unitaire HT ou TTC on passe l'autre à null
    //Ainsi nous connaissons notre base de calcul
    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
        this.taxedUnitPrice = null;
    }

    public Double getTaxedUnitPrice() {
        return taxedUnitPrice;
    }

    public void setTaxedUnitPrice(Double taxedUnitPrice) {
        this.taxedUnitPrice = taxedUnitPrice;
        this.unitPrice = null;
    }

    public Double getTaxedPrice() {
        return taxedPrice;
    }

    public void setTaxedPrice(Double taxedPrice) {
        this.taxedPrice = taxedPrice;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    public Long getTariffAreaId() {
        return (tariffArea == null ? null : tariffArea.getId());
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSTicketLinesDTO.class)) {
            POSTicketLinesDTO pOSTicketLinesDTO = new POSTicketLinesDTO();
            pOSTicketLinesDTO.setAttributes(getAttributes());
            pOSTicketLinesDTO.setDiscountRate(getDiscountRate());
            pOSTicketLinesDTO.setDispOrder(getLine());
            //EDU - transition V8S - price contenait auparavant le prix unitaire
            pOSTicketLinesDTO.setPrice(getUnitPrice() == null ? getPrice() : getUnitPrice());
            if (getProduct() != null) {
                pOSTicketLinesDTO.setProductId(getProduct().getId());
            }
            pOSTicketLinesDTO.setQuantity(getUnits());
            if (getTax() != null) {
                pOSTicketLinesDTO.setVatId(getTax().getId());
            }
            pOSTicketLinesDTO.setType(this.getType());
            pOSTicketLinesDTO.setOrder(isOrder());
            pOSTicketLinesDTO.setTariffAreaId(getTariffAreaId());
            pOSTicketLinesDTO.setNote(getNote());
            pOSTicketLinesDTO.setParentId(getParentTicketId());
            pOSTicketLinesDTO.setParentLine(getParentLineNumber());
            return (T) pOSTicketLinesDTO;
        } else if (selector.equals(TicketsLineInfoDTO.class)) {
            return (T) new TicketsLineInfoDTO();
        }
        return null;
    }

    //EDU Taux de remise effectif ramené à la ligne
    public double getGlobalDiscountRate(double additionalDiscount) {
        return ( discountRate + additionalDiscount - additionalDiscount*discountRate);
    }

    // Montant total TTC de la remise accodée
    //EDU on modifie pour pouvoir tenir compte de la remise totale accordée sur la ligne (ie remise en pied de ticket inclue)
    public double getFinalDiscountAmount(double additionalDiscount) {
        return Math.rint(100*getUnitPrice() * getGlobalDiscountRate(additionalDiscount)  * getUnits() * (1 + getTax().getRate()))/100;
    }

    public double getFinalDiscountAmount() {
        return getFinalDiscountAmount(0);
    }
}
