package org.pasteque.api.model.tickets;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.dto.tickets.TicketsTaxLineInfoDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Taxes;

@Entity
public class TaxLines implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String RECEIPTS = "receipts";
        public static final String TAXES = "taxes";
        public static final String BASE = "base";
        public static final String AMOUNT = "amount";
    }

    @Id
    private String id;
    @ManyToOne
    @JoinColumn(name = "receipt")
    private Receipts receipts;
    @ManyToOne
    @JoinColumn(name = "taxid")
    private Taxes taxes;
    private double base;
    private double amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Receipts getReceipts() {
        return receipts;
    }

    public void setReceipts(Receipts receipts) {
        this.receipts = receipts;
    }

    public Taxes getTaxes() {
        return taxes;
    }

    public void setTaxes(Taxes taxes) {
        this.taxes = taxes;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTaxesId() {
        return (taxes == null ? null : taxes.getId());
    }

    public String getReceiptsId() {
        return (receipts == null ? null : receipts.getId());
    }

    /**
     * EDU : On surcharge pour s'affranchir de la différence d'Id
     */
    public boolean equals(Object b) {
        boolean retour = false;
        if (b != null && TaxLines.class.isInstance(b)) {
            TaxLines comp = (TaxLines) b;
            if (comp.getAmount() == getAmount() && comp.getBase() == getBase()) {
                if ((getReceiptsId() != null && getReceiptsId().equals(comp.getReceiptsId()))
                        || (getReceiptsId() == null && comp.getReceiptsId() == null)) {
                    retour = ((getTaxesId() != null && getTaxesId().equals(comp.getTaxesId())) || (getTaxesId() == null && comp
                            .getTaxesId() == null));
                }
            }
        }
        return retour;
    }

    /**
     * La création d'Id simplement à partir du timestamp n'est pas assez robuste Il est en effet tout à fait possible d'avoir deux objets
     * dans la même ms surtout qu'on peut avoir une chaine de taxes (avec eco truc et redevance machin)
     */
    public void createId(int index) {
        this.setId(String.format("%d-%02d-%s", new Date().getTime(), index, this.receipts == null ? "" : this.receipts.getId()));

    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(TicketsTaxLineInfoDTO.class)) {
            TicketsTaxLineInfoDTO taxLineInfoDTO = new TicketsTaxLineInfoDTO();
            taxLineInfoDTO.setTax(getTaxes().getName());
            taxLineInfoDTO.setBase(getBase());
            taxLineInfoDTO.setAmount(getAmount());
            return (T) taxLineInfoDTO;
        }
        return null;
    }
}
