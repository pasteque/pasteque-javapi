package org.pasteque.api.model.tickets;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.pasteque.api.dto.customers.TicketsHistoryDTO;
import org.pasteque.api.dto.tickets.DetailsPaymentsDTO;
import org.pasteque.api.dto.tickets.POSPaymentsDTO;
import org.pasteque.api.dto.tickets.POSTicketLinesDTO;
import org.pasteque.api.dto.tickets.POSTicketsDTO;
import org.pasteque.api.dto.tickets.TicketsInfoDTO;
import org.pasteque.api.dto.tickets.TicketsLineInfoDTO;
import org.pasteque.api.dto.tickets.TicketsTaxLineInfoDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.customers.Customers;
import org.pasteque.api.model.customers.DiscountProfil;
import org.pasteque.api.model.products.TariffAreas;
import org.pasteque.api.model.saleslocations.Insee;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.saleslocations.SalesLocations;
import org.pasteque.api.model.security.People;

@Entity
public class Tickets implements IAdaptable {

    public final class Fields {
        public static final String RECEIPTS = "receipts";
        public static final String TICKET_TYPE = "ticketType";
        public static final String TICKET_ID = "ticketId";
        public static final String PERSON = "person";
        public static final String CUSTOMER = "customer";
        public static final String STATUS = "status";
        public static final String CUSTOMER_COUNT = "custCount";
        public static final String TARIFF_AREA = "tariffArea";
        public static final String DISCOUNT_RATE = "discountRate";
        public static final String DISCOUNT_PROFILE = "discountProfile";

        public static final String CUSTOMER_ZIPCODE = "customerZipCode";

        public static final String PARENT = "parent";

        public static final String LINES = "lines";
        
        public static final String NOTE = "note";

        public static final String SEQUENCE = "sequence";
        public static final String NUMBER = "number";
        public static final String FINAL_PRICE = "finalPrice";
        public static final String FINAL_TAXED_PRICE = "finalTaxedPrice";
        public static final String PRICE = "price";
        public static final String TAXED_PRICE = "taxedPrice";
        public static final String CUSTOMER_BALANCE = "custBalance";
        public static final String DATE = "date";
    }
    public final class Types {
        public static final int VENTE = 0;
        public static final int RETOUR = 1;
    }
    
    @Id
    private String id;
    
    @OneToOne
    @JoinColumn(name = "id" , insertable = false, updatable = false)
    private Receipts receipts;
    
    private int ticketType;
    private long ticketId;
    @ManyToOne
    @JoinColumn(name = "person")
    private People person;
    @ManyToOne
    @JoinColumn(name = "customer")
    private Customers customer;
    private int status;
    private Integer custCount;
    @ManyToOne
    @JoinColumn(name = "tariffArea")
    private TariffAreas tariffArea;
    private double discountRate;
    @ManyToOne
    @JoinColumn(name = "discountProfile_id")
    private DiscountProfil discountProfile;
    @ManyToOne
    @JoinColumn(name = "custzipcode")
    private Insee customerZipCode;
    @OneToOne
    @JoinColumn(name = "parent_ticket_id")
    private Tickets parent;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = TicketLines.Fields.TICKET, orphanRemoval = true)
    private List<TicketLines> lines;
    
    private String note;
    
    // EDU 2020 08 Nouveau champs
    //Numéro de la session à laquelle ce ticket appartient
    private Integer sequence;
    //Numéro du ticket dans cette session
    private Long number;
    //TODO voir ce qu'on en fait 
    //deux écoles on stock la date initiale et on ne la fait pas évoluer même si il y a une intervention sur le ticket
    //On lie à la date de receipts
    private Date date;
    
    //Gestion du montant total du ticket et TTC et HT
    //Avant et Après remise
    private Double price;
    private Double taxedPrice;
    private Double finalPrice;
    private Double finalTaxedPrice;
    
    //Changement du compte client induit par le ticket ( il paye 5 le ticket fait 10 ) +5
    private Double custBalance;

    public Receipts getReceipts() {
        return receipts;
    }

    public void setReceipts(Receipts receipts) {
        this.receipts = receipts;
        if(receipts != null) {
            this.id = receipts.getId();
            this.sequence = receipts.getClosedCash().getHostSequence();
            //this.date = receipts.getDateNew();
        }
    }

    public int getTicketType() {
        return ticketType;
    }

    public void setTicketType(int ticketType) {
        this.ticketType = ticketType;
    }

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public People getPerson() {
        return person;
    }

    public void setPerson(People person) {
        this.person = person;
    }

    public Customers getCustomer() {
        return customer;
    }

    public void setCustomer(Customers customer) {
        this.customer = customer;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getCustCount() {
        return custCount;
    }

    public void setCustCount(Integer custCount) {
        this.custCount = custCount == null ? 1 : custCount;
    }

    public TariffAreas getTariffArea() {
        return tariffArea;
    }

    public void setTariffArea(TariffAreas tariffArea) {
        this.tariffArea = tariffArea;
    }

    public double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(double discountRate) {
        this.discountRate = discountRate;
    }

    public DiscountProfil getDiscountProfile() {
        return discountProfile;
    }

    public void setDiscountProfile(DiscountProfil discountProfile) {
        this.discountProfile = discountProfile;
    }

    public List<TicketLines> getLines() {
        return lines;
    }

    public void setLines(List<TicketLines> lines) {
        this.lines = lines;
    }

    public TicketsManager getTicketsManager() {
        return new TicketsManager(this);
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSTicketsDTO.class)) {
            POSTicketsDTO pOSTicketsDTO = new POSTicketsDTO();
            pOSTicketsDTO.setCustCount(getCustCount());
            if (getCustomer() != null) {
                pOSTicketsDTO.setCustomerId(getCustomer().getId());
            }
            pOSTicketsDTO.setDatetime(getDate());
            if (getDiscountProfile() != null) {
                pOSTicketsDTO.setDiscountProfileId(getDiscountProfile().getId());
            }
            pOSTicketsDTO.setDiscountRate(getDiscountRate());
            pOSTicketsDTO.setId(getReceipts().getId());
            pOSTicketsDTO.setLines(AdaptableHelper.getAdapter(getLines(), POSTicketLinesDTO.class));
            pOSTicketsDTO.setPayments(AdaptableHelper.getAdapter(getReceipts().getPayments(), POSPaymentsDTO.class));
            if (getTariffArea() != null) {
                pOSTicketsDTO.setTariffAreaId(getTariffArea().getId());
            }
            pOSTicketsDTO.setTicketId(getTicketId());
            pOSTicketsDTO.setType(getTicketType());
            pOSTicketsDTO.setParentTicketId(getParentId());

            if (getPerson() != null) {
                pOSTicketsDTO.setUserId(getPerson().getId());
            }
            pOSTicketsDTO.setCashId(getReceipts().getClosedCash().getMoney());
            if (getCustomerZipCode() != null) {
                pOSTicketsDTO.setInseeNum(getInseeId());
            }
            pOSTicketsDTO.setNote(getNote());
            
            return (T) pOSTicketsDTO;
        } else if (selector.equals(TicketsHistoryDTO.class)) {
            TicketsHistoryDTO dto = new TicketsHistoryDTO();
            dto.setDate(getReceipts().getDateNew());
            dto.setTicketId(getTicketId());
            dto.setProductsUnit(getLines().size());
            dto.setTicketsAmount(getFinalTaxedPrice());
            if(getReceipts().getClosedCash().getCashRegister() == null ) {
                if(getReceipts().getClosedCash().getSalesLocation() == null || getReceipts().getClosedCash().getSalesLocation().getTour() == null) {
                    dto.setLocationsName("");
                } else {
                    dto.setLocationsName(getReceipts().getClosedCash().getSalesLocation().getTour().getName());
                }
            } else {
                dto.setLocationsName(getReceipts().getClosedCash().getCashRegister().getLocation().getName());
            }
            return (T) dto;
        } else if (selector.equals(TicketsInfoDTO.class)) {
            TicketsInfoDTO dto = new TicketsInfoDTO();
            dto.setDate(getReceipts().getDateNew());
            if (getCustomer() != null) {
                dto.setCustomersLabel(getCustomer().getName());
                dto.setCustomersId(getCustomerId());
            }
            dto.setUsername(getPerson().getUsername());
            dto.setTicketId(getTicketId());
            dto.setAmount(getFinalTaxedPrice());
            dto.setDiscount(getDiscountRate());
            dto.setLines(AdaptableHelper.getAdapter(getLines(), TicketsLineInfoDTO.class));
            dto.setTaxes(AdaptableHelper.getAdapter(getReceipts().getTaxes(), TicketsTaxLineInfoDTO.class));
            
            dto.setPayments(AdaptableHelper.getAdapter(getReceipts().getPayments(), DetailsPaymentsDTO.class));
            if (getTariffArea() != null) {
                dto.setCatalogue(getTariffArea().getName());
            }
            
            dto.setPointVente(getLocation().getName());
            //TODO Obtenir les infos associées au déballage
            if(getReceipts() != null && getReceipts().getClosedCash() != null 
                    && getReceipts().getClosedCash().getSalesLocation() != null 
                    && getReceipts().getClosedCash().getSalesLocation().getTour() != null ) {
                SalesLocations deballage = getReceipts().getClosedCash().getSalesLocation();
                
                dto.setTournee(deballage.getTour().getName());
                dto.setDeballage(deballage.toString());
                
            }
            dto.setNote(getNote());
            
            return (T) dto;
        } 
        return null;
    }

    public String getUserId() {
        return (person == null ? null : person.getId());
    }

    public String getCustomerId() {
        return (customer == null ? null : customer.getId());
    }
    
    public String getCustomerSearchKey() {
        return (customer == null ? null : customer.getSearchKey());
    }

    public Long getDiscountProfileId() {
        return (discountProfile == null ? null : discountProfile.getId());
    }

    public Long getTariffAreaId() {
        return (tariffArea == null ? null : tariffArea.getId());
    }

    public Insee getCustomerZipCode() {
        return customerZipCode;
    }

    public void setCustomerZipCode(Insee customerZipCode) {
        this.customerZipCode = customerZipCode;
    }

    public String getInseeNum() {
        return (customerZipCode == null ? null : customerZipCode.getInseeNum());
    }
    
    public String getInseeId() {
        return (customerZipCode == null ? null : customerZipCode.getId());
    }

    public Tickets getParent() {
        return parent;
    }

    public void setParent(Tickets parent) {
        this.parent = parent;
    }

    public Long getParentId() {
        return this.parent == null ? null : parent.getTicketId();
    }

    public Locations getLocation() {
        if (getReceipts() != null && getReceipts().getClosedCash() != null && getReceipts().getClosedCash().getCashRegister() != null
                && getReceipts().getClosedCash().getCashRegister().getLocation() != null) {
            return getReceipts().getClosedCash().getCashRegister().getLocation();
        }
        return null;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getId() {
        return this.receipts.getId();
    }
    
    public void setId(String id) {
        this.receipts.setId(id);
        this.id = id;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTaxedPrice() {
        return taxedPrice;
    }

    public void setTaxedPrice(Double taxedPrice) {
        this.taxedPrice = taxedPrice;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Double getFinalTaxedPrice() {
        return finalTaxedPrice;
    }

    public void setFinalTaxedPrice(Double finalTaxedPrice) {
        this.finalTaxedPrice = finalTaxedPrice;
    }

    public Double getCustBalance() {
        return custBalance;
    }

    public void setCustBalance(Double custBalance) {
        this.custBalance = custBalance;
    }
    
    public void computeAllPrices() {
        /**
         * Voici tout ce qui peut être calculé
         * Deux options en @PrePersist
         * ou alors via des appels
         *     private Double price;
               private Double taxedPrice;
               private Double finalPrice;
               private Double finalTaxedPrice;
         */
        taxedPrice = computeAmountFullTaxes();
        price = computeAmountWitoutTaxes();
        finalPrice = applydiscount(price);
        finalTaxedPrice = applydiscount(taxedPrice);
        
    }
    
    private double computeAmountFullTaxes() {
        double acc = 0;
        for (TicketLines ticketLines : getLines()) {
            acc += ticketLines.getFinalTaxedPrice();
        }
        return acc;
    }

    private double computeAmountWitoutTaxes() {
        double acc = 0;
        for (TicketLines ticketLines : getLines()) {
            acc += ticketLines.getFinalPrice();
        }
        return acc;
    }
    
    private double applydiscount(Double base) {
        base = (base == null ? 0 : base);
        return base * (1 - getDiscountRate());
    }


}
