package org.pasteque.api.model.tickets;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.pasteque.api.model.cash.ClosedCash;

@Entity
public class Receipts implements Serializable{

    private static final long serialVersionUID = -7561244188367900351L;

    public final class Fields {
        public static final String ID = "id";
        public static final String CLOSED_CASH = "closedCash";
        public static final String DATE_NEW = "dateNew";
        public static final String ATTRIBUTES = "attributes";
        public static final String TICKETS = "tickets";
        public static final String PAYMENTS = "payments";
        public static final String TAXES = "taxes";
    }

    @Id
    private String id;

    @OneToOne(mappedBy=Tickets.Fields.RECEIPTS)
    @MapsId
    private Tickets tickets;
    
    @ManyToOne
    @JoinColumn(name = "money")
    private ClosedCash closedCash;
    private Date dateNew;
    private byte[] attributes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = Payments.Fields.RECEIPTS, orphanRemoval = true)
    private List<Payments> payments;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = TaxLines.Fields.RECEIPTS, orphanRemoval = true)
    private List<TaxLines> taxes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ClosedCash getClosedCash() {
        return closedCash;
    }

    public void setClosedCash(ClosedCash closedCash) {
        this.closedCash = closedCash;
    }

    public Date getDateNew() {
        return dateNew;
    }

    public void setDateNew(Date dateNew) {
        this.dateNew = dateNew;
    }

    public byte[] getAttributes() {
        return attributes;
    }

    public void setAttributes(byte[] attributes) {
        this.attributes = attributes;
    }

    public List<Payments> getPayments() {
        return payments;
    }

    public void setPayments(List<Payments> payments) {
        this.payments = payments;
    }

    public List<TaxLines> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<TaxLines> taxes) {
        this.taxes = taxes;
    }

    public String getClosedCashId() {
        return (closedCash == null ? null : closedCash.getMoney());
    }
    
    public Tickets getTickets() {
        return tickets;
    }

    public void setTickets(Tickets tickets) {
        this.tickets = tickets;
    }

}
