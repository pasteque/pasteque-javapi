package org.pasteque.api.model.tickets;

import java.util.List;

import org.pasteque.api.model.products.Taxes;

public class TicketsManager {
    private Tickets tickets;

    public TicketsManager(Tickets tickets) {
        this.tickets = tickets;
    }

    /**
     * Ajouter certaines information au récapitulatif des taxes EDU 2015 04 15 : Modification pour changer le fonctionnement de
     * l'accumulateur.
     * 
     * @param taxesLines
     *            - Liste récapitulative par type de taxe <- c'est elle qui va porter les modifs
     * @param receipts
     *            - Le ticket
     * @param tax
     *            - La tax concernée
     * @param taxAmount
     *            - La base - contrairement à ce que le nom indique lorsqu'on parle de taxe proportionnelle sinon (taxe à montant fixe) le
     *            montant.
     * 
     * 
     */
    public static void addTaxesLines(List<TaxLines> taxesLines, Receipts receipts, Taxes tax, double taxAmount) {
        for (TaxLines taxLines : taxesLines) {
            if (taxLines.getTaxes().getId().equals(tax.getId())) {
                taxLines.setBase(taxLines.getBase() + taxAmount);
                if (tax.getAmount() == null || tax.getAmount() == 0D) {
                    // cas d'une taxe proportionnelle
                    taxLines.setAmount(taxLines.getAmount() + taxAmount * tax.getRate());
                } else {
                    // cas d'une taxe à montant fixe
                    taxLines.setAmount(taxLines.getAmount() + taxAmount);
                }
                return;
            }
        }
        TaxLines taxLines = new TaxLines();
        taxLines.setBase(taxAmount);
        if (tax.getAmount() != null && tax.getAmount() != 0D) {
            taxLines.setAmount(taxAmount);
        } else {
            taxLines.setAmount(taxAmount * tax.getRate());
        }
        taxLines.setReceipts(receipts);
        taxLines.setTaxes(tax);
        // Danger !
        // TODO voir ce qu'on peut faire de mieux !
        //EDU :  un générateur d'Id ?
        taxLines.createId(taxesLines.size());
        taxesLines.add(taxLines);
    }

    public double getDiscountAmount() {
        double acc = 0;
        for (TicketLines ticketLines : tickets.getLines()) {
            acc += ticketLines.getFinalDiscountAmount(tickets.getDiscountRate());
        }
        return acc;
    }
    
    public double getNumberOfLines() {
        double acc = 0;
        for (TicketLines ticketLines : tickets.getLines()) {
            acc += ticketLines.getUnits();
        }
        return acc;
    }
}
