package org.pasteque.api.model.tickets;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.pasteque.api.dto.tickets.POSSharedTicketsDTO;
import org.pasteque.api.model.adaptable.IAdaptable;

@Entity
public class SharedTickets implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String CONTENT = "content";
    }

    @Id
    private String id;
    private String name;
    @Lob
    private byte[] content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
           return getAdapter(selector , false);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector , boolean filtered) {
        if (selector.equals(POSSharedTicketsDTO.class)) {
            POSSharedTicketsDTO pOSSharedTicketsDTO=new POSSharedTicketsDTO();
            pOSSharedTicketsDTO.setId(getId());
            pOSSharedTicketsDTO.setLabel(getName());
            //byte[] encoded = Base64Utils.encode(getContent());
            pOSSharedTicketsDTO.setData(getContent());
            return (T) pOSSharedTicketsDTO;
        }
        return null;
    }
}
