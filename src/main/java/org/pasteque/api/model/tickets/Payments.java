package org.pasteque.api.model.tickets;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.dto.tickets.DetailsPaymentsDTO;
import org.pasteque.api.dto.tickets.POSPaymentsDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.cash.Currencies;

@Entity
public class Payments implements IAdaptable {

    public final class Type {
        public static final String PREPAID = "prepaid";
        public static final String DEBT = "debt";
        public static final String DEBT_PAID = "debtpaid";
        public static final String PAPER_OUT = "paperout";
        public static final String PAPER_IN = "paperin";
        public static final String MAGCARD = "magcard";
        public static final String CHEQUE = "cheque";
        public static final String DROIT_DE_PLACE = "ddp";
        public static final String CHEQUE_REFUND = "chequerefund";
        public static final String TRANSFER = "transfer";
        public static final String CASH = "cash";
        public static final String CASH_IN = "cashin";
        public static final String CASH_OUT = "cashout";
        public static final String CASH_FEE = "place";
        public static final String CASH_BUY = "buyout";
        public static final String CASH_REFUND = "cashrefund";
        public static final String FREE = "free";
    }
    
    public final class Categ {
        public static final String MOUVEMENT = "Mouvements";
        public static final String VENTE = "Encaissements";
    }

    public final class Fields {
        public static final String ID = "id";
        public static final String RECEIPTS = "receipts";
        public static final String PAYMENT = "payment";
        public static final String TOTAL = "total";
        public static final String CURRENCY = "currency";
        public static final String TOTAL_CURRENCY = "totalCurrency";
        public static final String TRANS_ID = "transId";
        public static final String RETURN_MESSAGE = "returnMessage";
        public static final String NOTE = "note";
        public static final String ECHEANCE = "echeance";
    }

    @Id
    private String id;
    @ManyToOne
    @JoinColumn(name = "receipt")
    private Receipts receipts;
    @ManyToOne
    @JoinColumn(name = "receipt", insertable = false, updatable = false)
    private Tickets tickets;
    private String payment;
    private double total;
    @ManyToOne
    @JoinColumn(name = "currency")
    private Currencies currency;
    private double totalCurrency;
    @Column(name = "reference")
    private String transId;
    @Column(name = "returnMsg")
    private byte[] returnMessage;
    private String note;
    private Date echeance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Receipts getReceipts() {
        return receipts;
    }

    public void setReceipts(Receipts receipts) {
        this.receipts = receipts;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Currencies getCurrency() {
        return currency;
    }

    public void setCurrency(Currencies currency) {
        this.currency = currency;
    }

    public double getTotalCurrency() {
        return totalCurrency;
    }

    public void setTotalCurrency(double totalCurrency) {
        this.totalCurrency = totalCurrency;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public byte[] getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(byte[] returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Tickets getTickets() {
        return tickets;
    }

    public void setTickets(Tickets tickets) {
        this.tickets = tickets;
    }

    public void createId(int index) {
        this.setId(String.format("%d-%02d-%s", new Date().getTime(), index, this.receipts == null ? "" : this.receipts.getId()));
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSPaymentsDTO.class)) {
            POSPaymentsDTO pOSPaymentsDTO = new POSPaymentsDTO();
            pOSPaymentsDTO.setId(getId());
            pOSPaymentsDTO.setAmount(getTotal());
            pOSPaymentsDTO.setCurrencyAmount(getTotalCurrency());
            if (getCurrency() != null) {
                pOSPaymentsDTO.setCurrencyId(getCurrency().getId());
            }
            pOSPaymentsDTO.setType(getPayment());
            pOSPaymentsDTO.setNote(getNote());
            pOSPaymentsDTO.setTransId(getTransId());
            if (getEcheance() != null) {
                pOSPaymentsDTO.setEcheance(getEcheance().getTime());
            }
            pOSPaymentsDTO.setReturnMessage(getReturnMessage());
            return (T) pOSPaymentsDTO;
        } else if (selector.equals(DetailsPaymentsDTO.class)) {
            DetailsPaymentsDTO dto = new DetailsPaymentsDTO();
            dto.setTicketId(getTickets() == null ? getReceipts().getId() : new Long (getTickets().getTicketId()).toString());
            dto.setAmount(getTotal());
            dto.setEcheance(getEcheance());
            dto.setNote(getNote());
            dto.setReference(getTransId());
            dto.setDatenew(getReceipts().getDateNew());
            dto.setPayment(getPaymentsLabel(getPayment()));
            return (T) dto;
        } 
        return null;
    }

    public Date getEcheance() {
        return echeance;
    }

    public void setEcheance(Date echeance) {
        this.echeance = echeance;
    }

    public static String getPaymentsLabel(String code) {
        switch (code) {
        case Payments.Type.DROIT_DE_PLACE:
            return "droit de place";
        case Payments.Type.PREPAID:
            return "pré payé";
        case Payments.Type.FREE:
            return "offert";
        case Payments.Type.DEBT:
            return "dette";
        case Payments.Type.DEBT_PAID:
            return "dette payee";
        case Payments.Type.PAPER_OUT:
            return "avoir emis";
        case Payments.Type.PAPER_IN:
            return "avoir encaisse";
        case Payments.Type.MAGCARD:
            return "carte bancaire";
        case Payments.Type.CHEQUE:
            return "cheque";
        case Payments.Type.CHEQUE_REFUND:
            return "cheque - sur ticket retour";
        case Payments.Type.TRANSFER:
            return "virement";
        case Payments.Type.CASH:
            return "espece";
        case Payments.Type.CASH_REFUND:
            return "espece - retour";
        case Payments.Type.CASH_IN:
            return "appro espece";
        case Payments.Type.CASH_OUT:
            return "sortie espece";
        case Payments.Type.CASH_FEE:
            return "sortie espece - droit de place";
        case Payments.Type.CASH_BUY:
            return "sortie espece - achats";
        default:
            break;
        }
        return "";
    }
    
    public static String getPaymentsCateg(String code) {
        switch (code) {
        case Payments.Type.PREPAID:
        case Payments.Type.FREE:
        case Payments.Type.DEBT:
        case Payments.Type.PAPER_OUT:
        case Payments.Type.PAPER_IN:
        case Payments.Type.MAGCARD:
        case Payments.Type.CHEQUE:
        case Payments.Type.CHEQUE_REFUND:
        case Payments.Type.TRANSFER:
        case Payments.Type.CASH:
        case Payments.Type.CASH_REFUND:
        case Payments.Type.DROIT_DE_PLACE:    
            return Payments.Categ.VENTE;

        default:
            return Payments.Categ.MOUVEMENT;

        }
    }
    
    public static ArrayList<String> getCategPayments(String code) {
        ArrayList<String> retour = new ArrayList<String>(); 
        switch (code) {
        case Payments.Categ.VENTE :
            retour.add(Payments.Type.PREPAID);
            retour.add(Payments.Type.FREE);
            retour.add(Payments.Type.DEBT);
            retour.add(Payments.Type.PAPER_OUT);
            retour.add(Payments.Type.PAPER_IN);
            retour.add(Payments.Type.MAGCARD);
            retour.add(Payments.Type.CHEQUE);
            retour.add(Payments.Type.CHEQUE_REFUND);
            retour.add(Payments.Type.TRANSFER);
            retour.add(Payments.Type.CASH);
            retour.add(Payments.Type.CASH_REFUND);
            retour.add(Payments.Type.DROIT_DE_PLACE);
            break ;

        default:
            retour.add(Payments.Type.CASH_IN);
            retour.add(Payments.Type.CASH_BUY);
            retour.add(Payments.Type.CASH_FEE);
            retour.add(Payments.Type.CASH_OUT);
            retour.add(Payments.Type.DEBT_PAID);
        }
        return retour;
    }

}
