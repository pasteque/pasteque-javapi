package org.pasteque.api.model.tickets;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.dto.tickets.POSTicketLinesDeletedDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.TariffAreas;
import org.pasteque.api.model.products.Taxes;

@Entity
public class TicketLinesDeleted implements IAdaptable {

    public final class Fields {
        public static final String TICKET = "ticket";
        public static final String LINE = "line";
        public static final String PRODUCT = "product";
        public static final String ATTRIBUTE_SET_INSTANCE_ID = "attributeSetInstanceId";
        public static final String UNITS = "units";
        public static final String PRICE = "price";
        public static final String TAX = "tax";
        public static final String DISCOUNT_RATE = "discountRate";
        public static final String ATTRIBUTES = "attributes";
        public static final String TYPE = "type";
        public static final String TARIFF_AREA = "tariffArea";
        public static final String NOTE = "note";
        public static final String  CREATED_DATE = "CREATED_DATE";
        public static final String  DELETED_DATE = "DELETED_DATE";
        public static final String IS_OPENED_PAYMENT = "IS_OPENED_PAYMENT";
        //public static final String PARENTLINE = "parentLine";
    }
    @Id
    @Column(name = "ticket")
    private String ticketId;
    private int line;
    @ManyToOne
    @JoinColumn(name = "product")
    private Products product;
    @Column(name = "attributesetinstance_id")
    private String attributeSetInstanceId;
    private double units;
    private double price;
    @ManyToOne
    @JoinColumn(name = "taxid")
    private Taxes tax;
    private double discountRate;
    private byte[] attributes;
    private char type;
    @ManyToOne
    @JoinColumn(name = "tariffArea")
    private TariffAreas tariffArea;
    private String note;
    @Column(name = "DELETED_DATE")
    private Date deletedDate;
    @Column(name = "CREATED_DATE")
    private Date createdDate;
    @Column(name = "IS_OPENED_PAYMENT")
    private boolean isOpenedPayment;
   
 

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    public String getAttributeSetInstanceId() {
        return attributeSetInstanceId;
    }

    public void setAttributeSetInstanceId(String attributeSetInstanceId) {
        this.attributeSetInstanceId = attributeSetInstanceId;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Taxes getTax() {
        return tax;
    }

    public void setTax(Taxes tax) {
        this.tax = tax;
    }

    public double getDiscountRate() {
        return discountRate;
    }
    
    //EDU Taux de remise effectif ramené à la ligne
    public double getGlobalDiscountRate(double additionalDiscount) {
        return ( discountRate + additionalDiscount - additionalDiscount*discountRate);
    }

    public void setDiscountRate(double discountRate) {
        this.discountRate = discountRate;
    }

    public byte[] getAttributes() {
        return attributes;
    }

    public void setAttributes(byte[] attributes) {
        this.attributes = attributes;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    // TODO faire sauter la chaine en dur
    public boolean isOrder() {
        return "O".equals(String.valueOf(type));
    }
    
    // Montant total TTC de la remise accodée
    //EDU on modifie pour pouvoir tenir compte de la remise totale accordée sur la ligne (ie remise en pied de ticket inclue)
    public double getFinalDiscountAmount(double additionalDiscount) {
        return getPrice() * getGlobalDiscountRate(additionalDiscount)  * getUnits() * (1 + getTax().getRate());
    }
    
    public double getFinalDiscountAmount() {
        return getFinalDiscountAmount(0);
    }

    // Montant de la ligne du ticket (PU TTC * units)
    // TODO : AME => Attention pour les eco... la taxe est hors remise
    public double getFinalPriceWhitoutTaxes() {
        return (getPrice() * (1 - getDiscountRate())) * getUnits();
    }

    public TariffAreas getTariffArea() {
        return tariffArea;
    }

    public void setTariffArea(TariffAreas tariffArea) {
        this.tariffArea = tariffArea;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isOpenedPayment() {
        return isOpenedPayment;
    }

    public void setOpenedPayment(boolean isOpenedPayment) {
        this.isOpenedPayment = isOpenedPayment;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    public Long getTariffAreaId() {
        return (tariffArea == null ? null : tariffArea.getId());
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(POSTicketLinesDeletedDTO.class)) {
            POSTicketLinesDeletedDTO pOSTicketLinesDeletedDTO = new POSTicketLinesDeletedDTO();
            pOSTicketLinesDeletedDTO.setTicketId(getTicketId());
            pOSTicketLinesDeletedDTO.setAttributes(getAttributes());
            pOSTicketLinesDeletedDTO.setDiscountRate(getDiscountRate());
            //TicketLinesDeletedDTO.setOrderOfLine(get);
            pOSTicketLinesDeletedDTO.setCreatedDate(getCreatedDate());
            pOSTicketLinesDeletedDTO.setPrice(getPrice());
            if (getProduct() != null) {
                pOSTicketLinesDeletedDTO.setProductId(getProduct().getId());
            }
            pOSTicketLinesDeletedDTO.setQuantity(getUnits());
            if (getTax() != null) {
                pOSTicketLinesDeletedDTO.setVatId(getTax().getId());
            }
            pOSTicketLinesDeletedDTO.setType(this.getType());
            pOSTicketLinesDeletedDTO.setOrder(isOrder());
            pOSTicketLinesDeletedDTO.setTariffAreaId(getTariffAreaId());
            pOSTicketLinesDeletedDTO.setNote(getNote());
            //TicketLinesDeletedDTO.setParentId(getParentTicketId());
            //TicketLinesDeletedDTO.setParentLine(getParentLineNumber());
            return (T) pOSTicketLinesDeletedDTO;
        } 
        return null;
    }
}
