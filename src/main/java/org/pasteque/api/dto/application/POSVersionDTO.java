package org.pasteque.api.dto.application;

public class POSVersionDTO {

	private String version;
	private String level;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
}
