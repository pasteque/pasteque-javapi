package org.pasteque.api.dto.security;

import java.util.Date;

public class POSUsersDTO {

    private String id;
    private String name;
    private String password;
    private String roleId;
    private boolean visible;
    private boolean hasImage;
    private String card;
    private String nom;
    private String prenom;
    private Date ddebut;
    private Date dfin;
    private Date lastupdate;
    private boolean actif;

    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public boolean isVisible() {
        return visible && isActif();
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isHasImage() {
        return hasImage;
    }

    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public Date getDdebut() {
        return ddebut;
    }

    public void setDdebut(Date ddebut) {
        this.ddebut = ddebut;
    }

    public Date getDfin() {
        return dfin;
    }

    public void setDfin(Date dfin) {
        this.dfin = dfin;
    }

    public Date getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(Date lastupdate) {
        this.lastupdate = lastupdate;
    }

    public boolean isActif() {
        Date currentDate = new Date();
        // EDU 2020-05 : un utilisateur est actif si il n'y a pas de date de fin ou que nous ne sommes pas après celle ci
        // et si il n'y a pas de date de début ou que nous ne sommes pas avant celle ci
        actif = this.dfin == null || this.dfin.after(currentDate) || this.dfin.equals(currentDate) ;
        actif = actif && ( this.ddebut == null || currentDate.after(this.ddebut) || this.ddebut.equals(currentDate)) ;
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

}
