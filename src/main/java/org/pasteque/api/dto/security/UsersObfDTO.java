package org.pasteque.api.dto.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UsersObfDTO implements UserDetails{

    /**
     * 
     */
    private static final long serialVersionUID = -401681861567443407L;

    private String id;
    private String name;
    private String password;
    private String roleId;
    private boolean visible;
    private boolean hasImage;
    private String card;
    private ArrayList<String> authorizations;
    private Date ddebut;
    private Date dfin;
    private Date lastupdate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isHasImage() {
        return hasImage;
    }

    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public ArrayList<String> getAuthorizations() {
        return authorizations;
    }

    public void setAuthorizations(ArrayList<String> authorizations) {
        this.authorizations = authorizations;
    }

    public Date getDdebut() {
        return ddebut;
    }

    public void setDdebut(Date ddebut) {
        this.ddebut = ddebut;
    }

    public Date getDfin() {
        return dfin;
    }

    public void setDfin(Date dfin) {
        this.dfin = dfin;
    }

    public Date getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(Date lastupdate) {
        this.lastupdate = lastupdate;
    }

    //EDU 2020-07 TODO faire le tour
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
        if(authorizations != null && !authorizations.isEmpty()) {
            for(String auth : authorizations) {
                list.add(new SimpleGrantedAuthority(auth));
            }
        }
        return list;
    }

    @Override
    public String getUsername() {
        return this.name;
    }

    @Override
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return true;
    }

}
