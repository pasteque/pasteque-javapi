package org.pasteque.api.dto.security;

import java.util.Date;

import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;

public class UserIndexDTO implements ILoadable{

    private String id;
    private String nomCaisse;
    private String nom;
    private String prenom;
    private Date ddebut;
    private Date dfin;
    private Date lastupdate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array, int starting_index) {
        boolean retour = false;
        
        if(array[0+starting_index] != null && (array.length - starting_index) >= 4) {

            try {
                this.setId((String) array[0+starting_index]);
                this.setNomCaisse((String) array[1+starting_index]);
                this.setNom((String) array[2+starting_index]);
                this.setPrenom((String) array[3+starting_index]);
                retour = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                retour = false;
            }
        }

        return retour;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNomCaisse() {
        return nomCaisse;
    }

    public void setNomCaisse(String nom_caisse) {
        this.nomCaisse = nom_caisse;
    }

	public Date getDdebut() {
		return ddebut;
	}

	public void setDdebut(Date ddebut) {
		this.ddebut = ddebut;
	}

	public Date getDfin() {
		return dfin;
	}

	public void setDfin(Date dfin) {
		this.dfin = dfin;
	}

	public Date getLastupdate() {
		return lastupdate;
	}

	public void setLastupdate(Date lastupdate) {
		this.lastupdate = lastupdate;
	}
    
}
