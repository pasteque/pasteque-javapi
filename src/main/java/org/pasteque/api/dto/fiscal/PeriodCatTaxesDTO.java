package org.pasteque.api.dto.fiscal;

import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.dto.products.POSTaxesDTO;

public class PeriodCatTaxesDTO {

    private Double montant;
    private Double base;
    private POSCategoriesDTO categorie;
    private POSTaxesDTO taxe;
    private String idPeriode;
    public Double getMontant() {
        return montant;
    }
    public void setMontant(Double montant) {
        this.montant = montant;
    }
    public Double getBase() {
        return base;
    }
    public void setBase(Double base) {
        this.base = base;
    }
    public POSCategoriesDTO getCategorie() {
        return categorie;
    }
    public void setCategorie(POSCategoriesDTO categorie) {
        this.categorie = categorie;
    }
    public POSTaxesDTO getTaxe() {
        return taxe;
    }
    public void setTaxe(POSTaxesDTO taxe) {
        this.taxe = taxe;
    }
    public String getIdPeriode() {
        return idPeriode;
    }
    public void setIdPeriode(String idPeriode) {
        this.idPeriode = idPeriode;
    }



}
