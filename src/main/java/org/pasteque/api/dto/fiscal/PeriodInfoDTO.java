package org.pasteque.api.dto.fiscal;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dto.cash.POSCashRegistersDTO;

public class PeriodInfoDTO {

    private String version;
    private String failure;
    
    private String id;
    private Date dateStart;
    private Date dateEnd;
    private int sequence;
    private POSCashRegistersDTO cashRegister;
    private String type;
    private boolean closed;
    private boolean continuous;
    private long custCount;
    private long ticketCount;
    
    private double cumulCATTC;
    private double cumulCATTCMois;
    private double cumulCATTCAnneeFiscale;
    private double cumulCATTCInstallation;
    
    
    private List<PeriodCatsDTO> cumulCAParCategorie;
    private List<PeriodCatTaxesDTO> cumulTaxesParCategorie;
    private List<PeriodCustBalancesDTO> balancesClients;
    private List<PeriodPaymentsDTO> encaissements;
    private List<PeriodTaxesDTO> taxes;    
    

    public List<PeriodCatsDTO> getCumulCAParCategorie() {
        return cumulCAParCategorie;
    }

    public void setCumulCAParCategorie(List<PeriodCatsDTO> cumulCAParCategorie) {
        this.cumulCAParCategorie = cumulCAParCategorie;
    }

    public List<PeriodCatTaxesDTO> getCumulTaxesParCategorie() {
        return cumulTaxesParCategorie;
    }

    public void setCumulTaxesParCategorie(
            List<PeriodCatTaxesDTO> cumulTaxesParCategorie) {
        this.cumulTaxesParCategorie = cumulTaxesParCategorie;
    }

    public List<PeriodCustBalancesDTO> getBalancesClients() {
        return balancesClients;
    }

    public void setBalancesClients(List<PeriodCustBalancesDTO> balancesClients) {
        this.balancesClients = balancesClients;
    }

    public List<PeriodPaymentsDTO> getEncaissements() {
        return encaissements;
    }

    public void setEncaissements(List<PeriodPaymentsDTO> encaissements) {
        this.encaissements = encaissements;
    }

    public List<PeriodTaxesDTO> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<PeriodTaxesDTO> taxes) {
        this.taxes = taxes;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFailure() {
        return failure;
    }

    public void setFailure(String failure) {
        this.failure = failure;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public POSCashRegistersDTO getCashRegister() {
        return cashRegister;
    }

    public void setCashRegister(POSCashRegistersDTO cashRegister) {
        this.cashRegister = cashRegister;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public boolean isContinuous() {
        return continuous;
    }

    public void setContinuous(boolean continuous) {
        this.continuous = continuous;
    }

    public long getCustCount() {
        return custCount;
    }

    public void setCustCount(long custCount) {
        this.custCount = custCount;
    }

    public long getTicketCount() {
        return ticketCount;
    }

    public void setTicketCount(long ticketCount) {
        this.ticketCount = ticketCount;
    }

    public double getCumulCATTC() {
        return cumulCATTC;
    }

    public void setCumulCATTC(double cumulCATTC) {
        this.cumulCATTC = cumulCATTC;
    }

    public double getCumulCATTCMois() {
        return cumulCATTCMois;
    }

    public void setCumulCATTCMois(double cumulCATTCMois) {
        this.cumulCATTCMois = cumulCATTCMois;
    }

    public double getCumulCATTCAnneeFiscale() {
        return cumulCATTCAnneeFiscale;
    }

    public void setCumulCATTCAnneeFiscale(double cumulCATTCAnneeFiscale) {
        this.cumulCATTCAnneeFiscale = cumulCATTCAnneeFiscale;
    }

    public double getCumulCATTCInstallation() {
        return cumulCATTCInstallation;
    }

    public void setCumulCATTCInstallation(double cumulCATTCInstallation) {
        this.cumulCATTCInstallation = cumulCATTCInstallation;
    }


}
