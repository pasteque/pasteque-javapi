package org.pasteque.api.dto.fiscal;

import org.pasteque.api.dto.products.POSTaxesDTO;

public class PeriodTaxesDTO {

    private Double taux;
    private Double montant;
    private Double cumulMensuel;
    private Double cumulExercice;
    private Double base;
    private Double  baseMensuelle;
    private Double baseExercice;
    private POSTaxesDTO taxe;
    private String idPeriode;
    
    public Double getTaux() {
        return taux;
    }
    public void setTaux(Double taux) {
        this.taux = taux;
    }
    public Double getCumulMensuel() {
        return cumulMensuel;
    }
    public void setCumulMensuel(Double cumulMensuel) {
        this.cumulMensuel = cumulMensuel;
    }
    public Double getCumulExercice() {
        return cumulExercice;
    }
    public void setCumulExercice(Double cumulExercice) {
        this.cumulExercice = cumulExercice;
    }
    public Double getBase() {
        return base;
    }
    public void setBase(Double base) {
        this.base = base;
    }
    public Double getBaseMensuelle() {
        return baseMensuelle;
    }
    public void setBaseMensuelle(Double baseMensuelle) {
        this.baseMensuelle = baseMensuelle;
    }
    public Double getBaseExercice() {
        return baseExercice;
    }
    public void setBaseExercice(Double baseExercice) {
        this.baseExercice = baseExercice;
    }
    public POSTaxesDTO getTaxe() {
        return taxe;
    }
    public void setTaxe(POSTaxesDTO taxe) {
        this.taxe = taxe;
    }
    public String getIdPeriode() {
        return idPeriode;
    }
    public void setIdPeriode(String idPeriode) {
        this.idPeriode = idPeriode;
    }
    public Double getMontant() {
        return montant;
    }
    public void setMontant(Double montant) {
        this.montant = montant;
    }
    
    
}
