package org.pasteque.api.dto.fiscal;

import java.util.Date;

public class FiscalTicketsDTO {

    boolean signatureOK;
    Date date;
    String type;
    String sequence;
    Integer number;
    String signature;
    String content;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isSignatureOK() {
        return signatureOK;
    }

    public void setSignatureOK(boolean signatureOK) {
        this.signatureOK = signatureOK;
    }
    
    
}
