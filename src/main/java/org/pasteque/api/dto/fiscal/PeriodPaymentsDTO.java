package org.pasteque.api.dto.fiscal;

import org.pasteque.api.dto.cash.POSCurrenciesDTO;

public class PeriodPaymentsDTO {
    private String idPeriode;
    private Double montant;
    private Double montantMonnaie;
    private POSCurrenciesDTO monnaie;
    
    public String getIdPeriode() {
        return idPeriode;
    }
    public void setIdPeriode(String idPeriode) {
        this.idPeriode = idPeriode;
    }
    public Double getMontant() {
        return montant;
    }
    public void setMontant(Double montant) {
        this.montant = montant;
    }
    public Double getMontantMonnaie() {
        return montantMonnaie;
    }
    public void setMontantMonnaie(Double montantMonnaie) {
        this.montantMonnaie = montantMonnaie;
    }
    public POSCurrenciesDTO getMonnaie() {
        return monnaie;
    }
    public void setMonnaie(POSCurrenciesDTO monnaie) {
        this.monnaie = monnaie;
    }


}
