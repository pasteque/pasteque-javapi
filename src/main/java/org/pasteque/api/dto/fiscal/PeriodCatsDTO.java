package org.pasteque.api.dto.fiscal;

import org.pasteque.api.dto.products.POSCategoriesDTO;

public class PeriodCatsDTO {

    private String idPeriode;
    private Double montant;
    private POSCategoriesDTO categorie;
    public String getIdPeriode() {
        return idPeriode;
    }
    public void setIdPeriode(String idPeriode) {
        this.idPeriode = idPeriode;
    }
    public Double getMontant() {
        return montant;
    }
    public void setMontant(Double montant) {
        this.montant = montant;
    }
    public POSCategoriesDTO getCategorie() {
        return categorie;
    }
    public void setCategorie(POSCategoriesDTO categorie) {
        this.categorie = categorie;
    }



}
