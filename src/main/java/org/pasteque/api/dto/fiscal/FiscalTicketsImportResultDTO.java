package org.pasteque.api.dto.fiscal;


import java.util.ArrayList;
import java.util.List;

public class FiscalTicketsImportResultDTO {

    List<FiscalTicketsDTO> dejaPresents ;
    List<FiscalTicketsDTO>  importes ;
    List<FiscalTicketsDTO>  echecsImport ;
    
    
    public FiscalTicketsImportResultDTO() {
        
        dejaPresents = new ArrayList<FiscalTicketsDTO>();
        importes = new ArrayList<FiscalTicketsDTO>();
        echecsImport = new ArrayList<FiscalTicketsDTO>();
    }
    
    
    public List<FiscalTicketsDTO> getDejaPresents() {
        return dejaPresents;
    }
    public void setDejaPresents(List<FiscalTicketsDTO> dejaPresents) {
        this.dejaPresents = dejaPresents;
    }
    public List<FiscalTicketsDTO> getImportes() {
        return importes;
    }
    public void setImportes(List<FiscalTicketsDTO> importes) {
        this.importes = importes;
    }
    public List<FiscalTicketsDTO> getEchecsImport() {
        return echecsImport;
    }
    public void setEchecsImport(List<FiscalTicketsDTO> echecsImport) {
        this.echecsImport = echecsImport;
    }
    
    public void addEchec(FiscalTicketsDTO ticket) {
        this.echecsImport.add(ticket);
    }
    
    public void addImport(FiscalTicketsDTO ticket) {
        this.importes.add(ticket);
    }
    
    public void addPresent(FiscalTicketsDTO ticket) {
        this.dejaPresents.add(ticket);
    }
    
    
}
