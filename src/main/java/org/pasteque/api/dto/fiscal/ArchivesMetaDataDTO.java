package org.pasteque.api.dto.fiscal;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ArchivesMetaDataDTO {

    private Integer number;
    private String account;
    private String dateStart;
    private String dateStop;
    private String generated;
    private boolean purged;
    public Integer getNumber() {
        return number;
    }
    public void setNumber(Integer number) {
        this.number = number;
    }
    public String getAccount() {
        return account;
    }
    public void setAccount(String account) {
        this.account = account;
    }
    public String getDateStart() {
        return dateStart;
    }
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }
    
    public void setDateStart(LocalDateTime  dateStart) {
        this.dateStart = dateStart.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
    
    public String getDateStop() {
        return dateStop;
    }
    public void setDateStop(String dateStop) {
        this.dateStop = dateStop;
    }
    
    public void setDateStop(LocalDateTime  dateStop) {
        this.dateStop = dateStop.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
    
    public String getGenerated() {
        return generated;
    }
    public void setGenerated(String generated) {
        this.generated = generated;
    }
    
    public void setGenerated(LocalDateTime  generated) {
        this.generated = generated.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
    public boolean isPurged() {
        return purged;
    }
    public void setPurged(boolean purged) {
        this.purged = purged;
    }
    
    
}
