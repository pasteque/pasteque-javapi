package org.pasteque.api.dto.fiscal;

import org.pasteque.api.dto.customers.CustomersLightDTO;

public class PeriodCustBalancesDTO {
    
    private Double balance;
    private String idPeriode;
    private CustomersLightDTO client;

    public Double getBalance() {
        return balance;
    }
    public void setBalance(Double balance) {
        this.balance = balance;
    }
    public String getIdPeriode() {
        return idPeriode;
    }
    public void setIdPeriode(String idPeriode) {
        this.idPeriode = idPeriode;
    }
    public CustomersLightDTO getClient() {
        return client;
    }
    public void setClient(CustomersLightDTO client) {
        this.client = client;
    }

}
