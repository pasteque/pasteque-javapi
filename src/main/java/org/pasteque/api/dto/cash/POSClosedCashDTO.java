package org.pasteque.api.dto.cash;

import java.util.Date;

import org.pasteque.api.dto.locations.POSSalesLocationsDTO;

public class POSClosedCashDTO {

    private String id;
    private String cashRegisterId;
    private int sequence;
    private Date openDate;
    private Date closeDate;
    private String openCash;
    private String closeCash;
    private String expectedCash;
    private POSSalesLocationsDTO salesLocation;
    
    private Object tickets;
    private Object total;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCashRegisterId() {
        return cashRegisterId;
    }

    public void setCashRegisterId(String cashRegisterId) {
        this.cashRegisterId = cashRegisterId;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public String getOpenCash() {
        return openCash;
    }

    public void setOpenCash(String openCash) {
        this.openCash = openCash;
    }

    public String getCloseCash() {
        return closeCash;
    }

    public void setCloseCash(String closeCash) {
        this.closeCash = closeCash;
    }

    public String getExpectedCash() {
        return expectedCash;
    }

    public void setExpectedCash(String expectedCash) {
        this.expectedCash = expectedCash;
    }

    public Object getTickets() {
        return tickets;
    }

    public void setTickets(Object tickets) {
        this.tickets = tickets;
    }

    public Object getTotal() {
        return total;
    }

    public void setTotal(Object total) {
        this.total = total;
    }

    public POSSalesLocationsDTO getSalesLocation() {
        return salesLocation;
    }
    
    public Long getSalesLocationId() {
        return salesLocation == null ? null : salesLocation.getId();
    }

    public void setSalesLocation(POSSalesLocationsDTO salesLocation) {
        this.salesLocation = salesLocation;
    }

}
