package org.pasteque.api.dto.cash;

public class CashRegistersSimpleDTO {
    
    private String id;
    private String locationId;
    private String label;
    private long nextTicketId;
    private int nextSessionId;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public long getNextTicketId() {
        return nextTicketId;
    }
    public void setNextTicketId(long nextTicketId) {
        this.nextTicketId = nextTicketId;
    }
    public String getLocationId() {
        return locationId;
    }
    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
    public int getNextSessionId() {
        return nextSessionId;
    }
    public void setNextSessionId(int nextSessionId) {
        this.nextSessionId = nextSessionId;
    }
    
    
}
