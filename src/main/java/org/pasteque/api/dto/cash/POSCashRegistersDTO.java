package org.pasteque.api.dto.cash;

import org.pasteque.api.dto.locations.POSLocationsDTO;

public class POSCashRegistersDTO {

    private String id;
    private String label;
    private long nextTicketId;
    private int nextSessionId;
    private String locationId;
    private String taxCustCategId;
    private POSLocationsDTO location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getNextTicketId() {
        return nextTicketId;
    }

    public void setNextTicketId(long nextTicketId) {
        this.nextTicketId = nextTicketId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getTaxCustCategId() {
        return taxCustCategId;
    }

    public void setTaxCustCategId(String taxCustCategId) {
        this.taxCustCategId = taxCustCategId;
    }

    public POSLocationsDTO getLocation() {
        return location;
    }

    public void setLocation(POSLocationsDTO location) {
        this.location = location;
    }

    public int getNextSessionId() {
        return nextSessionId;
    }

    public void setNextSessionId(int nextSessionId) {
        this.nextSessionId = nextSessionId;
    }

}
