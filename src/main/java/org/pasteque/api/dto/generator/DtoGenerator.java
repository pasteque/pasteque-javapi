package org.pasteque.api.dto.generator;

import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.CtNewMethod;
import javassist.LoaderClassPath;
import javassist.NotFoundException;

public class DtoGenerator {

    public static Class<?> generate(String className, Map<String, Class<?>>  properties) throws NotFoundException,
    CannotCompileException {

        ClassPool pool = ClassPool.getDefault();
        CtClass cc = pool.getOrNull(className);
        
        //On a toutefois un pb car la class est chargée
        if(cc != null) {
            className = String.format("%s%d", className , System.currentTimeMillis());
        } 
        
        cc = pool.makeClass(className);
        
        // add this to define a super class to extend
        // cc.setSuperclass(resolveCtClass(MySuperClass.class));

        // add this to define an interface to implement
        cc.addInterface(resolveCtClass(Serializable.class));

        for (Entry<String, Class<?>> entry : properties.entrySet()) {
            String fieldName = generateFieldName(entry.getKey());
            cc.addField(new CtField(resolveCtClass(entry.getValue()), fieldName , cc));

            // add getter
            cc.addMethod(generateGetter(cc, fieldName, entry.getValue()));

            // add setter
            cc.addMethod(generateSetter(cc, fieldName, entry.getValue()));
        }

        return cc.toClass();
    }
    public static String generateFieldName(String fieldName) {
        if(!Character.isLetter(fieldName.charAt(0)) && fieldName.charAt(0) != '_') {
            fieldName = "_".concat(fieldName);
        }
        //TODO EDU : attention au contenu de la regex - il faut que ça passe 
        // dans les noms de méthodes mais aussi dans les nom des propriétés
        return fieldName.replaceAll("[^A-Za-z0-9_éèàêâôùïîë]", "_");
    }
    
    public static String generateFieldNameForMethod(String fieldName) {
        if(Character.isLetter(fieldName.charAt(0))) {
            fieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
        }
        return fieldName;
    }
    private static CtMethod generateGetter(CtClass declaringClass, String fieldName, Class<?> fieldClass)
            throws CannotCompileException {

        String getterName = "get" + generateFieldNameForMethod(fieldName);

        StringBuffer sb = new StringBuffer();
        sb.append("public ").append(fieldClass.getName()).append(" ")
        .append(getterName).append("(){").append("return this.")
        .append(fieldName).append(";").append("}");
        return CtNewMethod.make(sb.toString(), declaringClass);
    }

    private static CtMethod generateSetter(CtClass declaringClass, String fieldName, Class<?> fieldClass)
            throws CannotCompileException {

        String setterName = "set" + generateFieldNameForMethod(fieldName);

        StringBuffer sb = new StringBuffer();
        sb.append("public void ").append(setterName).append("(")
        .append(fieldClass.getName()).append(" ").append(fieldName)
        .append(")").append("{").append("this.").append(fieldName)
        .append("=").append(fieldName).append(";").append("}");
        return CtNewMethod.make(sb.toString(), declaringClass);
    }

    private static CtClass resolveCtClass(Class<?> class1) throws NotFoundException {
        ClassPool pool = ClassPool.getDefault();
        ClassLoader loader = class1.getClassLoader();
        pool.insertClassPath(new LoaderClassPath( loader));
        return pool.get(class1.getName());
    }

}
