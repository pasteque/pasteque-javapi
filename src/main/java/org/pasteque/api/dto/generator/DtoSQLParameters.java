package org.pasteque.api.dto.generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.pasteque.api.model.adaptable.ILoadable;

public class DtoSQLParameters {

    public ArrayList<String> tableList;
    public ArrayList<String> constraintList;
    public ArrayList<String> havingList;
    public HashMap<Integer,Object> parameterList;
    public HashMap<String,String> tableHash;
    public String groupBy;
    public String select;
    private List<Object[]> result;
    private int rowSize;
    
    public DtoSQLParameters() {
        tableList = new ArrayList<String>() ;
        constraintList =new ArrayList<String>() ;
        havingList = new ArrayList<String>() ;
        parameterList = new HashMap<Integer,Object>();
        tableHash = new HashMap<String,String>();
        groupBy = null ;
        select = null ;
    }

    public void setResult(List<Object[]> resultList) {
        result = resultList;  
    }
    
    public int getResultSize() {
        return(result == null ? 0 : result.size());  
    }
    
    public int getRowSize() {
        //By default we consider default rowsize to be 1024 Bit
        return rowSize > 0 ? rowSize : 1024 ;
    }

    public void setRowSize(int rowSize) {
        this.rowSize = rowSize;
    }

    public void appendResult(List<Object[]> resultList) {
        if(result == null || result.size() == 0 ) {
            setResult(resultList);
        } else {
            result.addAll(resultList);
        }
    } 
    
    public <T extends ILoadable> List<T> getResult( Class<T> selector) {
        List<T> listDTO = new ArrayList<>();
        
        for (Object[] adaptable : result) {
            T instance;
            try {
                instance = selector.newInstance();

                instance.loadFromObjectArray(adaptable,0);
                listDTO.add(instance);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return listDTO;
    }
    
    
}
