package org.pasteque.api.dto.products;

import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.response.ICsvResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TariffAreasIndexDTO implements ICsvResponse , ILoadable{

    private String id;
    private String nom;
    @JsonIgnore
    private Boolean exclu;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String[] header() {
        return new String[] { "Code", "Nom"};
    }

    @Override
    public String[] CSV() {
        return new String[] {getId(), getNom()};
    }

    public boolean isExclu() {
        return exclu == null ? false : exclu;
    }

    public void setExclu(Boolean exclus) {
        this.exclu = exclus;
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array, int starting_index) {
        boolean retour = false;
        
        if(array[0+starting_index] != null && (array.length - starting_index) >= 2) {

            try {
                this.setId( array[0+starting_index] instanceof Integer ? Integer.toString((Integer) array[0+starting_index]) : Long.toString((Long) array[0+starting_index]));
                this.setNom((String) array[1+starting_index]);
            
                retour = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                retour = false;
            }
        }

        return retour;
    }

}
