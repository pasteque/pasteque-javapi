package org.pasteque.api.dto.products;

public class POSGroupsDTO {

    public String id;
    public String compositionId;
    public String label;
    public boolean hasImage;
    public int dispOrder;
    public int choices;

}
