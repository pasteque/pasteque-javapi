package org.pasteque.api.dto.products;

import java.util.List;

public class POSTaxCategoriesDTO {

    private String id;
    private String label;
    private List<POSTaxesDTO> taxes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<POSTaxesDTO> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<POSTaxesDTO> taxes) {
        this.taxes = taxes;
    }
}
