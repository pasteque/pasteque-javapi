package org.pasteque.api.dto.products;

import java.util.List;

public class POSCompositionsDTO extends POSProductsDTO {
    
    private List<POSGroupsDTO> groups;

    public List<POSGroupsDTO> getGroups() {
        return groups;
    }

    public void setGroups(List<POSGroupsDTO> groups) {
        this.groups = groups;
    }

}
