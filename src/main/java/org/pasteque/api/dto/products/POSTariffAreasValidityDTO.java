package org.pasteque.api.dto.products;

public class POSTariffAreasValidityDTO {

    private Long id;
    private Long areaId;
    private String locationId;
    private Long startDate;
    private Long endDate;
    private boolean actif;
    
    public long getId() {
        return id == null ? 0L : id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public long getAreaId() {
        return areaId;
    }
    public void setAreaId(long areaId) {
        this.areaId = areaId;
    }
    public String getLocationId() {
        return locationId;
    }
    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
    public Long getStartDate() {
        return startDate;
    }
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }
    public Long getEndDate() {
        return endDate;
    }
    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }
	public boolean isActif() {
		return actif;
	}
	public void setActif(boolean actif) {
		this.actif = actif;
	}
    
    
}
