package org.pasteque.api.dto.products;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class POSTaxesDTO {

    private String id;
    private String taxCatId;
    private String taxCustCategoryId;
    private String label;
    private Long startDate;
    private Long endDate;
    private Double rate;
    private Double amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaxCatId() {
        return taxCatId;
    }

    public void setTaxCatId(String taxCatId) {
        this.taxCatId = taxCatId;
    }

    public String getTaxCustCategoryId() {
        return taxCustCategoryId;
    }

    public void setTaxCustCategoryId(String taxCustCategoryId) {
        this.taxCustCategoryId = taxCustCategoryId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    @JsonIgnore
    public Date getStartDatetime() {
        return new Date(startDate);
    }

    @JsonIgnore
    public void setStartDatetime(Date startDate) {
        if (startDate != null) {
            this.startDate = (startDate.getTime());
        } else {
            this.startDate = null;
        }
    }

    @JsonIgnore
    public Date getEndDatetime() {
        return new Date(endDate);
    }

    @JsonIgnore
    public void setEndDatetime(Date endDate) {
        if (endDate != null) {
            this.endDate = (endDate.getTime());
        } else {
            endDate = null;
        }
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
