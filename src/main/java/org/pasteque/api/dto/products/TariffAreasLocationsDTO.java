package org.pasteque.api.dto.products;

import java.util.Date;

import org.pasteque.api.dto.locations.LocationsInfoDTO;

public class TariffAreasLocationsDTO {

    private long id;
    private Date startDate;
    private Date endDate;
    private LocationsInfoDTO location;
    private TariffAreasIndexDTO tariffArea;
    private boolean actif ;
    
    public TariffAreasLocationsDTO() {
        
    }
    
    public TariffAreasLocationsDTO(POSTariffAreasValidityDTO source) {
        id = source.getId();
        startDate = source.getStartDate() == null ? null : new Date(source.getStartDate());
        endDate = source.getEndDate() == null ? null :  new Date(source.getEndDate());
        if(source.getLocationId() != null &&  ! source.getLocationId().isEmpty()) {
            location = new LocationsInfoDTO();
            location.setId(source.getLocationId());
        }
        if(source.getAreaId() != 0L) {
            tariffArea = new TariffAreasIndexDTO();
            tariffArea.setId(String.valueOf(source.getAreaId()));
        }
    }
    
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public TariffAreasIndexDTO getTariffArea() {
        return tariffArea;
    }
    public void setTariffArea(TariffAreasIndexDTO tariffArea) {
        this.tariffArea = tariffArea;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public LocationsInfoDTO getLocation() {
        return location;
    }
    public void setLocation(LocationsInfoDTO location) {
        this.location = location;
    }
	public boolean isActif() {
		return actif;
	}
	public void setActif(boolean actif) {
		this.actif = actif;
	}
    
}
