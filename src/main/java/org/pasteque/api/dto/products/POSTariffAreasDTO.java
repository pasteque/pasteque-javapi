package org.pasteque.api.dto.products;

import java.util.List;

public class POSTariffAreasDTO {

    private Long id;
    private String label;
    private int dispOrder;
    private List<POSTariffAreaPriceDTO> prices;
    private List<POSTariffAreasValidityDTO> validity;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getDispOrder() {
        return dispOrder;
    }

    public void setDispOrder(int dispOrder) {
        this.dispOrder = dispOrder;
    }

    public List<POSTariffAreaPriceDTO> getPrices() {
        return prices;
    }

    public void setPrices(List<POSTariffAreaPriceDTO> prices) {
        this.prices = prices;
    }

    public List<POSTariffAreasValidityDTO> getValidity() {
        return validity;
    }

    public void setValidity(List<POSTariffAreasValidityDTO> validity) {
        this.validity = validity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
