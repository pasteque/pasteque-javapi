package org.pasteque.api.dto.products;

public class POSTariffAreaPriceDTO {

    private Long arreaId;
    private String productId;
    private double price;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getAreaId() {
        return arreaId;
    }

    public void setArreaId(Long arreaId) {
        this.arreaId = arreaId;
    }

}
