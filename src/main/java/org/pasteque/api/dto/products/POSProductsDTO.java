package org.pasteque.api.dto.products;

import java.util.List;

import org.pasteque.api.dto.application.POSMessagesDTO;

public class POSProductsDTO {

    private String id;
    private String reference;
    private String barcode;
    private String label;
    private String priceBuy;
    private String priceSell;
    private boolean visible;
    private boolean scaled;
    private String categoryId;
    private Integer dispOrder;
    private String taxCatId;
    private String attributeSetId;
    private boolean hasImage;
    private boolean discountEnabled;
    private String discountRate;
    private List<String> barcodes;
    private List<POSProductsComponentsDTO> parentProducts;
    private List<POSProductsComponentsDTO> childProducts;
    private List<POSMessagesDTO> messages;
    private List<String> taxCategories;
    private boolean stockable;
    private boolean isCom;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPriceBuy() {
        return priceBuy;
    }

    public void setPriceBuy(String priceBuy) {
        this.priceBuy = priceBuy;
    }

    public String getPriceSell() {
        return priceSell;
    }

    public void setPriceSell(String priceSell) {
        this.priceSell = priceSell;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isScaled() {
        return scaled;
    }

    public void setScaled(boolean scaled) {
        this.scaled = scaled;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getDispOrder() {
        return dispOrder;
    }

    public void setDispOrder(Integer dispOrder) {
        this.dispOrder = dispOrder;
    }

    public String getTaxCatId() {
        return taxCatId;
    }

    public void setTaxCatId(String taxCatId) {
        this.taxCatId = taxCatId;
    }

    public String getAttributeSetId() {
        return attributeSetId;
    }

    public void setAttributeSetId(String attributeSetId) {
        this.attributeSetId = attributeSetId;
    }

    public boolean isHasImage() {
        return hasImage;
    }

    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
    }

    public boolean isDiscountEnabled() {
        return discountEnabled;
    }

    public void setDiscountEnabled(boolean discountEnabled) {
        this.discountEnabled = discountEnabled;
    }

    public String getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(String discountRate) {
        this.discountRate = discountRate;
    }

    public List<String> getBarcodes() {
        return barcodes;
    }

    public void setBarcodes(List<String> barcodes) {
        this.barcodes = barcodes;
    }

    public List<POSProductsComponentsDTO> getParentProducts() {
        return parentProducts;
    }

    public void setParentProducts(List<POSProductsComponentsDTO> parentProducts) {
        this.parentProducts = parentProducts;
    }

    public List<POSProductsComponentsDTO> getChildProducts() {
        return childProducts;
    }

    public void setChildProducts(List<POSProductsComponentsDTO> childProducts) {
        this.childProducts = childProducts;
    }

    public List<POSMessagesDTO> getMessages() {
        return messages;
    }

    public void setMessages(List<POSMessagesDTO> messages) {
        this.messages = messages;
    }

    public List<String> getTaxCategories() {
        return taxCategories;
    }

    public void setTaxCategories(List<String> taxCategories) {
        this.taxCategories = taxCategories;
    }

    public boolean isStockable() {
        return stockable;
    }

    public void setStockable(boolean stockable) {
        this.stockable = stockable;
    }

    public boolean isCom() {
        return isCom;
    }
    
    public void setCom(boolean com) {
        this.isCom = com ; 
    }



}
