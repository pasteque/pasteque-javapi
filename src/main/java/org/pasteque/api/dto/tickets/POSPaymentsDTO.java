package org.pasteque.api.dto.tickets;


public class POSPaymentsDTO {

    private String id;
    private double amount;
    private double currencyAmount;
    private Long currencyId;
    private String type;
    private String transId;
    private byte[] returnMessage;
    private String note;
    private Long echeance;
    private Long paymentsNumber;
    private String paymentCateg;
    private String paymentLabel;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getCurrencyAmount() {
        return currencyAmount;
    }

    public void setCurrencyAmount(double currencyAmount) {
        this.currencyAmount = currencyAmount;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public byte[] getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(byte[] returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getEcheance() {
        return echeance;
    }

    public void setEcheance(Long echeance) {
        this.echeance = echeance;
    }

    public long getPaymentsNumber() {
        return paymentsNumber == null ? 1 : paymentsNumber;
    }

    public void setPaymentsNumber(Long paymentsNumber) {
        this.paymentsNumber = paymentsNumber;
    }

    public String getPaymentCateg() {
        return paymentCateg;
    }

    public void setPaymentCateg(String paymentCateg) {
        this.paymentCateg = paymentCateg;
    }

    public String getPaymentLabel() {
        return paymentLabel;
    }

    public void setPaymentLabel(String paymentLabel) {
        this.paymentLabel = paymentLabel;
    }

}
