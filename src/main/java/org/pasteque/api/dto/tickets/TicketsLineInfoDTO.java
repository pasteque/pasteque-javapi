package org.pasteque.api.dto.tickets;

import java.util.ArrayList;
import java.util.List;

public class TicketsLineInfoDTO {

    private TicketsProductsInfoDTO product;
    private List<TicketsProductsInfoDTO> subProducts = new ArrayList<TicketsProductsInfoDTO>();
    private double units;
    private double ecotaxes;
    private double fullPrice;
    private double finalPrice;
    private double discount;
    private String vat;
    private String note;
    private boolean commande;

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public double getFullPrice() {
        return fullPrice;
    }

    public void setFullPrice(double fullPrice) {
        this.fullPrice = fullPrice;
    }

    public List<TicketsProductsInfoDTO> getSubProducts() {
        return subProducts;
    }

    public void setSubProducts(List<TicketsProductsInfoDTO> subProducts) {
        this.subProducts = subProducts;
    }

    public double getEcotaxes() {
        return ecotaxes;
    }

    public void setEcotaxes(double ecotaxes) {
        this.ecotaxes = ecotaxes;
    }

    public TicketsProductsInfoDTO getProduct() {
        return product;
    }

    public void setProduct(TicketsProductsInfoDTO product) {
        this.product = product;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public boolean isCommande() {
        return commande;
    }

    public void setCommande(boolean commande) {
        this.commande = commande;
    }

}
