package org.pasteque.api.dto.tickets;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.pasteque.api.response.ICsvResponse;

public class PaymentsInfoDTO implements ICsvResponse {

    private String payment;
    private String paymentLabel;
    private String paymentCateg;
    private Double amount = 0D;
    private Long number = 0L;

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getPaymentLabel() {
        return paymentLabel;
    }

    public void setPaymentLabel(String paymentLabel) {
        this.paymentLabel = paymentLabel;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getPaymentCateg() {
        return paymentCateg;
    }

    public void setPaymentCateg(String paymentCateg) {
        this.paymentCateg = paymentCateg;
    }

    @Override
    public String[] header() {
        return new String[] { "Moyen de paiment", "Montant" };
    }

    @Override
    public String[] CSV() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat decf = new DecimalFormat("#.00", otherSymbols);
        return new String[] { getPayment(), decf.format(getAmount()) };
    }

}
