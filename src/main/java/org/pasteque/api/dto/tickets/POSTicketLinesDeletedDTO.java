package org.pasteque.api.dto.tickets;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class POSTicketLinesDeletedDTO {

    private String ticketId;
    private double price;
    private String vatId;
    private double quantity;
    private byte[] attributes;
    private double discountRate;
    private String productId;
    private int dispOrder;
    private List<String> taxes;
    private boolean isOrder=false;
    private char type;
    private Long tariffAreaId;
    private String note;
    private Date createdDate;
    /*private Long parentId;
    private Integer parentLine;*/

    
    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public byte[] getAttributes() {
        return attributes;
    }

    public void setAttributes(byte[] attributes) {
        this.attributes = attributes;
    }

    public double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(double discountRate) {
        this.discountRate = discountRate;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getDispOrder() {
        return dispOrder;
    }

    public void setDispOrder(int dispOrder) {
        this.dispOrder = dispOrder;
    }

    public List<String> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<String> taxes) {
        this.taxes = taxes;
    }

    public boolean isOrder() {
        return isOrder;
    }

    public void setOrder(boolean isOrder) {
        this.isOrder = isOrder;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public Long getTariffAreaId() {
        return tariffAreaId;
    }

    public void setTariffAreaId(Long tariffAreaId) {
        this.tariffAreaId = tariffAreaId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    
   /* public Integer getParentLine() {
        return parentLine;
    }

    public void setParentLine(Integer parentLine) {
        this.parentLine = parentLine;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }*/

   
}
