package org.pasteque.api.dto.tickets;

import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.response.IXlsResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class POSTicketsDTO implements IXlsResponse, ILoadable{
    
    private String cashId;
    private String id;
    private Long tariffAreaId;
    private Long ticketId;
    private String customerId;
    private Integer custCount;
    private Long discountProfileId;
    private String userId;
    private List<POSPaymentsDTO> payments;
    private List<POSTicketLinesDTO> lines;
    private double discountRate;
    private int type;
    private long date;
    private String inseeNum;
    private Long parentTicketId;
    private String note;
    @JsonIgnore
    private Long logicalDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTariffAreaId() {
        return tariffAreaId;
    }

    public void setTariffAreaId(Long tariffAreaId) {
        this.tariffAreaId = tariffAreaId;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getCustCount() {
        return custCount;
    }

    public void setCustCount(Integer custCount) {
        this.custCount = custCount;
    }

    public Long getDiscountProfileId() {
        return discountProfileId;
    }

    public void setDiscountProfileId(Long discountProfileId) {
        this.discountProfileId = discountProfileId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<POSPaymentsDTO> getPayments() {
        return payments;
    }

    public void setPayments(List<POSPaymentsDTO> payments) {
        this.payments = payments;
    }

    public List<POSTicketLinesDTO> getLines() {
        return lines;
    }

    public void setLines(List<POSTicketLinesDTO> lines) {
        this.lines = lines;
    }

    public double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(double discountRate) {
        this.discountRate = discountRate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    @JsonIgnore
    public Date getDatetime() {
        return new Date(this.date);
    }
    
    @JsonIgnore
    public void setDatetime(Date date) {
        this.date = (date.getTime());
    }
    
    @JsonIgnore
    public void setLogicalDatetime(Date date) {
        this.logicalDate = (date.getTime());
    }
    
    @JsonIgnore
    public Date getLogicalDatetime() {
        return new Date(this.logicalDate == null ? this.date : this.logicalDate);
    }

    public String getCashId() {
        return cashId;
    }

    public void setCashId(String cashId) {
        this.cashId = cashId;
    }

    public String getInseeNum() {
        return inseeNum;
    }

    public void setInseeNum(String inseeNum) {
        this.inseeNum = inseeNum;
    }

    public Long getParentTicketId() {
        return parentTicketId;
    }

    public void setParentTicketId(Long parentTicketId) {
        this.parentTicketId = parentTicketId;
    }

    @Override
    public String[] header() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String[] CSV() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array , int starting_index) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        // TODO Auto-generated method stub
        return null;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getColWidth(int index) {
        return 0 ;
    }

}
