package org.pasteque.api.dto.tickets;

import java.util.List;

import org.pasteque.api.dto.cash.POSClosedCashCategoryInfoDTO;
import org.pasteque.api.dto.cash.POSClosedCashTaxInfoDTO;

public class POSZTicketsDTO {

    private String cashId;
    private Double openCash;
    private Double closeCash;
    private long ticketCount;
    private Integer custCount;
    private long paymentCount;
    private Double cs;
    private Double discount;
    private List<POSPaymentsDTO> payments;
    private List<POSClosedCashTaxInfoDTO> taxes;
    private List<POSClosedCashCategoryInfoDTO> catSales;

    public String getCashId() {
        return cashId;
    }

    public void setCashId(String cashId) {
        this.cashId = cashId;
    }

    public long getTicketCount() {
        return ticketCount;
    }

    public void setTicketCount(long ticketCount) {
        this.ticketCount = ticketCount;
    }

    public Integer getCustCount() {
        return custCount;
    }

    public void setCustCount(Integer custCount) {
        this.custCount = custCount;
    }

    public long getPaymentCount() {
        return paymentCount;
    }

    public void setPaymentCount(long paymentCount) {
        this.paymentCount = paymentCount;
    }

    public Double getCs() {
        return cs;
    }

    public void setCs(Double cs) {
        this.cs = cs;
    }

    public List<POSPaymentsDTO> getPayments() {
        return payments;
    }

    public void setPayments(List<POSPaymentsDTO> payments) {
        this.payments = payments;
    }

    public List<POSClosedCashTaxInfoDTO> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<POSClosedCashTaxInfoDTO> taxes) {
        this.taxes = taxes;
    }

    public List<POSClosedCashCategoryInfoDTO> getCatSales() {
        return catSales;
    }

    public void setCatSales(List<POSClosedCashCategoryInfoDTO> catSales) {
        this.catSales = catSales;
    }

    public Double getOpenCash() {
        return openCash;
    }

    public void setOpenCash(Double openCash) {
        this.openCash = openCash;
    }

    public Double getCloseCash() {
        return closeCash;
    }

    public void setCloseCash(Double closeCash) {
        this.closeCash = closeCash;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

}
