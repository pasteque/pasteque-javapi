package org.pasteque.api.dto.tickets;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.pasteque.api.response.ICsvResponse;


public class DetailsPaymentsDTO implements ICsvResponse {

    private String ticketId;
    private String payment;
    private Double amount;
    private String reference;
    private String note;
    private Date echeance;
    private Date datenew;

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getEcheance() {
        return echeance;
    }

    public void setEcheance(Date echeance) {
        this.echeance = echeance;
    }

    public Date getDatenew() {
        return datenew;
    }

    public void setDatenew(Date datenew) {
        this.datenew = datenew;
    }

    @Override
    public String[] header() {
        return new String[] { "Numero ticket", "Moyen de paiment", "Montant", "Date" , "Reference", "Note", "Echeance" };
    }

    @Override
    public String[] CSV() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dateFormatted = getEcheance() != null ? df.format(getEcheance()) : "";
        SimpleDateFormat hdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dateHourFormatted = getDatenew() != null ? hdf.format(getDatenew()) : "";
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat decf = new DecimalFormat("#.00", otherSymbols);
        return new String[] { getTicketId(), getPayment(),decf.format( getAmount()), dateHourFormatted , getReference(), getNote(), dateFormatted };
    }
}
