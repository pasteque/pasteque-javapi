package org.pasteque.api.dto.tickets;

import java.util.Date;
import java.util.List;

public class TicketsInfoDTO {

    private Date date;
    private String customersLabel;
    private String customersId;
    private String username;
    private String pointVente;
    private String tournee;
    private String deballage;
    private long ticketId;

    private double discount;
    private double amount;

    private List<TicketsLineInfoDTO> lines;
    private List<TicketsTaxLineInfoDTO> taxes;
    private List<DetailsPaymentsDTO> payments;
    
    private String catalogue;
    private String note;
    private String rejectReason;
    private String version;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCustomersLabel() {
        return customersLabel;
    }

    public void setCustomersLabel(String customersLabel) {
        this.customersLabel = customersLabel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<TicketsLineInfoDTO> getLines() {
        return lines;
    }

    public void setLines(List<TicketsLineInfoDTO> lines) {
        this.lines = lines;
    }

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public List<TicketsTaxLineInfoDTO> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<TicketsTaxLineInfoDTO> taxes) {
        this.taxes = taxes;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getCustomersId() {
        return customersId;
    }

    public void setCustomersId(String customersId) {
        this.customersId = customersId;
    }

    public List<DetailsPaymentsDTO> getPayments() {
        return payments;
    }

    public void setPayments(List<DetailsPaymentsDTO> list) {
        this.payments = list;
    }

    public String getCatalogue() {
        return catalogue;
    }

    public void setCatalogue(String catalogue) {
        this.catalogue = catalogue;
    }

    public String getPointVente() {
        return pointVente;
    }

    public void setPointVente(String pointVente) {
        this.pointVente = pointVente;
    }

    public String getTournee() {
        return tournee;
    }

    public void setTournee(String tournee) {
        this.tournee = tournee;
    }

    public String getDeballage() {
        return deballage;
    }

    public void setDeballage(String deballage) {
        this.deballage = deballage;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getFailure() {
        return this.rejectReason;
    }
    
    public void setFailure(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
