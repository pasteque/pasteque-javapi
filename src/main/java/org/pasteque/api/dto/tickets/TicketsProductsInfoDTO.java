package org.pasteque.api.dto.tickets;

import java.util.ArrayList;
import java.util.List;

public class TicketsProductsInfoDTO {

    private String id;
    private String label;
    private double quantity;
    private List<String> taxes = new ArrayList<String>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<String> taxes) {
        this.taxes = taxes;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

}
