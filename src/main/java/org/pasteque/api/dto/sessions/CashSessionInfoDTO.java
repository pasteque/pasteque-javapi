package org.pasteque.api.dto.sessions;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.pasteque.api.dto.cash.CashRegistersSimpleDTO;
import org.pasteque.api.dto.fiscal.PeriodCatTaxesDTO;
import org.pasteque.api.dto.fiscal.PeriodCatsDTO;
import org.pasteque.api.dto.fiscal.PeriodCustBalancesDTO;
import org.pasteque.api.dto.fiscal.PeriodPaymentsDTO;
import org.pasteque.api.dto.fiscal.PeriodTaxesDTO;

/**
 * Un cousin de ZTickets DTO utilisé pour l'enregistrement fiscal
 * avec en plus les notions de 
 * version de l'API qui a généré
 * raison de l'impossibilité de l'enregistrer
 * 
 * et les infos plus détaillées sur les balances clients
 * 
 * @author educray
 *
 */
public class CashSessionInfoDTO {

    private String version;
    private String failure;
    private String openDate;
    private String closeDate;
    private CashRegistersSimpleDTO cashRegister;
    private List<PeriodPaymentsDTO> payments;
    private List<PeriodTaxesDTO> taxes;
    private List<PeriodCustBalancesDTO> custBalances;
    private List<PeriodCatsDTO> catSales;
    private List<PeriodCatTaxesDTO> catTaxes;
    
    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }
    public String getFailure() {
        return failure;
    }
    public void setFailure(String failure) {
        this.failure = failure;
    }
    public String getOpenDate() {
        return openDate;
    }
    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }
    public String getCloseDate() {
        return closeDate;
    }
    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }
    public CashRegistersSimpleDTO getCashRegister() {
        return cashRegister;
    }
    public void setCashRegister(CashRegistersSimpleDTO cashRegister) {
        this.cashRegister = cashRegister;
    }
    public List<PeriodPaymentsDTO> getPayments() {
        return payments;
    }
    public void setPayments(List<PeriodPaymentsDTO> list) {
        this.payments = list;
    }
    public List<PeriodTaxesDTO> getTaxes() {
        return taxes;
    }
    public void setTaxes(List<PeriodTaxesDTO> list) {
        this.taxes = list;
    }
    public List<PeriodCustBalancesDTO> getCustBalances() {
        return custBalances;
    }
    public void setCustBalances(List<PeriodCustBalancesDTO> custBalances) {
        this.custBalances = custBalances;
    }
    public List<PeriodCatsDTO> getCatSales() {
        return catSales;
    }
    public void setCatSales(List<PeriodCatsDTO> list) {
        this.catSales = list;
    }
    public List<PeriodCatTaxesDTO> getCatTaxes() {
        return catTaxes;
    }
    public void setCatTaxes(List<PeriodCatTaxesDTO> list) {
        this.catTaxes = list;
    }
    public void setOpenDate(LocalDateTime dateStart) {
        openDate  = dateStart.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
    public void setCloseDate(LocalDateTime dateEnd) {
        closeDate  = dateEnd.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }


    /** Create struct of the full data to be written in stone
     * (that is, FiscalTicket). The cash session must be closed. */
    /*
    public function toStone() {

        // Fetch associative fields and include the data.
        unset($struct['id']);
        $struct['cashRegister'] = ['reference' => $this->getCashRegister()->getReference(),
                'label' => $this->getCashRegister()->getLabel()];
        for ($i = 0; $i < count($struct['payments']); $i++) {
            $payment = $struct['payments'][$i];
            unset($payment['id']);
            unset($payment['cashSession']);
            $paymentMode = $this->getPayments()->get($i)->getPaymentMode();
            $currency = $this->getPayments()->get($i)->getCurrency();
            $payment['paymentMode'] = ['reference' =>$paymentMode->getReference(),
                    'label' => $paymentMode->getLabel()];
            $payment['currency'] = ['reference' => $currency->getReference(),
                    'label' => $currency->getLabel()];
            $struct['payments'][$i] = $payment;
        }
        for ($i = 0; $i < count($struct['taxes']); $i++) {
            $tax = $struct['taxes'][$i];
            unset($tax['id']);
            unset($tax['cashSession']);
            unset($tax['sequence']);
            unset($tax['tax']);
            $struct['taxes'][$i] = $tax;
        }
        for ($i = 0; $i < count($struct['custBalances']); $i++) {
            $bal = $struct['custBalances'][$i];
            unset($bal['id']);
            unset($bal['cashSession']);
            unset($bal['sequence']);
            $c = $this->getCustBalances()->get($i)->getCustomer();
            $bal['customer'] = ['dispName' => $c->getDispName(),
                    'firstName' => $c->getFirstName(),
                    'lastName' => $c->getLastName()];
            $struct['custBalances'][$i] = $bal;
        }
        for ($i = 0; $i < count($struct['catSales']); $i++) {
            $cat = $struct['catSales'][$i];
            unset($cat['id']);
            unset($cat['cashSession']);
            $struct['catSales'][$i] = $cat;
        }
        for ($i = 0; $i < count($struct['catTaxes']); $i++) {
            $catTax = $struct['catTaxes'][$i];
            $tax = $this->getCatTaxes()->get($i)->getTax();
            unset($catTax['id']);
            unset($catTax['cashSession']);
            $catTax['tax'] = $tax->getLabel();
            $catTax['taxRate'] = $tax->getRate();
            $struct['catTaxes'][$i] = $catTax;
        }
        return $struct;
    }

     */
}
