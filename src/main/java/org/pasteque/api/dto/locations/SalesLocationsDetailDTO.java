package org.pasteque.api.dto.locations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SalesLocationsDetailDTO {

    private Long id;
    private String tourName;
    private String locationName;
    private String inseeNb;
    private String city;
    private Date startDate;
    private Date endDate;
    private Date nextStartDate;
    private Date nextEndDate;
    private int pubNb;
    private double fee;
    private boolean paid;
    private SalesLocationsStatsDTO currentStats;
    private List<SalesLocationsStatsDTO> salesHistory = new ArrayList<SalesLocationsStatsDTO>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInseeNb() {
        return inseeNb;
    }

    public void setInseeNb(String inseeNb) {
        this.inseeNb = inseeNb;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getPubNb() {
        return pubNb;
    }

    public void setPubNb(int pubNb) {
        this.pubNb = pubNb;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public Date getNextStartDate() {
        return nextStartDate;
    }

    public void setNextStartDate(Date nextStartDate) {
        this.nextStartDate = nextStartDate;
    }

    public Date getNextEndDate() {
        return nextEndDate;
    }

    public void setNextEndDate(Date nextEndDate) {
        this.nextEndDate = nextEndDate;
    }

    public SalesLocationsStatsDTO getCurrentStats() {
        return currentStats;
    }

    public void setCurrentStats(SalesLocationsStatsDTO currentStats) {
        this.currentStats = currentStats;
    }

    public List<SalesLocationsStatsDTO> getSalesHistory() {
        return salesHistory;
    }

    public void setSalesHistory(List<SalesLocationsStatsDTO> salesHistory) {
        this.salesHistory = salesHistory;
    }

    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

}
