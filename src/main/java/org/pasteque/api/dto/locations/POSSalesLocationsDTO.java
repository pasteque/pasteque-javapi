package org.pasteque.api.dto.locations;

import java.util.Date;
import java.util.List;


public class POSSalesLocationsDTO {
    
    private Long id;
    private long tourId;
    private String tourName;
    private String inseeNb;
    private String city;
    private Date startDate;
    private Date endDate;
    private int pubNb;
    private double fee;
    private boolean paid;
    private List<String> salesSessions;
    private Date nextStartDate;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public long getTourId() {
        return tourId;
    }
    public void setTourId(long tourId) {
        this.tourId = tourId;
    }
    public String getInseeNb() {
        return inseeNb;
    }
    public void setInseeNb(String inseeNb) {
        this.inseeNb = inseeNb;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public int getPubNb() {
        return pubNb;
    }
    public void setPubNb(int pubNb) {
        this.pubNb = pubNb;
    }
    public double getFee() {
        return fee;
    }
    public void setFee(double fee) {
        this.fee = fee;
    }
    public boolean isPaid() {
        return paid;
    }
    public void setPaid(boolean paid) {
        this.paid = paid;
    }
    public List<String> getSalesSessions() {
        return salesSessions;
    }
    public void setSalesSessions(List<String> salesSessions) {
        this.salesSessions = salesSessions;
    }
    public String getTourName() {
        return tourName;
    }
    public void setTourName(String tourName) {
        this.tourName = tourName;
    }
    public Date getNextStartDate() {
        return nextStartDate;
    }
    public void setNextStartDate(Date nextStartDate) {
        this.nextStartDate = nextStartDate;
    }

}
