package org.pasteque.api.dto.locations;

import java.util.Date;

public class POSToursLocationsDTO {

    private Long id;
    private Date startDate;
    private Date endDate;
    private LocationsInfoDTO location;
    private TourIndexDTO tour;
    
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public TourIndexDTO getTour() {
        return tour;
    }
    public void setTour(TourIndexDTO tour) {
        this.tour = tour;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public LocationsInfoDTO getLocation() {
        return location;
    }
    public void setLocation(LocationsInfoDTO location) {
        this.location = location;
    }
    
    
}
