package org.pasteque.api.dto.locations;

import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;

public class TourIndexDTO implements ILoadable{

    private Long id;
    private String nom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array, int starting_index) {
        boolean retour = false;
        
        if(array[0+starting_index] != null && (array.length - starting_index) >= 2) {

            try {
                this.setId(((Integer)array[0+starting_index]).longValue());
                this.setNom((String)array[1+starting_index]);
            
                retour = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                retour = false;
            }
        }

        return retour;
    }

}
