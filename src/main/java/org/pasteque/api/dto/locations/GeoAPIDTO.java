package org.pasteque.api.dto.locations;

import java.util.List;

public class GeoAPIDTO {

    String nom;
    String code;
    List<String> codesPostaux;
    GeoAPICoordonneesDTO centre;
    Double surface;
    Integer population;
    SimpleDTO departement;
    SimpleDTO region;
    
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public List<String> getCodesPostaux() {
        return codesPostaux;
    }
    public void setCodesPostaux(List<String> codesPostaux) {
        this.codesPostaux = codesPostaux;
    }
    public GeoAPICoordonneesDTO getCentre() {
        return centre;
    }
    public void setCentre(GeoAPICoordonneesDTO centre) {
        this.centre = centre;
    }
    public Double getSurface() {
        return surface;
    }
    public void setSurface(Double surface) {
        this.surface = surface;
    }
    public Integer getPopulation() {
        return population;
    }
    public void setPopulation(Integer population) {
        this.population = population;
    }
    public SimpleDTO getDepartement() {
        return departement;
    }
    public void setDepartement(SimpleDTO departement) {
        this.departement = departement;
    }
    public SimpleDTO getRegion() {
        return region;
    }
    public void setRegion(SimpleDTO region) {
        this.region = region;
    }   
    
    
}

/**
 *      
Exemple de retour de l'appel à GeoAPI
https://geo.api.gouv.fr/communes?codePostal=42000&fields=nom,code,codesPostaux,centre,surface,departement,region,population&format=json&geometry=centre
Response body
Download
[
  {
    "nom": "Saint-Étienne",
    "code": "42218",
    "codesPostaux": [
      "42000",
      "42100",
      "42230"
    ],
    "centre": {
      "type": "Point",
      "coordinates": [
        4.3807,
        45.4283
      ]
    },
    "surface": 8002.81,
    "population": 171924,
    "departement": {
      "code": "42",
      "nom": "Loire"
    },
    "region": {
      "code": "84",
      "nom": "Auvergne-Rhône-Alpes"
    }
  }
]
*/
