package org.pasteque.api.dto.locations;

public class GeoIPDTO {

    private Double lat=0D;
    private Double lon=0D;
    
    public Double getLat() {
        return lat;
    }
    public void setLat(Double lat) {
        this.lat = lat;
    }
    public Double getLon() {
        return lon;
    }
    public void setLon(Double lon) {
        this.lon = lon;
    }
    
    
}
