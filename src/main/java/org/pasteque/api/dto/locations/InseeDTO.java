package org.pasteque.api.dto.locations;


public class InseeDTO implements Cloneable {

    private String insee;
    private String zipCode;
    private String commune;
    private Double latitude;
    private Double longitude;
    private GeoIPDTO location;
    private String codeDepartement;
    private String nomDepartement;
    private String codeRegion;
    private String nomRegion;
    private String etat;
    private Double surface;
    private Integer population;

    public InseeDTO(GeoAPIDTO geoAPIdto) {
        
        //System.out.println(geoAPIdto);
        this.setPopulation(geoAPIdto.getPopulation());
        this.setSurface(geoAPIdto.getSurface());
        if(geoAPIdto.getRegion() != null ) {
        this.setNomRegion(geoAPIdto.getRegion().getNom());
        this.setCodeRegion(geoAPIdto.getRegion().getCode());
        }
        if(geoAPIdto.getDepartement() != null) {
        this.setNomDepartement(geoAPIdto.getDepartement().getNom());
        this.setCodeDepartement(geoAPIdto.getDepartement().getCode());
        }
        this.setCommune(geoAPIdto.getNom());
        this.setInsee(geoAPIdto.getCode());
        if(geoAPIdto.getCentre() != null && geoAPIdto.getCentre().getCoordinates().size()>1) {
            this.setLongitude(geoAPIdto.getCentre().getCoordinates().get(0));
            this.setLatitude(geoAPIdto.getCentre().getCoordinates().get(1));
        }
    }
    
    public InseeDTO() {
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }
    
    public String getCodeDepartement() {
        return codeDepartement;
    }
    public void setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
    }
    public String getNomDepartement() {
        return nomDepartement;
    }
    public void setNomDepartement(String nomDepartement) {
        this.nomDepartement = nomDepartement;
    }
    public String getCodeRegion() {
        return codeRegion;
    }
    public void setCodeRegion(String codeRegion) {
        this.codeRegion = codeRegion;
    }
    public String getNomRegion() {
        return nomRegion;
    }
    public void setNomRegion(String nomRegion) {
        this.nomRegion = nomRegion;
    }
    public Double getSurface() {
        return surface;
    }
    public void setSurface(Double surface) {
        this.surface = surface;
    }
    public Integer getPopulation() {
        return population;
    }
    public void setPopulation(Integer population) {
        this.population = population;
    }
    public GeoIPDTO getLocation() {
        return location;
    }
    public void setLocation(GeoIPDTO location) {
        this.location = location;
    }
    public String getInsee() {
        return insee;
    }
    public void setInsee(String insee) {
        this.insee = insee;
    }
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipcode) {
        this.zipCode = zipcode;
    }
    public String getCommune() {
        return commune;
    }
    public void setCommune(String commune) {
        this.commune = commune;
    }
    public Double getLatitude() {
        return latitude;
    }
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
        if(this.location == null) {
            this.location = new GeoIPDTO();
        }
        this.location.setLat(latitude);
    }
    public Double getLongitude() {
        return longitude;
    }
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
        if(this.location == null) {
            this.location = new GeoIPDTO();
        }
        this.location.setLon(longitude);
    }

    public InseeDTO clone() throws
    CloneNotSupportedException 
    { 
        return (InseeDTO) super.clone(); 
    }

}
