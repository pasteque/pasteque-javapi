package org.pasteque.api.dto.locations;

public class POSLocationsUrlDTO {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
