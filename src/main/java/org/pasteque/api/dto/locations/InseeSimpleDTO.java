package org.pasteque.api.dto.locations;


public class InseeSimpleDTO {

    private String insee;
    private String zipCode;
    private String commune;
    private Double latitude;
    private Double longitude;
    private GeoIPDTO location;
    
    public GeoIPDTO getLocation() {
        return location;
    }
    public void setLocation(GeoIPDTO location) {
        this.location = location;
    }
    public String getInsee() {
        return insee;
    }
    public void setInsee(String insee) {
        this.insee = insee;
    }
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipcode) {
        this.zipCode = zipcode;
    }
    public String getCommune() {
        return commune;
    }
    public void setCommune(String commune) {
        this.commune = commune;
    }
    public Double getLatitude() {
        return latitude;
    }
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
        if(this.location == null) {
            this.location = new GeoIPDTO();
        }
        this.location.setLat(latitude);
    }
    public Double getLongitude() {
        return longitude;
    }
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
        if(this.location == null) {
            this.location = new GeoIPDTO();
        }
        this.location.setLon(longitude);
    }
    
}
