package org.pasteque.api.dto.locations;

import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;

public class LocationsInfoDTO implements ILoadable{

    private String id;
    private String label;
    private String category;
    private String parentId;
    private String etat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object other)
    {
        if(this==other) return true;
        if(other==null || !(other instanceof LocationsInfoDTO)) return false;
        return this.id.equals(LocationsInfoDTO.class.cast(other).getId());
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array, int starting_index) {
        boolean retour = false;
        
        if(array[0+starting_index] != null && (array.length - starting_index) >= 4) {

            try {
                this.setLabel((String)array[0+starting_index]);
                this.setId((String)array[1+starting_index]);
                this.setCategory((String)array[2+starting_index]);
                this.setParentId((String)array[3+starting_index]);
                retour = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                retour = false;
            }
        }

        return retour;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

}
