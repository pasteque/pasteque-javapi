package org.pasteque.api.dto.locations;

import java.util.List;

/**
 * On va recevoit ce type d'objet
 * {
      "type": "Point",
      "coordinates": [
        4.3807,
        45.4283
      ]
    }
 * @author educray
 *
 */
public class GeoAPICoordonneesDTO {
    String type;
    List<Double> coordinates;
    
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public List<Double> getCoordinates() {
        return coordinates;
    }
    public void setCoordinates(List<Double> coordinates) {
        this.coordinates = coordinates;
    }
    
    
}
