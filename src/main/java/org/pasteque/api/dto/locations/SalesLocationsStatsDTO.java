package org.pasteque.api.dto.locations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SalesLocationsStatsDTO {

    private Date date;
    private double sellAmount;
    private double sellAmountTTC;
    private long sellNb;
    private double averageBasket;
    private double ratio;
    private List<String> sessions = new  ArrayList<String>();
    private Long salesLocationId ;

    public double getSellAmount() {
        return sellAmount;
    }

    public void setSellAmount(double sellAmount) {
        this.sellAmount = sellAmount;
    }

    public long getSellNb() {
        return sellNb;
    }

    public void setSellNb(long sellNb) {
        this.sellNb = sellNb;
    }

    public double getAverageBasket() {
        return averageBasket;
    }

    public void setAverageBasket(double averageBasket) {
        this.averageBasket = averageBasket;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getSellAmountTTC() {
        return sellAmountTTC;
    }

    public void setSellAmountTTC(double sellAmountTTC) {
        this.sellAmountTTC = sellAmountTTC;
    }

    public List<String> getSessions() {
        return sessions;
    }

    public void setSessions(List<String> sessions) {
        this.sessions = sessions;
    }

    public Long getSalesLocationId() {
        return salesLocationId;
    }

    public void setSalesLocationId(Long salesLocationId) {
        this.salesLocationId = salesLocationId;
    }

}
