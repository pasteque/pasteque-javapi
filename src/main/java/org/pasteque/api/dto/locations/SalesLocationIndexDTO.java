package org.pasteque.api.dto.locations;

import java.util.Date;

import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;

public class SalesLocationIndexDTO implements ILoadable{

    private Long id;
    private InseeSimpleDTO commune;
    private Date dateHeureOuverture;
    private Date dateHeureFermeture;
    private int nombrePub;
    private boolean emplacementPaye;
    private double droitPlace;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InseeSimpleDTO getCommune() {
        return commune;
    }

    public void setCommune(InseeSimpleDTO commune) {
        this.commune = commune;
    }

    public Date getDateHeureOuverture() {
        return dateHeureOuverture;
    }

    public void setDateHeureOuverture(Date dateHeureOuverture) {
        this.dateHeureOuverture = dateHeureOuverture;
    }

    public Date getDateHeureFermeture() {
        return dateHeureFermeture;
    }

    public void setDateHeureFermeture(Date dateHeureFermeture) {
        this.dateHeureFermeture = dateHeureFermeture;
    }

    public int getNombrePub() {
        return nombrePub;
    }

    public void setNombrePub(int nombrePub) {
        this.nombrePub = nombrePub;
    }

    public boolean isEmplacementPaye() {
        return emplacementPaye;
    }

    public void setEmplacementPaye(boolean emplacementPaye) {
        this.emplacementPaye = emplacementPaye;
    }

    public double getDroitPlace() {
        return droitPlace;
    }

    public void setDroitPlace(double droitPlace) {
        this.droitPlace = droitPlace;
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array , int starting_index) {

        GeoIPDTO geoIP;
        boolean retour = false;
        //Deballage
        if(array[0+starting_index] != null && (array.length - starting_index ) >= 11) {
            try {
                this.setId(((Long)array[0+starting_index]).longValue());
                //TODO -Insee
                if(array[7+starting_index] != null) {
                    commune = new InseeSimpleDTO();
                    geoIP = new GeoIPDTO();
                    commune.setCommune((String)array[8+starting_index]);
                    commune.setInsee((String)array[1+starting_index]);
                    commune.setLatitude((Double)array[9+starting_index]);
                    commune.setLongitude((Double)array[10+starting_index]);
                    commune.setZipCode((String)array[7+starting_index]);
                    geoIP.setLat(commune.getLatitude());
                    geoIP.setLon(commune.getLongitude());
                    commune.setLocation(geoIP);

                    this.setCommune(commune);

                }
                this.setDateHeureOuverture((Date) array[2+starting_index]);
                this.setDateHeureFermeture((Date) array[3+starting_index]);
                this.setNombrePub((int) array[4+starting_index]);
                this.setDroitPlace((double) array[5+starting_index]);
                this.setEmplacementPaye((boolean)array[6+starting_index]);
                retour = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                retour = false;
            }

        }

        return retour;
    }

}
