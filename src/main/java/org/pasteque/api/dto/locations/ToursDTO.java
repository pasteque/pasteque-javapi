package org.pasteque.api.dto.locations;

import java.util.List;


public class ToursDTO {

    private long id;
    private String name;
    List<POSSalesLocationsDTO> salesLocations;
    //List<StocksLocationsDTO> stocksLocations;
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<POSSalesLocationsDTO> getSalesLocations() {
        return salesLocations;
    }
    public void setSalesLocations(List<POSSalesLocationsDTO> salesLocations) {
        this.salesLocations = salesLocations;
    }
}
