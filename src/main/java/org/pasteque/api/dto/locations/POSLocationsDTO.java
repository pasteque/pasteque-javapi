package org.pasteque.api.dto.locations;

import java.util.List;

import org.pasteque.api.dto.products.POSCategoriesDTO;

public class POSLocationsDTO {

    private String id;
    private String label;
    private String address;
    /** EDU ne semble pas utile dans la caisse on n'utilise que le Label */
    //private List<MessagesDTO> messages;
    private List<POSToursLocationsDTO> tours;
    private String inseeNum;
    private String parentId;
    private boolean serverDefault;
    private POSCategoriesDTO category;
    private String etat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

//    public List<MessagesDTO> getMessages() {
//        return messages;
//    }
//
//    public void setMessages(List<MessagesDTO> messages) {
//        this.messages = messages;
//    }
//
    public List<POSToursLocationsDTO> getTours() {
        return tours;
    }

    public void setTours(List<POSToursLocationsDTO> tours) {
        this.tours = tours;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInseeNum() {
        return inseeNum;
    }

    public void setInseeNum(String inseeNum) {
        this.inseeNum = inseeNum;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public boolean isServerDefault() {
        return serverDefault;
    }

    public void setServerDefault(boolean serverDefault) {
        this.serverDefault = serverDefault;
    }

    public POSCategoriesDTO getCategory() {
        return category;
    }

    public void setCategory(POSCategoriesDTO category) {
        this.category = category;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }
}
