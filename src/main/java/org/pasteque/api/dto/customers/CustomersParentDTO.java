package org.pasteque.api.dto.customers;

import javax.persistence.Id;

public class CustomersParentDTO {
    
    @Id
    private String id;
    private String searchKey;
    private String idParent;
    private String searchKeyParent;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getSearchKey() {
        return searchKey;
    }
    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }
    public String getIdParent() {
        return idParent;
    }
    public void setIdParent(String idParent) {
        this.idParent = idParent;
    }
    public String getSearchKeyParent() {
        return searchKeyParent;
    }
    public void setSearchKeyParent(String searchKeyParent) {
        this.searchKeyParent = searchKeyParent;
    }

}
