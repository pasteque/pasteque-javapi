package org.pasteque.api.dto.customers;

import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;

public class CustomersSearchTemporaryDTO implements ILoadable {

    private String value;
    private String level;
    private String error;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
    
    @Override
    public DtoSQLParameters getSQLParameters() {
        
        DtoSQLParameters retour = new DtoSQLParameters();
        
        return retour;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array, int starting_index) {
        // TODO Auto-generated method stub
        return false;
    }    
}
