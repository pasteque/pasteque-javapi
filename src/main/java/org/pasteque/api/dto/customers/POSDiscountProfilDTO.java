package org.pasteque.api.dto.customers;



public class POSDiscountProfilDTO {

    private String id;
    private String label;
    private String rate;
    private String disporder;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String nom) {
		this.label = nom;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getDisporder() {
		return disporder;
	}
	public void setDisporder(String disporder) {
		this.disporder = disporder;
	}
	
}
    
    
    
   