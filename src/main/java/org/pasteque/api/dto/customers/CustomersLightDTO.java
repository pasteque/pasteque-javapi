package org.pasteque.api.dto.customers;

public class CustomersLightDTO {

    private String id;
    private String key;
    private String firstName;
    private String lastName;

    private String phone1;
    private String zipCode;
    private String city;
    
    private String locationLabel;
    private String discountProfilLabel;    
    
    public CustomersLightDTO() {        
    }
    
    public CustomersLightDTO(String id , String searchkey , String firstName , String lastName , String phone , String zipCode , String city , String locationLabel , String discountProfilLabel) {
        setId(id);
        setKey(searchkey);
        setFirstName(firstName);
        setLastName(lastName);
        setPhone1(phone);
        setZipCode(zipCode);
        setCity(city);
        setLocationLabel(locationLabel);
        setDiscountProfilLabel(discountProfilLabel);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }


    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLocationLabel() {
        return locationLabel;
    }

    public void setLocationLabel(String locationLabel) {
        this.locationLabel = locationLabel;
    }

    public String getDiscountProfilLabel() {
        return discountProfilLabel;
    }

    public void setDiscountProfilLabel(String discountProfilLabel) {
        this.discountProfilLabel = discountProfilLabel;
    }
}
