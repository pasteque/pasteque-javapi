package org.pasteque.api.dto.customers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.model.customers.Customers;
import org.pasteque.api.response.IXlsResponse;

public class CustomersDetailDTO implements IXlsResponse {

    private String id;
    private String searchKey;
    private String firstName;
    private String lastName;
    private String cardNumber;
    private String note;
    private String address1;
    private String address2;
    private String zipCode;
    private String insee;
    private String city;
    private String country;
    private String phone1;
    private String phone2;
    private String email;
    private Date dateOfBirth;
    private Date creationDate;
    private Date lastUpdate;
    private String type;
    private String source;
    private boolean newsletter;
    private boolean partenaires;
    private String civilite;
    private String idParent;
    private String doublon;
    private LocationsInfoDTO location;

    private String parentsearchkey;

//    private List<TicketsHistoryDTO> tickets;

    private List<CustomersDetailDTO> customers;

    private CustomersParentDTO customerParent;

    private POSDiscountProfilDTO discountProfile;

    public CustomersDetailDTO() {

    }

    public CustomersDetailDTO(  String id, String searchKey, String firstName, String lastName,
            String address1, String address2, String zipCode, String insee, String city, String country,
            String phone1, String phone2, String email, Date dateOfBirth, Date creationDate,
            Date lastUpdate, boolean newsletter, boolean partenaires, String civilite, String doublon,
            String parentsearchkey, String locationId , String locationLabel , String discountProfileName ) {

        setId(id);
        setSearchKey(searchKey);
        setFirstName(firstName);
        setLastName(lastName);
        setAddress1(address1);
        setAddress2(address2);
        setZipCode(zipCode);
        setInsee(insee);
        setCity(city);
        setCountry(country);
        setPhone1(phone1);
        setPhone2(phone2);
        setEmail(email);
        setDateOfBirth(dateOfBirth);
        setCreationDate(creationDate);
        setLastUpdate(lastUpdate);
        setNewsletter(newsletter);
        setPartenaires(partenaires);
        setCivilite(civilite);
        setParentsearchkey(parentsearchkey);

        if(doublon!= null && doublon.length() > 0) {
            setDoublon(doublon);
        }else {
            setDoublon("");
        }
        if(locationId!= null && locationId.length() > 0) {
            LocationsInfoDTO location = new LocationsInfoDTO();
            location.setId(locationId);
            location.setLabel(locationLabel);
            setLocation(location);
        }

        if(discountProfileName!= null && discountProfileName.length() > 0) {
            POSDiscountProfilDTO discountProfile = new POSDiscountProfilDTO();
            discountProfile.setLabel(discountProfileName);
            setDiscountProfile(discountProfile);
        }


    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getInsee() {
        return insee;
    }

    public void setInsee(String insee) {
        this.insee = insee;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    public List<TicketsHistoryDTO> getTickets() {
//        return tickets;
//    }
//
//    public void setTickets(List<TicketsHistoryDTO> tickets) {
//        this.tickets = tickets;
//    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isNewsletter() {
        return newsletter;
    }

    public void setNewsletter(boolean newsletter) {
        this.newsletter = newsletter;
    }

    public boolean isPartenaires() {
        return partenaires;
    }

    public void setPartenaires(boolean partenaires) {
        this.partenaires = partenaires;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate ;
    }

    public Date getCreationDate() {
        return this.creationDate ;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getIdParent() {
        return idParent;
    }

    public void setIdParent(String idParent) {
        this.idParent = idParent;
    }

    public List<CustomersDetailDTO> getCustomers() {
        return customers;
    }

    public void setCustomers(List<CustomersDetailDTO> customers) {
        this.customers = customers;
    }

    public CustomersParentDTO getCustomerParent() {
        return customerParent;
    }

    public void setCustomerParent(CustomersParentDTO customerParent) {
        this.customerParent = customerParent;
    }

    public String getDoublon() {
        return doublon;
    }

    public void setDoublon(String doublon) {
        this.doublon = doublon;
    }

    public LocationsInfoDTO getLocation() {
        return location;
    }

    public void setLocation(LocationsInfoDTO location) {
        this.location = location;
    }

    public POSDiscountProfilDTO getDiscountProfile() {
        return discountProfile;
    }

    public void setDiscountProfile(POSDiscountProfilDTO discountProfile) {
        this.discountProfile = discountProfile;
    }

    public String getParentsearchkey() {
        return parentsearchkey;
    }

    public void setParentsearchkey(String parentsearchkey) {
        this.parentsearchkey = parentsearchkey;
    }

    public String getLongCivilite() {
        String shortCiv = getCivilite() == null ? "" : getCivilite() ;
        switch(shortCiv) {
        case "M" :
            return "Monsieur";
        case "MME" :
            return "Madame";
        default : 
            return "STE";
        }
    }

    @Override
    public String[] header() {
        return new String[] { 
                Customers.CustColumns.IDCLIENT  , Customers.CustColumns.CODE, Customers.CustColumns.CIV , Customers.CustColumns.NOM , Customers.CustColumns.PRENOM , 
                Customers.CustColumns.BIRTH , Customers.CustColumns.PROFIL, Customers.CustColumns.ADDRESS , Customers.CustColumns.ADDRESS2 , Customers.CustColumns.ZIPCODE , 
                Customers.CustColumns.INSEE , Customers.CustColumns.CITY , Customers.CustColumns.COUNTRY , Customers.CustColumns.MOBILE , Customers.CustColumns.PHONE , 
                Customers.CustColumns.MAIL ,Customers.CustColumns.CODEMAG , Customers.CustColumns.CODEMAGRAT ,Customers.CustColumns.MAGRAT,Customers.CustColumns.SOURCE , 
                Customers.CustColumns.PART ,Customers.CustColumns.NEWS , Customers.CustColumns.CREATION, Customers.CustColumns.MODIFICATION , Customers.CustColumns.DOUBLON,
                Customers.CustColumns.CODEPARENT};
    }

    @Override
    public String[] CSV() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dateCreationFormatted = getCreationDate() != null ? df.format(getCreationDate()) : "";
        String dateModicationFormatted = getLastUpdate() != null ? df.format(getLastUpdate()) : "";
        String birthFormatted = getDateOfBirth() != null ? df.format(getDateOfBirth()) : "";
        String origin = ( getSearchKey() == null || getSearchKey().length() < 4 )? "" : getSearchKey().substring(0, 4);
        String source =  ( getSearchKey() == null || getSearchKey().length() < 1 ) ? "" : "1".equals(getSearchKey().substring(0,1)) ? "ShopixMagasin" : "ShopixCamion";
        return new String[] {
                getId() , getSearchKey(), getLongCivilite() , getLastName() , getFirstName() == null ? "" : getFirstName() ,
                        birthFormatted ,getDiscountProfile()== null ? "": getDiscountProfile().getLabel(), getAddress1() == null ?  "" : getAddress1() , getAddress2() == null ?  "" : getAddress2() , getZipCode() == null ? "" : getZipCode() ,
                                getInsee() == null ? "" : getInsee().substring(0, 5),getCity() == null ?  "" : getCity() , getCountry() == null ?  "" : getCountry() , getPhone1() == null ?  "" : getPhone1() , getPhone2() == null ?  "" : getPhone2() ,  
                                        getEmail() , origin ,getLocation() == null ? "" : getLocation().getId(), getLocation() == null ? "" : getLocation().getLabel(),  source , 
                                                isPartenaires() ? "1" :"0" , isNewsletter() ? "1" :"0", dateCreationFormatted , dateModicationFormatted ,  getDoublon(),getParentsearchkey()};
    //EDU 2021-03 Dans le DTO et dans le model nous avons une chaine contenant l'id de l'insee
    //Hors depuis le découpage selon les code postaux, lid ne correspond pas exactement au code insee
    //Pour l'export excel on souhait avoir le code insee et les 5 premier caractères correspondent    
    }

    public static CellType[] types = {
            CellType.STRING, CellType.STRING , CellType.STRING , CellType.STRING ,  CellType.STRING , 
            CellType.NUMERIC , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , 
            CellType.STRING , CellType.STRING ,CellType.STRING , CellType.STRING , CellType.STRING ,
            CellType.STRING ,CellType.STRING ,CellType.STRING ,CellType.STRING , CellType.STRING , 
            CellType.STRING ,CellType.STRING,CellType.NUMERIC,CellType.NUMERIC,CellType.STRING ,
            CellType.STRING};

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getColWidth(int index) {
        return 0 ;
    }

}
