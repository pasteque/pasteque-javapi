package org.pasteque.api.dto.customers;

import java.util.Date;

import org.pasteque.api.dto.locations.LocationsInfoDTO;

public class POSCustomersDTO {

    private String id;
    private String number;
    private String key;
    private String dispName;
    private String card;
    private String custTaxId;
    private double prepaid;
    private double maxDebt;
    private Double currDebt;
    private Date debtDate;
    private String firstName;
    private String lastName;
    private String email;
    private String phone1;
    private String phone2;
    private String fax;
    private String addr1;
    private String addr2;
    private String zipCode;
    private String insee;
    private String city;
    private String region;
    private String country;
    private String note;
    private boolean visible;
    private Date dateOfBirth;
    private String type;
    private String source;
    private boolean newsletter;
    private boolean partenaires;
    private String civilite;
    private Date dateCreation;
    private Date dateUpdate;
    private String idParent;
    private Long discountProfileId;
    private LocationsInfoDTO location;
    private POSDiscountProfilDTO discountProfile;
    private String parentsearchkey;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDispName() {
        return dispName;
    }

    public void setDispName(String dispName) {
        this.dispName = dispName;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getCustTaxId() {
        return custTaxId;
    }

    public void setCustTaxId(String custTaxId) {
        this.custTaxId = custTaxId;
    }

    public double getPrepaid() {
        return prepaid;
    }

    public void setPrepaid(double prepaid) {
        this.prepaid = prepaid;
    }

    public double getMaxDebt() {
        return maxDebt;
    }

    public void setMaxDebt(double maxDebt) {
        this.maxDebt = maxDebt;
    }

    public Double getCurrDebt() {
        return currDebt;
    }

    public void setCurrDebt(Double currDebt) {
        this.currDebt = currDebt;
    }

    public Date getDebtDate() {
        return debtDate;
    }

    public void setDebtDate(Date debtDate) {
        this.debtDate = debtDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getInsee() {
		return insee;
	}

	public void setInsee(String insee) {
		this.insee = insee;
	}

	public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean getVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isNewsletter() {
        return newsletter;
    }

    public void setNewsletter(boolean newsletter) {
        this.newsletter = newsletter;
    }

    public boolean isPartenaires() {
        return partenaires;
    }

    public void setPartenaires(boolean partenaires) {
        this.partenaires = partenaires;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getIdParent() {
        return idParent;
    }

    public void setIdParent(String idParent) {
        this.idParent = idParent;
    }

	public LocationsInfoDTO getLocation() {
		return location;
	}

	public void setLocation(LocationsInfoDTO location) {
		this.location = location;
	}

	public POSDiscountProfilDTO getDiscountProfile() {
		return discountProfile;
	}

	public void setDiscountProfile(POSDiscountProfilDTO discountProfile) {
		this.discountProfile = discountProfile;
	}

	public Long getDiscountProfileId() {
		return discountProfileId;
	}

	public void setDiscountProfileId(Long discountProfileId) {
		this.discountProfileId = discountProfileId;
	}

	public String getParentsearchkey() {
		return parentsearchkey;
	}

	public void setParentsearchkey(String parentsearchkey) {
		this.parentsearchkey = parentsearchkey;
	}

}
