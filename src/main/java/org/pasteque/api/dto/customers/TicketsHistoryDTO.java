package org.pasteque.api.dto.customers;

import java.util.Date;

public class TicketsHistoryDTO {

    private Date date;
    private long ticketId;
    private int productsUnit;
    private double ticketsAmount;
    private int ticketNumber;
    private String locationsName;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getProductsUnit() {
        return productsUnit;
    }

    public void setProductsUnit(int productsUnit) {
        this.productsUnit = productsUnit;
    }

    public double getTicketsAmount() {
        return ticketsAmount;
    }

    public void setTicketsAmount(double ticketsAmount) {
        this.ticketsAmount = ticketsAmount;
    }

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public int getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getLocationsName() {
        return locationsName;
    }

    public void setLocationsName(String locationsName) {
        this.locationsName = locationsName;
    }

}
