package org.pasteque.api.dto.customers;

public class CustomersTopDTO {
    
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
