package org.pasteque.api.dto.customers;

import java.util.List;

public class CustomersCirteriaSearchDTO {
    
    private String name;
    private String column;
    private List<String> searchvalue;
    
    CustomersCirteriaSearchDTO(){}
    
    public CustomersCirteriaSearchDTO(String Name, String Column, List<String> SearchValue)
    {
        this.name = Name;
        this.column = Column;
        this.searchvalue = SearchValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public List<String> getSearchvalue() {
        return searchvalue;
    }

    public void setSearchvalue(List<String> searchvalue) {
        this.searchvalue = searchvalue;
    }

}
