package org.pasteque.api.dto.customers;

import java.util.Date;

import org.pasteque.api.dto.locations.InseeSimpleDTO;

public class CustomersIndexDTO {

    private String id;
    private String numeroCarte;
    private String nom;
    private String prenom;
    private InseeSimpleDTO commune;
    private String telephone;
    private String email;
    private Date dateNaissance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumeroCarte() {
        return numeroCarte;
    }

    public void setNumeroCarte(String numeroCarte) {
        this.numeroCarte = numeroCarte;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public InseeSimpleDTO getCommune() {
        return commune;
    }

    public void setCommune(InseeSimpleDTO commune) {
        this.commune = commune;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

}
