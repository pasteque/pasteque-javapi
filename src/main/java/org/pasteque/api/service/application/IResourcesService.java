package org.pasteque.api.service.application;

import java.util.List;

import org.pasteque.api.dto.application.POSResourcesDTO;


public interface IResourcesService {

    POSResourcesDTO getByLabel(String label);

    List<POSResourcesDTO> getAll();

    Object save(POSResourcesDTO dto);

    Object delete(POSResourcesDTO dto);

}
