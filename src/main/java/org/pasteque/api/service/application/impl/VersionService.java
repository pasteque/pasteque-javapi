package org.pasteque.api.service.application.impl;

import org.pasteque.api.service.application.IVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.pasteque.api.dao.application.IApplicationApiDAO;
import org.pasteque.api.dto.application.POSVersionDTO;

@Service
public class VersionService implements IVersionService {

	@Autowired
	private IApplicationApiDAO applicationApiDAO;

	@Override
	public POSVersionDTO getCurrent(String type) {
		return applicationApiDAO.findCurrent(type).getAdapter(POSVersionDTO.class);
	}

}
