package org.pasteque.api.service.application.impl;

import java.util.List;
import java.util.Optional;

import org.pasteque.api.dao.application.IResourcesApiDAO;
import org.pasteque.api.dto.application.POSResourcesDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.application.Resources;
import org.pasteque.api.service.application.IResourcesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ResourcesService implements IResourcesService {

    @Autowired
    private IResourcesApiDAO resourcesApiDAO;

    @Override
    public POSResourcesDTO getByLabel(String label) {
        return resourcesApiDAO.findByName(label).getAdapter(POSResourcesDTO.class);
    }
    
    @Override
    public List<POSResourcesDTO> getAll() {
        return AdaptableHelper.getAdapter(resourcesApiDAO.findAll(), POSResourcesDTO.class);
    }

    @Override
    public Object save(POSResourcesDTO dto) {
        Optional<Resources> resource = ( dto.getId() == null ? Optional.ofNullable(null) : resourcesApiDAO.findById(dto.getId()));
        Resources realObject = null;
        if (resource.isPresent()) {
            realObject = resource.get();
            if(realObject.loadFromDTO(dto)){
                resourcesApiDAO.save(realObject);
            }
        } else {
            realObject = new Resources();
            realObject.loadFromDTO(dto);

            resourcesApiDAO.save(realObject);
        }
        return true;
    }

    @Override
    public Object delete(POSResourcesDTO dto) {
        Optional<Resources> resource = ( dto.getId() == null ? Optional.ofNullable(null) : resourcesApiDAO.findById(dto.getId()));

        if (resource.isPresent()) {
            resourcesApiDAO.delete(resource.get());
        }
        return true;
    }

}
