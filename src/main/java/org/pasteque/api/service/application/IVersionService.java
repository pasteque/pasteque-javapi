package org.pasteque.api.service.application;

import org.pasteque.api.dto.application.POSVersionDTO;

public interface IVersionService {
	
	POSVersionDTO getCurrent(String type);

}
