package org.pasteque.api.service.customers.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.pasteque.api.dao.customers.ICustomersApiDAO;
import org.pasteque.api.dao.customers.IDiscountProfilApiDAO;
import org.pasteque.api.dao.locations.IInseeApiDAO;
import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dto.customers.POSCustomersDTO;
import org.pasteque.api.dto.customers.CustomersDetailDTO;
import org.pasteque.api.dto.customers.CustomersLightDTO;
import org.pasteque.api.dto.customers.CustomersParentDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.customers.Customers;
import org.pasteque.api.model.customers.DiscountProfil;
import org.pasteque.api.model.saleslocations.Insee;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.service.customers.ICustomersService;
import org.pasteque.api.util.DamerauLevenshteinDistance;
import org.pasteque.api.util.FilteringAndFormattingData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomersService implements ICustomersService {

    @Autowired
    private ICustomersApiDAO customersApiDAO;
    @Autowired
    private ILocationsApiDAO locationsApiDAO;
    @Autowired
    private IDiscountProfilApiDAO discountProfilApiDAO;
    @Autowired
    private IInseeApiDAO inseeApiDAO;

    @Override
    public List<POSCustomersDTO> getAll() {
        return AdaptableHelper.getAdapter(customersApiDAO.findAll(), POSCustomersDTO.class);
    }


    @Override
    public List<String> getTop() {
        //        List<Object[]> l = customersDAO.findTop(10);
        List<String> ids = new ArrayList<String>();
        //        for (Object[] objects : l) {
        //            ids.add((String) objects[0]);
        //        }
        return ids;
    }

    @Override
    public CustomersDetailDTO save(CustomersDetailDTO dto) {
        Customers customers = new Customers();
        Boolean changes ;
        if (dto.getId() != null) {
            customers = customersApiDAO.findById(dto.getId()).orElse(null);
        } else {
            customers.setId(createId(dto));
        }

        if (customers == null) {
            customers = new Customers();
        }

        changes = loadFromDTO(dto , customers);

        //EDU - vérifier ici les objets Location et DiscountProfil
        if(customers.getDiscountProfil() != null && customers.getDiscountProfil().getName() == null) {
            customers.setDiscountProfil(discountProfilApiDAO.findById(Long.valueOf(dto.getDiscountProfile().getId())).orElse(null));
        }
        if(customers.getLocation() != null && customers.getLocation().getName() == null) {
            customers.setLocation(locationsApiDAO.findById(dto.getLocation().getId()).orElse(null));
        }

        FilteringAndFormattingData.filter(customers);
        FilteringAndFormattingData.format(customers);

        if (dto.getId() != null && customers.getId() == null) {
            customers.setId(dto.getId());
            customers = customersApiDAO.save(customers);

        }else if (dto.getId() != null && customers.getId() != null) {
            if(changes) {
                customers.setDateUpdate(new Date());
                customers = customersApiDAO.save(customers);
            }
        } else {
            customers.setDateCreation(new Date());
            customers.setDateUpdate(new Date());
            customers = customersApiDAO.save(customers);
        }
        return customers.getAdapter(CustomersDetailDTO.class);
    }

    @Override
    public <T> T getById(String id, Class<T> selector) {
        //TODO completer avec searchkey parent si besoin
        Customers cust = customersApiDAO.findById(id).orElse(null);
        return cust == null ? null : cust.getAdapter(selector);
    }

    @Override
    public List<POSCustomersDTO> getSinceDate(Date date) {
        return AdaptableHelper.getAdapter(customersApiDAO.findByDateUpdateAfter(date), POSCustomersDTO.class);
    }

    @Override
    public <T> List<T> getByCriteria(String reference, String pdvId, String from, String to , String insee , Integer radius , Boolean mailConstraint , Boolean parentConstraint , String card , String address , String address2 , String zipCode , String city , String region , String country , String firstName , String lastName , String email , String phone , String phone2 , String notes , String customersExcept , Boolean parentOnly , Long maxResults ,String profilId, Boolean profilConstraint,Class<T> selector) {  
        return getByCriteria( reference,  pdvId,  from,  to ,  insee ,  radius ,  mailConstraint ,  parentConstraint ,  card ,  address ,  address2 ,  zipCode ,  city ,  region ,  country ,  firstName ,  lastName ,  email ,  phone ,  phone2 ,  notes ,  customersExcept ,  parentOnly , maxResults, profilId,profilConstraint,selector ,false) ;
    }

    @Override
    public <T> List<T> getByCriteria(String reference, String pdvId, String from, String to , String insee , Integer radius , Boolean mailConstraint , Boolean parentConstraint , String card , String address , String address2 , String zipCode , String city , String region , String country , String firstName , String lastName , String email , String phone , String phone2 , String notes , String customersExcept , Boolean parentOnly , Long maxResults ,String profilId, Boolean profilConstraint,Class<T> selector , boolean filtered) {  
        //EDU 2020 05 - Explosion en fonction de l'action menée pour éviter de passer par l'objet Customer dans certains cas
        // ça permet d'enrichir le contenu du DTO "avec un bête join"

        if( selector.equals(CustomersLightDTO.class) || selector.equals(CustomersDetailDTO.class)) {
            return customersApiDAO.findDTOByCriteria(reference, pdvId, from, to, insee , radius , mailConstraint, parentConstraint, card, address, address2, zipCode, city, region, country, firstName, lastName, email, phone, phone2, notes, customersExcept, parentOnly, profilId,profilConstraint,maxResults,selector);
        }
        else {
            return AdaptableHelper.getAdapter(customersApiDAO.findByCriteria(reference, pdvId, from, to, insee , radius , mailConstraint, parentConstraint, card, address, address2, zipCode, city, region, country, firstName, lastName, email, phone, phone2, notes, customersExcept, parentOnly, profilId,profilConstraint,maxResults), selector, filtered);   
        }        
    }

    @Override
    public Object deleteCustomerParent(CustomersParentDTO customerIdParent) {

        Customers customerEnfant = null;
        Customers customerParent = null;

        //PGA: Suppression de l'ID parent sur la fiche client enfant + mise
        //à jour date de modification fiche client enfant
        if (customerIdParent.getId() != null) {
            customerEnfant = customersApiDAO.findById(customerIdParent.getId()).orElse(null);
        }
        if(customerEnfant == null && customerIdParent.getSearchKey() != null) {
            customerEnfant = customersApiDAO.findFirstBySearchKey(customerIdParent.getSearchKey()).orElse(null);
        }
        if(customerEnfant != null) {
            customerEnfant.setIdParent(null);
            customerEnfant.setDateUpdate(new Date());
            customersApiDAO.save(customerEnfant);

            //PGA: Suppression de l'ID enfant dans l'objet client parent afin de couper la liaison entre
            //le parent et l'enfant lors de l'affichage des enfants sur la fiche client parent
            if (customerIdParent.getIdParent() != null) {
                customerParent = customersApiDAO.findById(customerIdParent.getIdParent()).orElse(null);
            }
            if(customerParent == null && customerIdParent.getSearchKeyParent() != null) {
                customerParent = customersApiDAO.findFirstBySearchKey(customerIdParent.getSearchKeyParent()).orElse(null);
            }

            if(customerParent != null) {
                if(customerParent.getCustomers().size() > 0) {
                    for(int i = 0; i < customerParent.getCustomers().size(); i++) {
                        if(customerParent.getCustomers().get(i).getId().equals(customerEnfant.getId())){
                            customerParent.getCustomers().remove(i);
                        }
                    }
                }
                customersApiDAO.save(customerParent);
            }
            return customerEnfant.getAdapter(CustomersDetailDTO.class);
        } else {
            return null;
        }
    }

    @Override
    public Object saveCustomerParent(Map<String,CustomersParentDTO> ListcustomerIdParent, Boolean updateParent) {

        HashMap<Customers, List<Customers>> listDedup = new LinkedHashMap<>();
        LinkedHashMap<String, CustomersParentDTO> todo = new LinkedHashMap<String, CustomersParentDTO>();
        Customers customerEnfant = null;

        //si parent est dans la liste non traitée et parent de parent == null -> on traite le parent
        //si parent est dans la liste non traitée et parent de parent == soi même -> on devient le parent donc parent = null
        //si parent est dans la liste non traitée et parent de parent != soi même -> parent devient parent de parent et on boucle

        //si parent dans liste traitée -> on update infos par rapport au parent
        //sinon on update infos par rapport à BdD

        try {
            for(CustomersParentDTO customerId : ListcustomerIdParent.values()) {
                if(!todo.containsKey(customerId.getSearchKey())) {
                    //On n'a pas encore traité cet enregistrement
                    //on va le faire
                    Boolean casEnCours = true;
                    List<String> famille = new ArrayList<String>();
                    while(casEnCours) {
                        if(customerId.getSearchKey().equals(customerId.getSearchKeyParent())) {
                            customerId.setSearchKeyParent(null);
                            customerId.setIdParent(null);
                        }
                        CustomersParentDTO customerIdParent = ListcustomerIdParent.get(customerId.getSearchKeyParent());
                        if(customerIdParent != null) {
                            // le parent annoncé est dans le fichier - Danger
                            if(!todo.containsKey(customerIdParent.getSearchKey())) {
                                //On ne l'a pas encore traité
                                String grandParent = customerIdParent.getSearchKeyParent();
                                if(grandParent == null) {
                                    //on ajoute le parent dans les todo
                                    todo.put(customerId.getSearchKeyParent(), customerIdParent);
                                    casEnCours = false;
                                } else if (grandParent.equals(customerId.getSearchKey())){
                                    //on a une boucle on dit que le client en cours est le parent
                                    customerId.setSearchKeyParent(null);
                                    customerId.setIdParent(null);
                                    casEnCours = false;
                                } else {
                                    famille.add(customerIdParent.getSearchKey());
                                    if(famille.contains(grandParent)) {
                                        //On a déjà croisé le grandParent dans les parents successifs 
                                        //On a une boucle imbriquée
                                        customerIdParent.setSearchKeyParent(null);
                                        customerIdParent.setIdParent(null);
                                        todo.put(customerId.getSearchKeyParent(), customerIdParent);
                                        casEnCours = false;
                                    } else {
                                        customerId.setSearchKeyParent(customerIdParent.getSearchKeyParent());
                                        customerId.setIdParent(customerIdParent.getIdParent());
                                    }
                                }
                            } else {
                                //On l'a traité, on vérifie qu'il ne faille pas changer le parent de client en cours
                                if(customerIdParent.getSearchKeyParent() != null || customerIdParent.getIdParent() != null) {
                                    customerId.setSearchKeyParent(customerIdParent.getSearchKeyParent());
                                    customerId.setIdParent(customerIdParent.getIdParent());
                                }
                                casEnCours = false;
                            }
                        } else {
                            casEnCours = false;
                        }
                    }
                    todo.put(customerId.getSearchKey(), customerId);
                }
            }
            for(CustomersParentDTO customerIdParent : todo.values()) {
                customerEnfant = new Customers();
                //PGA: Si customerIdParent.getId() n'est pas null, alors recherche depuis l'id le client enfant et récupération de celui-ci sous forme d'objet
                if (customerIdParent.getId() != null) {
                    customerEnfant = customersApiDAO.findById(customerIdParent.getId()).orElse(null);
                    //PGA: Sinon si customerIdParent.getSearchKey() n'est pas null, alors recherche depuis le code client le client enfant et récupération de celui-ci sous forme d'objet 
                }else if(customerIdParent.getSearchKey() != null){
                    customerEnfant = customersApiDAO.findFirstBySearchKey(customerIdParent.getSearchKey()).orElse(null);
                }

                //PGA: Test si client enfant est le client parent qu'on essaye de lier ne sont 
                //pas les mêmes car un client ne peut pas être le parent-enfant de lui-même
                if(customerEnfant != null && customerEnfant.getId() != null && customerIdParent.getSearchKeyParent() != null 
                        && !customerIdParent.getSearchKey().equals(customerIdParent.getSearchKeyParent())) {     
                    //PGA: Recherche depuis le code client parent le client correspondant 
                    //et récupération de celui-ci sous forme d'objet 
                    List<Customers> customersParent = new ArrayList<Customers>();
                    customersParent.add(customersApiDAO.findFirstBySearchKey(customerIdParent.getSearchKeyParent()).orElse(null));
                    //PGA: Si uniquement un client (parent) est trouvé lors de la recherche avec
                    //son code client, alors on teste d'abord si le parent a un Id parent de renseigner. 
                    if(customersParent.size() == 1 && customersParent.get(0) != null) {
                        //PGA : Si non, alors on enregistre la liaison simplement entre l'enfant et le parent.
                        if( customersParent.get(0).getIdParent() == null) {
                            //PGA : On teste si l'enfant est déjà un parent d'autres enfants et si ce n'est 
                            //pas le cas, alors on enregistre la liaison simplement entre l'enfant et le parent.
                            if(customerEnfant.getCustomers().size() <= 0) {
                                listDedup.put(customerEnfant, customersParent);
                                //PGA : Sinon si l'enfant est déjà un parent d'autres enfants, alors l'enfant et
                                //ses propres enfants deviennent ceux du parent.
                            }else {
                                for(Customers customerEnfantOfEnfant : customerEnfant.getCustomers()) {
                                    listDedup.put(customerEnfantOfEnfant, customersParent);
                                }
                                listDedup.put(customerEnfant, customersParent);
                            }
                            //PGA: Si oui, alors on enregistre la liaison non pas entre l'enfant et le parent direct, mais entre l'enfant et le parent du parent direct.
                        }else {
                            if(customerEnfant.getId().equals(customersParent.get(0).getIdParent())) {
                                deleteCustomerParent(customerIdParent);
                            } else {
                                List<Customers> customersParentOfParent = new ArrayList<Customers>();
                                customersParentOfParent.add(customersApiDAO.findById(customersParent.get(0).getIdParent()).orElse(null));
                                listDedup.put(customerEnfant, customersParentOfParent);
                            }
                        }
                    }
                } else {
                    // il faut faire sauter la liaison
                    deleteCustomerParent(customerIdParent);
                }
            }
            Customers customerEnfantUpdated = createLiaisonParentEnfant(listDedup, updateParent);
            CustomersDetailDTO retour = customerEnfantUpdated == null? ( customerEnfant == null ? null : customerEnfant.getAdapter(CustomersDetailDTO.class)) : customerEnfantUpdated.getAdapter(CustomersDetailDTO.class);
            if (retour != null && retour.getIdParent() != null) {
                retour.getCustomerParent().setSearchKeyParent(findSearchKey(retour.getIdParent()));
            }
            //2021 07 - problème de cache lors de l'import de fichier de déduplication
            customersApiDAO.invalidateCache();
            return retour;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String findSearchKey(String id) {
        return customersApiDAO.findById(id).map(Customers::getSearchKey).orElse(null);
    }

    public Customers createLiaisonParentEnfant(HashMap<Customers, List<Customers>> hashMapDedup, Boolean updateParent) {
        List<Customers> listCustomerEnfant = new ArrayList<Customers>();
        Customers customerEnfant;
        List<Customers> listCustomerParent = new ArrayList<Customers>();
        Customers customerParent;
        for(Entry<Customers, List<Customers>> listdedup : hashMapDedup.entrySet()) {
            //PGA : on ajoute l'ID parent sur la fiche client enfant + mise
            //à jour date de modification fiche client enfant.
            customerEnfant = listdedup.getKey();
            customerParent = listdedup.getValue().get(0);
            customerEnfant.setIdParent(customerParent.getId());
            customerEnfant.setDateUpdate(new Date());
            listCustomerEnfant.add(customerEnfant);
        }

        //PGA : on ajoute l'ID parent sur la fiche client enfant + mise
        //à jour date de modification fiche client enfant.
        customersApiDAO.saveAll(listCustomerEnfant);
        if (updateParent)
        {
            for(Customers customersEnfant : listCustomerEnfant) {
                customerParent = updateParentCustomer(customersEnfant, hashMapDedup.get(customersEnfant));
                if (customerParent!=null) {
                    listCustomerParent.add(customerParent);
                }
            }
        }
        customersApiDAO.saveAll(listCustomerParent);
        return listCustomerEnfant == null || listCustomerEnfant.isEmpty() ? null : listCustomerEnfant.get(0);

    }

    public Customers updateParentCustomer(Customers enfant , List<Customers> parentCustomerList) {
        Customers parentCustomer = parentCustomerList.isEmpty() ? null : parentCustomerList.get(0);
        if(parentCustomer != null) {
            if (parentCustomer.getName() == null || parentCustomer.getName().trim().isEmpty()) {
                parentCustomer.setName(enfant.getName());
            }
            if (parentCustomer.getCard() == null || parentCustomer.getCard().trim().isEmpty()) {
                parentCustomer.setCard(enfant.getCard());
            }
            if (parentCustomer.getCivilite() == null || parentCustomer.getCivilite().trim().isEmpty()) {
                parentCustomer.setCivilite(enfant.getCivilite());
            }
            if (parentCustomer.getDateOfBirth() == null) {
                parentCustomer.setDateOfBirth(enfant.getDateOfBirth());
            }
            if (parentCustomer.getDiscountProfil() == null) {
                parentCustomer.setDiscountProfil(enfant.getDiscountProfil());
            }
            if (parentCustomer.getEmail() == null || parentCustomer.getEmail().trim().isEmpty()) {
                parentCustomer.setEmail(enfant.getEmail());
            }
            if (parentCustomer.getFax() == null || parentCustomer.getFax().trim().isEmpty()) {
                parentCustomer.setFax(enfant.getFax());
            }
            if (parentCustomer.getFirstName() == null || parentCustomer.getFirstName().trim().isEmpty()) {
                parentCustomer.setFirstName(enfant.getFirstName());
            }
            if (parentCustomer.getLastName() == null || parentCustomer.getLastName().trim().isEmpty()) {
                parentCustomer.setLastName(enfant.getLastName());
            }
            if (parentCustomer.getLocation() == null) {
                parentCustomer.setLocation(enfant.getLocation());
            }
            if (parentCustomer.getMaxDebt() == 0) {
                parentCustomer.setMaxDebt(enfant.getMaxDebt());
            }
            if (parentCustomer.getNotes() == null || parentCustomer.getNotes().trim().isEmpty()) {
                parentCustomer.setNotes(enfant.getNotes());
            }
            if (parentCustomer.getPhone() == null || parentCustomer.getPhone().trim().isEmpty()) {
                parentCustomer.setPhone(enfant.getPhone());
            }
            if (parentCustomer.getPhone2() == null || parentCustomer.getPhone2().trim().isEmpty()) {
                parentCustomer.setPhone2(enfant.getPhone2());
            }
            if (parentCustomer.getPrePaid() == 0) {
                parentCustomer.setPrePaid(enfant.getPrePaid());
            }

            if (parentCustomer.getSource() == null || parentCustomer.getSource().trim().isEmpty()) {
                parentCustomer.setSource(enfant.getSource());
            }
            if (parentCustomer.getTaxCategory() == null) {
                parentCustomer.setTaxCategory(enfant.getTaxCategory());
            }
            if (parentCustomer.getType() == null || parentCustomer.getType().trim().isEmpty()) {
                parentCustomer.setType(enfant.getType());
            }
            if (parentCustomer.getTax() == null) {
                parentCustomer.setTax(enfant.getTax());
            }
            if (parentCustomer.getLocation() == null) {
                parentCustomer.setLocation(enfant.getLocation());
            }

            if ((parentCustomer.getAddress() == null || parentCustomer.getAddress().trim().isEmpty())
                    && (parentCustomer.getAddress2() == null || parentCustomer.getAddress2().trim().isEmpty())
                    && (parentCustomer.getCity() == null || parentCustomer.getCity().trim().isEmpty())
                    && (parentCustomer.getCountry() == null || parentCustomer.getCountry().trim().isEmpty())
                    && (parentCustomer.getInsee() == null || parentCustomer.getInsee().trim().isEmpty())
                    && (parentCustomer.getRegion() == null || parentCustomer.getRegion().trim().isEmpty())
                    && (parentCustomer.getZipCode() == null || parentCustomer.getZipCode().trim().isEmpty())) {

                parentCustomer.setAddress(enfant.getAddress());
                parentCustomer.setAddress2(enfant.getAddress2());
                parentCustomer.setCity(enfant.getCity());
                parentCustomer.setCountry(enfant.getCountry());
                parentCustomer.setRegion(enfant.getRegion());
                parentCustomer.setZipCode(enfant.getZipCode());
                parentCustomer.setInsee(enfant.getInsee());

            }
            List<Customers> enfants = parentCustomer.getCustomers();
            if(!enfants.contains(enfant)) {
                enfants.add(enfant);
            }
        }
        return parentCustomer;
    }

    /**
     * Modif EDU - 2021 04
     * moindre modif sur groupe de champs adresse -> on écrase tout
     * les membres du bloc adresse :
     * adresse1
     * adresse2
     * code postal
     * insee
     * ville 
     * pays
     */
    @Override
    public boolean loadFromDTO(Object source , Customers customers) {
        boolean retour = false;
        // EDU dans un premier temps on gère juste le CustomersDetailDTO qui nous intéresse
        if (CustomersDetailDTO.class.isInstance(source)) {
            CustomersDetailDTO dto = (CustomersDetailDTO) source;
            //Pour mémoire - si on est dans le premier cas : date modification local inférieurs à date modification fichier
            //On ne prend QUE les informations qui n'écrasent rien en local
            if (customers.getDateUpdate() != null && dto.getLastUpdate() != null && customers.getDateUpdate().after(dto.getLastUpdate())) {
                if ((dto.getAddress1() != null && customers.getAddress() == null) &&
                        (dto.getAddress2() != null && customers.getAddress2() == null) &&
                        (dto.getCity() != null && customers.getCity() == null) && 
                        (dto.getZipCode() != null && customers.getZipCode() == null) && 
                        (dto.getInsee() != null && customers.getInsee() == null) &&
                        (dto.getCountry() != null && customers.getCountry() == null)) {
                    retour = true;
                    customers.setAddress(dto.getAddress1());
                    customers.setAddress2(dto.getAddress2());
                    customers.setCity(dto.getCity());
                    customers.setZipCode(dto.getZipCode());
                    customers.setInsee(dto.getInsee());
                    customers.setCountry(dto.getCountry());
                }

                if (dto.getCardNumber() != null && customers.getCard() == null) {
                    retour = true;
                    customers.setCard(dto.getCardNumber());
                }

                if (dto.getEmail() != null && customers.getEmail() == null) {
                    retour = true;
                    customers.setEmail(dto.getEmail());
                }

                if (dto.getFirstName() != null && customers.getFirstName() == null) {
                    retour = true;
                    customers.setFirstName(dto.getFirstName());
                }

                if (dto.getLastName() != null && customers.getLastName() == null) {
                    retour = true;
                    customers.setLastName(dto.getLastName());
                }

                customers.setName(customers.getFirstName() + " " + customers.getLastName());

                if (dto.getNote() != null && customers.getNotes() == null) {
                    retour = true;
                    customers.setNotes(dto.getNote());
                }
                if (dto.getPhone1() != null && customers.getPhone() == null) {
                    retour = true;
                    customers.setPhone(dto.getPhone1());
                }
                if (dto.getPhone2() != null && customers.getPhone2() == null) {
                    retour = true;
                    customers.setPhone2(dto.getPhone2());
                }
                if (dto.getSearchKey() != null && customers.getSearchKey() == null) {
                    retour = true;
                    customers.setSearchKey(dto.getSearchKey());
                }
                if (dto.getCivilite() != null && customers.getCivilite() == null) {
                    retour = true;
                    customers.setCivilite(dto.getCivilite());
                }
                if (dto.getDateOfBirth() != null && customers.getDateOfBirth() == null) {
                    retour = true;
                    customers.setDateOfBirth(dto.getDateOfBirth());
                }
                if (dto.getCreationDate() != null && customers.getDateCreation() == null) {
                    retour = true;
                    customers.setDateCreation(dto.getCreationDate());
                }
                if (dto.isNewsletter() != customers.isNewsletter()) {
                    retour = true;
                    customers.setNewsletter(dto.isNewsletter());
                }
                if (dto.isPartenaires() != customers.isPartenaires()) {
                    retour = true;
                    customers.setPartenaires(dto.isPartenaires());
                }
                if ((dto.getSource() != null && customers.getSource() == null)) {
                    retour = true;
                    customers.setSource(dto.getSource());
                }
                if ((dto.getLastUpdate() != null && customers.getDateUpdate() == null)) {
                    customers.setDateUpdate(dto.getLastUpdate());
                }
                if ((dto.getLocation() != null && customers.getLocation() == null)) {
                    retour = true;
                    Locations location = locationsApiDAO == null ? null :locationsApiDAO.findById(dto.getLocation().getId()).orElse(null);
                    if(location == null ) {
                        location = new Locations();
                        location.setId(dto.getLocation().getId());
                    }
                    customers.setLocation(location);
                }
                if ((dto.getDiscountProfile() != null && customers.getDiscountProfil() == null)) {
                    retour = true;
                    DiscountProfil discountProfil = discountProfilApiDAO ==null ? null : discountProfilApiDAO.findById(Long.valueOf(dto.getDiscountProfile().getId())).orElse(null);
                    if(discountProfil == null ) {
                        discountProfil = new DiscountProfil();
                        discountProfil.setId(Long.getLong(dto.getDiscountProfile().getId()));
                    }
                    customers.setDiscountProfil(discountProfil);
                }

            } else {
                if ((dto.getAddress1() != null && !dto.getAddress1().equals(customers.getAddress()))
                        || (dto.getAddress1() == null && customers.getAddress() != null)) {
                    retour = true;
                    customers.setAddress(dto.getAddress1());
                }
                if ((dto.getAddress2() != null && !dto.getAddress2().equals(customers.getAddress2()))
                        || (dto.getAddress2() == null && customers.getAddress2() != null)) {
                    retour = true;
                    customers.setAddress2(dto.getAddress2());
                }
                if ((dto.getCardNumber() != null && !dto.getCardNumber().equals(customers.getCard()))
                        || (dto.getCardNumber() == null && customers.getCard() != null)) {
                    retour = true;
                    customers.setCard(dto.getCardNumber());
                }
                if ((dto.getCity() != null && !dto.getCity().equals(customers.getCity())) || (dto.getCity() == null && customers.getCity() != null)) {
                    retour = true;
                    customers.setCity(dto.getCity());
                }
                if ((dto.getCountry() != null && !dto.getCountry().equals(customers.getCountry()))
                        || (dto.getCountry() == null && customers.getCountry() != null)) {
                    retour = true;
                    customers.setCountry(dto.getCountry());
                }
                if ((dto.getEmail() != null && !dto.getEmail().equals(customers.getEmail())) || (dto.getEmail() == null && customers.getEmail() != null)) {
                    retour = true;
                    customers.setEmail(dto.getEmail());
                }

                if ((dto.getFirstName() != null && !dto.getFirstName().equals(customers.getFirstName()))
                        || (dto.getFirstName() == null && customers.getFirstName() != null)) {
                    retour = true;
                    customers.setFirstName(dto.getFirstName());
                }

                if ((dto.getLastName() != null && !dto.getLastName().equals(customers.getLastName()))
                        || (dto.getLastName() == null && customers.getLastName() != null)) {
                    retour = true;
                    customers.setLastName(dto.getLastName());
                    customers.setName(customers.getName() + dto.getLastName());
                }

                customers.setName("");
                if (dto.getFirstName() != null) {
                    customers.setName(dto.getFirstName());
                }
                if (dto.getLastName() != null) {
                    if (!"".equals(customers.getName())) {
                        customers.setName(customers.getName() + " ");
                    }
                    customers.setName(customers.getName() + dto.getLastName());
                }

                // EDU : pour éviter d'avoir des Null qui remontent à l'affichage
                // customers.setName(dto.getFirstName() + " " + dto.getLastName());
                if ((dto.getNote() != null && !dto.getNote().equals(customers.getNotes())) || (dto.getNote() == null && customers.getNotes() != null)) {
                    retour = true;
                    customers.setNotes(dto.getNote());
                }
                if ((dto.getPhone1() != null && !dto.getPhone1().equals(customers.getPhone())) || (dto.getPhone1() == null && customers.getPhone() != null)) {
                    retour = true;
                    customers.setPhone(dto.getPhone1());
                }
                if ((dto.getPhone2() != null && !dto.getPhone2().equals(customers.getPhone2())) || (dto.getPhone2() == null && customers.getPhone2() != null)) {
                    retour = true;
                    customers.setPhone2(dto.getPhone2());
                }
                if ((dto.getSearchKey() != null && !dto.getSearchKey().equals(customers.getSearchKey()))
                        || (dto.getSearchKey() == null && customers.getSearchKey() != null)) {
                    retour = true;
                    customers.setSearchKey(dto.getSearchKey());
                }
                if ((dto.getZipCode() != null && !dto.getZipCode().equals(customers.getZipCode()))
                        || (dto.getZipCode() == null && customers.getZipCode() != null)) {
                    retour = true;
                    customers.setZipCode(dto.getZipCode());
                }
                if ((dto.getInsee() != null && !dto.getInsee().equals(customers.getInsee())) || (dto.getInsee() == null && customers.getInsee() != null)) {
                    retour = true;
                    customers.setInsee(dto.getInsee());
                }
                if ((dto.getCivilite() != null && !dto.getCivilite().equals(customers.getCivilite()))
                        || (dto.getCivilite() == null && customers.getCivilite() != null)) {
                    retour = true;
                    customers.setCivilite(dto.getCivilite());
                }
                if ((dto.getDateOfBirth() != null && !dto.getDateOfBirth().equals(customers.getDateOfBirth()))
                        || (dto.getDateOfBirth() == null && customers.getDateOfBirth() != null)) {
                    retour = true;
                    customers.setDateOfBirth(dto.getDateOfBirth());
                }
                if ((dto.getCreationDate() != null && !dto.getCreationDate().equals(customers.getDateCreation()))
                        || (dto.getCreationDate() == null && customers.getDateCreation() != null)) {
                    retour = true;
                    customers.setDateCreation(dto.getCreationDate());
                }
                if (dto.isNewsletter() != customers.isNewsletter()) {
                    retour = true;
                    customers.setNewsletter(dto.isNewsletter());
                }
                if (dto.isPartenaires() != customers.isPartenaires()) {
                    retour = true;
                    customers.setPartenaires(dto.isPartenaires());
                }
                if ((dto.getSource() != null && !dto.getSource().equals(customers.getSource())) || (dto.getSource() == null && customers.getSource() != null)) {
                    retour = true;
                    customers.setSource(dto.getSource());
                }
                if ((dto.getLastUpdate() != null && !dto.getLastUpdate().equals(customers.getDateUpdate()))
                        || (dto.getLastUpdate() == null && customers.getDateUpdate() != null)) {
                    customers.setDateUpdate(dto.getLastUpdate());
                }

                if ((dto.getLocation() != null && dto.getLocation().getId() != null
                        && !dto.getLocation().getId().equals(customers.getLocation() != null ? customers.getLocation().getId() : null))
                        || (dto.getLocation() == null && customers.getLocation() != null)
                        || (dto.getLocation() != null && dto.getLocation().getId() == null && customers.getLocation() != null)) {
                    retour = true;
                    Locations location = null ;
                    if( dto.getLocation() != null && dto.getLocation().getId() != null ) {
                        location = locationsApiDAO != null ? locationsApiDAO.findById(dto.getLocation().getId()).orElse(null) : null ;
                        if(location == null ) {
                            location = new Locations();
                            location.setId(dto.getLocation().getId());
                        }
                    }
                    customers.setLocation(location);
                }
                if ((dto.getDiscountProfile() != null && dto.getDiscountProfile().getId() != null
                        && !dto.getDiscountProfile().getId().equals(customers.getDiscountProfil() != null ? String.valueOf(customers.getDiscountProfil().getId()) : null))
                        || (dto.getDiscountProfile() == null && customers.getDiscountProfil() != null)
                        || (dto.getDiscountProfile() != null && dto.getDiscountProfile().getId() == null && customers.getDiscountProfil() != null)) {
                    retour = true;
                    DiscountProfil discountProfil = null ;
                    if( dto.getDiscountProfile() != null && dto.getDiscountProfile().getId() != null ) {
                        discountProfil = discountProfilApiDAO != null ? discountProfilApiDAO.findById(Long.valueOf(dto.getDiscountProfile().getId())).orElse(null) : null ;
                        if(discountProfil == null ) {
                            discountProfil = new DiscountProfil();
                            discountProfil.setId(Long.getLong(dto.getDiscountProfile().getId()));
                        }
                    }
                    customers.setDiscountProfil(discountProfil);
                }
            }
        }

        return retour;
    }

    public <T> T getInsee(Class<T> selector , Customers customers) {
        Insee insee = null;
        // EDU 2019 08 - InseeDAO peut être null si pb autowire
        // typiquement le cas où on arrive au customerManager par deux clients différents ( parent et enfant)
        // peu import on ne cherche que le searchkey du parent dans ce cas là
        // TODO à rendre propre
        if (customers.getCity() != null && customers.getZipCode() != null && inseeApiDAO != null) {
            // Trouver les infos qui collent
            // On part du principe que le problème vient de la ville
            // Et on attribue toujours une ville - celle dont le om est le plus proche
            // Au sens de la distance de Damereau Livenshtein

            List<Insee> candidates = inseeApiDAO.findByZipCode(customers.getZipCode());
            String finalCity = customers.getCity();
            if (candidates.size() > 0) {
                if (candidates.size() == 1 || finalCity == null) {
                    insee = candidates.get(0);
                } else {
                    int minDistance = finalCity.length();
                    for (Insee currentInsee : candidates) {
                        if (currentInsee.getCommune().equals(customers.getCity())) {
                            insee = currentInsee;
                            break;
                        }
                        int dist = new DamerauLevenshteinDistance().calculate(customers.getCity(), currentInsee.getCommune());
                        if (dist < minDistance) {
                            minDistance = dist;
                            finalCity = currentInsee.getCommune();
                        }
                    }
                }
            }
        }
        return insee == null ? null : insee.getAdapter(selector);
    }
    
    //EDU 2021 09 On uniformise la génération des Id
    // On abandonne la possibilité de modifier le searchkey
    // Cependant on confie l'id à une fonction pour plus de simplicité à l'avenir
    @Override
    public String createId(CustomersDetailDTO dto) {
        String retour = dto.getSearchKey();
        if(retour == null) {
            
        }
        return retour;
    }
}
