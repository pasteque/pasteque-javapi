package org.pasteque.api.service.customers.impl;
import java.util.List;

import org.pasteque.api.dao.customers.IDiscountProfilApiDAO;
import org.pasteque.api.dto.customers.POSDiscountProfilDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.service.customers.IDiscountProfilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiscountProfilService implements IDiscountProfilService {

    @Autowired
    private IDiscountProfilApiDAO DiscountProfilApiDAO;

   
	@Override
	public List<POSDiscountProfilDTO> getAll(Class<POSDiscountProfilDTO> selector) {
		return AdaptableHelper.getAdapter(DiscountProfilApiDAO.findAll(), selector);
	}
	

}
