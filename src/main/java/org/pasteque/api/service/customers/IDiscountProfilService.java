package org.pasteque.api.service.customers;

import java.util.List;

import org.pasteque.api.dto.customers.POSDiscountProfilDTO;

public interface IDiscountProfilService {

	List<POSDiscountProfilDTO> getAll(Class<POSDiscountProfilDTO> selector);
    
}
