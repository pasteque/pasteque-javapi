package org.pasteque.api.service.customers;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.pasteque.api.dto.customers.POSCustomersDTO;
import org.pasteque.api.model.customers.Customers;
import org.pasteque.api.dto.customers.CustomersDetailDTO;
import org.pasteque.api.dto.customers.CustomersParentDTO;

public interface ICustomersService {

    List<POSCustomersDTO> getAll();

    List<String> getTop();

    CustomersDetailDTO save(CustomersDetailDTO dto);

    <T> T getById(String id, Class<T> selector);
    
    <T> List<T> getByCriteria(String reference, String pdvId, String from, String to , String insee , Integer radius , Boolean mailConstraint , Boolean parentConstraint , String card , String address , String address2 , String zipCode , String city , String region , String country , String firstName , String lastName , String email , String phone , String phone2 , String notes , String customersExcept, Boolean parentOnly, Long maxResults, String profilId,Boolean profilConstraint,Class<T> selector);
    
    <T> List<T> getByCriteria(String reference, String pdvId, String from, String to , String insee , Integer radius , Boolean mailConstraint , Boolean parentConstraint , String card , String address , String address2 , String zipCode , String city , String region , String country , String firstName , String lastName , String email , String phone , String phone2 , String notes , String customersExcept, Boolean parentOnly, Long maxResults, String profilId,Boolean profilConstraint,Class<T> selector, boolean filtered);

    List<POSCustomersDTO> getSinceDate(Date date);
    
    Object deleteCustomerParent(CustomersParentDTO customerParent);

    Object saveCustomerParent(Map<String,CustomersParentDTO> listCustomersParentDedup, Boolean updateParent);

    String findSearchKey(String idParent);

    boolean loadFromDTO(Object source, Customers customers);

    String createId(CustomersDetailDTO dto);

}
