package org.pasteque.api.service.saleslocations;

import java.util.List;

import org.pasteque.api.dto.locations.POSLocationsDTO;
import org.pasteque.api.dto.locations.POSLocationsUrlDTO;

public interface ILocationsService {

    POSLocationsDTO getById(String id);

    POSLocationsDTO getDefault();
    
    List<POSLocationsUrlDTO> getUrl();

}
