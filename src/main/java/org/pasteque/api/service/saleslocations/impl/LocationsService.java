package org.pasteque.api.service.saleslocations.impl;

import java.util.List;
import java.util.Optional;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dto.locations.POSLocationsDTO;
import org.pasteque.api.dto.locations.POSLocationsUrlDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.service.saleslocations.ILocationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocationsService implements ILocationsService {

    @Autowired
    private ILocationsApiDAO locationsApiDAO;

    @Override
    public POSLocationsDTO getById(String id) {
        if(id == null) {
            return null;
        }
        Optional<Locations> location = locationsApiDAO.findById(id);
        return location.isPresent() ? location.get().getAdapter(POSLocationsDTO.class) : null ;
    }


    @Override
    public POSLocationsDTO getDefault() {
        Locations locations = locationsApiDAO.findTopByServerDefaultTrue();
        if (locations != null) {
            return locations.getAdapter(POSLocationsDTO.class);
        }
        return null;
    }
    
    @Override
    public List<POSLocationsUrlDTO> getUrl() {
        return AdaptableHelper.getAdapter(locationsApiDAO.findByUrlNotNull(),POSLocationsUrlDTO.class);
    }
    
}
