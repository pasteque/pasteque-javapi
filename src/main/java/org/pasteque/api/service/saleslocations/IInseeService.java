package org.pasteque.api.service.saleslocations;

import java.util.List;

import org.pasteque.api.dto.locations.POSInseeDTO;


public interface IInseeService {

    List<POSInseeDTO> getAll();
    
    <T> T getById(String inseeId, Class<T> selector);
    
    List<POSInseeDTO> getRecent(String date);

    Integer updateLocalData();
}
