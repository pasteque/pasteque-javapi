package org.pasteque.api.service.saleslocations.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.locations.IInseeApiDAO;
import org.pasteque.api.dto.locations.GeoAPIDTO;
import org.pasteque.api.dto.locations.InseeDTO;
import org.pasteque.api.dto.locations.POSInseeDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.saleslocations.Insee;
import org.pasteque.api.service.saleslocations.IInseeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class InseeService implements IInseeService {

    @Autowired
    private IInseeApiDAO inseeApiDAO;
    
    @Override
    public List<POSInseeDTO> getAll() {
        
        return AdaptableHelper.getAdapter(inseeApiDAO.findAll(), POSInseeDTO.class);
    }

    @Override
    public <T> T getById(String inseeId, Class<T> selector) {
        return inseeApiDAO.findById(inseeId).get().getAdapter(selector);
    }

    @Override
    public List<POSInseeDTO> getRecent(String date) {
        Date dStart = null ;
        if (date != null) {
            try{
                dStart = new Date(Long.parseLong(date));
                return AdaptableHelper.getAdapter(inseeApiDAO.findByLastUpdateAfter(dStart), POSInseeDTO.class);
            }
            catch(Exception e) {

            }
        } 
        return new ArrayList<POSInseeDTO>();
    }

    @Override
    public Integer updateLocalData() {
        Integer nbCommunes = 0 ;
        /**
         * On va joindre l'api geo du gouv fr pour raptrier les données
         * https://api.gouv.fr/api/api-geo#doc_tech
         * Requête type :
         * https://geo.api.gouv.fr/communes?fields=nom,code,codesPostaux,centre,surface,departement,region,population&format=json&geometry=centre
         * 
         * On va obtenir une liste de GeoAPIDTO
         * On va pouvoir la convertir en liste de InseeDTO
         * Et de là on a ce qu'il faut
         */
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<GeoAPIDTO[]> response = null ;
        List<InseeDTO> updateList =null;
        String requete = "https://geo.api.gouv.fr/communes?fields=nom,code,codesPostaux,centre,surface,departement,region,population&format=json&geometry=centre";
        
        response = restTemplate.getForEntity(requete , GeoAPIDTO[].class);
        if( HttpStatus.OK.equals(response.getStatusCode())) {

//            System.out.println(response.getStatusCode().name());
//            System.out.println(response.getBody());
            try {
                updateList= convertFromGeoAPI(Arrays.asList(response.getBody()));
                nbCommunes = updateList.size();
                
                /**
                 * C'est là que ca se passe
                 * On a une liste de DTO
                 * On lance un update basé sur cette liste
                 */
                saveInsee(updateList);
                
            } catch (CloneNotSupportedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }
        return nbCommunes;
    }
    
    private void saveInsee(List<InseeDTO> updateDtoList) {
        Boolean create = false;
        Boolean update = false;
        List<Insee> createList = new ArrayList<Insee>();
        List<Insee> updateList = new ArrayList<Insee>();
        for(InseeDTO inseeDto : updateDtoList ) {
            List<Insee> linsee = inseeApiDAO.findByZipCodeAndInseeNum(inseeDto.getZipCode(), inseeDto.getInsee());
            Insee insee = null;
            create = ( linsee.size() == 0 ) ;
            update = false;
            if (create ) {
                insee = new Insee();
                insee.setId(inseeDto.getInsee() + inseeDto.getZipCode());
                insee.setLastUpdate(new Date());
            } else {
                insee = linsee.get(0);
            }
            
            update = loadFromDTO(insee , inseeDto);
            
            if(create) {
                createList.add(insee);
            } else {
                if (update) {
                    insee.setLastUpdate(new Date());
                    updateList.add(insee);
                }
            }
        }
        
        inseeApiDAO.saveAll(createList);
        inseeApiDAO.saveAll(updateList);
        
    }

    private Boolean loadFromDTO(Insee insee, InseeDTO inseeDto) {
        Boolean retour = false;
        if(insee.getCodeDepartement() == null || !insee.getCodeDepartement().equals(inseeDto.getCodeDepartement())) {
            retour = true;
            insee.setCodeDepartement(inseeDto.getCodeDepartement());
        }
        if(insee.getCodeRegion() == null || !insee.getCodeRegion().equals(inseeDto.getCodeRegion())) {
            retour = true;
            insee.setCodeRegion(inseeDto.getCodeRegion());
        }
        if(insee.getNomDepartement() == null || !insee.getNomDepartement().equals(inseeDto.getNomDepartement())) {
            retour = true;
            insee.setNomDepartement(inseeDto.getNomDepartement());
        }
        if(insee.getNomRegion() == null || !insee.getNomRegion().equals(inseeDto.getNomRegion())) {
            retour = true;
            insee.setNomRegion(inseeDto.getNomRegion());
        }
        if(insee.getInseeNum() == null || !insee.getInseeNum().equals(inseeDto.getInsee())) {
            retour = true;
            insee.setInseeNum(inseeDto.getInsee());
        }
        if(insee.getZipCode() == null || !insee.getZipCode().equals(inseeDto.getZipCode())) {
            retour = true;
            insee.setZipCode(inseeDto.getZipCode());
        }
        if(insee.getCommune() == null || !insee.getCommune().equals(inseeDto.getCommune().toUpperCase())) {
            retour = true;
            insee.setCommune(inseeDto.getCommune().toUpperCase());
        }      
        if(insee.getLatitude() == null || !insee.getLatitude().equals(inseeDto.getLatitude())) {
            retour = true;
            insee.setLatitude(inseeDto.getLatitude());
        }
        if(insee.getLongitude() == null || !insee.getLongitude().equals(inseeDto.getLongitude())) {
            retour = true;
            insee.setLongitude(inseeDto.getLongitude());
        }
        if(insee.getSurface() == null || !insee.getSurface().equals(inseeDto.getSurface())) {
            retour = true;
            insee.setSurface(inseeDto.getSurface());
        }
        if(insee.getPopulation() == null || !insee.getPopulation().equals(inseeDto.getPopulation())) {
            retour = true;
            insee.setPopulation(inseeDto.getPopulation());
        }
        
        return retour;
    }

    List<InseeDTO> convertFromGeoAPI(List<GeoAPIDTO> updateList) throws CloneNotSupportedException{
        List<InseeDTO> retour = new ArrayList<InseeDTO>();
        for(GeoAPIDTO dto : updateList) {
            retour.addAll(convertFromGeoAPI(dto));
        }
        return retour;
    }
    
    List<InseeDTO> convertFromGeoAPI(GeoAPIDTO geoAPIdto) throws CloneNotSupportedException{
        List<InseeDTO> retour = new ArrayList<InseeDTO>();
        if(geoAPIdto != null && geoAPIdto.getCodesPostaux().size() >0) {
            InseeDTO dto = new InseeDTO(geoAPIdto);
            for(String zip : geoAPIdto.getCodesPostaux()) {
                InseeDTO local =dto.clone();
                
                local.setZipCode(zip);
                retour.add(local);
            }
        }
        return retour;
    }

}
