package org.pasteque.api.service.saleslocations;

import java.util.List;

import org.pasteque.api.dto.locations.POSSalesLocationsDTO;
import org.pasteque.api.model.saleslocations.SalesLocations;
import org.springframework.util.MultiValueMap;


public interface ISalesLocationsService {

    List<POSSalesLocationsDTO> getAll();
    
    Object save(List<POSSalesLocationsDTO> l);
    
    POSSalesLocationsDTO save(POSSalesLocationsDTO dto);

    boolean loadFromDTO(Object source, SalesLocations salesLocations);

    List<POSSalesLocationsDTO> getDTOByCriteria(
            MultiValueMap<String, String> map, Integer limit);

    List<SalesLocations> getByCriteria(MultiValueMap<String, String> map,
            Integer limit);

    <T> List<T> getAdapter(List<SalesLocations> list, Class<T> selector);

    <T> T getAdapter(SalesLocations salesLocations, Class<T> selector);

}
