package org.pasteque.api.service.saleslocations.impl;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.pasteque.api.constants.CriteriaParameters;
import org.pasteque.api.dao.locations.IInseeApiDAO;
import org.pasteque.api.dao.locations.ISalesLocationsApiDAO;
import org.pasteque.api.dao.locations.IToursApiDAO;
import org.pasteque.api.dto.locations.POSSalesLocationsDTO;
import org.pasteque.api.model.saleslocations.SalesLocations;
import org.pasteque.api.model.saleslocations.Tours;
import org.pasteque.api.service.saleslocations.ISalesLocationsService;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.pasteque.api.specs.saleslocations.SalesLocationsSpecification;
import org.pasteque.api.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

@Service
public class SalesLocationsService implements ISalesLocationsService {

    @Autowired
    private ISalesLocationsApiDAO salesLocationsApiDAO;

    @Autowired
    private IToursApiDAO toursApiDAO;

    @Autowired
    private IInseeApiDAO inseeApiDAO;

    @Override
    public List<POSSalesLocationsDTO> getAll() {
        return getAdapter(salesLocationsApiDAO.findAll(), POSSalesLocationsDTO.class);
    }

    @Override
    public Object save(List<POSSalesLocationsDTO> l) {
        for (POSSalesLocationsDTO pOSSalesLocationsDTO : l) {
            save(pOSSalesLocationsDTO);
        }
        return null;
    }
    
    @Override
    public List<SalesLocations> getByCriteria(MultiValueMap<String ,String> map ,  Integer limit) {
        SalesLocationsSpecification specs = new  SalesLocationsSpecification();
        SalesLocationsSpecification specsInsee = new  SalesLocationsSpecification();
        SalesLocationsSpecification specsCity = new  SalesLocationsSpecification();
        SalesLocationsSpecification specsZip = new  SalesLocationsSpecification();
        List<SalesLocations> l = null ;
        for(String str : map.keySet()){
            List<String> values = map.get(str);
            if(!CriteriaParameters.LIMIT.equals(str) && !CriteriaParameters.EXCLUDEACTION.equals(str) 
                    && !CriteriaParameters.EXCLUDELOGIN.equals(str) && !CriteriaParameters.EXCLUDEP.equals(str) && !CriteriaParameters.EXCLUDEPASSWORD.equals(str)) {
                if(values.size() == 1) {
                    //TODO repasser ici - à iso périmètre on ne fait rien avec ce paramètre
                    if(!SalesLocations.SearchCritria.LOCATION.equals(str)) {
                        if(SalesLocations.SearchCritria.REFERENCE.equals(str)) {
                            // Si on a ce paramètre il faut ajouter en OU un ensemble de contraintes
                            specsInsee.add(new SearchCriteria(SalesLocations.Fields.INSEE , values.get(0) , SearchOperation.MATCH_START ));
                            specsCity.add(new SearchCriteria(SalesLocations.Fields.CITY , values.get(0) , SearchOperation.MATCH ));
                            specsZip.add(new SearchCriteria(SalesLocations.SearchCritria.ZIP , values.get(0) , SearchOperation.MATCH_START ));
                        } else {
                            
                            String value = values.get(0);
                            SearchOperation operation = SearchOperation.EQUAL ;
                            if(SalesLocations.SearchCritria.FROM.equals(str) || SalesLocations.SearchCritria.STARTBEFORE.equals(str)) {
                                operation = SearchOperation.GREATER_THAN_EQUAL ;
                                if(DateHelper.SQLTIMESTAMP.length() != value.length()) {
                                    LocalDateTime date = Instant.parse(value).atZone(ZoneId.systemDefault() ).toLocalDateTime();
                                    value = DateHelper.formatSQLTimeStamp(date);
                                }
                            }
                            if(SalesLocations.SearchCritria.TO.equals(str) || SalesLocations.SearchCritria.ENDAFTER.equals(str)) {
                                operation = SearchOperation.LESS_THAN_EQUAL ;
                                if(DateHelper.SQLTIMESTAMP.length() != value.length()) {
                                    LocalDateTime date = Instant.parse(value).atZone(ZoneId.systemDefault() ).toLocalDateTime();
                                    value = DateHelper.formatSQLTimeStamp(date);
                                }
                            }
                            specs.add(new SearchCriteria(str , value , operation ));
                        }
                    }
                } else {
                    if(!SalesLocations.SearchCritria.LOCATION.equals(str) &&
                            !SalesLocations.SearchCritria.REFERENCE.equals(str)&&
                            !SalesLocations.SearchCritria.FROM.equals(str)&&
                            !SalesLocations.SearchCritria.TO.equals(str)) {
                        specs.add(new SearchCriteria(str , values , SearchOperation.IN));
                    }
                }
            }
        }
        //TODO gere reference : insee or city or zip like reference 
        // il y a un tri par défaut et une limite optionnelle
        Sort sort = Sort.by(SalesLocations.Fields.START_DATE).descending().and(Sort.by(SalesLocations.Fields.TOUR)) ;

        if(limit != null && limit > 1) {        
            Page<SalesLocations> p = salesLocationsApiDAO.findAll(specs.and(specsInsee.or(specsCity).or(specsZip)) , PageRequest.of(0, limit , sort) );
            l = p.getContent();
        } else {
            l = salesLocationsApiDAO.findAll(specs.and(specsInsee.or(specsCity).or(specsZip)) , sort );
        }
        return l;
    }

    @Override
    public List<POSSalesLocationsDTO> getDTOByCriteria(MultiValueMap<String ,String> map ,  Integer limit) {
        return getAdapter(getByCriteria(map , limit) , POSSalesLocationsDTO.class);
    }


    @Override
    public POSSalesLocationsDTO save(POSSalesLocationsDTO dto) {
        SalesLocations salesLocation = new SalesLocations();

        if (dto.getId() != null) {
            salesLocation = salesLocationsApiDAO.findById(dto.getId()).get();
        }
        /*
        salesLocation.setCity(dto.getCity());
        salesLocation.setEndDate(dto.getEndDate());
        salesLocation.setFee(dto.getFee());
        salesLocation.setInseeNum(dto.getInseeNb());
        salesLocation.setPaid(dto.isPaid());
        salesLocation.setPubNb(dto.getPubNb());
        salesLocation.setStartDate(dto.getStartDate());
        salesLocation.setTour(toursDAO.findByPrimaryKey(dto.getTourId()));*/

        loadFromDTO(dto , salesLocation);

        if (dto.getId() != null) {
            salesLocation = salesLocationsApiDAO.save(salesLocation);
        } else {
            salesLocation.createId();

            salesLocation = salesLocationsApiDAO.save(salesLocation);
        }
        return salesLocation.getAdapter(POSSalesLocationsDTO.class);
    }

    @Override
    public boolean loadFromDTO(Object source , SalesLocations salesLocations) {
        boolean retour = false ;
        //EDU dans un premier temps on gère juste le cas SalesLocationsDTO qui nous intéresse
        if (POSSalesLocationsDTO.class.isInstance(source)) {
            POSSalesLocationsDTO dto = (POSSalesLocationsDTO) source;
            if((dto.getCity()!= null && !dto.getCity().equals(salesLocations.getCity())) || (dto.getCity()==null && salesLocations.getCity() != null)) {
                retour = true ;
                salesLocations.setCity(dto.getCity());
            }
            if((dto.getEndDate()!= null && !dto.getEndDate().equals(salesLocations.getEndDate())) || (dto.getEndDate()==null && salesLocations.getEndDate() != null)) {
                retour = true ;
                salesLocations.setEndDate(dto.getEndDate());
            }
            if(dto.getFee() != salesLocations.getFee()) {
                retour = true ;
                salesLocations.setFee(dto.getFee());
            }
            if((dto.getInseeNb()!= null && !dto.getInseeNb().equals(salesLocations.getInsee() == null ? "" : salesLocations.getInsee().getId())) || (dto.getInseeNb()==null && salesLocations.getInseeNum() != null)) {
                retour = true ;
                salesLocations.setInsee(inseeApiDAO.findById(dto.getInseeNb()).orElse(null));
            }
            if(dto.isPaid() ^ salesLocations.isPaid()) {
                retour = true ;
                salesLocations.setPaid(dto.isPaid());
            }
            if(dto.getPubNb() != salesLocations.getPubNb()) {
                retour = true ;
                salesLocations.setPubNb(dto.getPubNb());
            }
            if((dto.getStartDate()!= null && !dto.getStartDate().equals(salesLocations.getStartDate())) || (dto.getStartDate()==null && salesLocations.getStartDate() != null)) {
                retour = true ;
                salesLocations.setStartDate(dto.getStartDate());
            }
            if((dto.getTourId()!= 0 && (salesLocations.getTour() == null || salesLocations.getTour().getId() != dto.getTourId())) || (dto.getTourId()==0 && salesLocations.getTour() != null)) {
                retour = true ;
                Tours theTour = toursApiDAO.findById(dto.getTourId()).orElse(null);
                if(theTour != null) {
                    List<SalesLocations> oldLocations = theTour.getSalesLocations();
                    oldLocations.add(salesLocations);
                    toursApiDAO.save(theTour);
                }
                theTour = salesLocations.getTour();
                if(theTour != null) {
                    List<SalesLocations> oldLocations = theTour.getSalesLocations();
                    oldLocations.remove(salesLocations);
                    toursApiDAO.save(theTour);
                }
                salesLocations.setTour(theTour);
            } 
        }
        return retour;
    }

    @Override
    public <T> T getAdapter(SalesLocations salesLocations , Class<T> selector) {
        T retour  = salesLocations.getAdapter(selector);
        if (selector.equals(POSSalesLocationsDTO.class) && retour != null) {
            POSSalesLocationsDTO dto = (POSSalesLocationsDTO) retour ;
            SalesLocations nextSalesLocations = salesLocationsApiDAO.findNextSalesLocation(salesLocations.getInseeNum(),
                    salesLocations.getStartDate());
            if (nextSalesLocations != null) {
                dto.setNextStartDate(nextSalesLocations.getStartDate());
            }
        } 
        
        return retour;
    }
    
    @Override
    public <T> List<T> getAdapter(List<SalesLocations> list, Class<T> selector) {
        List<T> listDTO = new ArrayList<>();
        for (SalesLocations adaptable : list) {
            listDTO.add((T) getAdapter(adaptable , selector));
        }
        return listDTO;
    }
    
}
