package org.pasteque.api.service.cash;

import org.pasteque.api.dto.tickets.POSPaymentsDTO;
import org.pasteque.api.model.exception.APIException;

public interface ICashMovementsService {

    Object move(String cashId, long date, String note, POSPaymentsDTO payment) throws APIException;

}
