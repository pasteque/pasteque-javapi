package org.pasteque.api.service.cash;

import java.util.List;

import org.pasteque.api.dto.cash.POSCashRegistersDTO;
import org.pasteque.api.model.cash.CashRegisters;

public interface ICashRegistersService {

    POSCashRegistersDTO getByLabel(String label);
    
    POSCashRegistersDTO get(String id);
    
    POSCashRegistersDTO getBy(String label, String location);

    List<POSCashRegistersDTO> getByCriteria(String label, String location);

    List<POSCashRegistersDTO> getAll();

    List<CashRegisters> findByNameAndLocation(String label, String location);

}
