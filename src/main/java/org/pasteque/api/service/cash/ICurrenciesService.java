package org.pasteque.api.service.cash;

import java.util.List;

import org.pasteque.api.dto.cash.POSCurrenciesDTO;

public interface ICurrenciesService {

    List<POSCurrenciesDTO> getAll();

}
