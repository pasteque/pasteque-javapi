package org.pasteque.api.service.cash;

import java.util.List;

import org.pasteque.api.dto.cash.POSClosedCashDTO;
import org.pasteque.api.dto.tickets.POSZTicketsDTO;
import org.pasteque.api.model.exception.APIException;

public interface ICashesService {

    public POSClosedCashDTO getClosedCashByCashRegisterId(String cashRegisterId);

    public POSClosedCashDTO updateClosedCash(POSClosedCashDTO pOSClosedCashDTO) throws APIException;

    public POSZTicketsDTO getZTicket(String id);

    public List<POSClosedCashDTO> search(String cashRegisterId, String dateStart, String dateStop);

    <T> T getById(String id, Class<T> selector);

}
