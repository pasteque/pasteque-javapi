package org.pasteque.api.service.cash.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.pasteque.api.dao.application.IApplicationApiDAO;
import org.pasteque.api.dao.cash.ICashRegistersApiDAO;
import org.pasteque.api.dao.cash.IClosedCashApiDAO;
import org.pasteque.api.dao.cash.ITaxesApiDAO;
import org.pasteque.api.dao.fiscal.IPeriodsApiDAO;
import org.pasteque.api.dao.locations.ISalesLocationsApiDAO;
import org.pasteque.api.dao.products.ICategoriesApiDAO;
import org.pasteque.api.dao.tickets.IPaymentsApiDAO;
import org.pasteque.api.dao.tickets.ITicketsApiDAO;
import org.pasteque.api.dto.cash.POSClosedCashCategoryInfoDTO;
import org.pasteque.api.dto.cash.POSClosedCashDTO;
import org.pasteque.api.dto.locations.POSSalesLocationsDTO;
import org.pasteque.api.dto.cash.POSClosedCashTaxInfoDTO;
import org.pasteque.api.dto.tickets.POSPaymentsDTO;
import org.pasteque.api.dto.tickets.POSZTicketsDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.model.fiscal.FiscalTickets;
import org.pasteque.api.model.fiscal.Periods;
import org.pasteque.api.model.tickets.Payments;
import org.pasteque.api.service.cash.ICashesService;
import org.pasteque.api.service.fiscal.IFiscalTicketsService;
import org.pasteque.api.service.saleslocations.ISalesLocationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CashesService implements ICashesService {

    @Autowired
    private IFiscalTicketsService fiscalTicketsService;
    @Autowired
    private IClosedCashApiDAO closedCashApiDAO;
    @Autowired
    private ICashRegistersApiDAO cashRegistersApiDAO;
    @Autowired
    private ITicketsApiDAO ticketsApiDAO;
    @Autowired
    private IPaymentsApiDAO paymentsApiDAO;
    @Autowired
    private ITaxesApiDAO taxesApiDAO;
    @Autowired
    private ICategoriesApiDAO categoriesApiDAO;
    @Autowired
    private ISalesLocationsApiDAO salesLocationDAO;
    @Autowired
    private IApplicationApiDAO applicationApiDAO;
    @Autowired
    private IPeriodsApiDAO periodsApiDAO;
    @Autowired
    private ISalesLocationsService salesLocationsService;
    
    @Override
    public POSClosedCashDTO getClosedCashByCashRegisterId(String cashRegisterId) {
        ClosedCash closedCash = closedCashApiDAO.findFirstByCashRegisterOrderByHostSequenceDesc(cashRegistersApiDAO.findById(cashRegisterId).orElse(null));
        if (closedCash == null || closedCash.getDateEnd() != null) {
            return null;
        }
        POSClosedCashDTO retour = closedCash.getAdapter(POSClosedCashDTO.class);
        if(closedCash.getSalesLocation() != null ) {
            retour.setSalesLocation(salesLocationsService.getAdapter(closedCash.getSalesLocation(), POSSalesLocationsDTO.class));
        }
        return retour;
    }

    @Override
    public POSClosedCashDTO updateClosedCash(POSClosedCashDTO pOSClosedCashDTO) throws APIException {
        ClosedCash closedCash = new ClosedCash();
        CashRegisters cashRegister = null ;
        
        if (pOSClosedCashDTO.getId() != null) {
            closedCash = closedCashApiDAO.findById(pOSClosedCashDTO.getId()).orElse(null);
        } else {
            cashRegister = cashRegistersApiDAO.findById(pOSClosedCashDTO.getCashRegisterId()).orElse(null);
            /** 
             * EDU  2020 08 Maintenant on a le nextSession dans le cashRegister 
             * Trois options :
             *  - on laisse tel quel et on check que tout concorde
             *  - on ne change rien et on update la valeur de nextsession
             *  - on utilise exclusivement la valeur de nextsession
             *  
             *  
             */

            ClosedCash session = closedCashApiDAO.findFirstByCashRegisterOrderByHostSequenceDesc(cashRegister);
            closedCash.setHostSequence((session == null ? 0 : session.getHostSequence() ) + 1);
            if(closedCash.getHostSequence() != cashRegister.getNextSessionId()) {
                //Alors on a un pb de cohérence ça veut dire une perte de session quelque part ou alors
                //un oublie d'update de cashRegister
                //TODO gérer le cas proprement
                //Ca ne doit pas pouvoir arriver : si on nous envoie plusieurs sessions complete de suite dans le désordre chronologique
                //Alors leurs sequences seront bien consécutives 
                //Meme si les dates d'ouvrtures ne le seront pas
                //De plus avec le client actuel ça ne peut pas se faire
            }
            cashRegister.setNextSessionId(closedCash.getHostSequence()+1);
            closedCash.setCashRegister(cashRegister);
            String id = Long.toString(new Date().getTime());
            closedCash.setMoney(id);


            //EDU 2020 08 - Amorcer les totaux perpétuels attention il faut avoir la date de début
            closedCash.setDateStart(pOSClosedCashDTO.getOpenDate());
            initPeriods(closedCash);
            initCs(closedCash , session);
        }

        //EDU 2020 - V8s
        //si on n'a pas trouvé la session ou si celle-ci est déjà fermé on sort en exception
        if(closedCash == null || closedCash.getDateEnd() != null) {
            String rejectReason = "No way to modify a non-existing session or an already closed";
            throw new APIException(rejectReason);
        }
        closedCash.setDateStart(pOSClosedCashDTO.getOpenDate());
        closedCash.setDateEnd(pOSClosedCashDTO.getCloseDate());

        if (pOSClosedCashDTO.getOpenCash() != null) {
            closedCash.setOpenCash(Double.valueOf(pOSClosedCashDTO.getOpenCash()));
        }
        if (pOSClosedCashDTO.getCloseCash() != null) {
            closedCash.setCloseCash(Double.valueOf(pOSClosedCashDTO.getCloseCash()));
        }
        if (pOSClosedCashDTO.getExpectedCash() != null) {
            closedCash.setExpectedCash(Double.valueOf(pOSClosedCashDTO.getExpectedCash()));
        }
        if (pOSClosedCashDTO.getSalesLocation() != null) {
            closedCash.setSalesLocation(salesLocationDAO.findById(pOSClosedCashDTO.getSalesLocation().getId()).orElse(null));
            ;
        }
        if(pOSClosedCashDTO.getCloseDate() != null) {
            closedCash = fiscalTicketsService.write(closedCash);
        }
        else if (pOSClosedCashDTO.getId() != null) {
            closedCash = closedCashApiDAO.save(closedCash);
        } else {
            closedCash = closedCashApiDAO.save(closedCash);
            cashRegistersApiDAO.save(cashRegister);
        }

        return closedCash.getAdapter(POSClosedCashDTO.class);
    }

    @Override
    public POSZTicketsDTO getZTicket(String id) {
        ClosedCash closedCash = closedCashApiDAO.findById(id).orElse(null);
        // Infos tickets
        List<Object[]> result = ticketsApiDAO.findTicketsByClosedCash(id);
        double sales = 0D;
        double discount = 0D;
        int custCount = 0;
        long nbTickets = 0;
        // result ne doit avoir qu'une ligne....
        for (Object[] objects : result) {
            if (objects[0] != null) {
                nbTickets = (int) objects[0];
            }
            if (objects[1] != null) {
                sales += (double) objects[1];
            }
            if (objects[2] != null) {
                custCount += (int) objects[2];
            }
            if (objects[3] != null) {
                discount += (double) objects[3];
            }
        }
        List<Object[]> listPayments = paymentsApiDAO.getPaymentsByClosedCash(id , null);
        long paymentsCount = 0;
        List<POSPaymentsDTO> payments = new ArrayList<POSPaymentsDTO>();
        for (Object[] objects : listPayments) {
            paymentsCount += (long) objects[3];
            POSPaymentsDTO pOSPaymentsDTO = new POSPaymentsDTO();
            pOSPaymentsDTO.setAmount((double) objects[2]);
            pOSPaymentsDTO.setCurrencyAmount((double) objects[4]);
            pOSPaymentsDTO.setCurrencyId((Long) objects[1]);
            pOSPaymentsDTO.setType((String) objects[0]);
            pOSPaymentsDTO.setPaymentsNumber((long) objects[3]);
            pOSPaymentsDTO.setPaymentLabel(Payments.getPaymentsLabel((String) objects[0]));
            pOSPaymentsDTO.setPaymentCateg(Payments.getPaymentsCateg((String) objects[0]));
            payments.add(pOSPaymentsDTO);
        }
        // Infos taxes
        List<Object[]> listTaxes = taxesApiDAO.getTaxesByClosedCash(id);
        List<POSClosedCashTaxInfoDTO> taxes = new ArrayList<POSClosedCashTaxInfoDTO>();
        for (Object[] objects : listTaxes) {
            POSClosedCashTaxInfoDTO pOSClosedCashTaxInfoDTO = new POSClosedCashTaxInfoDTO();
            pOSClosedCashTaxInfoDTO.setId((String) objects[0]);
            pOSClosedCashTaxInfoDTO.setBase((double) objects[1]);
            pOSClosedCashTaxInfoDTO.setAmount((double) objects[2]);
            taxes.add(pOSClosedCashTaxInfoDTO);
        }
        // Info categories
        List<Object[]> listCategories = categoriesApiDAO.getCategoriesByClosedCash(id);
        List<POSClosedCashCategoryInfoDTO> catSales = new ArrayList<POSClosedCashCategoryInfoDTO>();
        for (Object[] objects : listCategories) {
            POSClosedCashCategoryInfoDTO pOSClosedCashCategoryInfoDTO = new POSClosedCashCategoryInfoDTO();
            pOSClosedCashCategoryInfoDTO.setId((String) objects[0]);
            pOSClosedCashCategoryInfoDTO.setAmount((double) objects[1]);
            catSales.add(pOSClosedCashCategoryInfoDTO);
        }

        POSZTicketsDTO pOSZTicketsDTO = new POSZTicketsDTO();
        pOSZTicketsDTO.setCashId(id);
        pOSZTicketsDTO.setOpenCash(closedCash.getOpenCash());
        pOSZTicketsDTO.setCloseCash(closedCash.getCloseCash());
        pOSZTicketsDTO.setTicketCount(nbTickets);
        pOSZTicketsDTO.setCustCount(custCount);
        pOSZTicketsDTO.setPaymentCount(paymentsCount);
        pOSZTicketsDTO.setCs(sales);
        pOSZTicketsDTO.setDiscount(discount);
        pOSZTicketsDTO.setPayments(payments);
        pOSZTicketsDTO.setTaxes(taxes);
        pOSZTicketsDTO.setCatSales(catSales);

        return pOSZTicketsDTO;
    }

    @Override
    public List<POSClosedCashDTO> search(String cashRegisterId, String dateStart, String dateStop) {
        return AdaptableHelper.getAdapter(closedCashApiDAO.findByCriteria(cashRegisterId, dateStart, dateStop), POSClosedCashDTO.class);
    }

    @Override
    public <T> T getById(String id, Class<T> selector) {
        Optional<ClosedCash> session = closedCashApiDAO.findById(id);
        T retour = session.isPresent() ? session.get().getAdapter(selector) : null;
        if(retour != null && selector == POSClosedCashDTO.class) {
            POSClosedCashDTO dto = (POSClosedCashDTO) retour;
            if(session.get().getSalesLocation() != null) {
                dto.setSalesLocation(salesLocationsService.getAdapter(session.get().getSalesLocation(), POSSalesLocationsDTO.class));
            }
        }
        return retour;
    }

    private Periods initPeriods(ClosedCash closedCash) {
        Periods previous = periodsApiDAO.findTopByTypeAndCashRegisterOrderBySequenceDesc(Periods.Types.SESSION, closedCash.getCashRegister());
        if(previous == null) {
            previous = new Periods();
            previous.setCashRegister(closedCash.getCashRegister());
            previous.setDateStart(closedCash.getDateStart());
            previous.setType(Periods.Types.SESSION);
            previous.setClosed(true);
            previous.setContinuous(false);
            previous.setSequence(FiscalTickets.EOS);
            previous.setId(closedCash.getMoney() + "-" + FiscalTickets.EOS_STRING);
            periodsApiDAO.saveAndFlush(previous);
        }
        Periods retour = new Periods();
        retour.setId(closedCash.getMoney());
        retour.setContinuous(false);
        retour.setClosed(false);
        retour.setCashRegister(closedCash.getCashRegister());
        retour.setDateStart(closedCash.getDateStart());
        retour.setType(Periods.Types.SESSION);
        retour.setSequence(1+previous.getSequence());
        closedCash.setDetails(retour);
        return retour;
    }

    //Générer le montant initial des totaux perpétuels à partir de la session précédente
    public void initCs(ClosedCash closedCash , ClosedCash session) {
        if(session == null) {
            closedCash.setCsPerpetual(0D);
            closedCash.getDetails().setCsPerpetual(0D);  
            closedCash.setCsPeriod(0D);
            closedCash.getDetails().setCsPeriod(0D);
            closedCash.setCsFYear(0D);
            closedCash.getDetails().setCsFYear(0D);
        } else {
            if(session.getCsPerpetual() != null) {
                closedCash.setCsPerpetual(session.getCsPerpetual());
                closedCash.getDetails().setCsPerpetual(closedCash.getCsPerpetual());
            }
            if(session.getCsPeriod() != null && isSamePeriod(closedCash , session)) {
                closedCash.setCsPeriod(session.getCsPeriod());
                closedCash.getDetails().setCsPeriod(closedCash.getCsPeriod());
            }
            if(session.getCsFYear() != null && isSameFYear(closedCash ,session)) {
                closedCash.setCsFYear(session.getCsFYear());
                closedCash.getDetails().setCsFYear(closedCash.getCsFYear());
            }
        }

    }

    //TODO étendre si on veut des fonctions pour clotures auto de périodes

    private boolean isSameFYear(ClosedCash closedCash , ClosedCash session) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd"); 
        SimpleDateFormat yearOnly = new SimpleDateFormat("yyyy");
        boolean retour = closedCash != null && session != null 
                && session.getDateStart() != null &&  closedCash.getDateStart() != null ;
        String yearEnd = retour ? yearOnly.format(session.getDateStart()) + applicationApiDAO.getFiscalYearEnd() + "24" : "";
        retour = retour && ( 0 < fmt.format(closedCash.getDateStart()).compareTo(yearEnd) * 
                fmt.format(session.getDateStart()).compareTo(yearEnd));
        return retour;
    }

    private boolean isSamePeriod(ClosedCash closedCash , ClosedCash session) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd"); 
        boolean retour = closedCash != null && session != null 
                && session.getDateStart() != null &&  closedCash.getDateStart() != null  && 
                fmt.format(closedCash.getDateStart()).equals(fmt.format(session.getDateStart()));
        return retour;
    }
}
