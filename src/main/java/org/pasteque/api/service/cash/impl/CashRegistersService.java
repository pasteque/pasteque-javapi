package org.pasteque.api.service.cash.impl;

import java.util.List;

import org.pasteque.api.dao.cash.ICashRegistersApiDAO;
import org.pasteque.api.dto.cash.POSCashRegistersDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.service.cash.ICashRegistersService;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.pasteque.api.specs.cash.CashRegistersSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CashRegistersService implements ICashRegistersService {

    @Autowired
    private ICashRegistersApiDAO cashRegisterDAO;

    @Override
    public POSCashRegistersDTO getByLabel(String label) {
        List<CashRegisters> lCr = cashRegisterDAO.findByName(label) ;
        return lCr.size()> 0 ?  lCr.get(0).getAdapter(POSCashRegistersDTO.class) : null;
    }
    
    @Override
    public POSCashRegistersDTO get(String id) {
        return cashRegisterDAO.findById(id).get().getAdapter(POSCashRegistersDTO.class);
    }

    @Override
    public List<POSCashRegistersDTO> getAll() {
        return AdaptableHelper.getAdapter(cashRegisterDAO.findAll(), POSCashRegistersDTO.class);
    }
    
    @Override
    public List<CashRegisters> findByNameAndLocation(String label, String location) {
        CashRegistersSpecification specs = new  CashRegistersSpecification();
        specs.add(new SearchCriteria(CashRegisters.Fields.NAME , label , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(CashRegisters.Fields.LOCATION , location , SearchOperation.EQUAL));
        List<CashRegisters> l = cashRegisterDAO.findAll(specs);
        return l;
    }

    @Override
    public POSCashRegistersDTO getBy(String label, String location) {
        POSCashRegistersDTO retour = null;
        List<CashRegisters> l = findByNameAndLocation( label,  location);
        if(!l.isEmpty()) {
            retour = l.get(0).getAdapter(POSCashRegistersDTO.class , true);
        }
        return retour;
    }
    
    @Override
    public List<POSCashRegistersDTO> getByCriteria(String label, String location) {
        return AdaptableHelper.getAdapter(findByNameAndLocation(label,location), POSCashRegistersDTO.class);
    }
    
    

}
