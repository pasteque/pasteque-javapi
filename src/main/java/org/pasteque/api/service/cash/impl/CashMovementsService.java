package org.pasteque.api.service.cash.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.cash.IClosedCashApiDAO;
import org.pasteque.api.dao.cash.ICurrenciesApiDAO;
import org.pasteque.api.dao.tickets.IReceiptsApiDAO;
import org.pasteque.api.dto.tickets.POSPaymentsDTO;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.cash.Currencies;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.model.tickets.Payments;
import org.pasteque.api.model.tickets.Receipts;
import org.pasteque.api.service.cash.ICashMovementsService;
import org.pasteque.api.util.FilteringAndFormattingData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class CashMovementsService implements ICashMovementsService {

    @Autowired
    private IClosedCashApiDAO closedCashApiDAO;
    @Autowired
    private ICurrenciesApiDAO currenciesApiDAO;
    @Autowired
    private IReceiptsApiDAO receiptsApiDAO;

    @Override
    @Transactional
    public Object move(String cashId, long date, String note, POSPaymentsDTO payment) throws APIException {

        Receipts receipts = new Receipts();
        ClosedCash closedCash = closedCashApiDAO.findById(cashId).orElse(null);
        if (closedCash == null) {
            throw new APIException("Unknown cash session");
        }

        receipts.setId(Long.toString(new Date().getTime()).concat(closedCash.getCashRegister().getLocation().getId()));
        receipts.setClosedCash(closedCash);
        receipts.setAttributes(null);
        receipts.setDateNew(new Date(date));
        receipts.setTaxes(null);

        Payments payments = new Payments();
        Currencies currencies = currenciesApiDAO.findById(payment.getCurrencyId()).orElse(null);
        if (currencies == null) {
            throw new APIException("Unknown currency");
        }
        payments.setCurrency(currencies);
        payments.setId(receipts.getId());
        payments.setNote(note);
        payments.setPayment(payment.getType());
        payments.setTickets(null);
        payments.setReceipts(receipts);
        payments.setTotal(payment.getAmount());
        payments.setTotalCurrency(payment.getCurrencyAmount());
        
        FilteringAndFormattingData.filter(payments);

        List<Payments> l = new ArrayList<Payments>();
        l.add(payments);
        receipts.setPayments(l);

        receiptsApiDAO.save(receipts);

        return receipts.getId();
    }

}
