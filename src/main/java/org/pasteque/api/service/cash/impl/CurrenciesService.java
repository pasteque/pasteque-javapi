package org.pasteque.api.service.cash.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.pasteque.api.dao.cash.ICurrenciesApiDAO;
import org.pasteque.api.dto.cash.POSCurrenciesDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.service.cash.ICurrenciesService;

@Service
public class CurrenciesService implements ICurrenciesService {

    @Autowired
    private ICurrenciesApiDAO currenciesApiDAO;

    @Override
    public List<POSCurrenciesDTO> getAll() {
        return AdaptableHelper.getAdapter(currenciesApiDAO.findAll(), POSCurrenciesDTO.class);
    }

}
