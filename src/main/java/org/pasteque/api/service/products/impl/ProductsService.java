package org.pasteque.api.service.products.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dao.products.IProductsApiDAO;
import org.pasteque.api.dao.products.ITariffAreasProductsApiDAO;
import org.pasteque.api.dto.products.POSProductsDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.ProductsComponents;
import org.pasteque.api.model.products.TariffAreasProducts;
import org.pasteque.api.model.products.TaxCategories;
import org.pasteque.api.model.products.TaxCustCategories;
import org.pasteque.api.model.products.Taxes;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.service.fiscal.ITaxesService;
import org.pasteque.api.service.products.IProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductsService implements IProductsService {

    @Autowired
    private IProductsApiDAO productsApiDAO;
    @Autowired
    private ITariffAreasProductsApiDAO tariffAreasProductsApiDAO;
    @Autowired
    private ILocationsApiDAO locationsApiDAO;
    @Autowired
    private ITaxesService taxesService;

    @Override
    public List<POSProductsDTO> getAll() {
        return AdaptableHelper.getAdapter(productsApiDAO.findAll(), POSProductsDTO.class);
    }

    @Override
    public List<POSProductsDTO> getRecent(String date) {
        Date dStart = null ;
        if (date != null) {
            try{
                dStart = new Date(Long.parseLong(date));
                return AdaptableHelper.getAdapter(productsApiDAO.findByLastUpdateAfter(dStart), POSProductsDTO.class);
            }
            catch(Exception e) {

            }
        } 
        return new ArrayList<POSProductsDTO>();

    }

    private HashMap<String, Taxes> bufferedTaxes = new HashMap<String, Taxes>();

    // Prix de vente = TVA + DEEE
    // Variante où on ne connait que le lieu de vente (duquel on tire la TVA applicable)
    @Override
    public Double getFullPrice(Locations locations , Products products) {
        if (locations != null) {
            // TVA
            TaxCategories vatCateg = products.getTaxCategory();
            TaxCustCategories custCateg = locations.getTaxCustCategory();
            String key = vatCateg != null ? vatCateg.getId() : "-";
            key = key.concat("|").concat(custCateg != null ? custCateg.getId() : "-");

            if (!bufferedTaxes.containsKey(key)) {
                bufferedTaxes.put(key,
                        taxesService.findCurrentByCategoryAndCustCategory(vatCateg , custCateg ));
            }
            Taxes vat = bufferedTaxes.get(key);
            return getFullPrice(getLocationPriceSell(locations , products), vat , products);
        } else {
            return (double) -1;
        }
    }

    @Override
    public double getDefaultLocationFullPrice(Products products) {
        return getFullPrice(locationsApiDAO.findTopByServerDefaultTrue() , products);
    }

    // Prix de vente = TVA + DEEE
    // Variante où on connait la TVA applicable
    public double getFullPrice(Taxes vat, Products products) {
        return getFullPrice(getPriceSell(products), vat , products);
    }

    // Prix de vente = TVA + DEEE
    // Variante où on connait la TVA applicable et le prix de vente
    // Typiquement pour remonter au prix TTC depuis le prix HT
    @Override
    public double getFullPrice(double price, Taxes vat, Products products) {
        double rate = vat == null ? 0 : vat.getRate();
        price *= (1 + rate);
        price += getFullAdditionnalTaxes(products);

        return price;
    }

    // Prix hors taxe = sans TVA et sans DEEE
    // Variante où on connait le prix de vente final, et la TVA applicable
    // Typiquement pour remonter au prix HT depuis le prix de vente
    @Override
    public double getSellPrice(double price, Taxes vat, Products products) {
        double rate = vat == null ? 0 : vat.getRate();
        price -= getFullAdditionnalTaxes(products);
        price /= (1 + rate);

        return price;
    }

    // Permer d'obtenir le montant des ecotaxes
    @Override
    public double getFullAdditionnalTaxes(Products products) {
        double taxes = 0D;

        // si c'est une offre ou un produit composé, alors il faut faire la somme des ecotaxes des composants...
        if (products != null && !(products.getComponentsProducts() == null || products.getComponentsProducts().isEmpty())) {
            for (ProductsComponents components : products.getComponentsProducts()) {
                Products component = components.getComponent();
                taxes += components.getQuantity() * getFullAdditionnalTaxesSingleProduct(component);
            }
        } else {
            taxes = getFullAdditionnalTaxesSingleProduct(products);
        }

        return taxes;
    }

    private double getFullAdditionnalTaxesSingleProduct(Products products) {
        double taxes = 0D;

        // DEEE ECOMOB ....
        if (products != null && products.getTaxCategories() != null) {
            for (TaxCategories taxCategory : products.getTaxCategories()) {
                Taxes eco = taxesService.findCurrentByCategoryAndCustCategory(taxCategory, null );
                taxes += eco.getAmount();
            }
        }

        return taxes;
    }

    public Double getPriceSell(Products products) {
        return getLocationPriceSell(locationsApiDAO.findTopByServerDefaultTrue() , products);
    }

    public TariffAreasProducts getActiveTariffAreas(Products products) {
        return getLocationActiveTariffAreas(locationsApiDAO.findTopByServerDefaultTrue() , products);
    }

    @Override
    public Double getLocationPriceSell(Locations location , Products products) {
        TariffAreasProducts tariffAreasProducts = location == null ? null : getLocationActiveTariffAreas(location , products);
        if (tariffAreasProducts != null) {
            return tariffAreasProducts.getPriceSell();
        }
        return products.getPriceSell();
    }

    @Override
    public TariffAreasProducts getLocationActiveTariffAreas(Locations location , Products products) {
        return tariffAreasProductsApiDAO.findActive(location.getId(), products.getId());
    }

    // Permet de retourner une liste des libellés des ecotaxes
    @Override
    public List<String> getEcotaxes(Locations location , Products products) {

        List<String> l = new ArrayList<String>();
        // si c'est une offre ou un produit composé, alors il faut faire la somme des ecotaxes des composants...
        if (!products.getComponentsProducts().isEmpty()) {
            for (ProductsComponents components : products.getComponentsProducts()) {
                Products component = components.getComponent();
                l.addAll(getEcotaxes(location, components.getQuantity(), component));
            }
        } else {
            l.addAll(getEcotaxes(location, 1, products));
        }

        return l;
    }

    // Permet de retourner une liste des libellés des ecotaxes
    private List<String> getEcotaxes(Locations location, double quantity , Products products) {
        List<String> l = new ArrayList<String>();
        TaxCustCategories custCategory = null;
        if (location != null && location.getTaxCustCategory() != null) {
            custCategory = location.getTaxCustCategory() ;
        }
        for (TaxCategories taxCategory : products.getTaxCategories()) {
            Taxes eco = taxesService.findCurrentByCategoryAndCustCategory(taxCategory , custCategory );
            l.add("Dont " + eco.getAmount() * quantity + "€ " + eco.getCategory().getName());
        }
        return l;
    }

}
