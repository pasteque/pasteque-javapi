package org.pasteque.api.service.products.impl;

import java.util.Comparator;
import java.util.List;

import org.pasteque.api.dao.products.ITariffAreasApiDAO;
import org.pasteque.api.dto.products.POSTariffAreasDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.products.TariffAreas;
import org.pasteque.api.service.products.ITariffAreasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TariffAreasService implements ITariffAreasService {

    @Autowired
    private ITariffAreasApiDAO tariffAreasApiDAO;

    @Override
    public Object getAll(boolean indexFormat) {
        List<TariffAreas> tmpList = tariffAreasApiDAO.findAll();
        tmpList.sort(new Comparator<TariffAreas>() {
            @Override 
            public int compare(TariffAreas a , TariffAreas b) {
                if(a.getTariffAreasValidity().isEmpty() || a.getTariffAreasValidity().get(0).getEndDate() == null) {
                    if(b.getTariffAreasValidity().isEmpty() || b.getTariffAreasValidity().get(0).getEndDate() == null) {
                        return 0;
                    } else {
                        return 1;
                    }
                }
                if(b.getTariffAreasValidity().isEmpty() || b.getTariffAreasValidity().get(0).getEndDate() == null) {
                    return -1;
                }
                return  b.getTariffAreasValidity().get(0).getEndDate().compareTo(a.getTariffAreasValidity().get(0).getEndDate());
            }
            
        });
        return AdaptableHelper.getAdapter(tmpList, POSTariffAreasDTO.class);

    }


}
