package org.pasteque.api.service.products.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.pasteque.api.dao.products.ITaxCategoriesApiDAO;
import org.pasteque.api.dto.products.POSTaxCategoriesDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.service.products.ITaxCategoriesService;

@Service
public class TaxCategoriesService implements ITaxCategoriesService {

    @Autowired
    private ITaxCategoriesApiDAO taxCategoriesApiDAO;
    
    @Override
    public List<POSTaxCategoriesDTO> getAll() {
        return AdaptableHelper.getAdapter(taxCategoriesApiDAO.findAll(), POSTaxCategoriesDTO.class);
    }

}
