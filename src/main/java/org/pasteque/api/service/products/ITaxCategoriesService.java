package org.pasteque.api.service.products;

import java.util.List;

import org.pasteque.api.dto.products.POSTaxCategoriesDTO;

public interface ITaxCategoriesService {

    List<POSTaxCategoriesDTO> getAll();

}
