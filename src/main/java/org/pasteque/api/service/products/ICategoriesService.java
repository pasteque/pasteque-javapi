package org.pasteque.api.service.products;

import java.util.List;

import org.pasteque.api.dto.products.POSCategoriesDTO;

public interface ICategoriesService {

    List<POSCategoriesDTO> getAll();

    /** TODO EDU Voir si et quand c'est utilisé 
    List<CategoriesDTO> getByCriteria(String reference, Integer limit);
    */

}
