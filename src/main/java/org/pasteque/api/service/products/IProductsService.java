package org.pasteque.api.service.products;

import java.util.List;

import org.pasteque.api.dto.products.POSProductsDTO;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.TariffAreasProducts;
import org.pasteque.api.model.products.Taxes;
import org.pasteque.api.model.saleslocations.Locations;


public interface IProductsService {

    List<POSProductsDTO> getAll();
    
    List<POSProductsDTO> getRecent(String date);

    double getSellPrice(double taxedUnitPrice, Taxes tax, Products product);

    double getFullPrice(double unitPrice, Taxes tax, Products product);

    double getFullAdditionnalTaxes(Products product);

    List<String> getEcotaxes(Locations location, Products products);

    Double getFullPrice(Locations locations, Products products);

    double getDefaultLocationFullPrice(Products products);

    TariffAreasProducts getLocationActiveTariffAreas(Locations location,
            Products products);

    Double getLocationPriceSell(Locations location, Products products);

}
