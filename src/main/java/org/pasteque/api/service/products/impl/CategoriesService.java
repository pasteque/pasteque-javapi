package org.pasteque.api.service.products.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.pasteque.api.dao.products.ICategoriesApiDAO;
import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.service.products.ICategoriesService;

@Service
public class CategoriesService implements ICategoriesService {

    @Autowired
    private ICategoriesApiDAO categoriesApiDAO;

    @Override
    public List<POSCategoriesDTO> getAll() {
        return AdaptableHelper.getAdapter(categoriesApiDAO.findAll(), POSCategoriesDTO.class);
    }

    /**
    @Override
    public List<CategoriesDTO> getByCriteria(String reference, Integer limit) {
        return AdaptableHelper.getAdapter(categoriesDAO.findByCriteria(reference, limit), CategoriesDTO.class);
    }
    */

}
