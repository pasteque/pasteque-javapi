package org.pasteque.api.service.products;

import java.util.List;

import org.pasteque.api.dto.products.POSCompositionsDTO;

public interface ICompositionsService {

    List<POSCompositionsDTO> getAll();

}
