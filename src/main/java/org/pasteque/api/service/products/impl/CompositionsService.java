package org.pasteque.api.service.products.impl;

import java.util.List;

import org.pasteque.api.dao.products.ICategoriesApiDAO;
import org.pasteque.api.dao.products.IProductsApiDAO;
import org.pasteque.api.dto.products.POSCompositionsDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.products.Categories;
import org.pasteque.api.service.products.ICompositionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompositionsService implements ICompositionsService {

    @Autowired
    private IProductsApiDAO productsApiDAO;
    @Autowired
    private ICategoriesApiDAO categoriesApiDAO;

    @Override
    public List<POSCompositionsDTO> getAll() {
        Categories composition = categoriesApiDAO.findById(Categories.COMPOSITION).orElse(null);
        return AdaptableHelper.getAdapter(productsApiDAO.findByCategory(composition), POSCompositionsDTO.class);
    }

}
