package org.pasteque.api.service.security.impl;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.pasteque.api.dao.security.IRolesApiDAO;
import org.pasteque.api.dto.security.POSRolesDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.security.Roles;
import org.pasteque.api.service.security.IRolesService;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;


@Service
public class RolesService implements IRolesService {

    @Autowired
    private IRolesApiDAO rolesApiDAO;

    @Override
    public List<POSRolesDTO> getAll() {
        return AdaptableHelper.getAdapter(rolesApiDAO.findAll(), POSRolesDTO.class);
    }
    
    @Override
    public List<POSRolesDTO> getAllLog() {
        GenericSpecification<Roles> spec = new GenericSpecification<Roles>();
        spec.add(new SearchCriteria( Roles.Fields.NAME ,Roles.ROLE_LOG, SearchOperation.MATCH));
        return AdaptableHelper.getAdapter(rolesApiDAO.findAll(spec), POSRolesDTO.class);
    }

    @Override
    public POSRolesDTO getById(String id) {
        Optional<Roles> role  = rolesApiDAO.findById(id);
                return role.isPresent() ? role.get().getAdapter(POSRolesDTO.class) : null ;
    }
    
    @Override
    public Object save(POSRolesDTO dto) {
        Optional<Roles> optRole = ( dto.getId() == null ? Optional.ofNullable(null) : rolesApiDAO.findById(dto.getId()));
        Roles role = null ;
        if (! optRole.isPresent()) {
            role = new Roles();
            role.setId(dto.getId());
            role.setName(dto.getName());
            role.setPermissions(dto.getPermissions().getBytes(StandardCharsets.UTF_8));
            rolesApiDAO.saveAndFlush(role);
        } else {
            role = optRole.get();
            role.setName(dto.getName());
            role.setPermissions(dto.getPermissions().getBytes(StandardCharsets.UTF_8));
            rolesApiDAO.saveAndFlush(role);
        }
        return dto.getId() != null;
    }

    @Override
    public Object delete(POSRolesDTO dto) {
        Optional<Roles> role = ( dto.getId() == null ? null : rolesApiDAO.findById(dto.getId()));

        if (role.isPresent()) {
            rolesApiDAO.delete(role.get());
        }
        return true;
    }

}
