package org.pasteque.api.service.security;

import java.util.List;

import org.pasteque.api.model.application.ServerResponse;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;

import org.pasteque.api.dto.security.POSUsersDTO;
import org.pasteque.api.dto.security.UsersObfDTO;

public interface IUsersService extends UserDetailsService {

    List<POSUsersDTO> getAll();

    List<POSUsersDTO> getAllLog();

    <T> T getById(String id , Class<T> selector);

    ServerResponse updatePassword(String id, String oldPwd, String newPwd);

    boolean forceUpdatePassword(String id, String newPwd);

    boolean authenticate(String login, String password);

    UsersObfDTO getByName(String login, String password);

    List<GrantedAuthority> getAuthorities(String code);

    Object save(POSUsersDTO dto);

    Object delete(POSUsersDTO dto);

    List<POSUsersDTO> getAllByPermission(String permission);

    void createJobs(POSUsersDTO dto, String action);


}
