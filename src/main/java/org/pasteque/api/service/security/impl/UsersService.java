package org.pasteque.api.service.security.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import org.pasteque.api.dao.security.IPeopleApiDAO;
import org.pasteque.api.dao.security.IRolesApiDAO;
import org.pasteque.api.dto.security.POSUsersDTO;
import org.pasteque.api.dto.security.UsersObfDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.security.People;
import org.pasteque.api.model.security.Roles;
import org.pasteque.api.service.security.IUsersService;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.pasteque.api.specs.security.PeopleSpecification;



@Service
public class UsersService implements IUsersService {

    @Autowired
    private IPeopleApiDAO peopleApiDAO;
    @Autowired
    private IRolesApiDAO rolesApiDAO;
    /*
    @Autowired
    private ILocationsDAO locationDAO;
    @Autowired
    private IJobDAO jobDAO;
    */

    @Override
    public List<POSUsersDTO> getAll() {
        return AdaptableHelper.getAdapter(peopleApiDAO.findAll(), POSUsersDTO.class);
    }
    
    @Override
    public List<POSUsersDTO> getAllLog() {
        PeopleSpecification spec = new PeopleSpecification();
        spec.add(new SearchCriteria( People.Fields.ROLES ,Roles.ROLE_LOG, SearchOperation.MATCH));
        return AdaptableHelper.getAdapter(peopleApiDAO.findAll(spec), POSUsersDTO.class);
    }

    @Override
    public List<POSUsersDTO> getAllByPermission(String permission) {
        List<People> all = peopleApiDAO.findAll();
        List<People> filtered = new ArrayList<>();
        for (People people : all) {
            if (people.getRole().isAllowed(permission)) {
                filtered.add(people);
            }
        }
        return AdaptableHelper.getAdapter(filtered, POSUsersDTO.class);
    }

    @Override
    public <T> T getById(String id , Class<T> selector) {
        Optional<People> people =  peopleApiDAO.findById(id);
               return people.isPresent() ? people.get().getAdapter(selector) : null;
    }

    @Override
    public ServerResponse updatePassword(String id, String oldPwd, String newPwd) {
        Optional<People> people = peopleApiDAO.findById(id);
        if (!people.isPresent()) {
            return new ServerResponse(null, false);
        }

        if (people.get().getAppPassword() != null && !BCrypt.checkpw(oldPwd, people.get().getAppPassword())) {
            return new ServerResponse(null, false);
        }

        people.get().setAppPassword(BCrypt.hashpw(newPwd, BCrypt.gensalt(10)));
        try {
            peopleApiDAO.saveAndFlush(people.get());
        } catch (Exception exception) {
            new ServerResponse(null, false);
        }
        return new ServerResponse(people.get().getAppPassword(), true);
    }

    @Override
    public boolean forceUpdatePassword(String id, String newPwd) {

        Optional<People> people = peopleApiDAO.findById(id);
        if (people.isPresent()) {
            people.get().setAppPassword(BCrypt.hashpw(newPwd, BCrypt.gensalt(10)));

            try {
                peopleApiDAO.saveAndFlush(people.get());
            } catch (Exception exception) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean authenticate(String login, String password) {
        People people = peopleApiDAO.findByUsername(login);
        if (people != null && people.isActif()) {
            if (people.getAppPassword() != null) {
                return BCrypt.checkpw(password, people.getAppPassword());
            }
            return true;
        }
        return false;
    }

    @Override
    public UsersObfDTO getByName(String login, String password) {
        return peopleApiDAO.findByUsername(login).getAdapter(UsersObfDTO.class);
    }

    @Override
    public List<GrantedAuthority> getAuthorities(String code) {
        List<GrantedAuthority> retour = new ArrayList<GrantedAuthority>();
        People people = peopleApiDAO.findByUsername(code);

        if (people != null && people.getRole() != null) {
            retour = people.getRole().getAllowedOperations();
        }
        return retour;
    }

    @Override
    public Object save(POSUsersDTO dto) {
        Optional<People> people = peopleApiDAO.findById(dto.getId());
        Optional<Roles> role = dto.getRoleId() == null ? null : rolesApiDAO.findById(dto.getRoleId());
        People realPeople = null;
        
        if (! people.isPresent()) {
            realPeople = new People();
            realPeople.setId(dto.getId());
            realPeople.setUsername(dto.getName());
            realPeople.setNom(dto.getNom());
            realPeople.setPrenom(dto.getPrenom());
            realPeople.setVisible(dto.isVisible());
            realPeople.setRole(role.get());
            realPeople.setDdebut(dto.getDdebut() == null?new Date():dto.getDdebut());
            realPeople.setDfin(dto.getDfin());
            realPeople.setLastupdate(new Date());
            if (dto.getPassword() != null) {
                realPeople.setAppPassword(BCrypt.hashpw(dto.getPassword(), BCrypt.gensalt(10)));
            }
            dto.setDdebut(realPeople.getDdebut());
            peopleApiDAO.saveAndFlush(realPeople);
            //EDU TODO gérer les jobs
            //createJobs(dto, Job.Actions.INSERT);

        } else {
            Boolean modif = false;
            realPeople = people.get();
            if(dto.getName() != null && !dto.getName().equals(realPeople.getUsername())) {
                realPeople.setUsername(dto.getName());
                modif = true ;
            } else {
                dto.setName(null);
            }
            
            if(dto.getNom() != null && !dto.getNom().equals(realPeople.getNom())) {
                realPeople.setNom(dto.getNom());
                modif = true ;
            } else {
                dto.setNom(null);
            }
            
            if(dto.getPrenom() != null && !dto.getPrenom().equals(realPeople.getPrenom())) {
                realPeople.setPrenom(dto.getPrenom());
                modif = true ;
            } else {
                dto.setPrenom(null);
            }
            
            if(dto.isVisible() ^ realPeople.isVisible()) {
                realPeople.setVisible(dto.isVisible());
                modif = true ;
            }
            
            if(dto.getRoleId() != null && !dto.getRoleId().equals(realPeople.getRole() == null ? null : realPeople.getRole().getId())) {
                realPeople.setRole(role.get());
                modif = true ;
            } else {
                dto.setRoleId(null);
            }
            
            if((dto.getDfin() != null && !dto.getDfin().equals(realPeople.getDfin())) || (dto.getDfin() == null && realPeople.getDfin() != null)) {
                realPeople.setDfin(dto.getDfin());
                modif = true ;
            }
            
            if((dto.getDdebut() != null && !dto.getDdebut().equals(realPeople.getDdebut())) || (dto.getDdebut() == null && realPeople.getDdebut() != null)) {
                realPeople.setDdebut(dto.getDdebut());
                modif = true ;
            }

            //Plutôt que if(vrai){ truc qu ne sert à rien} else { action } 
            //On pourrait envisager if(faux) { action }
            if(    ( dto.getPassword() != null && dto.getPassword().startsWith("$2a$10$") && dto.getPassword().length() == 60 ) 
                || (dto.getPassword() == null) 
                || (dto.getPassword() != null && realPeople.getAppPassword() != null && BCrypt.checkpw(dto.getPassword(), realPeople.getAppPassword()))) {
                    dto.setPassword(realPeople.getAppPassword());
                } else {
                    realPeople.setAppPassword(BCrypt.hashpw(dto.getPassword(), BCrypt.gensalt(10)));
                    modif = true ;
            }
            //TODO gestion propagation versions encryptées
            //TODO interdire mot de passe vide ?
            
            if(modif) {
                realPeople.setLastupdate(new Date());
                peopleApiDAO.saveAndFlush(realPeople);
                //EDU TODO gérer job
                //createJobs(dto, Job.Actions.UPDATE);
            }
        }
        return true;
    }

    @Override
    public Object delete(POSUsersDTO dto) {
        Optional<People> people = peopleApiDAO.findById(dto.getId());
        boolean response = true;
        if (people.isPresent()) {
            try {
            peopleApiDAO.delete(people.get());
            }
            catch (Exception ex) {
                response = false;
            }
        }
        return response;
    }


    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        People user = peopleApiDAO.findByUsername(username);
        return user == null ? null : user.getAdapter(UsersObfDTO.class);
    }

    @Override
    public void createJobs(POSUsersDTO dto, String action) {
        // TODO Auto-generated method stub
        
    }


}
