package org.pasteque.api.service.security;

import java.util.List;

import org.pasteque.api.dto.security.POSRolesDTO;

public interface IRolesService {

    POSRolesDTO getById(String id);

    List<POSRolesDTO> getAll();

    Object delete(POSRolesDTO dto);

    Object save(POSRolesDTO dto);

    List<POSRolesDTO> getAllLog();

}
