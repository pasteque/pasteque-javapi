package org.pasteque.api.service.fiscal;

import java.util.Date;
import java.util.List;

import org.pasteque.api.model.archives.ArchiveRequests;
import org.springframework.util.MultiValueMap;

public interface IArchiveRequestsService {

    List<ArchiveRequests> getByCriteria(MultiValueMap<String, String> map, Integer limit, Integer pageNumber);
    void doRequest();
    ArchiveRequests addRequest(Date startDate ,Date stopDate);
}
