package org.pasteque.api.service.fiscal;

import org.pasteque.api.model.signable.ISignable;

public interface ISignableService {

    void sign(ISignable current , ISignable sign);
    
    boolean checkSignature(ISignable current , ISignable previous);
}
