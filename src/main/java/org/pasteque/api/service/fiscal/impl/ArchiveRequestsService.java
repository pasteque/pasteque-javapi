package org.pasteque.api.service.fiscal.impl;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.fiscal.IArchiveRequestsApiDAO;
import org.pasteque.api.model.archives.ArchiveRequests;
import org.pasteque.api.service.fiscal.IArchiveRequestsService;
import org.pasteque.api.service.fiscal.IArchivesService;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

@Service
public class ArchiveRequestsService implements IArchiveRequestsService {

    @Autowired
    IArchiveRequestsApiDAO archiveRequestsApiDAO;

    @Autowired
    IArchivesService archivesService;

    @Override
    public List<ArchiveRequests> getByCriteria(
            MultiValueMap<String, String> map, Integer limit,
            Integer pageNumber) {
        GenericSpecification<ArchiveRequests> specs = new GenericSpecification<ArchiveRequests>();
        List<ArchiveRequests> l = null ;
        for(String str : map.keySet()){
            List<String> values = map.get(str);
            if(values.size() == 1) {

                SearchOperation operation = SearchOperation.EQUAL ;
                if(ArchiveRequests.Fields.START_DATE.equals(str)) {
                    operation = SearchOperation.GREATER_THAN_EQUAL ;
                }
                if(ArchiveRequests.Fields.STOP_DATE.equals(str)) {
                    operation = SearchOperation.LESS_THAN_EQUAL ;
                }
                specs.add(new SearchCriteria(str , values.get(0) , operation ));
            }
        }
        // il y a un tri par défaut et une limite optionnelle
        Sort sort = Sort.by(ArchiveRequests.Fields.ID).descending() ;

        if(limit != null && limit > 1) { 
            if(pageNumber == null || pageNumber < 1 ) {
                pageNumber = 0 ;
            }
            Page<ArchiveRequests> p = archiveRequestsApiDAO.findAll(specs , PageRequest.of(pageNumber, limit , sort) );
            l = p.getContent();
        } else {
            l = archiveRequestsApiDAO.findAll(specs , sort );
        }
        return l;
    }

    //TODO intervalle d'éxécution des requêtes d'archivage paramétrable ?
    @Override
    //@Scheduled(fixedRate = 120000)
    @Scheduled(cron = "${cron.archive-requests-schedule:-}")
    public void doRequest() {
        /**
         * On vérifie que la requête existe
         * Qu'elle n'est pas en cours d'exécution
         * Qu'on peut la mettre en exécution ( = la sauver avec le flag à true)
         * 
         * Et si catch une exception ou un retour malheureux -> on y reset l'execution
         */
        ArchiveRequests request = getNextRequestToLaunch();
        if(markProcessing(request)) {
            try {
                archivesService.create(request);
            }catch(Exception e) {
                e.printStackTrace();
                cancelProcessing(request);
            }
        }

    }

    @Override
    public ArchiveRequests addRequest(Date startDate, Date stopDate) {
        //TODO retravailler les levées d'exceptions
        ArchiveRequests retour = null;
        Date current = new Date();
        if(startDate != null && current.after(startDate) 
                && stopDate != null && current.after(stopDate) 
                && stopDate.after(startDate)) {

            LocalDate start = new java.sql.Date(startDate.getTime()).toLocalDate();
            LocalDate end = new java.sql.Date(stopDate.getTime()).toLocalDate();
            end = end.minusYears(1);

            //Pas plus d'une année d'écart.
            if(start.isAfter(end)) {
                retour = new ArchiveRequests();
                retour.setStartDate(startDate);
                retour.setStopDate(stopDate);
                retour.setProcessing(false);
                retour = archiveRequestsApiDAO.saveAndFlush(retour);
            }
        }
        return retour;

    }

    public boolean markProcessing(ArchiveRequests request) {
        boolean retour = false;
        if(request != null && ! request.isProcessing()) {
            request.setProcessing(true);
            request = archiveRequestsApiDAO.saveAndFlush(request);
            retour = true;
        }
        return retour;
    }

    public boolean cancelProcessing(ArchiveRequests request) {
        boolean retour = false;
        if(request != null && request.isProcessing()) {
            request.setProcessing(false);
            request = archiveRequestsApiDAO.saveAndFlush(request);
            retour = true;
        }
        return retour;
    }
    /**
     * Equivalent de la méthode getFirstAvailableRequest de la V8 actuelle
     * On l'a renommé car on renvoie la première dispo si et seulement si 
     * il n'y en a pas en cours d'éxécution 
     * @return La prochaine requete à satisfaire si il n'y en a pas en cours d'éxécution
     */
    public ArchiveRequests getNextRequestToLaunch() {
        ArchiveRequests retour = null;
        retour = archiveRequestsApiDAO.findTopByProcessingTrue();
        if(retour == null) {
            retour = archiveRequestsApiDAO.findTopByProcessingFalseOrderByIdAsc();
        } else {
            retour = null;
        }
        return retour;
    }

}
