package org.pasteque.api.service.fiscal.impl;

import java.util.Date;

import org.pasteque.api.dao.cash.ITaxesApiDAO;
import org.pasteque.api.model.products.TaxCategories;
import org.pasteque.api.model.products.TaxCustCategories;
import org.pasteque.api.model.products.Taxes;
import org.pasteque.api.service.fiscal.ITaxesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaxesService implements ITaxesService {
    
    @Autowired
    private ITaxesApiDAO taxesApiDAO;
    
    @Override
    public Taxes findCurrentByCategoryAndCustCategory(TaxCategories vatCateg , TaxCustCategories custCateg){
        return taxesApiDAO.findByCategoryAndCustCategoryAndValidFromBeforeAndValidToIsNull(vatCateg , custCateg , new Date() ).stream().findFirst()
                .orElse(taxesApiDAO.findByCategoryAndCustCategoryAndValidFromBeforeAndValidToAfter(vatCateg , custCateg , new Date() , new Date() ).stream().findFirst().orElse(null));
    }

}
