package org.pasteque.api.service.fiscal;

import java.util.List;

import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.model.fiscal.Periods;
import org.pasteque.api.model.tickets.Tickets;

public interface IPeriodsService {

    List<String> getTypes();

    void addTickets(Tickets ticket, boolean newCust) throws APIException;

    Periods findByDescription(String period);

}
