package org.pasteque.api.service.fiscal.impl;

import org.pasteque.api.model.signable.ISignable;
import org.pasteque.api.service.fiscal.ISignableService;
import org.springframework.security.crypto.bcrypt.BCrypt;


public class SignableService implements ISignableService {

    @Override
    public void sign(ISignable current, ISignable sign) {
        String data = getSignBase(current , sign);
        String signature = BCrypt.hashpw(data, BCrypt.gensalt());
        if (signature == null) {
            signature = "";
            //TODO gerer l'erreur
        }
        current.setSignature(signature);       

    }

    @Override
    public boolean checkSignature(ISignable current,
            ISignable previous) {
        String data = getSignBase(current , previous);
        return BCrypt.checkpw(data , current.getSignature());
    }

    String getSignBase(ISignable current , ISignable sign) {
        String data = null;
        if (sign == null) {
            data = current.getHashBase();
        } else {
            data = sign.getSignature() + "-" + current.getHashBase();
        }
        return data;
    }

}
