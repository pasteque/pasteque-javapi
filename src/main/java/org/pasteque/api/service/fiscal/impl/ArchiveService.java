package org.pasteque.api.service.fiscal.impl;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.util.io.Streams;
import org.pasteque.api.dao.fiscal.IArchiveRequestsApiDAO;
import org.pasteque.api.dao.fiscal.IArchivesApiDAO;
import org.pasteque.api.dto.fiscal.ArchivesDTO;
import org.pasteque.api.dto.fiscal.ArchivesMetaDataDTO;
import org.pasteque.api.dto.fiscal.FiscalTicketsDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.archives.ArchiveRequests;
import org.pasteque.api.model.archives.Archives;
import org.pasteque.api.model.fiscal.FiscalTickets;
import org.pasteque.api.service.cash.ICashRegistersService;
import org.pasteque.api.service.fiscal.IArchivesService;
import org.pasteque.api.service.fiscal.IFiscalTicketsService;
import org.pasteque.api.util.DateHelper;
import org.pasteque.api.util.PastequeAPIKeyringConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import name.neuhalfen.projects.crypto.bouncycastle.openpgp.BouncyGPG;

@Service
public class ArchiveService extends SignableService implements IArchivesService {

    @Autowired
    IArchivesApiDAO archivesApiDAO;
    
    @Autowired
    IArchiveRequestsApiDAO archiveRequestsApiDAO;

    @Autowired
    IFiscalTicketsService fiscalTicketsService;
    
    @Autowired
    ICashRegistersService cashRegistersService;
    
    @Value("${signing.passphrase:-}")
    String passphrase;
    
    @Value("${signing.private-key:-}")
    String privateKey;
    
    @Value("${signing.public-key:-}")
    String publicKey;
    
    @Value("${signing.sender:-}")
    String sender;

    @Override
    public Archives getById(Integer number) {
        return archivesApiDAO.findById(number).orElse(null);
    }

    @Override
    public Archives getLastArchive() {
        Archives retour = archivesApiDAO.findTopByOrderByNumberDesc();
        return retour != null && retour.getNumber() > 0 ? retour : null ;
    }

    @Override
    public List<ArchivesDTO> getList() {
        return AdaptableHelper.getAdapter(archivesApiDAO.findAll() , ArchivesDTO.class);
    }

    @Override
    public void updateEOSArchive(Archives last) {
        if(last != null) {
            Archives eos = getById(Archives.EOS);
            if(eos == null) {
                eos = new Archives();
                eos.setNumber(Archives.EOS);
                eos.setInfo(Archives.EOS_STRING);
                eos.setContent(Archives.EOS_STRING.getBytes(StandardCharsets.UTF_8));
            }
            sign(eos , last);
            archivesApiDAO.save(eos);
        }

    }

    @Override
    public boolean create(ArchiveRequests request) throws IOException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException, PGPException {
        // TODO lever les bonnes exceptions

        /**
         * Pour toutes les séquences = caisses
         * Pour tous les types de tickets fiscaux ( Z , jour , mois , année fiscale ...) EOS exclu
         * On crée un fichier texte pour chaque lot de X (pagination - 100 en dur ? ) tickets 
         * qui appartiennent à l'intervalle de date de la demande nommé '%s-%s-%d.txt', type, seq, (page + 1)
         * les tickets y sont stockés au format json sous forme de liste
         * 
         * 
         * Ensuite on crée un fichier récapitulatif nommé archive.txt contenant un objet json
         *          'account' le demandeur,
                    'dateStart' au format('Y-m-d H:i:s'),
                    'dateStop' au format('Y-m-d H:i:s'),
                    'generated' l'instant de la génération au format('Y-m-d H:i:s'),
                    'number' le numéro de l'archive,
                    'purged' toujours false chez nous ( est ce que les données ont été purgées),
         * 
         * Enfin on crée un zip archive.zip contenant tous ces fichiers
         * 
         * Puis on crée l'objet Archive dans lequel on stocke :
         * info : notre objet descriptif de l'archive cf ci-dessus
         * content : le zip signé
         * number : le numéro
         * 
         * On signe
         * On sauve
         * On vire la requête ? Me semble douteux si un pb devait arriver maintenant plus de retour en arrière possible
         * Néanmoins vu qu'on a processing true ou false et non null, tel quel on n'a pas le choix 
         * On update EOS
         * 
         * On efface les fichiers temporaires
         * 
         * On commit l'ensemble
         */
        //TODO limite de la pagination paramétrable
        Integer limit = 100;
        List<String> types = fiscalTicketsService.getTypes();
        List<String> sequences = cashRegistersService.getAll().stream()
                .map(x -> x.getId()).collect(Collectors.toList());
        String fileName = null ;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(bos);
        ObjectMapper mapper = new ObjectMapper();
        byte[] bytes = null;
        ZipEntry zipEntry = null;
        
        //On construit les specs
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        if (request.getStartDate() != null ) {
            map.add(FiscalTickets.SearchCritria.FROM, DateHelper.formatSQLTimeStamp(request.getStartDate()));
        }
        if (request.getStopDate() != null ) {
            map.add(FiscalTickets.SearchCritria.TO, DateHelper.formatSQLTimeStamp(request.getStopDate()));
        }
        map.add(FiscalTickets.SearchCritria.EXCLUDE_NUMBER, String.valueOf(FiscalTickets.EOS));
        
        for(String sequence : sequences) {
            map.remove(FiscalTickets.Fields.SEQUENCE);
            map.add(FiscalTickets.Fields.SEQUENCE, sequence);
            for(String type : types) {
                Integer pageNumber = 0;    
                Boolean done = false;
                map.remove(FiscalTickets.Fields.TYPE);
                map.add(FiscalTickets.Fields.TYPE, type);
                FiscalTickets previous = null;

                while(!done) {
                    ArrayList<FiscalTicketsDTO> dtoList = new ArrayList<FiscalTicketsDTO>();
                    List<FiscalTickets> tickets = fiscalTicketsService.findByCriteria(map, limit, pageNumber,true);
                    if(tickets.isEmpty()) {
                        done = true;
                    } else {
                        pageNumber++;
                        fileName = String.format("%s-%s-%d.txt", type , sequence , pageNumber);
                        
                        zipEntry = new ZipEntry(fileName);
                        zip.putNextEntry(zipEntry);
                        
                        for(FiscalTickets ticket : tickets) {
                        //checkSignature sur tous les tickets
                            FiscalTicketsDTO dto = ticket.getAdapter(FiscalTicketsDTO.class);
                            if(previous == null && ticket.getNumber()-1 != FiscalTickets.EOS ) {
                                previous = fiscalTicketsService.getById(ticket.getSequence() , ticket.getType() , ticket.getNumber()-1);
                            }
                            dto.setSignatureOK(checkSignature(ticket, previous));
                            previous = ticket;
                            dtoList.add(dto);
                            
                        }
                        bytes = mapper.writeValueAsString(dtoList).getBytes(StandardCharsets.UTF_8);
                        zip.write(bytes);
                    }
                }
            }
        }
        
        LocalDateTime maintenant = LocalDateTime.now();
        Archives lastArchive = getLastArchive();
        Integer archNumber = 1 + ( lastArchive == null ? 0 : lastArchive.getNumber()) ;
        fileName = "archive.txt";
        ArchivesMetaDataDTO dto = new ArchivesMetaDataDTO();
        dto.setGenerated(maintenant);
        dto.setNumber(archNumber);
        dto.setPurged(false);
        dto.setDateStart(new Timestamp(request.getStartDate().getTime()).toLocalDateTime());
        dto.setDateStop(new Timestamp(request.getStopDate().getTime()).toLocalDateTime());
        //TODO voir comment on peut savoir qui a lancé la requete
        dto.setAccount("-");
        
        zipEntry = new ZipEntry(fileName);
        zip.putNextEntry(zipEntry);
        
        bytes = mapper.writeValueAsString(dto).getBytes(StandardCharsets.UTF_8);

        zip.write(bytes);
        zip.close();
        bytes = bos.toByteArray();
        
        //On a tout le zip dans bytes
        //Sans écrire quoi que ce soit sur le disque
        
        //Le zip est fini - Maintenant on chiffre si on a des infos
        
        if(! "-".equals(privateKey) && ! "-".equals(publicKey) && ! "-".equals(sender) ) {
        //On Signe et chiffre
        
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(result);
        //TODO paramétrer les receptionnaires / leur clef privée
        PastequeAPIKeyringConfig papi = new PastequeAPIKeyringConfig(passphrase , privateKey , publicKey , sender);
       //On envoie à soi-même
        try (
            final OutputStream outputStream = BouncyGPG
                    .encryptToStream()
                    .withConfig(papi.keyring())
                    .withStrongAlgorithms()
                    .toRecipients(papi.getSender())
                    .andSignWith(papi.getSender())
                    .armorAsciiOutput()
                    .andWriteTo(bufferedOutputStream);
            final ByteArrayInputStream is = new ByteArrayInputStream(bytes);
         ){   
            Streams.pipeAll(is, outputStream);
        }
        result.close();
        bytes = result.toByteArray();
        
        }
        
        Archives archive = new Archives();
        archive.setInfo(mapper.writeValueAsString(dto));
        archive.setContent(bytes);
        archive.setNumber(archNumber);
        sign(archive , lastArchive);
        
        archive = archivesApiDAO.save(archive);
        updateEOSArchive(archive);
        archivesApiDAO.flush();
        archiveRequestsApiDAO.delete(request);
        return archive != null ;
    }





}
