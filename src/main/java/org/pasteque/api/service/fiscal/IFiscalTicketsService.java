package org.pasteque.api.service.fiscal;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipOutputStream;

import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.model.fiscal.FiscalTickets;
import org.pasteque.api.model.fiscal.Periods;
import org.pasteque.api.model.tickets.Tickets;
import org.springframework.util.MultiValueMap;

public interface IFiscalTicketsService extends ISignableService{

    Long countTickets(MultiValueMap<String, String> map);
    Long countZ(MultiValueMap<String, String> map);
    Long countFiscalTickets(MultiValueMap<String, String> map);
    List<FiscalTickets> listTickets(MultiValueMap<String, String> map, Integer limit, Integer pageNumber);
    List<FiscalTickets> listZ(MultiValueMap<String, String> map, Integer limit, Integer pageNumber);
    List<FiscalTickets> findByCriteria(MultiValueMap<String, String> map, Integer limit, Integer pageNumber, boolean naturalSorting);
    
    void updateEOSFiscalTicket(FiscalTickets last);
    
    List<FiscalTickets> fiscalExport(Date dateStart , Date dateStop);
    void fiscalImport(List<FiscalTickets> tickets);
    List<String> getTypes();
    FiscalTickets getById(String sequence, String type, int number);
    
    Tickets write(Tickets tickets) throws APIException;
    ClosedCash write(ClosedCash closedCash) throws APIException;
    Periods write(Periods period) throws APIException;
    void createZipExport(ZipOutputStream zipOutputStream, Date dateStart, Date dateStop) throws IOException;
    Boolean fiscalImport(FiscalTickets tickets) throws APIException;
    
}
