package org.pasteque.api.service.fiscal.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.pasteque.api.dao.application.IApplicationApiDAO;
import org.pasteque.api.dao.fiscal.IPeriodCustBalancesApiDAO;
import org.pasteque.api.dao.fiscal.IPeriodsApiDAO;
import org.pasteque.api.dao.products.ICategoriesApiDAO;
import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.model.fiscal.Periods.Types;
import org.pasteque.api.model.products.Categories;
import org.pasteque.api.model.fiscal.FiscalTickets;
import org.pasteque.api.model.fiscal.PeriodCatTaxes;
import org.pasteque.api.model.fiscal.PeriodCats;
import org.pasteque.api.model.fiscal.PeriodCustBalances;
import org.pasteque.api.model.fiscal.PeriodCustBalancesId;
import org.pasteque.api.model.fiscal.PeriodPayments;
import org.pasteque.api.model.fiscal.PeriodTaxes;
import org.pasteque.api.model.fiscal.Periods;
import org.pasteque.api.model.tickets.Payments;
import org.pasteque.api.model.tickets.TaxLines;
import org.pasteque.api.model.tickets.TicketLines;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.service.fiscal.IFiscalTicketsService;
import org.pasteque.api.service.fiscal.IPeriodsService;
import org.pasteque.api.service.tickets.ITicketsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PeriodsService implements IPeriodsService {

    @Autowired
    private IFiscalTicketsService fiscalTicketsService;
    @Autowired
    IPeriodsApiDAO periodsApiDAO;
    @Autowired
    private IApplicationApiDAO applicationApiDAO;
    @Autowired
    private ICategoriesApiDAO categoriesApiDAO;
    @Autowired
    private IPeriodCustBalancesApiDAO periodCustBalancesApiDAO;
    @Autowired
    private ITicketsService ticketsService;
    
    private Logger logger = LoggerFactory.getLogger(PeriodsService.class);

    @Override
    public
    List<String> getTypes(){
        Periods jobject = new Periods();
        Types object = jobject.new Types();
        List<String> retour = new ArrayList<String>();
        Field[] fields = Periods.Types.class.getDeclaredFields();
        for (Field f : fields) {
            if (Modifier.isStatic(f.getModifiers()) && f.getType() == String.class) {
                try {
                    retour.add((String)f.get(object));
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } 
        }
        return retour;
    }


    @Override
    /**
     * Mettre à jour les périodes avec le ticket fourni
     * Il s'agit aussi de fermer la période si le ticket n'est pas dans une période ouverte
     * Et d'ouvrir la période suivante
     */
    public void addTickets(Tickets ticket , boolean newCust) throws APIException {
        List<String> types = getTypes();
        String rejectReason = null;
        Periods period = null;
        if(ticket == null || ticket.getReceipts() == null || ticket.getReceipts().getClosedCash() == null ) {
            rejectReason = "Tentative de mise à jour de périodes avec un ticket malformé";
            throw new APIException(rejectReason);
        }
        CashRegisters caisse = ticket.getReceipts().getClosedCash().getCashRegister();
        if(caisse == null || caisse.getId() == null) {
            rejectReason = "Tentative de mise à jour de périodes avec un ticket sans caisse";
            throw new APIException(rejectReason);            
        }

        for(String type : types) {  
            //On va récupérer les périodes ouvertes. 
            //Il doit y en avoir une et une seule pour chaque type continuous et au plus une pour les non continuous
            //Comme nous sommes dans le cas d'un ajout de ticket, même une discontinue devra exister
            List<Periods> periods = periodsApiDAO.findAllByTypeAndCashRegisterAndClosedFalseOrderBySequenceDesc(type , caisse);
            //Si il n' en a pas pour un type donné on crée la séquence et on crée la période 1

            if(periods.isEmpty()) {
                period = initPeriods(type , caisse);
            } else {
                if(periods.size() > 1) {
                    forceCloseOldPeriods(periods);
                    rejectReason = "Tentative de mise à jour de périodes - Trop de périodes ouvertes pour la caisse "+ caisse.getId() + " et le type : " + type;
                    //throw new APIException(rejectReason);
                    logger.warn(rejectReason);
                }
                period = periods.get(0);
            }
            if(period.getDateStart().after(ticket.getDate())) {
                period.setDateStart(ticket.getDate());
                periodsApiDAO.saveAndFlush(period);
            }
            while(! isSamePeriod(period, ticket.getDate())) {
                //Tant que nous sommes en dehors de la période
                //On ferme sauve et crée la suivante ( mode continuous )
                period = closePeriod(period);
            }

            //On doit maintenant avoir une période ouverte valide
            //On update les totaux
            Double amount = ticket.getFinalTaxedPrice();
            period.setCs(period.getCs() + amount);
            period.setCsFYear(period.getCsFYear() + amount);
            period.setCsPeriod(period.getCsPeriod() + amount);
            period.setCsPerpetual(period.getCsPerpetual() + amount);
            period.setTicketCount(period.getTicketCount() + 1);

            //Pas toujours plus 1 - tickets retours et/ modifs
            period.setCustCount(period.getCustCount() + ( newCust ? 1 : 0));

            //Gestion des listes

            List<PeriodCats> periodCats = period.getPeriodCats();
            PeriodCats periodCat = null;
            if(periodCats == null) {
                periodCats = new ArrayList<PeriodCats>(); 
                period.setPeriodCats(periodCats);
            }

            List<PeriodCatTaxes> periodCatTaxes = period.getPeriodCatTaxes();
            PeriodCatTaxes periodCatTaxe = null ;
            if(periodCatTaxes == null) {
                periodCatTaxes = new ArrayList<PeriodCatTaxes>(); 
                period.setPeriodCatTaxes(periodCatTaxes);
            }

            List<PeriodCustBalances> periodCustBalances = period.getPeriodCustBalances();
            PeriodCustBalances periodCustBalance = null ;
            if(periodCustBalances == null) {
                periodCustBalances = new ArrayList<PeriodCustBalances>(); 
                period.setPeriodCustBalances(periodCustBalances);
            }


            List<PeriodPayments> periodPayments = period.getPeriodPayments();
            PeriodPayments periodPayment = null;
            if(periodPayments == null) {
                periodPayments = new ArrayList<PeriodPayments>(); 
                period.setPeriodPayments(periodPayments);
            }


            List<PeriodTaxes> periodTaxes = period.getPeriodTaxes();
            PeriodTaxes periodTaxe = null;
            if(periodTaxes == null) {
                periodTaxes = new ArrayList<PeriodTaxes>(); 
                period.setPeriodTaxes(periodTaxes);
            }


            //là on ne va pas faire le tour des clients potentiellement il y en aura trop on va chercher dans la base.
            //On rempli
            if(ticket.getCustomer() != null) {
                periodCustBalance = periodCustBalancesApiDAO.findById(new PeriodCustBalancesId(period.getId() , ticket.getCustomerId())).orElse(null);
                if (periodCustBalance == null ) {
                    periodCustBalance = new PeriodCustBalances();
                    periodCustBalance.setPeriod(period);
                    periodCustBalance.setCustomer(ticket.getCustomer());
                    periodCustBalance.setBalance(0D);
                    periodCustBalances.add(periodCustBalance);
                }

                periodCustBalance.setBalance(periodCustBalance.getBalance() + ticket.getCustBalance());
            }

            for(Payments mdp : ticket.getReceipts().getPayments()) {
                //Moyen de paiement par moyen de paiement
                //il y a peu de moyens de paiement on fait le tour
                for(PeriodPayments element : periodPayments) {
                    if(mdp.getPayment().equals(element.getPaymentModeId()) && mdp.getCurrency().getId().equals(element.getCurrency().getId())) {
                        periodPayment = element ;
                        break;
                    }
                }
                if( periodPayment == null ) {
                    //on crée et on ajoute à la liste
                    periodPayment = new PeriodPayments();
                    periodPayment.setAmount(0D);
                    periodPayment.setPeriod(period);
                    periodPayment.setPaymentModeId(mdp.getPayment());
                    periodPayment.setCurrency(mdp.getCurrency());
                    periodPayment.setCurrencyAmount(0D);
                    periodPayments.add(periodPayment);
                }
                periodPayment.setAmount(periodPayment.getAmount() + mdp.getTotal());
                periodPayment.setCurrencyAmount(periodPayment.getCurrencyAmount() + mdp.getTotalCurrency());
            }

            for(TaxLines taxLine : ticket.getReceipts().getTaxes()) {
                //il y a peu de taxes on fait le tour
                for(PeriodTaxes element : periodTaxes) {
                    if(element.getTax().getId().equals(taxLine.getTaxesId())) {
                        periodTaxe = element ;
                        break;
                    }
                }
                if( periodTaxe == null ) {
                    //on crée et on ajoute à la liste
                    periodTaxe = new PeriodTaxes();
                    periodTaxe.setTax(taxLine.getTaxes());
                    periodTaxe.setPeriod(period);
                    periodTaxe.setAmount(0D);
                    periodTaxe.setAmountFYear(0D);
                    periodTaxe.setAmountPeriod(0D);
                    periodTaxe.setBase(0D);
                    periodTaxe.setBaseFYear(0D);
                    periodTaxe.setBasePeriod(0D);
                    periodTaxe.setLabel(taxLine.getTaxes().getName());
                    periodTaxe.setRate(taxLine.getTaxes().getRate());
                    periodTaxes.add(periodTaxe);
                }
                periodTaxe.setAmount(periodTaxe.getAmount() + taxLine.getAmount());
                periodTaxe.setBase(periodTaxe.getBase() + taxLine.getBase());
                periodTaxe.setAmountPeriod(periodTaxe.getAmountPeriod() + taxLine.getAmount());
                periodTaxe.setBasePeriod(periodTaxe.getBasePeriod() + taxLine.getBase());
                periodTaxe.setAmountFYear(periodTaxe.getAmountFYear() + taxLine.getAmount());
                periodTaxe.setBaseFYear(periodTaxe.getBaseFYear() + taxLine.getBase());
            }

            for(TicketLines ligne : ticket.getLines()) {
                //Ligne de vente par ligne de vente
                //il n'y a pas tellement de catégories on se permet de faire le parcours
                Categories categ = ligne.getProduct() == null ? categoriesApiDAO.findById(Categories.DEFAULT_CATEGORY).get() : ligne.getProduct().getCategory() ;
                String categId = categ.getId() ;
                Double totalPriceTaxIncl = ticketsService.computeFinalPrice(ticket.getDiscountRate(),ligne);
                Double totalPriceTaxExcl = Math.rint(100*ligne.getFinalPrice() * ( 1 - ticket.getDiscountRate()))/100;

                for( PeriodCats element : periodCats ) {
                    if(element.getCategory().getId().equals(categId)) {
                        periodCat = element;
                        break;
                    }
                }
                if( periodCat == null ) {
                    //on crée et on ajoute à la liste
                    periodCat = new PeriodCats();
                    periodCat.setPeriod(period);
                    periodCat.setAmount(0D);
                    periodCat.setCategory(categ);
                    periodCat.setLabel(categ.getName());
                    periodCats.add(periodCat);

                }

                //TODO voir si on passe par le champ calculé

                periodCat.setAmount(periodCat.getAmount() + totalPriceTaxIncl );

                //Il n'y a pas des milliers de combo catégories/ taxes on se permet de faire le parcours
                for(PeriodCatTaxes element : periodCatTaxes) {
                    if(element.getCategory().getId().equals(categId) && element.getTax().getId().equals(ligne.getTax().getId())) {
                        periodCatTaxe = element;
                        break;
                    }
                }
                if( periodCatTaxe == null ) {
                    //on crée et on ajoute à la liste
                    periodCatTaxe = new PeriodCatTaxes();
                    periodCatTaxe.setAmount(0D);
                    periodCatTaxe.setBase(0D);
                    periodCatTaxe.setCategory(categ);
                    periodCatTaxe.setPeriod(period);
                    periodCatTaxe.setTax(ligne.getTax());
                    periodCatTaxe.setLabel(ligne.getTax().getName());

                    periodCatTaxes.add(periodCatTaxe);
                }
                periodCatTaxe.setBase(periodCatTaxe.getBase() + totalPriceTaxExcl);
                periodCatTaxe.setAmount(periodCatTaxe.getAmount() + totalPriceTaxIncl - totalPriceTaxExcl);

            }

            periodsApiDAO.save(period);
        }
    }

    private void forceCloseOldPeriods(List<Periods> periods) {
        //EDU 2021 03
        // Ici nous voulons forcer la clôture sans autre forme de procès plutôt que de lever une exception
        //Car en prod ça demade une intervention en base pour corriger et bloque la caisse avec les risques sous jacents
        Date dateEnd = periods.get(0).getDateStart(); 
        for(int i = 1 ; i < periods.size() ; i++) {
            Periods period = periods.get(i);
            period.setClosed(true);
            
            period.setDateEnd(dateEnd);
            dateEnd = period.getDateStart();
            try {
                period = fiscalTicketsService.write(period);
            } catch (APIException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            periodsApiDAO.saveAndFlush(period);
        }
    }


    private Periods closePeriod(Periods period) throws APIException {
        boolean keepFYear = false ;
        boolean keepPeriod = false ;
        Periods retour = new Periods();
        //On ferme
        period.setClosed(true);
        period.setDateEnd(getMaxDate(period));
        period = fiscalTicketsService.write(period);

        periodsApiDAO.saveAndFlush(period);

        //On ouvre
        retour.setCashRegister(period.getCashRegister());
        retour.setType(period.getType());
        retour.setSequence(period.getSequence() + 1);
        retour.setDateStart(new Date(period.getDateEnd().getTime() + 1));
        retour.setId(createId(retour));
        retour.setClosed(false);
        retour.setContinuous(true);
        retour.setCs(0D);
        retour.setCustCount(0L);
        retour.setTicketCount(0L);
        retour.setCsPerpetual(period.getCsPerpetual());

        keepFYear = isSamePeriod(period.getDateStart(), retour.getDateStart(), Periods.Types.FYEAR) ;
        keepPeriod = isSamePeriod(period.getDateStart(), retour.getDateStart(), Periods.Types.MOIS) ;
        retour.setCsFYear( keepFYear ? period.getCsFYear() : 0);
        retour.setCsPeriod(keepPeriod ?period.getCsPeriod() : 0);

        List<PeriodTaxes> liste = new ArrayList<PeriodTaxes>();
        //Pour les taxes il faut reprendre pour avoir les cumuls Mois et Année Fiscal
        retour.setPeriodTaxes(liste );

        if(keepFYear || keepPeriod) {
            if(period.getPeriodTaxes() != null && ! period.getPeriodTaxes().isEmpty()) {
                for(PeriodTaxes previous : period.getPeriodTaxes()) {
                    PeriodTaxes current = new PeriodTaxes();
                    current.setLabel(previous.getLabel());
                    current.setPeriod(retour);
                    current.setRate(previous.getRate());
                    current.setTax(previous.getTax());
                    current.setAmount(0D);
                    current.setBase(0D);
                    current.setBaseFYear(keepFYear ? previous.getBaseFYear(): 0D);
                    current.setAmountFYear(keepFYear ? previous.getAmountFYear(): 0D);
                    current.setBasePeriod(keepPeriod ? previous.getBasePeriod(): 0D);
                    current.setAmountPeriod(keepPeriod ? previous.getAmountPeriod(): 0D);
                    liste.add(current);
                }
            }
        }
        periodsApiDAO.save(retour);
        return retour;
    }


    private Periods initPeriods(String type, CashRegisters caisse) {
        Periods retour = new Periods();
        retour.setSequence(FiscalTickets.EOS);
        retour.setDateStart(new Date());
        retour.setType(type);
        retour.setCashRegister(caisse);
        retour.setId(createId(retour));
        retour.setClosed(true);
        periodsApiDAO.saveAndFlush(retour);
        Periods prems = new Periods();
        prems.setClosed(false);
        prems.setSequence(1);
        prems.setDateStart(retour.getDateStart());
        prems.setType(type);
        prems.setCashRegister(caisse);
        prems.setId(createId(prems));
        prems.setCs(0D);
        prems.setCsFYear(0D);
        prems.setCsPeriod(0D);
        prems.setCsPerpetual(0D);
        prems.setCustCount(0L);
        prems.setContinuous(true);
        prems.setTicketCount(0L);
        periodsApiDAO.saveAndFlush(prems);
        return prems ;
    }


    private String createId(Periods period) {
        String retour ="";
        if(period != null) {
            retour += period.getType() +"-"+ period.getCashRegister().getId()+"-"+String.valueOf(period.getSequence());
        }
        return retour ;
    }


    /**
     * 
     * @param period
     * @param date
     * @return true si la date est dans la période passée en paramètre
     */
    private boolean isSamePeriod(Periods period , Date date) {
        boolean retour = false;
        if(period != null && date != null && period.getSequence() != 0 && period.getDateStart() != null) {
            retour = isSamePeriod( period.getDateStart() , date , period.getType());
        }
        return retour;
    }

    private boolean isSamePeriod(Date period , Date date , String type) {
        boolean retour = false;
        if(period != null && date != null ) {
            SimpleDateFormat dayOnly = new SimpleDateFormat("yyyyMMdd"); 
            SimpleDateFormat yearOnly = new SimpleDateFormat("yyyy");
            SimpleDateFormat monthOnly = new SimpleDateFormat("yyyyMM");
            switch(type) {
            case Periods.Types.JOUR :
                retour = dayOnly.format(period).equals(dayOnly.format(date));
                break;
            case Periods.Types.MOIS :
                retour = monthOnly.format(period).equals(monthOnly.format(date));
                break;
            case Periods.Types.FYEAR :
                String yearEnd = yearOnly.format(period) + applicationApiDAO.getFiscalYearEnd() + "24";
                retour = ( 0 < dayOnly.format(period).compareTo(yearEnd) * 
                        dayOnly.format(date).compareTo(yearEnd));
                break; 
            case Periods.Types.SESSION :
                retour = date.after(period);
            }
        }
        return retour;
    }

    private Date getMaxDate(Periods period) {
        Date retour = null;
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(period.getDateStart());
        if(period != null && period.getSequence() != 0 && period.getDateStart() != null) {
            calendar.set(Calendar.HOUR_OF_DAY , 23);
            calendar.set(Calendar.MINUTE , 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 999);
            switch(period.getType()) {
            case Periods.Types.JOUR :
                retour = calendar.getTime();
                break;
            case Periods.Types.MOIS :
                calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
                retour = calendar.getTime();
                break;
            case Periods.Types.FYEAR :
                String yearEnd = applicationApiDAO.getFiscalYearEnd();
                Integer month = Integer.valueOf(yearEnd.substring(0, 2));
                Integer date = Integer.valueOf(yearEnd.substring(2, 4));
                if(calendar.get(Calendar.MONTH)>= month) {
                    calendar.add(Calendar.YEAR, 1);
                }
                calendar.set(Calendar.MONTH, month - 1);
                calendar.set(Calendar.DATE, date);
                retour = calendar.getTime();
                break; 
            }   
        }
        return retour;
    }


    @Override
    public Periods findByDescription(String period) {
        // TODO Auto-generated method stub
        //TODO comment retrouver la période à partir du paramètre ?
        //TODO le paramètre devra à terme être un PeriodInfoDTO contenant au minimum dateStart et type
        //On retrouvera une période de ce tel que la date soit entre début et fin
        /*
        Date start = null;
        Date end = null;
        String type ;
        
        if(start != null) {
            end = start;
            //Modifier pour que la comparaison stricte ne nous pose pas de pb en particulier pour une période jour
            // 2020-09-21 -> 2020-09-01 + 1 seconde pour la comparaison avec début
        }
        return periodsDAO.findTopByTypeAndDateStartBeforeAndDateEndAfterOrderBySequenceDesc(type, start, end);
        */
        return periodsApiDAO.findById(period).orElse(null);
    }

}
