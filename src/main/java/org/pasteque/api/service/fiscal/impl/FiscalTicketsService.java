package org.pasteque.api.service.fiscal.impl;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.pasteque.api.dao.cash.IClosedCashApiDAO;
import org.pasteque.api.dao.fiscal.IFiscalTicketsApiDAO;
import org.pasteque.api.dao.fiscal.IPeriodsApiDAO;
import org.pasteque.api.dao.tickets.ITicketsApiDAO;
import org.pasteque.api.dto.fiscal.FiscalTicketsDTO;
import org.pasteque.api.dto.fiscal.PeriodInfoDTO;
import org.pasteque.api.dto.sessions.CashSessionInfoDTO;
import org.pasteque.api.dto.tickets.TicketsInfoDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.application.Applications;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.model.fiscal.FiscalTickets;
import org.pasteque.api.model.fiscal.FiscalTickets.Types;
import org.pasteque.api.model.fiscal.FiscalTicketsId;
import org.pasteque.api.model.fiscal.Periods;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.service.application.IVersionService;
import org.pasteque.api.service.fiscal.IFiscalTicketsService;
import org.pasteque.api.service.tickets.ITicketsService;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.pasteque.api.specs.fiscal.FiscalTicketsSpecification;
import org.pasteque.api.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class FiscalTicketsService extends SignableService implements IFiscalTicketsService {

    @Autowired
    private IFiscalTicketsApiDAO fiscalTicketsApiDAO;
    @Autowired
    private ITicketsApiDAO ticketsApiDAO;
    @Autowired
    private IClosedCashApiDAO closedCashApiDAO;
    @Autowired
    private IPeriodsApiDAO periodsApiDAO;
    @Autowired 
    private IVersionService versionService;
    @Autowired
    private ITicketsService ticketsService;

    @Override
    public Long countTickets(MultiValueMap<String, String> map) {
        map.remove(FiscalTickets.Fields.TYPE);
        map.add(FiscalTickets.Fields.TYPE, FiscalTickets.Types.TICKET);
        return countFiscalTickets(map);
    }

    @Override
    public Long countZ(MultiValueMap<String, String> map) {
        map.remove(FiscalTickets.Fields.TYPE);
        map.add(FiscalTickets.Fields.TYPE, FiscalTickets.Types.ZTICKET);
        return countFiscalTickets(map);
    }

    @Override
    public Long countFiscalTickets(MultiValueMap<String, String> map) {
        FiscalTicketsSpecification specs = new FiscalTicketsSpecification();
        for(String str : map.keySet()){
            List<String> values = map.get(str);
            if(values.size() == 1) {

                SearchOperation operation = SearchOperation.EQUAL ;
                if(FiscalTickets.SearchCritria.FROM.equals(str)) {
                    operation = SearchOperation.GREATER_THAN_EQUAL ;
                }
                if(FiscalTickets.SearchCritria.TO.equals(str)) {
                    operation = SearchOperation.LESS_THAN_EQUAL ;
                }
                if(FiscalTickets.SearchCritria.EXCLUDE_NUMBER.equals(str)) {
                    operation = SearchOperation.NOT_EQUAL ;
                }
                specs.add(new SearchCriteria(str , values.get(0) , operation ));
            }
        }
        return fiscalTicketsApiDAO.count(specs);
    }

    @Override
    public List<FiscalTickets> listTickets(MultiValueMap<String, String> map,
            Integer limit, Integer pageNumber) {
        map.remove(FiscalTickets.Fields.TYPE);
        map.add(FiscalTickets.Fields.TYPE, FiscalTickets.Types.TICKET);
        return findByCriteria(map,limit,pageNumber,false);
    }

    @Override
    public List<FiscalTickets> listZ(MultiValueMap<String, String> map,
            Integer limit, Integer pageNumber) {
        map.remove(FiscalTickets.Fields.TYPE);
        map.add(FiscalTickets.Fields.TYPE, FiscalTickets.Types.ZTICKET);
        return findByCriteria(map,limit,pageNumber,false);
    }

    @Override
    public List<FiscalTickets> findByCriteria(MultiValueMap<String, String> map,
            Integer limit, Integer pageNumber , boolean naturalSorting) {
        FiscalTicketsSpecification specs = new FiscalTicketsSpecification();
        List<FiscalTickets> l = null ;
        for(String str : map.keySet()){
            List<String> values = map.get(str);
            if(values.size() == 1) {

                SearchOperation operation = SearchOperation.EQUAL ;
                if(FiscalTickets.SearchCritria.FROM.equals(str)) {
                    operation = SearchOperation.GREATER_THAN_EQUAL ;
                }
                if(FiscalTickets.SearchCritria.TO.equals(str)) {
                    operation = SearchOperation.LESS_THAN_EQUAL ;
                }
                if(FiscalTickets.SearchCritria.EXCLUDE_NUMBER.equals(str)) {
                    operation = SearchOperation.NOT_EQUAL ;
                }
                specs.add(new SearchCriteria(str , values.get(0) , operation ));
            }
        }
        // il y a un tri par défaut type , sequence , date , number  les EOS en dernier
        // et l'ordre naturel qui est celui du numéro. 
        // Pour une modificaiton de ticket on en a deux à la même date, 
        //pour pouvoir vérifier la signature, il est impératif de les prendre dans l'ordre ce qui ne serait pas le cas avec l'autre tri
        //Et dans ce cas on se moque de EOS puisqu'il est en général exclu
        Sort sort = naturalSorting ? Sort.by(FiscalTickets.Fields.TYPE).ascending().and(Sort.by(FiscalTickets.Fields.SEQUENCE).ascending().and(Sort.by(FiscalTickets.Fields.NUMBER).ascending()))
                : Sort.by(FiscalTickets.Fields.TYPE).ascending().and(Sort.by(FiscalTickets.Fields.SEQUENCE).ascending().and(Sort.by(FiscalTickets.Fields.DATE).ascending().and(Sort.by(FiscalTickets.Fields.NUMBER).descending()))) ;

        if(limit != null && limit > 1) { 
            if(pageNumber == null || pageNumber < 1 ) {
                pageNumber = 0 ;
            }
            Page<FiscalTickets> p = fiscalTicketsApiDAO.findAll(specs , PageRequest.of(pageNumber, limit , sort) );
            l = p.getContent();
        } else {
            l = fiscalTicketsApiDAO.findAll(specs , sort );
        }
        return l;
    }

    /**
     * Dans la V8 il y a une fonction d'update pour chaque type ( Ticket , Zticket ...)
     * Pour le moment on va considérer que le type correspond à celui du 'last'
     * Ainsi nous avons une unique fonction et nous pouvons avoir autant de types que l'on veut.
     */
    @Override
    public void updateEOSFiscalTicket(FiscalTickets last) {
        if (last != null) {
            FiscalTicketsId eosId = new FiscalTicketsId(last.getType() , last.getSequence() , FiscalTickets.EOS);
            FiscalTickets eos = fiscalTicketsApiDAO.findById(eosId).orElse(null);

            if(eos == null) {
                eos = new FiscalTickets();
                eos.setNumber(FiscalTickets.EOS);
                eos.setType(last.getType());
                eos.setSequence(last.getSequence());
                eos.setContent(FiscalTickets.EOS_STRING);
            }

            eos.setDate(last.getDate());
            sign(eos , last);
            //TODO - Flush ou pas ?
            fiscalTicketsApiDAO.save(eos);
        }

    }

    @Override
    public List<FiscalTickets> fiscalExport(Date dateStart, Date dateStop) {
        FiscalTicketsSpecification specs = new FiscalTicketsSpecification();
        //On fait un export en fonction de la date de tous les tickets fiscaux         
        specs.add(new SearchCriteria(FiscalTickets.SearchCritria.FROM , DateHelper.formatSQLTimeStamp(dateStart) , SearchOperation.GREATER_THAN_EQUAL ));
        specs.add(new SearchCriteria(FiscalTickets.SearchCritria.TO , DateHelper.formatSQLTimeStamp(dateStop) , SearchOperation.LESS_THAN_EQUAL ));

        //celui ci devra ensuite être transporté soit sous forme de fichier soit sous forme de liste d'objets JSON
        return fiscalTicketsApiDAO.findAll(specs , Sort.unsorted());
    }
    
    @Override
    public void createZipExport(ZipOutputStream zip , Date dateStart, Date dateStop) throws IOException {
            List<FiscalTicketsDTO> dtoList = AdaptableHelper.getAdapter(fiscalExport(dateStart, dateStop), FiscalTicketsDTO.class);

            ObjectMapper mapper = new ObjectMapper();
            byte[] bytes = null;
            ZipEntry zipEntry = null;
            String maintenant = LocalDateTime.now().toString();
            String fileName = String.format("FiscalExport-%s-%d-%d.txt",maintenant , dateStart.getTime() , dateStop.getTime());
            
            zipEntry = new ZipEntry(fileName);
            zip.putNextEntry(zipEntry);
            bytes = mapper.writeValueAsString(dtoList).getBytes(StandardCharsets.UTF_8);
            zip.write(bytes);            
    }

    @Override
    public void fiscalImport(List<FiscalTickets> tickets) {
        //On intègre les tickets de la liste dans le système
        //On écrase le EOS autant que demandé
        //On passe en silence sur un ticket exitant à l'identique
        //On lève une exception si on essaye de modifier un ticket existant
        //On sauve si il s'git d'un nouveau ticket
        
        //Cette liste est issue soit d'un fichier soit d'un JSON
        for(FiscalTickets ticket : tickets) {
            try {
                fiscalImport(ticket);
            } catch (APIException e) {
                e.printStackTrace();
            }
        }
        
        //TODO : voir quel retour on fait sur les erreurs
        
    }
    
    
    @Override
    public  Boolean fiscalImport(FiscalTickets tickets) throws APIException {
        Boolean retour = false ;
        if(tickets.getNumber() != FiscalTickets.EOS ) {
            FiscalTickets local = getById(tickets.getSequence(), tickets.getType(), tickets.getNumber());
            if(local != null) {
                if(!local.getSignature().equals(tickets.getSignature()) 
                        || !local.getContent().equals(tickets.getContent())) {
                    throw new APIException("Trying to override an existing fiscal ticket.");
                } else {
                    //on ne fait rien c'est le même ticket que celui en local
                    return retour;
                }
            }
        }
        
        fiscalTicketsApiDAO.saveAndFlush(tickets);
        retour = true;
        return retour;
    }

    @Override
    public
    List<String> getTypes(){
        FiscalTickets jobject = new FiscalTickets();
        Types object = jobject.new Types();
        List<String> retour = new ArrayList<String>();
        Field[] fields = FiscalTickets.Types.class.getDeclaredFields();
        for (Field f : fields) {
            if (Modifier.isStatic(f.getModifiers()) && f.getType() == String.class) {
                try {
                    retour.add((String)f.get(object));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } 
        }
        return retour;
    }

    @Override
    public FiscalTickets getById(String sequence, String type, int number) {
        return fiscalTicketsApiDAO.findById(new FiscalTicketsId(type , sequence, number)).orElse(null);
    }
    
    @Override
    public Periods write(Periods period) throws APIException {
        String rejectReason =null;
        ObjectMapper mapper = new ObjectMapper();
        if (period.getSequence() == 0L) {
            rejectReason = "Period number 0 is reserved.";
            registerFailure(period, rejectReason);
            throw new APIException(rejectReason);
        }
        
     // Create associated fiscal ticket
        FiscalTickets fiscalTicket = createfiscalTickets( FiscalTickets.Types.CLOTURE , getPeriodSequence(period));
        //On met le json de la session dans le contenu
        PeriodInfoDTO dto = period.getAdapter(PeriodInfoDTO.class);
        dto.setVersion(versionService.getCurrent(Applications.Type.SERVER_OSE).getVersion());
        try {
            fiscalTicket.setContent(mapper.writeValueAsString(dto));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            rejectReason = "Unable to get proper DTO for the closedCash.";
            registerFailure(period, rejectReason);
            throw new APIException(rejectReason);
        }
        // Sign
        sign(fiscalTicket , getLastFiscalTicket(fiscalTicket.getType(), fiscalTicket.getSequence()));
        // Write
        period = periodsApiDAO.save(period);
        fiscalTicketsApiDAO.save(fiscalTicket);
        updateEOSFiscalTicket(fiscalTicket);
        
        return period;
    }

    @Override
    public ClosedCash write(ClosedCash closedCash) throws APIException {
        String rejectReason =null;
        ObjectMapper mapper = new ObjectMapper();
        if (closedCash.getHostSequence() == 0L) {
            rejectReason = "ClosedCash number 0 is reserved.";
            registerFailure(closedCash, rejectReason);
            throw new APIException(rejectReason);
        }
        
        // Create associated fiscal ticket
        FiscalTickets fiscalTicket = createfiscalTickets( FiscalTickets.Types.ZTICKET , getClosedCashSequence(closedCash));
        //On met le json de la session dans le contenu
        CashSessionInfoDTO dto = closedCash.getAdapter(CashSessionInfoDTO.class);
        dto.setVersion(versionService.getCurrent(Applications.Type.SERVER_OSE).getVersion());
        try {
            fiscalTicket.setContent(mapper.writeValueAsString(dto));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            rejectReason = "Unable to get proper DTO for the closedCash.";
            registerFailure(closedCash, rejectReason);
            throw new APIException(rejectReason);
        }
        // Sign
        sign(fiscalTicket , getLastFiscalTicket(fiscalTicket.getType(), fiscalTicket.getSequence()));
        // Write
        closedCash = closedCashApiDAO.save(closedCash);
        fiscalTicketsApiDAO.save(fiscalTicket);
        updateEOSFiscalTicket(fiscalTicket);
                
        return closedCash;
    }

    @Override
    public Tickets write(Tickets tickets) throws APIException {
        String rejectReason =null;
        ObjectMapper mapper = new ObjectMapper();
        if (tickets.getNumber() == 0L) {
            rejectReason = "Ticket number 0 is reserved.";
            registerFailure(tickets, rejectReason);
            throw new APIException(rejectReason);
        }

        // Overwriting an existing ticket
        if (!canInsert(tickets)) {
            rejectReason = "Tickets are read only.";
            registerFailure(tickets, rejectReason);
            throw new APIException(rejectReason);
        }
        // Cash session must be opened
        ClosedCash session = tickets.getReceipts().getClosedCash();

        if (session == null) {
            rejectReason = "Cash session not found.";
            registerFailure(tickets, rejectReason);
            throw new APIException(rejectReason);
        } else if (session.getDateEnd() != null){
            rejectReason = "Tickets must be assigned to an opened cash session.";
            registerFailure(tickets, rejectReason);
            throw new APIException(rejectReason);
        }
        // Create associated fiscal ticket
        FiscalTickets fiscalTicket = createfiscalTickets( FiscalTickets.Types.TICKET , getTicketSequence(tickets));
        //On met le json du ticket dans le contenu
        TicketsInfoDTO dto = tickets.getAdapter(TicketsInfoDTO.class);
        ticketsService.fillTicketsInfoDTO(dto , tickets);
        dto.setVersion(versionService.getCurrent(Applications.Type.SERVER_OSE).getVersion());
        try {
            fiscalTicket.setContent(mapper.writeValueAsString(dto));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            rejectReason = "Unable to get proper DTO for the ticket.";
            registerFailure(tickets, rejectReason);
            throw new APIException(rejectReason);
        }
        // Sign
        sign(fiscalTicket , getLastFiscalTicket(fiscalTicket.getType(), fiscalTicket.getSequence()));
        // Write
        tickets = ticketsApiDAO.save(tickets);
        fiscalTicketsApiDAO.save(fiscalTicket);
        updateEOSFiscalTicket(fiscalTicket);
        
        return tickets;
    }

    private String getTicketSequence(Tickets tickets) {
        //En V8 les id sont numérique donc il y a une manipulation
        //Ici ce sont des String donc à priori pas de manip nécessaire
        //Cependant on conserve la fonction
        return getClosedCashSequence(tickets.getReceipts().getClosedCash());
    }
    
    private String getFailureTicketSequence(Tickets tickets) {

        return FiscalTickets.Prefixes.FAILURE + getTicketSequence(tickets);
    }
    
    private String getClosedCashSequence(ClosedCash closedCash) {
        //En V8 les id sont numérique donc il y a une manipulation
        //Ici ce sont des String donc à priori pas de manip nécessaire
        //Cependant on conserve la fonction
        return closedCash.getCashRegisterId();
    }
    
    private String getFailureClosedCashSequence(ClosedCash closedCash) {

        return FiscalTickets.Prefixes.FAILURE + getClosedCashSequence(closedCash);
    }

    /**
     * Retrouver le dernier ticket fiscal qui ne soit pas EOS pour ce couple type / sequence
     * @param ticket
     * @param sequence
     * @return le ticket fiscal recherché ou null si il n'y en a pas ou si il y a seulement EOS
     */
    private FiscalTickets getLastFiscalTicket(String type, String sequence) {
        FiscalTickets retour = fiscalTicketsApiDAO.findTopByTypeAndSequenceOrderByNumberDesc(type, sequence);
        return retour != null && retour.getNumber() == FiscalTickets.EOS ? null : retour;
    }

    //TODO Factoriser !
    private boolean canInsert(Tickets tickets) {
        //On ne peut pas manipuler un ticket déjà existant
        return !ticketsApiDAO.findById(tickets.getId()).isPresent();
    }
    
    
    //TODO Factoriser sinon on se retrouvera avec une fonction par type
    private void registerFailure(ClosedCash closedCash, String rejectReason) throws APIException{
     // Create the failure fiscal ticket.
        FiscalTickets fiscalTicket =  createfiscalTickets( FiscalTickets.Types.ZTICKET , getFailureClosedCashSequence(closedCash));
        
        CashSessionInfoDTO dto = closedCash.getAdapter(CashSessionInfoDTO.class);
        dto.setFailure(rejectReason);
        dto.setVersion(versionService.getCurrent(Applications.Type.SERVER_OSE).getVersion());
        //On met le json du ticket dans le contenu
        try {
            ObjectMapper mapper = new ObjectMapper();
            fiscalTicket.setContent(mapper.writeValueAsString(dto));
        } catch (JsonProcessingException e) {
            //TODO on risque de boucler là non !
            e.printStackTrace();
            rejectReason = "Unable to get proper DTO for the ticket.";
            registerFailure(closedCash, rejectReason);
            throw new APIException(rejectReason);
        }
        // Sign
        sign(fiscalTicket , getLastFiscalTicket(fiscalTicket.getType(), fiscalTicket.getSequence()));
        fiscalTicketsApiDAO.save(fiscalTicket);
        updateEOSFiscalTicket(fiscalTicket);
        fiscalTicketsApiDAO.flush();
        
    }
    
    private void registerFailure(Periods period, String rejectReason) throws APIException{
        // Create the failure fiscal ticket.
           FiscalTickets fiscalTicket =  createfiscalTickets( FiscalTickets.Types.CLOTURE , getFailurePeriodSequence(period));
           
           PeriodInfoDTO dto = period.getAdapter(PeriodInfoDTO.class);
           dto.setFailure(rejectReason);
           dto.setVersion(versionService.getCurrent(Applications.Type.SERVER_OSE).getVersion());
           //On met le json du ticket dans le contenu
           try {
               ObjectMapper mapper = new ObjectMapper();
               fiscalTicket.setContent(mapper.writeValueAsString(dto));
           } catch (JsonProcessingException e) {
               //TODO on risque de boucler là non !
               e.printStackTrace();
               rejectReason = "Unable to get proper DTO for the ticket.";
               registerFailure(period, rejectReason);
               throw new APIException(rejectReason);
           }
           // Sign
           sign(fiscalTicket , getLastFiscalTicket(fiscalTicket.getType(), fiscalTicket.getSequence()));
           fiscalTicketsApiDAO.save(fiscalTicket);
           updateEOSFiscalTicket(fiscalTicket);
           fiscalTicketsApiDAO.flush();
           
       }


    private String getFailurePeriodSequence(Periods period) {
        return FiscalTickets.Prefixes.FAILURE + getPeriodSequence(period);
    }
    
    private String getPeriodSequence(Periods period) {
        return period.getType() + "-" + period.getCashRegister().getId();
    }

    private void registerFailure(Tickets tickets, String rejectReason) throws APIException {

        // Create the failure fiscal ticket.
        FiscalTickets fiscalTicket =  createfiscalTickets( FiscalTickets.Types.TICKET , getFailureTicketSequence(tickets));
        
        TicketsInfoDTO dto = tickets.getAdapter(TicketsInfoDTO.class);
        ticketsService.fillTicketsInfoDTO(dto , tickets);
        dto.setFailure(rejectReason);
        dto.setVersion(versionService.getCurrent(Applications.Type.SERVER_OSE).getVersion());
        //On met le json du ticket dans le contenu
        try {
            ObjectMapper mapper = new ObjectMapper();
            fiscalTicket.setContent(mapper.writeValueAsString(dto));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            rejectReason = "Unable to get proper DTO for the ticket.";
            registerFailure(tickets, rejectReason);
            throw new APIException(rejectReason);
        }
        // Sign
        sign(fiscalTicket , getLastFiscalTicket(fiscalTicket.getType(), fiscalTicket.getSequence()));
        fiscalTicketsApiDAO.save(fiscalTicket);
        updateEOSFiscalTicket(fiscalTicket);
        fiscalTicketsApiDAO.flush();
        
    }
    
    private FiscalTickets createfiscalTickets( String type , String sequence) {
        FiscalTickets previousFTicket = getLastFiscalTicket(type, sequence);
        FiscalTickets retour = new FiscalTickets();
        retour.setType(type);
        retour.setSequence(sequence);
        retour.setNumber((previousFTicket == null ? 0 :  previousFTicket.getNumber() ) + 1);

        retour.setDate(new Date());
        return retour;
    }

}
