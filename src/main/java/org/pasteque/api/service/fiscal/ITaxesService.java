package org.pasteque.api.service.fiscal;

import org.pasteque.api.model.products.TaxCategories;
import org.pasteque.api.model.products.TaxCustCategories;
import org.pasteque.api.model.products.Taxes;

public interface ITaxesService {

    Taxes findCurrentByCategoryAndCustCategory(TaxCategories category , TaxCustCategories custCategory);

}
