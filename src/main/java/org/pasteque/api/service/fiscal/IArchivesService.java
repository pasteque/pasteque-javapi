package org.pasteque.api.service.fiscal;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.List;

import org.bouncycastle.openpgp.PGPException;
import org.pasteque.api.dto.fiscal.ArchivesDTO;
import org.pasteque.api.model.archives.ArchiveRequests;
import org.pasteque.api.model.archives.Archives;

public interface IArchivesService extends ISignableService{
    
    Archives getById(Integer number);
    
    Archives getLastArchive();
    
    List<ArchivesDTO> getList();
    
    /**
     * * Commentaires originaux
     * Generate an archive from a request. The request must have been
     * registered and not currently being processed.
     * The request is marked being processed, the archive is generated and
     * the request is deleted.
     * @return True when everything went well. False if the archive couldn't
     * be generated.
     * @throws IOException 
     * @throws PGPException 
     * @throws NoSuchProviderException 
     * @throws NoSuchAlgorithmException 
     * @throws SignatureException 
     * @throws RecordNotFoundException When no request can be found with the
     * given number.
     * @throws ConfigurationException When the signing key is not available.
     */
     boolean create(ArchiveRequests request) throws IOException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException, PGPException;
     
     void updateEOSArchive(Archives last);

}
