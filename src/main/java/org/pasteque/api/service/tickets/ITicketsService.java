package org.pasteque.api.service.tickets;

import java.util.List;

import org.pasteque.api.dto.tickets.POSSharedTicketsDTO;
import org.pasteque.api.dto.tickets.POSTicketsDTO;
import org.pasteque.api.dto.tickets.TicketsInfoDTO;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.model.tickets.TicketLines;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.response.ICsvResponse;
import org.pasteque.api.util.ComplexReturnType;

public interface ITicketsService {

    List<POSSharedTicketsDTO> getAllSharedTickets();

    POSSharedTicketsDTO getSharedTicketsById(String id);

    boolean deleteSharedTicketsById(String id);

    Object save(String cashId, List<POSTicketsDTO> l) throws APIException;

    boolean share(POSSharedTicketsDTO pOSSharedTicketsDTO);

    List<POSTicketsDTO> getOpen();

    <T extends ICsvResponse> List<T> search(String ticketId, String ticketType, String cashId, String dateStart, 
            String dateStop, String customerId, String userId, String locationId, String locationCategoryName , 
            String saleLocationId, String tourId, String fromValue, String toValue, Boolean orderConstraint, String resultType);

    Object getByTicketId(String ticketId);

    double computeFinalPrice(double additionalDiscount,
            TicketLines ticketLines);

    void fillTicketsInfoDTO(TicketsInfoDTO dto , Tickets tickets);

    ComplexReturnType getTicketReturnInfos(String resultType);

    double getTaxedUnitPrice(TicketLines ticketLines);

}
