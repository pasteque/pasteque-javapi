package org.pasteque.api.service.tickets.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.pasteque.api.dto.tickets.POSSharedTicketsDTO;
import org.pasteque.api.dto.tickets.TicketsInfoDTO;
import org.pasteque.api.service.tickets.ITicketsService;
import org.springframework.beans.factory.annotation.Autowired;

public class TicketsFactory {

    @Autowired
    private static ITicketsService ticketsService;

    public static TicketsInfoDTO getByTicketId(String ticketId) {
        return (TicketsInfoDTO) ticketsService.getByTicketId(ticketId);
    }

    public static Collection<TicketsInfoDTO> getAll() {
        return new ArrayList<TicketsInfoDTO>();
    }

    //TODO EDU Il semble que cette fonction a été banalisée - a reprendre en fonction des besoins
    public static List<POSSharedTicketsDTO> load() {
//        if (ticketsService != null) {
//            return ticketsService.getAllSharedTickets();
//        } else {
            List<POSSharedTicketsDTO> l = new ArrayList<POSSharedTicketsDTO>();
            POSSharedTicketsDTO dto = new POSSharedTicketsDTO();
            dto.setLabel("test");
            l.add(dto);
            return l;
//        }
    }
}
