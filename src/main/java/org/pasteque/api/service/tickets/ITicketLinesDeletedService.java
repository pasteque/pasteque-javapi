package org.pasteque.api.service.tickets;

import java.util.List;

import org.pasteque.api.dto.tickets.POSTicketLinesDeletedDTO;
import org.pasteque.api.model.exception.APIException;

public interface ITicketLinesDeletedService {

    Object save(List<POSTicketLinesDeletedDTO> l, String ticketId, String isOpenedPayment) throws APIException;
   
}
