package org.pasteque.api.service.tickets.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.pasteque.api.dao.cash.ICashRegistersApiDAO;
import org.pasteque.api.dao.cash.IClosedCashApiDAO;
import org.pasteque.api.dao.cash.ICurrenciesApiDAO;
import org.pasteque.api.dao.cash.ITaxesApiDAO;
import org.pasteque.api.dao.customers.ICustomersApiDAO;
import org.pasteque.api.dao.customers.IDiscountProfilApiDAO;
import org.pasteque.api.dao.locations.IInseeApiDAO;
import org.pasteque.api.dao.products.IProductsApiDAO;
import org.pasteque.api.dao.products.ITariffAreasApiDAO;
import org.pasteque.api.dao.security.IPeopleApiDAO;
import org.pasteque.api.dao.tickets.IReceiptsApiDAO;
import org.pasteque.api.dao.tickets.ISharedTicketsApiDAO;
import org.pasteque.api.dao.tickets.ITicketsApiDAO;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.dto.tickets.POSPaymentsDTO;
import org.pasteque.api.dto.tickets.POSSharedTicketsDTO;
import org.pasteque.api.dto.tickets.POSTicketLinesDTO;
import org.pasteque.api.dto.tickets.POSTicketsDTO;
import org.pasteque.api.dto.tickets.TicketsInfoDTO;
import org.pasteque.api.dto.tickets.TicketsLineInfoDTO;
import org.pasteque.api.dto.tickets.TicketsProductsInfoDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.customers.Customers;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.ProductsComponents;
import org.pasteque.api.model.products.TaxCategories;
import org.pasteque.api.model.products.TaxCustCategories;
import org.pasteque.api.model.products.Taxes;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.tickets.Payments;
import org.pasteque.api.model.tickets.Receipts;
import org.pasteque.api.model.tickets.SharedTickets;
import org.pasteque.api.model.tickets.TaxLines;
import org.pasteque.api.model.tickets.TicketLines;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.model.tickets.TicketsManager;
import org.pasteque.api.response.ICsvResponse;
import org.pasteque.api.service.fiscal.IFiscalTicketsService;
import org.pasteque.api.service.fiscal.IPeriodsService;
import org.pasteque.api.service.fiscal.ITaxesService;
import org.pasteque.api.service.products.IProductsService;
import org.pasteque.api.service.stock.IStocksService;
import org.pasteque.api.service.tickets.ITicketsService;
import org.pasteque.api.util.ComplexReturnType;
import org.pasteque.api.util.FilteringAndFormattingData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TicketsService implements ITicketsService {
    
    protected static final Map<String, ComplexReturnType> returnTypesRules;

    // Gestion des cas 'composés disponibles pour les recherches
    static {
        HashMap<String, ComplexReturnType> map = new HashMap<String, ComplexReturnType>();
        map.put("TicketsDTO", new ComplexReturnType("TicketsDTO", "TicketsDTO", "ticketsDetails", POSTicketsDTO.types, null, null, null));
        returnTypesRules = Collections.unmodifiableMap(map);
    }

    @Autowired
    private ISharedTicketsApiDAO sharedTicketsApiDAO;
    @Autowired
    protected IClosedCashApiDAO closedCashApiDAO;
    @Autowired
    private ICashRegistersApiDAO cashRegistersApiDAO;
    @Autowired
    protected ITicketsApiDAO ticketsApiDAO;
    @Autowired
    protected IReceiptsApiDAO receiptsApiDAO;
    @Autowired
    protected ICustomersApiDAO customersApiDAO;
    @Autowired
    protected IDiscountProfilApiDAO discountProfileDAO;
    @Autowired
    protected IPeopleApiDAO peopleApiDAO;
    @Autowired
    protected ITariffAreasApiDAO tariffAreasApiDAO;
    @Autowired
    protected IProductsApiDAO productsApiDAO;
    @Autowired
    protected ITaxesApiDAO taxesApiDAO;
    @Autowired
    protected ICurrenciesApiDAO currenciesApiDAO;
    @Autowired
    private IStocksService stocksService;
    @Autowired
    protected IInseeApiDAO inseeApiDAO;
    @Autowired
    private IFiscalTicketsService fiscalTicketsService; 
    @Autowired
    private IPeriodsService periodsService;
    @Autowired
    private IProductsService productsService;
    @Autowired
    private ITicketsService ticketsService;
    @Autowired
    private ITaxesService taxesService;

    @Override
    public List<POSSharedTicketsDTO> getAllSharedTickets() {
        return AdaptableHelper.getAdapter(sharedTicketsApiDAO.findAll(), POSSharedTicketsDTO.class);
    }

    @Override
    public POSSharedTicketsDTO getSharedTicketsById(String id) {
        SharedTickets shared = sharedTicketsApiDAO.findById(id).orElse(null);
        return shared == null ? null : shared.getAdapter(POSSharedTicketsDTO.class);
    }

    @Override
    @Transactional
    public boolean deleteSharedTicketsById(String id) {
        SharedTickets sharedTickets = sharedTicketsApiDAO.findById(id).orElse(null);
        if(sharedTickets != null ) sharedTicketsApiDAO.delete(sharedTickets) ;
        return sharedTickets != null ;
    }

    @Override
    @Transactional
    public Object save(String cashId, List<POSTicketsDTO> l) throws APIException {

        ClosedCash closedCash = closedCashApiDAO.findById(cashId).orElse(null);
        if (closedCash == null) {
            throw new APIException("Unknown cash session");
        }
        if (closedCash.getCashRegister() == null) {
            throw new APIException("Cash register not found");
        }
        if (closedCash.getCashRegister().getLocation() == null) {
            throw new APIException("Location not set");
        }
        
        //EDU 2021 05 
        //Gérer à ce niveau les cas des tickets hors session
        //On a eu des pb avec l'horodatage sur des mouvements sur l'heure du PC  qui pouvaient envoyer les
        //Tickets avant l'ouverture de session
        //Ceci pose pb par la suite car ça entre dans les contrôles pour les tickets fiscaux
        //Et on se retrouve avec un ticket enregistré mais pas le ticket fiscal et ça reste bloqué en cache
        //Du coup tout est bloqué

        if (closedCash.getDateEnd() != null) {
            throw new APIException("Cannot edit a ticket from a closed cash");
        }
        
        Date minDate = closedCash.getDateStart();
        Receipts lastReceipts = receiptsApiDAO.findTopByClosedCashOrderByDateNewDesc(closedCash);
        
        if(lastReceipts != null && minDate.before(lastReceipts.getDateNew())) {
            minDate = lastReceipts.getDateNew();
        }
        
        ListIterator<POSTicketsDTO> it = l.listIterator();
        while(it.hasNext()){
            POSTicketsDTO pOSTicketsDTO = it.next();
            // TODO EDU : doublon avec TicketsManager LoadFromDTO ?
            Tickets tickets = new Tickets();
            List<Tickets> parent = null;
            List<Payments> payments = new ArrayList<Payments>();
            boolean isOrder = false;

            if (pOSTicketsDTO.getId() != null) {

                //On viens de nous fournir un id 

                FilteringAndFormattingData.filter(pOSTicketsDTO);
                tickets = ticketsApiDAO.findById(pOSTicketsDTO.getId()).orElse(null);
                if(tickets != null) {
                    // nous avons effectivement trouvé le ticket en question 
                    if (tickets.getReceipts().getClosedCash().getDateEnd() != null) {
                        throw new APIException("Cannot edit a ticket from a closed cash");
                    }

                    // la session est ouverte on va ajouter un ticket contrepassé à la liste
                    POSTicketsDTO tmp = tickets.getAdapter(POSTicketsDTO.class);
                    tmp.setId(null);
                    //Comme il n'y aura plus de manip de numéro de ticket on doit ajouter assigner le bon dès maintenant
                    tmp.setTicketId(tmp.getTicketId() + 1);
                    
                    //Décalage de 1s car sinon on a deux tickets exactement au même instant ce qui posera des pb d'Id
                    tmp.setDate(pOSTicketsDTO.getDatetime().getTime()+1000);
                    //On le lie au ticket d'origine
                    tmp.setParentTicketId(tmp.getTicketId());
                    //Ainsi que le nouveau
                    pOSTicketsDTO.setParentTicketId(tmp.getTicketId());

                    //TODO ce critère parentticketid = ticketid devrait être un critère nécessaire et suffisant 
                    //pour ne pas comptabiliser un client de plus

                    tmp.setType(Tickets.Types.RETOUR);

                    //On inverse le sens de la vente
                    List<POSTicketLinesDTO> lines = tmp.getLines();
                    for (POSTicketLinesDTO line : lines) {
                        line.setQuantity(line.getQuantity() * -1);
                        //TODO check pour le prix
                    }

                    //On inverse le sens de l'encaissement
                    List<POSPaymentsDTO> paymentsDTO = tmp.getPayments();
                    for(POSPaymentsDTO payment : paymentsDTO) {
                        payment.setAmount(payment.getAmount()*-1);
                        payment.setCurrencyAmount(payment.getCurrencyAmount()*-1);
                    }

                    FilteringAndFormattingData.filter(tmp);

                    //fonctionnement du listIterator : ce qu'on ajoute est en previous ( ie ça ne change pas le next ) comme on veut que ce soit le next on remet le curseur en place
                    it.add(tmp);
                    it.previous(); // retourne tmp - cf définition de ListIterator.add et il y a forcement un previous puisqu'on a ajouté
                    //donc logiquement next sera bien le dernier ajouté

                }
                tickets = new Tickets();
                //TODO vérifier qu'on n'écras pas un cas : ticket arrivce avec un numéro mais ce numéro n'existe pas dans la base ?
                pOSTicketsDTO.setId(null);
            } 

            //EDU - 2020-09 Désormais si nous sommes ici c'est que nous avons un ticket à encaisser
            //Les cas problématiques sont gérés avant
            //Modif 2021 05 AME EDU - pas d'altération du numéro de ticket
            long nextTicketId = closedCash.getCashRegister().getNextTicketId();
            Date ticketDate = pOSTicketsDTO.getDatetime() ;
            
            if(! ticketDate.after(minDate)) {
                ticketDate = new Date(minDate.getTime() + 1000 );
                minDate = ticketDate ;
                pOSTicketsDTO.setLogicalDatetime(ticketDate);
            }

            // Ajouter des tests : ticket existant on sort - ticket incohérent avec nextticketId on sort!
            if(pOSTicketsDTO.getTicketId().equals(pOSTicketsDTO.getParentTicketId())) {
                tickets.setTicketId(nextTicketId);
            } else {
                tickets.setTicketId(pOSTicketsDTO.getTicketId());
            }
            
            if(! ticketsApiDAO.findByTicketId(tickets.getTicketId()).isEmpty()) {
                throw new APIException("Tickets are read only - This ticket already exists");
            }

            Receipts receipts = new Receipts();
            receipts.setId(Long.toString(ticketDate.getTime()).concat(closedCash.getCashRegister().getLocation().getId()));
            
            receipts.setDateNew(ticketDate);
            // tout ceci ne doit jamais être altéré après édition du ticket
            receipts.setClosedCash(closedCash);

            tickets.setReceipts(receipts);
            tickets.setDate(pOSTicketsDTO.getDatetime());

            parent = pOSTicketsDTO.getParentTicketId() == null ? null : ticketsApiDAO.findByCriteria(pOSTicketsDTO.getParentTicketId()
                    .toString(), null, null, null, null, null, null, null, null, null, null, null, null);
            tickets.setParent(parent == null || parent.size() == 0 ? null : parent.get(0));

            tickets.setDiscountRate(pOSTicketsDTO.getDiscountRate());
            tickets.setTicketType(pOSTicketsDTO.getType());
            if (pOSTicketsDTO.getTariffAreaId() != null) {
                tickets.setTariffArea(tariffAreasApiDAO.findById(pOSTicketsDTO.getTariffAreaId()).orElse(null));
            }

            List<TicketLines> lines = new ArrayList<TicketLines>();
            List<TaxLines> taxesLines = new ArrayList<TaxLines>();
            List<Integer> numerosLines = new ArrayList<Integer>();
            for (POSTicketLinesDTO pOSTicketLinesDTO : pOSTicketsDTO.getLines()) {

                TicketLines parentLine = null;
                Products products = pOSTicketLinesDTO.getProductId() == null ? null : productsApiDAO.findById(pOSTicketLinesDTO.getProductId()).orElse(null);
                TicketLines ticketLines = new TicketLines();
                ticketLines.setLine(pOSTicketLinesDTO.getDispOrder());
                
                if (pOSTicketLinesDTO.getProductId() != null && products == null) {
                    pOSTicketLinesDTO.setNote("Produit inexistant en base : " + pOSTicketLinesDTO.getProductId());
                }
                
                while (numerosLines.contains(ticketLines.getLine())){ 
                    //pour eviter les doublons au niveau du numero de ligne
                    //c'est possible en fonction de la version du client
                    //et dans le cas des commandes partielles sur une offre/kit
                    ticketLines.setLine(ticketLines.getLine()+1);
                    //ne pas oublier de mettre la valeur à jour dans le DTO pour que le mvt de stck soit convenablement identifié
                    pOSTicketLinesDTO.setDispOrder(ticketLines.getLine());
                }
                numerosLines.add(ticketLines.getLine());

                ticketLines.setProduct(products);
                // ticketLines.setAttributeSetInstanceId(attributeSetInstanceId);
                ticketLines.setAttributes(pOSTicketLinesDTO.getAttributes());
                ticketLines.setDiscountRate(pOSTicketLinesDTO.getDiscountRate());
                ticketLines.setUnitPrice(pOSTicketLinesDTO.getPrice());
                if (pOSTicketLinesDTO.getVatId() == null) {
                    throw new APIException("Unknown tax");
                }
                Taxes tax = taxesApiDAO.findById(pOSTicketLinesDTO.getVatId()).orElse(null);
                if (tax == null) {
                    throw new APIException("Unknown tax");
                }
                ticketLines.setTax(tax);
                ticketLines.setUnits(pOSTicketLinesDTO.getQuantity());
                ticketLines.setTicket(tickets);
                ticketLines.setType((!pOSTicketLinesDTO.isOrder() && pOSTicketLinesDTO.getType() == 0 ? 'S' : pOSTicketLinesDTO.getType()));
                isOrder = isOrder || pOSTicketLinesDTO.isOrder() || pOSTicketLinesDTO.getType() == 'O';
                
                if (pOSTicketLinesDTO.getTariffAreaId() != null) {
                    ticketLines.setTariffArea(tariffAreasApiDAO.findById(pOSTicketLinesDTO.getTariffAreaId()).orElse(null));
                }
                ticketLines.setNote(pOSTicketLinesDTO.getNote());
                pOSTicketLinesDTO.setOrder(ticketLines.isOrder());

                if (pOSTicketLinesDTO.getParentLine() != null && pOSTicketLinesDTO.getParentId() != null) {
                    parent = ticketsApiDAO.findByCriteria(pOSTicketLinesDTO.getParentId().toString(), null, null, null, null, null, null, null, null, null, null, null, null);
                    if (parent != null && parent.size() > 0) {
                        for (int i = 0; i < parent.get(0).getLines().size(); i++) {
                            if (parent.get(0).getLines().get(i).getLine() == pOSTicketLinesDTO.getParentLine()) {
                                parentLine = parent.get(0).getLines().get(i);
                                i = parent.get(0).getLines().size();
                            }
                        }
                    }
                }

                ticketLines.setParentLine(parentLine);
                if (parentLine == null && pOSTicketLinesDTO.getParentLine() != null && pOSTicketLinesDTO.getParentId() != null) {
                    ticketLines.setParentLineNumber(pOSTicketLinesDTO.getParentLine());
                    ticketLines.setParentTicketId(pOSTicketLinesDTO.getParentId());

                }
                computeAllPrices(ticketLines);
                FilteringAndFormattingData.filter(ticketLines);
                lines.add(ticketLines);

                // TRAITEMENT DE LA TVA
                Taxes vat = taxesApiDAO.findById(pOSTicketLinesDTO.getVatId()).orElse(null);
                double discount = (1D - pOSTicketLinesDTO.getDiscountRate()) * (1D - pOSTicketsDTO.getDiscountRate());
                double vatAmount = pOSTicketLinesDTO.getPrice() * discount * pOSTicketLinesDTO.getQuantity();
                TicketsManager.addTaxesLines(taxesLines, tickets.getReceipts(), vat, vatAmount);

                // TRAITEMENT DES AUTRES TAXES : DEEE, ECOMOB, ...
                // products peut être null
                if (products == null || products.getComponentsProducts() == null || products.getComponentsProducts().isEmpty()) {
                    if ( pOSTicketLinesDTO.getTaxes() != null && ! pOSTicketLinesDTO.getTaxes().isEmpty()) {
                        for (String taxId : pOSTicketLinesDTO.getTaxes()) {
                            Taxes eco = taxesApiDAO.findById(taxId).orElse(null);
                            if(eco != null) {
                                double ecoAmount = (eco.getAmount() / (1D + vat.getRate())) * pOSTicketLinesDTO.getQuantity();
                                // Ajout de la taxe DEEE, ECOMOB, ...
                                TicketsManager.addTaxesLines(taxesLines, tickets.getReceipts(), eco, ecoAmount);
                                // Ajout de la TVA sur la taxe DEEE, ECOMOB, ...
                                TicketsManager.addTaxesLines(taxesLines, tickets.getReceipts(), vat, ecoAmount);
                            }
                        }
                    }
                } else {
                    // AME : Hack pour gérer les taxes des produits composés...
                    // C'est moche et j'en suis pas fier ...
                    // A refactoriser si on peut ...
                    Locations locations = tickets.getReceipts().getClosedCash().getCashRegister().getLocation();
                    TaxCustCategories custCategory = locations.getTaxCustCategory() ;
                    for (ProductsComponents components : products.getComponentsProducts()) {
                        Products component = components.getComponent();
                        for (TaxCategories taxCategories : component.getTaxCategories()) {
                            Taxes eco = taxesService.findCurrentByCategoryAndCustCategory(taxCategories , custCategory );

                            if(eco != null) {
                                double ecoAmount = ((eco.getAmount() / (1D + vat.getRate())) * pOSTicketLinesDTO.getQuantity())
                                        * components.getQuantity();
                                // Ajout de la taxe DEEE, ECOMOB, ...
                                TicketsManager.addTaxesLines(taxesLines, tickets.getReceipts(), eco, ecoAmount);
                                // Ajout de la TVA sur la taxe DEEE, ECOMOB, ...
                                TicketsManager.addTaxesLines(taxesLines, tickets.getReceipts(), vat, ecoAmount);
                            }
                        }

                    }
                }

                //TODO 2020 EDU - Voir comment on gère les mouvements de stocks
                //Pas forcement dans la pasteque api quoique ce ne soit pas déconnant d'y fournir un socle
                //                    if (!ticketLinesDTO.isOrder() && products != null) {
                //                        StockMovement movement = new StockMovement();
                //                        movement.setDate(tickets.getReceipts().getDateNew());
                //                        movement.setTypeMovement(StockDiary.Type.REASON_OUT_SELL);
                //                        movement.setLocations(closedCash.getCashRegister().getLocation());
                //                        movement.setProducts(products);
                //                        // movement.setAttributeSetInstanceId(attributeSetInstanceId);
                //                        movement.setUnits(ticketLinesDTO.getQuantity());
                //                        movement.setPrice(ticketLinesDTO.getPrice());
                //                        movement.setReference(String.valueOf(tickets.getTicketId()));
                //                        //TODO repasser ici sur la logique
                //                        //EDU On stocke dans la note le numéro de ligne de ticket concerné
                //                        movement.setNote(Integer.toString(ticketLinesDTO.getDispOrder()));
                //                        stocksService.generateMovement(movement);
                //                    }
                //stocksService.generateMovement(tickets,products,pOSTicketLinesDTO);
                // Modif : EDU : Attention on passe par là même si on doit avoir un soucis plus tard 
                // conséquences -> altération des stocks
                // workaround -> on a ajouté des contraintes sur l'enregistrement des tickets pour que
                // si ça devait planter au niveau des tickets fiscaux alors ça plante aussi au niveau du ticket en lui même
                // si ce n'est pas suffisant il faut envisager de ne déplacer cette génération des mouvments de stocks après
                // tous les cas d'echec potentiels
                // TODO : AME => Save ticket : Check prepayment refill
            }
            tickets.setLines(lines);
            tickets.getReceipts().setTaxes(taxesLines);

            tickets.computeAllPrices();

            tickets.setNote(pOSTicketsDTO.getNote());
            tickets.setCustCount(pOSTicketsDTO.getCustCount());
            FilteringAndFormattingData.filter(tickets);
            Customers customers =null;
            if (pOSTicketsDTO.getCustomerId() != null) {
                customers = customersApiDAO.findById(pOSTicketsDTO.getCustomerId()).orElse(null);
                tickets.setCustomer(customers);
            }
            if(customers == null && isOrder) {
                //EDU 2020-12 Pour prévenir une commande de se retrouver sans client associé
                //après une édition
                if(tickets.getParent() != null) {
                    customers = tickets.getParent().getCustomer();
                    tickets.setCustomer(customers);
                }
            }
            if (pOSTicketsDTO.getDiscountProfileId() != null) {
                tickets.setDiscountProfile(discountProfileDAO.findById(pOSTicketsDTO.getDiscountProfileId()).orElse(null));
            }
            if (pOSTicketsDTO.getUserId() != null) {
                tickets.setPerson(peopleApiDAO.findById(pOSTicketsDTO.getUserId()).orElse(null));
            }

            if (pOSTicketsDTO.getInseeNum() != null) {
                tickets.setCustomerZipCode(inseeApiDAO.findById(pOSTicketsDTO.getInseeNum()).orElse(null));
            }

            Double paid = 0D;
            for (POSPaymentsDTO pOSPaymentsDTO : pOSTicketsDTO.getPayments()) {
                Payments payment = new Payments();

                payment.setTickets(tickets);
                payment.setReceipts(tickets.getReceipts());
                payment.setPayment(pOSPaymentsDTO.getType());
                payment.setTotal(pOSPaymentsDTO.getAmount());
                if (pOSPaymentsDTO.getCurrencyId() != null) {
                    payment.setCurrency(currenciesApiDAO.findById(pOSPaymentsDTO.getCurrencyId()).orElse(null));
                }
                payment.setTotalCurrency(pOSPaymentsDTO.getCurrencyAmount());
                payment.setNote(pOSPaymentsDTO.getNote());
                payment.setReturnMessage(pOSPaymentsDTO.getReturnMessage());
                payment.setTransId(pOSPaymentsDTO.getTransId());
                if (pOSPaymentsDTO.getEcheance() != null) {
                    payment.setEcheance(new Date(pOSPaymentsDTO.getEcheance()));
                }
                // TODO EDU gérer ID et note
                payment.createId(payments.size());
                FilteringAndFormattingData.filter(payment);
                payments.add(payment);

                //TODO EDU voir comment on gère les prepaid debt et cie 
                paid+=pOSPaymentsDTO.getAmount();

                if (pOSPaymentsDTO.getType().equals(Payments.Type.PREPAID)) {
                    if (pOSTicketsDTO.getCustomerId() != null) {

                        if (customers != null) {
                            customers.setPrePaid(customers.getPrePaid() - pOSPaymentsDTO.getAmount());
                            customersApiDAO.update(customers);
                        }
                    }
                } else if (pOSPaymentsDTO.getType().equals(Payments.Type.DEBT)) {
                    if (pOSTicketsDTO.getCustomerId() != null) {
                        if (customers != null) {
                            customers.setCurrentDebt(customers.getCurrentDebt() == null ? 0D : customers.getCurrentDebt()
                                    + pOSPaymentsDTO.getAmount());
                            customers.setCurrentDate(pOSTicketsDTO.getDatetime());
                            customersApiDAO.update(customers);
                        }
                    }
                } else if (pOSPaymentsDTO.getType().equals(Payments.Type.DEBT_PAID)) {
                    if (pOSTicketsDTO.getCustomerId() != null) {
                        if (customers != null) {
                            customers.setCurrentDebt(customers.getCurrentDebt() == null ? 0D : customers.getCurrentDebt()
                                    + pOSPaymentsDTO.getAmount());
                            if (customers.getCurrentDebt() <= 0) {
                                customers.setCurrentDate(pOSTicketsDTO.getDatetime());
                            } else {
                                customers.setCurrentDate(null);
                            }
                            customersApiDAO.update(customers);
                        }
                    }
                }
            }
            tickets.getReceipts().setPayments(payments);
            //EDU - 2020 08 On stock le mouvement induit sur le compte client
            tickets.setCustBalance(tickets.getFinalTaxedPrice()-paid);
            //TODO voir si on doit agir côté client pour refleter le niveau du compte client

            //if (pOSTicketsDTO.getId() == null) {
            try {
                // Boolean : new Customer ? on ajoute un passage client sauf si 
                // Ticket contrepassé ie manipulé, arrivé avec un numéro de ticket et modifié pour le rattacher au parent 
                boolean newCust = pOSTicketsDTO.getTicketId() == null || pOSTicketsDTO.getParentTicketId() == null || ! pOSTicketsDTO.getTicketId().equals(pOSTicketsDTO.getParentTicketId()) ;
                closedCash.getClosedCashManager().updateCs(tickets , newCust);
                tickets.setNumber(closedCash.getTicketCount());
                //EDU - V8S C'est là qu'on lie avec les tickets fiscaux
                //Tout ticket doit avoir son ticket fiscal 'gravé dans le marbre'
                tickets = fiscalTicketsService.write(tickets);
                
                //ticketsDAO.save(tickets);

                //EDU - On sauve chaque fois qu'un ticket est sauvé
                periodsService.addTickets(tickets, newCust);
                closedCash.getCashRegister().setNextTicketId(++nextTicketId);
                closedCashApiDAO.save(closedCash);
                //TODO vérifier que les totaux perpétuels ne gonflent pas si seul un ticket est ok
                
                for (POSTicketLinesDTO pOSTicketLinesDTO : pOSTicketsDTO.getLines()) {
                    Products products = pOSTicketLinesDTO.getProductId() == null ? null : productsApiDAO.findById(pOSTicketLinesDTO.getProductId()).orElse(null);
                    stocksService.generateMovement(tickets,products,pOSTicketLinesDTO);
                    // TODO : EDU : Attention on passe par là même si on doit avoir un soucis plus tard 
                    // conséquences -> altération des stocks
                    // workaround -> on a ajouté des contraintes sur l'enregistrement des tickets pour que
                    // si ça devait planter au niveau des tickets fiscaux alors ça plante aussi au niveau du ticket en lui même
                    // si ce n'est pas suffisant il faut envisager de ne déplacer cette génération des mouvments de stocks après
                    // tous les cas d'echec potentiels
                    
                }
                
            } catch (Exception exception) {
                exception.printStackTrace();
                throw new APIException("Unable to edit ticket");
            }
            //}
            /* 
            else {
                try {
                    tickets = fiscalTicketsService.write(tickets);
                    //ticketsDAO.save(tickets);

                } catch (Exception exception) {
                    throw new APIException("Unable to save ticket");
                }
            }
             */
            if (customers != null) {
                /*
                //d'abord enlever le ticket si i faisait déjà partie de la liste
                if(pOSTicketsDTO.getId() != null){
                    customers.removeTickets(pOSTicketsDTO.getId());
                }
                 */
//                customers.addTickets(tickets);
                customersApiDAO.update(customers);
            }


            //TODO - Voir ces points 
            //EDU en V8 l'écriture des écarts de soldes sur le compte client se fait après 
            //la sauvegarde du ticket

            //Idem la mise à jour de la caisse (next ticket id ) se fait après la sauvegarde en base
            //du ticket : si ticket rejeté pas de n+1
            cashRegistersApiDAO.save(closedCash.getCashRegister());

        }
        return true;
    }

    @Override
    public boolean share(POSSharedTicketsDTO pOSSharedTicketsDTO) {
        SharedTickets sharedTickets = new SharedTickets();
        boolean create = true;
        if (pOSSharedTicketsDTO.getId() != null) {
            if (sharedTicketsApiDAO.findById(pOSSharedTicketsDTO.getId()).isPresent()) {
                create = false;
            }
        }
        sharedTickets.setId(pOSSharedTicketsDTO.getId());
        sharedTickets.setName(pOSSharedTicketsDTO.getLabel());
        sharedTickets.setContent(pOSSharedTicketsDTO.getData());
        try {
            if (create) {
                sharedTicketsApiDAO.save(sharedTickets);
            } else {
                sharedTicketsApiDAO.save(sharedTickets);
            }
        } catch (Exception exception) {
            return false;
        }
        return true;
    }

    @Override
    public List<POSTicketsDTO> getOpen() {
        return AdaptableHelper.getAdapter(ticketsApiDAO.findOpen(), POSTicketsDTO.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends ICsvResponse> List<T> search(String ticketId, String ticketType, String cashId, String dateStart, String dateStop, String customerId,
            String userId, String locationId, String locationCategoryName , String saleLocationId, String tourId, String fromValue, String toValue, Boolean orderConstraint, String resultType) {
        //resultType = "com.ose.dto.tickets." + resultType;
        boolean validated = false;
        Class<? extends ILoadable> selector = POSTicketsDTO.class;

        if(validated) {
            DtoSQLParameters model = ticketsApiDAO.findByCriteriaDTO(ticketId, ticketType, cashId, dateStart, dateStop, customerId, userId, locationId, locationCategoryName ,saleLocationId, tourId, fromValue, toValue , orderConstraint, selector );
            return  model == null ? null : (List<T>) model.getResult(selector);
        }
        return (List<T>) AdaptableHelper.getAdapter(ticketsApiDAO.findByCriteria(ticketId, ticketType, cashId, dateStart, dateStop, customerId, userId, locationId , saleLocationId, tourId, fromValue, toValue, orderConstraint),
                selector);
    }

    @Override
    public TicketsInfoDTO getByTicketId(String ticketId) {
        Tickets tickets = ticketsApiDAO.findByTicketId(Long.valueOf(ticketId)).stream().findFirst().orElse(null);
        TicketsInfoDTO retour =  tickets != null ?  tickets.getAdapter(TicketsInfoDTO.class) : null;
        if( retour != null ) {
            ticketsService.fillTicketsInfoDTO(retour , tickets);
        }
        return retour ;
    }
    
    // Prix unitaire TTC
    private double computeFullPrice(TicketLines ticketLines) {
        double retour = 0D;
        if (ticketLines.getProduct() != null) {
            retour =  productsService.getFullPrice(ticketLines.getUnitPrice(), ticketLines.getTax(),ticketLines.getProduct());
        } else {
            retour =  ticketLines.getUnitPrice() * (1 + ticketLines.getTax().getRate());
        }
        return Math.rint(retour*100) / 100 ;
    }

    // Prix unitaire HT
    private double computeUntaxedPrice(TicketLines ticketLines) {
        if (ticketLines.getProduct() != null) {
            return productsService.getSellPrice(ticketLines.getTaxedUnitPrice(), ticketLines.getTax() , ticketLines.getProduct());
        } else {
            return ticketLines.getUnitPrice() * (1 + ticketLines.getTax().getRate());
        }
    }
    
    // Montant TTC de la ligne du ticket (PU TTC * units) avec la remise en paramètre ( ticket) en plus de la remise ligne
    //EDU on modifie pour pouvoir tenir compte de la remise totale accordée sur la ligne (ie remise en pied de ticket inclue)
    @Override
    public double computeFinalPrice(double additionalDiscount , TicketLines ticketLines) {
        return Math.rint(100*(computeFullPrice(ticketLines) * (1 - ticketLines.getGlobalDiscountRate(additionalDiscount))) * ticketLines.getUnits())/100;
    }
    
    // On récupère les montant total TVA incl. des taxes aditionnelles
    public double computeEcoTaxes(TicketLines ticketLines) {
        return productsService.getFullAdditionnalTaxes(ticketLines.getProduct());
    }
    
    // Montant TTC de la ligne du ticket (PU TTC * units) avec uniquement la remise de la ligne
    private double computeFinalPriceWithTaxes(TicketLines ticketLines) {
        return computeFinalPrice(0,ticketLines);
    }
    
    public void computeAllPrices(TicketLines ticketLines) {
        /**
         * Voici tout ce qui peut être calculé
         * Deux options en @PrePersist
         * ou alors via des appels
        //Taux de Taxes
        private Double taxRate;
        //Prix Unitaire HT
        private Double unitPrice;
        //Prix Unitaire TTC
        private Double taxedUnitPrice;
        //Montant HT de la ligne sans remises
        private Double price;
        //Montant TTC de la ligne sans remises
        private Double taxedPrice;

        //Montant HT de la ligne  
        private Double finalPrice;
        //Montant TTC de la ligne
        private Double finalTaxedPrice;


         */
        Taxes tax = ticketLines.getTax();
        Double unitPrice = ticketLines.getUnitPrice();
        Double taxedUnitPrice = ticketLines.getTaxedUnitPrice();
        ticketLines.setTaxRate(tax == null ? 0 : tax.getRate());
        double units = ticketLines.getUnits();
        double puHT = unitPrice == null ? 0 : unitPrice ;
        //EDU - voir si c'est suffisant à défaut de BigDecimals pour avoir une précision au centime
        double puTTC = taxedUnitPrice == null ? 0 : Math.rint(taxedUnitPrice * 100)/100 ;

        if(taxedUnitPrice != null && unitPrice == null) {
            puHT = computeUntaxedPrice(ticketLines);
        }
        if(taxedUnitPrice == null && unitPrice != null) {
            puTTC = computeFullPrice(ticketLines) ;
        }

        ticketLines.setPrice(puHT*units);
        ticketLines.setTaxedPrice(Math.rint(puTTC*units*100)/100);

        ticketLines.setFinalTaxedPrice(computeFinalPriceWithTaxes(ticketLines));
        ticketLines.setFinalPrice(computeFinalPriceWhitoutTaxes(ticketLines));

    }
    
    // Montant de la ligne du ticket (PU TTC * units)
    // TODO : AME => Attention pour les eco... la taxe est hors remise
    private double computeFinalPriceWhitoutTaxes(TicketLines ticketLines) {
        return (ticketLines.getUnitPrice() * (1 - ticketLines.getDiscountRate())) * ticketLines.getUnits();
    }
    
    public TicketsLineInfoDTO getTicketsLineInfoDTO(TicketLines ticketLines) {
        TicketsLineInfoDTO ticketLinesDTO = new TicketsLineInfoDTO();
        Locations location = ticketLines.getTicket().getLocation();

        if (ticketLines.getProduct() != null) {
            TicketsProductsInfoDTO productsInfoDTO = new TicketsProductsInfoDTO();
            productsInfoDTO.setId(ticketLines.getProduct().getId());
            productsInfoDTO.setLabel(ticketLines.getProduct().getName());
            productsInfoDTO.setTaxes(productsService.getEcotaxes(location , ticketLines.getProduct()));
            ticketLinesDTO.setProduct(productsInfoDTO);

            for (ProductsComponents productsComponents : ticketLines.getProduct().getComponentsProducts()) {
                Products component = productsComponents.getComponent();
                TicketsProductsInfoDTO subProductsInfoDTO = new TicketsProductsInfoDTO();
                subProductsInfoDTO.setId(component.getId());
                subProductsInfoDTO.setLabel(component.getName());
                subProductsInfoDTO.setTaxes(productsService.getEcotaxes(location , component));
                subProductsInfoDTO.setQuantity(productsComponents.getQuantity());
                ticketLinesDTO.getSubProducts().add(subProductsInfoDTO);
            }

        }
        ticketLinesDTO.setNote(ticketLines.getNote());
        ticketLinesDTO.setFullPrice(getTaxedUnitPrice(ticketLines));
        ticketLinesDTO.setUnits(ticketLines.getUnits());
        ticketLinesDTO.setDiscount(ticketLines.getDiscountRate());
        if (ticketLines.getTax() != null) {
            ticketLinesDTO.setVat(ticketLines.getTax().getName());
            ticketLinesDTO.setFinalPrice(ticketLines.getFinalTaxedPrice());
        }
        ticketLinesDTO.setCommande(ticketLines.isOrder());
        return ticketLinesDTO;
    }
    
    // Prix unitaire TTC
    @Override
    public double getTaxedUnitPrice(TicketLines ticketLines) {
        if( ticketLines.getTaxedUnitPrice() != null ) {
            return ticketLines.getTaxedUnitPrice();
        }
        if (ticketLines.getProduct() != null) {
            return productsService.getFullPrice(ticketLines.getUnitPrice(), ticketLines.getTax(),ticketLines.getProduct());
        } else {
            return ticketLines.getUnitPrice() * (1 + ticketLines.getTax().getRate());
        }
    }

    @Override
    public void fillTicketsInfoDTO(TicketsInfoDTO dto , Tickets tickets) {
        List<TicketsLineInfoDTO> nllLignes = new ArrayList<TicketsLineInfoDTO>();
        for(TicketLines ligne : tickets.getLines()) {
            nllLignes.add(getTicketsLineInfoDTO(ligne));
        }
        dto.setLines(nllLignes);
    }

    @Override
    public ComplexReturnType getTicketReturnInfos(String resultType) {
        ComplexReturnType returnInfos = returnTypesRules.get(resultType);

        if (returnInfos == null) {
            returnInfos = returnTypesRules.get("TicketsDTO");
        }
        return returnInfos;
    }

}
