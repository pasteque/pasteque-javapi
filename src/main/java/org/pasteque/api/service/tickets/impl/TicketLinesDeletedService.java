package org.pasteque.api.service.tickets.impl;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.cash.ITaxesApiDAO;
import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dao.products.IProductsApiDAO;
import org.pasteque.api.dao.products.ITariffAreasApiDAO;
import org.pasteque.api.dao.tickets.ITicketLinesDeletedApiDAO;
import org.pasteque.api.dto.tickets.POSTicketLinesDeletedDTO;
import org.pasteque.api.model.exception.APIException;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.Taxes;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.tickets.TicketLinesDeleted;
import org.pasteque.api.service.products.IProductsService;
import org.pasteque.api.service.tickets.ITicketLinesDeletedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TicketLinesDeletedService implements ITicketLinesDeletedService {
    
    
    @Autowired
    private ILocationsApiDAO locationsApiDAO;
    @Autowired
    private ITariffAreasApiDAO tariffAreasApiDAO;
    @Autowired
    private IProductsApiDAO productsApiDAO;
    @Autowired
    private ITaxesApiDAO taxesApiDAO;
    @Autowired
    private ITicketLinesDeletedApiDAO ticketLinesDeletedApiDAO;
    @Autowired
    private IProductsService productsService;
    
    @Override
    @Transactional
    public Object save(List<POSTicketLinesDeletedDTO> l, String ticketId, String isOpenedPayment) throws APIException {
            
        for (POSTicketLinesDeletedDTO pOSTicketLinesDeletedDTO : l) {
    
                //TicketLines parentLine = null;
                Products products = pOSTicketLinesDeletedDTO.getProductId() == null ? null : productsApiDAO.findById(pOSTicketLinesDeletedDTO.getProductId()).orElse(null);
                TicketLinesDeleted ticketLinesDeleted = new TicketLinesDeleted();
                ticketLinesDeleted.setLine(pOSTicketLinesDeletedDTO.getDispOrder());
                ticketLinesDeleted.setProduct(products);
                ticketLinesDeleted.setAttributes(pOSTicketLinesDeletedDTO.getAttributes());
                ticketLinesDeleted.setDiscountRate(pOSTicketLinesDeletedDTO.getDiscountRate());
                ticketLinesDeleted.setPrice(pOSTicketLinesDeletedDTO.getPrice());
                ticketLinesDeleted.setCreatedDate(pOSTicketLinesDeletedDTO.getCreatedDate());
                ticketLinesDeleted.setDeletedDate(new Date());
                ticketLinesDeleted.setOpenedPayment(Boolean.valueOf(isOpenedPayment));
                if (pOSTicketLinesDeletedDTO.getVatId() == null) {
                    throw new APIException("Unknown tax");
                }
                Taxes tax = taxesApiDAO.findById(pOSTicketLinesDeletedDTO.getVatId()).orElse(null);
                if (tax == null) {
                    throw new APIException("Unknown tax");
                }
                ticketLinesDeleted.setTax(tax);
                ticketLinesDeleted.setUnits(pOSTicketLinesDeletedDTO.getQuantity());
               
                ticketLinesDeleted.setType((!pOSTicketLinesDeletedDTO.isOrder() && pOSTicketLinesDeletedDTO.getType() == 0 ? 'S' : pOSTicketLinesDeletedDTO.getType()));
                if (pOSTicketLinesDeletedDTO.getTariffAreaId() != null) {
                    ticketLinesDeleted.setTariffArea(tariffAreasApiDAO.findById(pOSTicketLinesDeletedDTO.getTariffAreaId()).orElse(null));
                }
                ticketLinesDeleted.setNote(pOSTicketLinesDeletedDTO.getNote());
                pOSTicketLinesDeletedDTO.setOrder(ticketLinesDeleted.isOrder());
                
                if (pOSTicketLinesDeletedDTO.getTicketId() == null) {
                    try 
                    {
                        //EDU - 2020 08 Modification pour le transport entre les différents systèmes
                        //On suffixe le ticketId par le Default Location Id
                        Locations loc = locationsApiDAO.findTopByServerDefaultTrue();
                        ticketLinesDeleted.setTicketId(( loc == null ? "" : loc.getId()) + "-" + ticketId );
                        ticketLinesDeletedApiDAO.save(ticketLinesDeleted);
                        
                    } catch (Exception exception) {
                        throw new APIException("Unable to create line ticket deleted");
                    }
                } else {
                    try {
                        ticketLinesDeletedApiDAO.save(ticketLinesDeleted);
                    } catch (Exception exception) {
                        throw new APIException("Unable to update line ticket deleted");
                    }
                }
                
            }
            return true;

    }
    
    // Prix unitaire TTC
    public double getFullPrice(TicketLinesDeleted ticketLinesDeleted) {
        if (ticketLinesDeleted.getProduct() != null) {
            return productsService.getFullPrice(ticketLinesDeleted.getPrice(), ticketLinesDeleted.getTax() , ticketLinesDeleted.getProduct());
        } else {
            return ticketLinesDeleted.getPrice() * (1 + ticketLinesDeleted.getTax().getRate());
        }
    }
    
    // On récupère les montant total TVA incl. des taxes aditionnelles
    public double getEcoTaxes(TicketLinesDeleted ticketLinesDeleted) {
        return productsService.getFullAdditionnalTaxes(ticketLinesDeleted.getProduct());
    }
    
    // Montant de la ligne du ticket (PU TTC * units)
  //EDU on modifie pour pouvoir tenir compte de la remise totale accordée sur la ligne (ie remise en pied de ticket inclue)
    public double getFinalPrice(double additionalDiscount , TicketLinesDeleted ticketLinesDeleted) {
        return (getFullPrice(ticketLinesDeleted) * (1 - ticketLinesDeleted.getGlobalDiscountRate(additionalDiscount))) * ticketLinesDeleted.getUnits();
    }
    
    public double getFinalPrice(TicketLinesDeleted ticketLinesDeleted) {
        return getFinalPrice(0,ticketLinesDeleted);
    }
}