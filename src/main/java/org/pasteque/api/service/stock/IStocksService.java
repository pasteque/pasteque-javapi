package org.pasteque.api.service.stock;

import org.pasteque.api.dto.tickets.POSTicketLinesDTO;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.tickets.Tickets;

public interface IStocksService {

    public void generateMovement(Tickets tickets, Products products, POSTicketLinesDTO pOSTicketLinesDTO);

}
